+++
title = "About OpenFA"
description = ""

date = 2022-11-19
draft = false
+++

OpenFA is a black-box, open-source, re-implementation of the Janes Fighters Anthology engine,
derived solely from inspection of the game's assets and behavior.

It started over the holiday break in 2017, because Terrence wanted to build a flight sim and got
completely [nerd sniped](https://xkcd.com/356/) by the fact that SH files start with the
string "This program cannot be run in DOS mode". Yeah. We noticed. But _why_?

OpenFA is developed entirely in Terrence's spare time, to his own capricious whims and interests.
He might work on your project if it's easy, interests him, or moves OpenFA in the right direction,
but don't count on it.
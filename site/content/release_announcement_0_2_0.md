+++
title = "OpenFA Alpha Release 0.2"
description = "Second alpha release of OpenFA"
date = 2023-08-06
draft = false
slug = "openfa-release-2"

[taxonomies]
categories = ["releases"]
tags = ["release", "sh-editing", "tutorial", "demo", "video"]

[extra]
comments = false
+++

# Video Announcement
[![Watch on Youtube](http://i3.ytimg.com/vi/x3h5iFSY8-0/hqdefault.jpg)](https://www.youtube.com/watch?v=x3h5iFSY8-0)

# Transcript

Welcome Fighters Anthologists to the second alpha release of OpenFA!

OpenFA is a black-box re-implementation of the Janes
Fighters Anthology engine, derived solely from inspection of the game assets and
observation of the game's behavior.

The last 9 months of development has largely been focused on paying down high-
interest tech debt that I took out early in the project to get moving a bit faster.
I also needed to do some work on gaps that have developed between OpenFA and the WebGPU
ecosystem, since I was building against a pre-alpha spec in the early days.
We should now be on relatively solid footing for doing more game focused development
going forward, with a couple other (hopefully smaller) development efforts required
before we can go full-steam on re-implementing FA.

Also, not to bury the lede: I got SH file editing more or less working. There are still
some major gaps in functionality and lots of patience and working around FA crashes and
weirdness, but it's totally usable for creating shapes, potentially even from scratch.

Given how shapes are structured, I'm not sure if we will ever get a graphical editor
for shapes worked out, but the new tooling should make it much faster to discover
what lurks in the areas we're still unsure about, so who knows what could be possible
eventually.

Major thanks to @plurry for the steady stream of ideas, findings, and encouragement that
kept me going. This accomplishment has been 6+ years in the making. As little as
six months ago I would have sworn than even this much editing functionality was obviously
impossible. I'm really looking forward to seeing what the community can (and can't) do
with this tooling, now that it exists, and figuring out how to push it even further.

As always, the latest release is available to download at gitlab.com/openfa/openfa, or via
the OpenFA website: [https://openfa.org](https://openfa.org). To play with what you've seen
here, grab the latest [OFA Tools](https://openfa.s3.us-west-1.amazonaws.com/ofa-tools-alpha-0.2.0-win-x86_64.zip):
* [OFA Tools](https://openfa.s3.us-west-1.amazonaws.com/ofa-tools-alpha-0.2.0-win-x86_64.zip)
* [OpenFA](https://openfa.s3.us-west-1.amazonaws.com/openfa-alpha-0.2.0-win-x86_64.zip)

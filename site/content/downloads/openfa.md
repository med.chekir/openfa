+++
title = "Downloads"
description = ""

date = 2022-11-19
draft = false
+++

# Download
[Latest OpenFA Release](https://gitlab.com/openfa/openfa/-/releases/permalink/latest)

## Installing OpenFA

1. Install any of the following games:
    * USNF
    * US Marine Fighters (e.g. USNF with the Marine Fighters expansion)
    * ATF: Advanced Tactical Fighters
    * ATF: Nato (e.g. ATF with the Nato expansion installed)
    * ATF: Gold
    * USNF '97
    * Fighters Anthology
2. Download the [![Latest OpenFA Release](https://gitlab.com/openfa/openfa/-/badges/release.svg)](https://gitlab.com/openfa/openfa/-/releases/permalink/latest)
   for your platform and architecture
3. Extract the downloaded zip file into the install directory of the game
    * Example: C:\JANES\FA
    * `openfa.exe should be FA.EXE`
4. Double-click openfa.exe to run
    * `Or right-click and drag to create a shortcut wherever you want one`

# OpenFA

A black-box, open-source, re-implementation of the Janes Fighters Anthology's engine.

[![Latest Release](https://gitlab.com/openfa/openfa/-/badges/release.svg)](https://gitlab.com/openfa/openfa/-/releases)
[![Build Status](https://gitlab.com/openfa/openfa/badges/main/pipeline.svg)](https://gitlab.com/openfa/openfa/-/commits/main)
[![License](https://img.shields.io/static/v1.svg?label=license&message=GPLv3&color=informational)](https://github.com/terrence2/openfa/blob/master/LICENSE)

* Official Home: [https://openfa.org](https://openfa.org)
* Official Repo: [https://gitlab.com/openfa/openfa](https://gitlab.com/openfa/openfa)

[[_TOC_]]

## Installing OpenFA

1. Install any of the following games:
    * USNF
    * US Marine Fighters (e.g. USNF with the Marine Fighters expansion)
    * ATF: Advanced Tactical Fighters
    * ATF: Nato (e.g. ATF with the Nato expansion installed)
    * ATF: Gold
    * USNF '97
    * Fighters Anthology
2. Download the [![Latest OpenFA Release](https://gitlab.com/openfa/openfa/-/badges/release.svg)](https://gitlab.com/openfa/openfa/-/releases/permalink/latest)
   for your platform and architecture
3. Extract the downloaded zip file into the install directory of the game.
    * Example: C:\JANES\FA
    * `openfa.exe should be next to FA.EXE in your Fighters Anthology directory`
    * The `openfa.exe` engine is the only file included in the release so far.
4. Double-click openfa.exe to run
    * `Or right-click and drag to create a shortcut wherever you want one`
5. OpenFA also supports drag-n-drop to view various files:
    * Example: Drag a SH file onto openfa.exe to open a window showing that shape
    * Example: Drag an M file onto openfa.exe to start that mission

## Installing FA Modding Tools

1) Install any of the following games:
    * USNF
    * US Marine Fighters (e.g. USNF with the Marine Fighters expansion)
    * ATF: Advanced Tactical Fighters
    * ATF: Nato (e.g. ATF with the Nato expansion installed)
    * ATF: Gold
    * USNF '97
    * Fighters Anthology
2) Download the [![Latest OFA Tools Release](https://gitlab.com/openfa/openfa/-/badges/release.svg)](https://gitlab.com/openfa/openfa/-/releases/permalink/latest)
   for your platform and architecture
3) Extract the downloaded zip file into the install directory of the game.
    * Example: C:\JANES\FA
    * `ofa-tools.exe, etc should be next to FA.EXE`
    * The `ofa-tools.exe` binary is the only file included in the release now.
4) Drag and drop assets onto the appropriate tool to perform the action
    * Example: Drag FA_2.LIB onto ofa-tools.exe to extract the LIB to FA_2/
    * Example: Drag one or more PIC files onto ofa-tools.exe to create PNGs next to those PIC

## Using the Command Line Tool (Detailed)

The command line tool supports a wide variety of uses in addition to drag-and-drop that may be accessed from the command line.

1) Install ofa-tools.exe as described above
2) Open a command prompt. I've tested with cmd, powershell, and git-bash.
3) Change directory into the game directory
    * Example: `> cd C:\JANES\FA`
4) Run `ofa-tools --help`
    * Each asset is a sub-command; some assets have further sub-commands. Use --help to see them.
    * Example: listing the content in a lib: `ofa-tools lib ls FA_2.LIB`

## Why Fighters Anthology
1) The technology of the original Fighters Anthology engine represents the best of a bygone era of computing. Reverse
   engineering it lets us understand the severe limits of that era. By extension, it lets us tap into the amazing grit
   and daring of the creators that built it, despite everything stacked against them.
1) Since there was no illusion that a game of the era could faithfully simulate a real cockpit, there was a strong focus
   on the gameplay, in addition to building a facsimile of reality. As computers became powerful enough to be "realistic",
   it left behind the broad base of people who enjoy flight, but don't have the free time to spend 30+ hours memorizing where
   all the switches are in each new cockpit. In short, I wanted my mid-tier sims back and Fighters Anthology is the best
   of the bunch.

## Project Status

* Asset Discovery
  * Libraries
    * [x] LIB
      * Example output:
          ```
          K212244.PIC     PKWare     1.36 KiB    16.56 KiB  0.082x
          ROUND17.PIC     PKWare     1.41 KiB    16.56 KiB  0.085x
          $LAU10.PIC      PKWare      418 B       1.43 KiB  0.286x
          ROCKER01.PIC    PKWare      554 B       1.39 KiB  0.388x
          ...
          ```
  * Images
    * [x] PIC: Full support for texture, screen, and embedded jpg content.
    * [x] PAL
  * Objects
    * [x] OT, PT, JT, NT
      * Example output:
          ```
               ObjectType
               ==========
              struct_type: Plane
                type_size: 636
            instance_size: 609
                 ot_names: ObjectNames { short_name: "F-22", long_name: "F- 22A Raptor", file_name: "F22.PT" }
                    flags: 8416243
                obj_class: Fighter
                    shape: Some("F22.SH")
                      ...
          ```
    * [ ] GAS
    * [ ] ECM
    * [ ] SEE
  * PE
    * [x] PE Wrapper
  * Font
    * [x] FNT
  * Shape
    * [x] SH
      * Example output:
        ```
        0: @0000 Header: FF FF| 0000(0) 8C00(140) 0800(8) 9900(153) 4800(72) F200(242) 
        1: @000E 2EndO: F2 00| BD 4D  (delta:4DBD, target:4DCF)
        2: UnkCE @ 0012: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 C0 FF FF 00 01 00 00 00 E5 FF FF 00 3F 00 00 00 01 00 00 00 E5 FF FF 
        3: @003A SrcRf: f22.asm
        4: @0044 Unk7A: 7A 00| 00 00 10 00 07 00 00 00 
        5: @004E Unk78: 78 00| 00 00 4C 00 24 00 79 00 00 00 
        6: @005A ToLOD: C8 00| E8 00 10 00 21 4B  (unk0:00E8, unk1:0010 target:4B83)
        7: @0062 ToLOD: C8 00| E8 00 21 00 A8 3B  (unk0:00E8, unk1:0021 target:3C12)
        8: @006A ToDtl: A6 00| A2 3B 01 00  (level:0001, target:3C12)
        9: @0070 TexRf: _f22.PIC
        ...
        ```
  * Terrain
    * [x] T2: 3 bytes with height, metadata, and color; older versions have strange packing, probably for disk
              performance reasons. The terrains are all on the order of 256x256, so can be uploaded to a GPU and
              rendered as one strip.
    * [x] LAY: A PE header to handle relocation around a pile of structs. The content appears to be largely tables that
               plug into the core rendering system. The existing solution is just robust enough to find the palette entries
               to get the colors needed to render T2 and associated PIC files.
    * [x] M / MM: Mission and Mission Map formats. These tie together the T2, the LAY providing the palette and list all
                  of the associated textures and their placement. It also contains a list of every entity in the mission.
                  This capability would presumably let the designer mutate the map in significant ways along a campaign,
                  damaging regions of the map and whatnot; it appears to be completely unused, at least in the base
                  games. It appears to be completely unused. I believe that M files are used for campaigns and loose
                  missions and MM files are the "base" maps for the mission editor, but that is just supposition at this
                  point.
    * [ ] MC: A PE file. Appears to be scripted mission events.
  * Sound
    * [ ] 11K, 8K, 5K: Raw PCM with the given sample rate
    * [ ] XMI: eXtended MIdi, probably
  * AI
    * [ ] BI: compiled AI program
    * [ ] AI: a textual version of the BI
  * Gameplay
    * [ ] CAM: textual definition of a campaign
    * [ ] MT: Mission briefing blurbs
    * [ ] SEQ: Textual representation of a scene with timelines; e.g. death or campaign completion
    * [ ] TXT: Campaign intro text
    * [ ] HGR: PE defining the hanger / plane management screens in campaigns
    * [ ] HUD: PE defining the HUD of an aircraft
  * Menu
    * [ ] DLG: PE laying out the main game menu system
    * [ ] MNU: PE laying out the in-game menu bar at the top of the pause screen
    * [ ] PTS: PE file that has something to do with how planes are laid out in the plane selection screens.
  * Encyclopedia
    * [ ] INF: Textual encyclopedia entries
    * [ ] VDO: Video encyclopedia entries
  * Unknown Purpose
    * [ ] MUS: PE; maybe music sequencing?
    * [ ] BIN: binary
    * [ ] CB8: binary
    * [ ] FBC: binary
    
* Game Engine
  * Shape System
    * SH
      * [x] Chunked SH upload
      * [x] Chunked Texture atlases
      * [ ] Linear filtering support
      * [ ] Mipmapping support
      * [ ] Regalia support
      * [ ] Correct scaling
      * [x] Instance data uploads in blocks
      * [x] ECS Integration
      * [x] Parallel simulation of embedded i386 code
      * [x] Shape classes to limit uploads depending on entity type; e.g. no per-frame positions for buildings
      * [ ] Self shadowing
    * Modern format: we want to be able to do optimistic replacement with higher quality models at some point.
      * [ ] Do discovery around modern shape formats
  * Terrain System
    * T2 Rendering
      * [ ] Linear filtering
      * [x] Correct alignment with globe
      * [x] Eye relative uploads
      * [x] Atmospheric blending
      * [ ] Self shadowing
      * [ ] Shape shadowing
  * Text
    * [\] FNT loading; not yet supporting pre-blended text
    * [x] 2d screen-space rendering
  * Basic Flight Dynamics Model
    * [ ] Basic input and reactions
    * [ ] Apply envelope data to reactions
  * HUD
    * [ ] Render Tape in 2d in screenspace
    * [ ] Render Cockpit in 2d in screenspace
    * [ ] Render onto a 3d surface and sit in a virtual cockpit
  * Mirrors
    * [ ] Rear-view mirrors
  * MFD
    * [ ] Render several cameras on screen at once
  * Inventory Management
    * [ ] Add information to legion
    * [ ] integrate stores with MFD
  * AI
    * [ ] Action / goal system
    * [ ] Parallel simulation
  * Sound
    * [ ] XMI Decode
  * Menu Bar
    * [ ] Implement menu-mode in the game loop
    * [ ] Render top-level
    * [ ] Handle clicks on menu bar
    * [ ] Render sub-menus
    * [ ] Render nested menus
    * [ ] Handle clicks on menu items
  * Game menus
    * [ ] Layout components and text on screen
    * [ ] Handle button animations
    * [ ] Hook up scroll wheel
    * [ ] Perform nice wipes/fades between menus
  * Hangars
  * Death / Campaign Events Screens
    * [ ] Build a timeline
    * [ ] Show screens and text
    * [ ] Play sounds
    * [ ] Make gameplay state changes
  * Player profile management
    * [ ] Do discovery around the existing format
  * Campaign management
  * Encyclopedia
    * [ ] Inline SH renderer
    * [ ] Image carousel
    * [ ] Video viewer
  * Opening Videos
    * [ ] VDO decode

#### Specific Format Notes

* **PAL**: PALETTE.PAL is the only file of this type. It contains palette data consisting of 256 3-byte entries.
  Each byte contains a 6-bit (VGA) color, so must be re-sampled for use in modern systems. Large parts of this
  palette contain the "transparent" color #FF00FF. These sections are used by the terrain and (presumably) the HUD/menu
  to give custom look and feel to each area and plane.
* **SH**: Shape files contain a virtual machine using word codes inside the PE wrapper, with embedded fragments of x86.
  Execution jumps between virtual and machine instructions in order to achieve most dynamic plane effects.
* **T2**: Just heights and metadata about the terrain. The textures to be rendered onto that heightmap are stored
  in the MM/M files in tmap and tdict sections. Both the textures and the base colors in the T2 itself are outside
  the range of the base PALETTE.PAL and depend on a fragment of the LAY file being copied into the right part of
  the palette. Time-of-day effects as well as distance fogging are acheived by swapping out the palette with values
  from the LAY.
* **VDO**: These video files start with RATPAC, which is probably short for Rate-Packed. This is probably a standard
  format of some sort. Unfortunately, a basic google search for files with that header turned up absolutely nothing.
  If anyone has any information about this format, please drop me a line.


## Development Environment Setup

1) Pull from git. The [main branch](https://gitlab.com/openfa/openfa.git) if you do not plan to submit changes, 
   or your own fork if you do.
   1) `git clone --recursive https://gitlab.com/openfa/openfa.git`
2) Move into the newly downloaded directory.
   1) `cd openfa`
3) Install the Rust language via rustup.rs
4) Prepare your testing environment. OpenFA expects a `JANES` directory in the root project folder. 
   Generally you can `cp -r C:\JANES .` to copy it into the project directory. It is recommended that
   you copy all CD libs into their respective game folders to test all assets. OpenFA does not officially
   support any mods at this time, so any mod libs may or may not work with our tests.
5) Ensure that `crates/nitrogen` exists and if not follow the platform specific directions to create it.
6) `cargo run`; by default OpenFA will use the latest game in JANES if it does not find a game in
   the current directory. You can use the `--game-path`, `--cd-path`, and `--cd2-path` to select the
   relevant directories, if needed. CD paths are not needed if you copy the LIBs on the CD into the
   game directory.

### Developing OpenFA (Windows)

There is a variety of software that is needed to build OpenFA. On Windows,
the `scoop` installer is the best way to grab most of these dependencies, but you
will still have to install some of them manually. Specifically, you will need the
shell that ships with the MSI installer for Git and the latest stable version of
Rust via Rustup.

1. Rust: visit rustup.rs and follow the directions
2. install git and the Git Bash shell on Windows
3. install C build tools (VS Build Tools on Windows, LLVM on other platforms)
4. install "scoop" installer
5. scoop install cmake
6. scoop install ninja
7. scoop install SSL # do we still need this?

The Git Bash shell is recommended over CMD or the Windows terminal, even the shiny new one.
In particular, I noticed issues with case insensitivity, though the build still ran fine. YMMV.

Git symlinks do not currently create windows symlinks: the mklink command is only available
from CMD and is not yet exposed in msys. Once created in a windows CMD shell, however, git will
treat the Windows symlink like a normal symlink and not be problematic. You will need to open
up a CMD instance with "Run as Administrator" and then type:
```
> cd C:\Path\To\nitrogen\libs
> del nitrogen
> mklink /D nitrogen ..\nitrogen\libs
```

Once that is set up, you can test that your development environment is ready by running a quick check.
```
$ cargo check --all --all-targets
$ cargo test --all --all-targets
```

If that succeeds, you can build the release by running:
```
$ cargo build --all --all-targets --release
```

## CI/CD Infrastructure
### Linux Builders
The linux CI/CD expects to be run on a nixos system, or at least one with nix installed.
1) Set up gitlab-runner locally.
2) Create a new runner instance in the Group
3) Install gitlab-runner with the provided runner id and make sure it connects
4) Copy the JANES directory to /var/lib/private/gitlab-runner with appropriate permissions and ownership.
5) Create a token for the gitlab release-cli tool and install it in /var/lib/private/gitlab-runner/gitlab_token
6) Create an AWS token with privilege to write to the openfa bucket on S3 and install it in /etc
7) Download (release-cli)[https://docs.gitlab.com/ee/user/project/releases/release_cli.html], set it executable and
    copy it into /var/lib/private/gitlab-runner

### Windows Builders
1) Create a new local user with a strong password. The name doesn't matter, but the current one is called glrun
2) In an admin account, install gitlab-runner per the instructions, using the new user as the target for running stuff
3) Switch to the new glrun account and ensure that all the required developer dependencies are installed
    and available in the path
// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
mod lay;
mod lib_ext;
mod m;
mod mm;
mod pal;
mod pe;
mod pic;
mod sh;
mod t2;
mod xt;

use crate::{
    lay::{handle_lay_command, LayCommand},
    lib_ext::{handle_lib_command, pack_lib, unpack_lib, LibCommand},
    m::{handle_m_command, MCommand},
    mm::{handle_mm_command, MMCommand},
    pal::{dump_pal, handle_pal_command, PalCommand},
    pe::{handle_pe_command, PeCommand},
    pic::{handle_pic_command, pic_to_png, png_to_pic, PicCommand},
    sh::{handle_sh_command, sh_to_yaml, yaml_to_sh, ShCommand},
    t2::{handle_t2_command, T2Command},
    xt::{handle_xt_command, XtCommand},
};
use anyhow::Result;
use catalog::AssetCatalog;
use clap::{Parser, Subcommand};
use installations::{Installations, LibsArgs};
use std::{env, path::PathBuf};

/// Command Line tool for interacting with Fighters Anthology files.
///
/// Drag-and-Drop files to unpack to modern formats or pack back to FA formats, with common options.
/// Use the commands below to process files with non-standard options, show information, etc.
#[derive(Parser, Debug)]
#[command(author, version)]
struct Args {
    #[command(flatten)]
    libs_args: LibsArgs,

    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand, Debug)]
enum Commands {
    #[command()]
    Lay(LayCommand),

    #[command()]
    Lib(LibCommand),

    #[command()]
    M(MCommand),

    #[command()]
    Mm(MMCommand),

    #[command()]
    Pal(PalCommand),

    #[command()]
    Pe(PeCommand),

    #[command()]
    Pic(PicCommand),

    #[command()]
    Sh(ShCommand),

    #[command()]
    T2(T2Command),

    #[command()]
    Xt(XtCommand),
}

fn main() -> Result<()> {
    env_logger::init();

    // The primary interaction mode of ofa-tools is as a drag-n-drop target. If the args are
    // entirely a list of files, don't bother with the full cli processor and instead process
    // each with the default action for that file kind.
    if let Some(file_list) = is_file_list(env::args()) {
        for file in &file_list {
            let raw = file.to_string_lossy();
            let lower = raw.to_lowercase();
            if lower.ends_with(".pal") {
                dump_pal(file)?;
            } else if lower.ends_with(".sh.yaml") {
                yaml_to_sh(file)?;
            } else if lower.ends_with(".sh") {
                sh_to_yaml(file)?;
            } else if lower.ends_with(".pic") {
                let mut catalog = AssetCatalog::default();
                let installs = Installations::bootstrap_args(&mut catalog, &LibsArgs::default())?;
                pic_to_png(file, installs.primary_palette())?;
            } else if lower.ends_with(".png")
                || lower.ends_with(".jpg")
                || lower.ends_with(".jpeg")
                || lower.ends_with(".gif")
                || lower.ends_with(".webp")
                || lower.ends_with(".pnm")
                || lower.ends_with(".tiff")
                || lower.ends_with(".tga")
                || lower.ends_with(".dds")
                || lower.ends_with(".bmp")
                || lower.ends_with(".ico")
                || lower.ends_with(".hdr")
                || lower.ends_with(".exr")
                || lower.ends_with(".ff")
                || lower.ends_with(".avif")
                || lower.ends_with(".qoi")
            {
                let mut catalog = AssetCatalog::default();
                let installs = Installations::bootstrap_args(&mut catalog, &LibsArgs::default())?;
                png_to_pic(file, &installs, &catalog)?;
            } else if lower.ends_with(".lib") {
                unpack_lib(&[file.to_owned()], &None)?;
            } else if file.is_dir() {
                let output_path = file.with_extension("LIB");
                let inputs = file
                    .read_dir()?
                    .filter_map(|entry| entry.ok())
                    .map(|entry| entry.path())
                    .collect::<Vec<PathBuf>>();
                pack_lib(
                    &inputs.iter().map(|v| v.as_path()).collect::<Vec<_>>(),
                    &output_path,
                )?;
            } else {
                println!("don't know how to process {raw}");
            }
        }
    } else {
        let args = Args::parse();
        let mut catalog = AssetCatalog::default();
        let installs = Installations::bootstrap_args(&mut catalog, &args.libs_args)?;
        match &args.command {
            Commands::Lay(cmd) => handle_lay_command(cmd, &installs, &catalog)?,
            Commands::Lib(cmd) => handle_lib_command(cmd, &installs, &catalog)?,
            Commands::M(cmd) => handle_m_command(cmd, &installs, &catalog)?,
            Commands::Mm(cmd) => handle_mm_command(cmd, &installs, &catalog)?,
            Commands::Pal(cmd) => handle_pal_command(cmd, &installs, &catalog)?,
            Commands::Pe(cmd) => handle_pe_command(cmd, &installs, &catalog)?,
            Commands::Pic(cmd) => handle_pic_command(cmd, &installs, &catalog)?,
            Commands::Sh(cmd) => handle_sh_command(cmd, &installs, &catalog)?,
            Commands::T2(cmd) => handle_t2_command(cmd, &installs, &catalog)?,
            Commands::Xt(cmd) => handle_xt_command(cmd, &installs, &catalog)?,
        }
    }

    Ok(())
}

fn is_file_list(args: impl Iterator<Item = String>) -> Option<Vec<PathBuf>> {
    let out = args.skip(1).map(PathBuf::from).collect::<Vec<_>>();
    if out.iter().any(|pb| !pb.exists()) {
        return None;
    }
    Some(out)
}

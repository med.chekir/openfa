// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::Result;
use catalog::{AssetCatalog, FileSystem, Search};
use clap::Args;
use i386::{Disassembler, DisassemblyError};
use installations::Installations;
use peff::{
    COFFHeader, DataDirectory, ImportDirectoryEntry, OptionalHeader, PortableExecutable,
    SectionFlags, SectionHeader, WindowsHeader,
};
use std::mem;

/// Show PE information about any file packed in a PE structure.
#[derive(Args, Debug)]
pub struct PeCommand {
    #[arg(short, long)]
    disassemble: bool,

    /// PE files to dump
    inputs: Vec<String>,
}

pub fn handle_pe_command(
    cmd: &PeCommand,
    _installs: &Installations,
    catalog: &AssetCatalog,
) -> Result<()> {
    for input in &cmd.inputs {
        for info in catalog.search(Search::for_any_input(input))? {
            let long_name = info.to_string();
            println!("{long_name}");
            println!("{}", "=".repeat(long_name.len()));
            show_pe(&info.data()?, cmd.disassemble)?;
        }
    }
    Ok(())
}

fn show_pe(content: &[u8], disassemble: bool) -> Result<()> {
    let pe = PortableExecutable::from_bytes(content)?;
    println!("{:04X}-{:04X} DOS header & stub program (unused)", 0, 128);

    // Magic
    println!("{:04X}-{:04X} PL magic word", 128, 128 + 4);

    // Coff Header
    println!(
        "{:04X}-{:04X} COFF Header",
        128 + 4,
        128 + 4 + mem::size_of::<COFFHeader>()
    );
    println!(
        "    +{:04X}     number_of_sections:       {:08X} ({})",
        COFFHeader::offset_of_number_of_sections(),
        pe.coff_header().number_of_sections(),
        pe.coff_header().number_of_sections(),
    );
    println!(
        "    +{:04X}     time_date_stamp:          {:08X} ({})",
        COFFHeader::offset_of_time_date_stamp(),
        pe.coff_header().time_date_stamp(),
        pe.coff_header().time_date_stamp(),
    );
    println!("              ... other fields hard-coded ...");

    // Common Optional Header
    println!(
        "{:04X}-{:04X} OPT Header",
        128 + 4 + mem::size_of::<COFFHeader>(),
        128 + 4 + mem::size_of::<COFFHeader>() + mem::size_of::<OptionalHeader>()
    );
    println!(
        "    +{:04X}     size_of_code:             {:08X} ({})",
        OptionalHeader::offset_of_size_of_code(),
        pe.opt_header().size_of_code(),
        pe.opt_header().size_of_code(),
    );
    println!(
        "    +{:04X}     size_of_initialized_data: {:08X} ({})",
        OptionalHeader::offset_of_size_of_initialized_data(),
        pe.opt_header().size_of_initialized_data(),
        pe.opt_header().size_of_initialized_data(),
    );
    println!(
        "    +{:04X}     base_of_code:             {:08X} ({})",
        OptionalHeader::offset_of_base_of_code(),
        pe.opt_header().base_of_code(),
        pe.opt_header().base_of_code(),
    );
    println!(
        "    +{:04X}     base_of_data:             {:08X} ({})",
        OptionalHeader::offset_of_base_of_data(),
        pe.opt_header().base_of_data(),
        pe.opt_header().base_of_data(),
    );
    println!("              ... other fields hard-coded ...");

    // Windows Optional Header
    println!(
        "{:04X}-{:04X} WIN Header",
        128 + 4 + mem::size_of::<COFFHeader>() + mem::size_of::<OptionalHeader>(),
        128 + 4
            + mem::size_of::<COFFHeader>()
            + mem::size_of::<OptionalHeader>()
            + mem::size_of::<WindowsHeader>()
    );
    println!(
        "    +{:04X}     size_of_image:            {:08X} ({})",
        WindowsHeader::offset_of_size_of_image(),
        pe.win_header().size_of_image(),
        pe.win_header().size_of_image(),
    );
    println!(
        "    +{:04X}     size_of_headers:          {:08X} ({})",
        WindowsHeader::offset_of_size_of_headers(),
        pe.win_header().size_of_headers(),
        pe.win_header().size_of_headers(),
    );
    println!("              ... other fields hard-coded ...");

    // Data Directory (cross reference with section headers)
    println!(
        "{:04X}-{:04X} DataDirectory (must match correct section header values)",
        128 + 4
            + mem::size_of::<COFFHeader>()
            + mem::size_of::<OptionalHeader>()
            + mem::size_of::<WindowsHeader>(),
        128 + 4
            + mem::size_of::<COFFHeader>()
            + mem::size_of::<OptionalHeader>()
            + mem::size_of::<WindowsHeader>()
            + 16 * mem::size_of::<DataDirectory>()
    );
    println!(
        "    +{:04X}     dirs[1].virtual_address:  {:08X} ({})",
        8, // offset * 2 * size_of(u32)
        pe.data_dir(1).virtual_address(),
        pe.data_dir(1).virtual_address()
    );
    println!(
        "    +{:04X}     dirs[1].size:             {:08X} ({})",
        12, // offset * 2 * size_of(u32) + size_of(u32),
        pe.data_dir(1).size(),
        pe.data_dir(1).size()
    );
    println!(
        "    +{:04X}     dirs[5].virtual_address:  {:08X} ({})",
        5 * 2 * 4,
        pe.data_dir(5).virtual_address(),
        pe.data_dir(5).virtual_address()
    );
    println!(
        "    +{:04X}     dirs[5].size:             {:08X} ({})",
        5 * 2 * 4 + 4,
        pe.data_dir(5).size(),
        pe.data_dir(5).size()
    );
    println!("              ... other fields are zero ...");

    // Section Headers
    let mut section_header_offset = 128
        + 4
        + mem::size_of::<COFFHeader>()
        + mem::size_of::<OptionalHeader>()
        + mem::size_of::<WindowsHeader>()
        + 16 * mem::size_of::<DataDirectory>();
    for section in pe.section_headers() {
        println!(
            "{:04X}-{:04X} SectionHeader ({})",
            section_header_offset,
            section_header_offset + mem::size_of::<SectionHeader>(),
            section.name_str()?,
        );
        println!(
            "    +{:04X}     name:                     {:?} ({})",
            SectionHeader::offset_of_name(),
            section.name(),
            section.name_str()?,
        );
        println!(
            "    +{:04X}     virtual_size:             {:08X} ({})",
            SectionHeader::offset_of_virtual_size(),
            section.virtual_size(),
            section.virtual_size(),
        );
        println!(
            "    +{:04X}     virtual_address:          {:08X} ({})",
            SectionHeader::offset_of_virtual_address(),
            section.virtual_address(),
            section.virtual_address(),
        );
        println!(
            "    +{:04X}     size_of_raw_data:         {:08X} ({})",
            SectionHeader::offset_of_size_of_raw_data(),
            section.size_of_raw_data(),
            section.size_of_raw_data(),
        );
        println!(
            "    +{:04X}     pointer_to_raw_data:      {:08X} ({})",
            SectionHeader::offset_of_pointer_to_raw_data(),
            section.pointer_to_raw_data(),
            section.pointer_to_raw_data(),
        );
        println!(
            "    +{:04X}     characteristics:          {:08X} ({:?})",
            SectionHeader::offset_of_characteristics(),
            section.characteristics(),
            SectionFlags::from_bits(section.characteristics()).unwrap(),
        );
        println!("              ... other fields hard-coded ...");
        section_header_offset += mem::size_of::<SectionHeader>();
    }
    println!(
        "{:04X}-{:04X}  zero-padding to start of CODE",
        section_header_offset,
        pe.win_header().size_of_headers()
    );

    for section in pe.section_headers() {
        println!(
            "{:04X}-{:04X} {} data",
            section.pointer_to_raw_data(),
            section.pointer_to_raw_data() + section.virtual_size(),
            section.name_str()?
        );

        if section.name_str()? == "CODE" || section.name_str()? == ".text" {
            println!("          TODO: print parsed trampolines? <-");
            if disassemble {
                let mut disasm = Disassembler::default();
                let out = disasm.disassemble_at(0, &pe);
                if let Err(ref e) = out {
                    if !DisassemblyError::maybe_show(e, pe.code()) {
                        println!("ERROR: {e}");
                    }
                }
                let blocks = disasm.build_memory_view(&pe);
                if !blocks.is_empty() {
                    println!("i386 -");
                    for bc in &blocks {
                        println!("{bc}");
                    }
                }
            }
        } else if section.name_str()? == ".idata" {
            println!(
                "    +{:04X}     import_lut_rva:           {:08X} ({})",
                ImportDirectoryEntry::offset_of_import_lut_rva(),
                pe.import_dir().import_lut_rva(),
                pe.import_dir().import_lut_rva(),
            );
            println!(
                "    +{:04X}     name_rva:                 {:08X} ({})",
                ImportDirectoryEntry::offset_of_name_rva(),
                pe.import_dir().name_rva(),
                pe.import_dir().name_rva(),
            );
            println!(
                "    +{:04X}     import_address_table_rva: {:08X} ({})",
                ImportDirectoryEntry::offset_of_import_address_table_rva(),
                pe.import_dir().import_address_table_rva(),
                pe.import_dir().import_address_table_rva(),
            );
            for (ordinal, import) in pe.imports().iter().enumerate() {
                let offset = 2 * mem::size_of::<ImportDirectoryEntry>() + 6 * ordinal;
                println!(
                    "    +{:04X}     @{} lut:+{:03X} iat:+{:03X} name:+{:03X}     {}",
                    offset,
                    ordinal,
                    import.lut_rva() - section.virtual_address(),
                    import.iat_rva() - section.virtual_address(),
                    import.name_rva() - section.virtual_address(),
                    import.name(),
                );
            }
        } else if section.name_str()? == ".reloc" {
            let mut offset = 0;
            for (i, base_reloc) in pe.reloc_base_table().iter().enumerate() {
                println!(
                    "    +{:04X} Base Block rva:{:04X}",
                    offset,
                    base_reloc.page_rva()
                );
                for reloc in pe.relocs() {
                    if reloc.reloc_base_block_offset() == i {
                        println!(
                            "    +{:04X}     reloc flags:{}, raw:{:03X} rva:{:04X} code:{:04X}",
                            offset + 8 + reloc.reloc_offset() * 2,
                            reloc.raw() >> 12,
                            reloc.raw() & 0x0FFF,
                            reloc.rva(),
                            reloc.code_offset()
                        );
                    }
                }
                offset += base_reloc.block_size() as usize;
            }

            // Note: size_of_raw_data for reloc includes the padding for some reason.
            println!(
                "{:04X}-{:04X} zero-padding to start of next section",
                section.pointer_to_raw_data() + pe.data_dir(5).size(),
                section.pointer_to_raw_data() + section.virtual_size(),
            );
        }

        if section.virtual_size() != section.size_of_raw_data() {
            println!(
                "{:04X}-{:04X} zero-padding to start of next section",
                section.pointer_to_raw_data() + section.virtual_size(),
                section.pointer_to_raw_data() + section.size_of_raw_data(),
            );
        }
    }

    Ok(())
}

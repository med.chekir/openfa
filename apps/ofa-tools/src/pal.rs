// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::Result;
use catalog::{AssetCatalog, FileInfo, FileSystem, Search};
use clap::Args;
use image::{ImageBuffer, Rgb};
use installations::{GameInfo, Installations};
use pal::Palette;
use std::{fs, path::Path};

/// Query PAL values and show as png
#[derive(Debug, Args)]
pub struct PalCommand {
    /// Dump the palette to a png
    #[arg(short, long)]
    dump: bool,

    /// Show the color at this offset
    #[arg(short, long)]
    position: Option<String>,

    /// PAL file to analyze
    inputs: Vec<String>,
}

pub fn handle_pal_command(
    cmd: &PalCommand,
    installs: &Installations,
    catalog: &AssetCatalog,
) -> Result<()> {
    for input in &cmd.inputs {
        for info in catalog.search(Search::for_input("PAL", input))? {
            let long_name = info.to_string();
            println!("{long_name}");
            println!("{}", "=".repeat(long_name.len()));
            show_pal(info, installs.game_info(info.collection_name())?, cmd)?;
        }
    }
    Ok(())
}

fn pal_to_image(pal: &Palette) -> Result<ImageBuffer<Rgb<u8>, Vec<u8>>> {
    let size = 80;
    let mut buf = ImageBuffer::new(16u32 * size, 16u32 * size);
    for i in 0..16 {
        for j in 0..16 {
            let off = (j << 4 | i) as usize;
            for ip in 0..size {
                for jp in 0..size {
                    buf.put_pixel(i * size + ip, j * size + jp, pal.rgb(off)?);
                }
            }
        }
    }
    Ok(buf)
}

pub fn dump_pal(input: &Path) -> Result<()> {
    let mut output = input.to_owned();
    output.set_extension("PAL.png");

    let pal = Palette::from_bytes(&fs::read(input)?)?;
    let buf = pal_to_image(&pal)?;
    buf.save(&output)?;
    println!("Created {output:?}");

    Ok(())
}

fn show_pal(info: FileInfo, game: &GameInfo, cmd: &PalCommand) -> Result<()> {
    let pal = Palette::from_bytes(&info.data()?)?;
    if cmd.dump {
        println!("Dumping palette: {}:{}", game.test_dir, info.name());

        let buf = pal_to_image(&pal)?;
        fs::create_dir_all(format!("dump/palette/{}-{}", game.test_dir, info.name()))?;
        buf.save(&format!(
            "dump/palette/{}-{}/palette.png",
            game.test_dir,
            info.name()
        ))?;

        return Ok(());
    }

    if let Some(pos_str) = cmd.position.clone() {
        let pos = if let Some(hex) = pos_str.strip_prefix("0x") {
            usize::from_str_radix(hex, 16)?
        } else {
            pos_str.parse::<usize>()?
        };
        println!("{:02X} => {:?}", pos, pal.rgb(pos)?);
    }
    println!();

    Ok(())
}

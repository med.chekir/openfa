// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::Result;
use catalog::{AssetCatalog, FileSystem, Search};
use clap::Args;
use installations::Installations;
use std::time::Instant;
use t2::Terrain;

/// Show contents of T2 files
#[derive(Debug, Args)]
pub struct T2Command {
    /// Back of the envelop profiling.
    #[arg(long)]
    profile: bool,

    /// One or more MM files to process
    inputs: Vec<String>,
}

const PROFILE_COUNT: usize = 10000;

pub fn handle_t2_command(
    cmd: &T2Command,
    _installs: &Installations,
    catalog: &AssetCatalog,
) -> Result<()> {
    for input in &cmd.inputs {
        for info in catalog.search(Search::for_input("T2", input))? {
            let long_name = info.to_string();
            println!("{long_name}");
            println!("{}", "=".repeat(long_name.len()));
            show_t2(&info.data()?, cmd.profile)?;
        }
    }
    Ok(())
}

fn show_t2(raw: &[u8], profile: bool) -> Result<()> {
    if profile {
        let start = Instant::now();
        for _ in 0..PROFILE_COUNT {
            let _ = Terrain::from_bytes(raw)?;
        }
        println!(
            "load time: {}ms",
            (start.elapsed().as_micros() / PROFILE_COUNT as u128) as f64 / 1000.0
        );
        return Ok(());
    }
    let t2 = Terrain::from_bytes(raw)?;
    println!("map name:    {}", t2.name());
    println!("width:       {}", t2.width());
    println!("height:      {}", t2.height());
    println!("extent e-w:  {}", t2.extent_east_west_in_ft());
    println!("extent n-s:  {}", t2.extent_north_south_in_ft());
    println!();

    Ok(())
}

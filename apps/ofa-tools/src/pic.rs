// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::{ensure, Result};
use catalog::{AssetCatalog, FileSystem, Search};
use clap::{Args, Subcommand};
use image::GenericImageView;
use installations::Installations;
use pal::Palette;
use pic::{Pic, PicPalette};
use std::{
    fs::{self, File},
    io::Write,
    path::{Path, PathBuf},
};

/// PICs to PNG files and show PIC metadata
#[derive(Debug, Args)]
pub struct PicCommand {
    #[command(subcommand)]
    command: PicCommands,
}

/// Extract PICs to modern png files.
#[derive(Debug, Args)]
struct PicUnpackCommand {
    /// Dump the palette here as a PNG
    #[arg(short, long)]
    show_palette: Option<String>,

    /// Dump the palette here as a PAL
    #[arg(short, long)]
    dump_palette: Option<String>,

    /// Print the image as ascii
    #[arg(short = 'a', long = "ascii")]
    show_ascii: bool,

    /// Output as grayscale rather than palettized
    #[arg(short = 'b', long = "gray-scale")]
    grayscale: bool,

    /// Use the given palette when decoding
    #[arg(short = 'u', long = "use-palette")]
    use_palette: Option<String>,

    /// Write the image to the given file
    #[arg(short = 'o', long = "output")]
    write_image: Option<String>,

    /// One or more PIC files to process
    inputs: Vec<String>,
}

/// Encode texture format PICs (non-font) from any input image.
///
/// Examples:
///   Encode file.png into FILE.PIC, using the default palette (PALETTE.PAL) from
///   the game files in the current directory.
///
///   > ofa-tools pic pack -o FILE.PIC file.png
///
///   Encode file.png into FILE.PIC, using the default palette from the game files
///   in the directory specified after -g (--game-path).
///
///   > ofa-tools pic pack -g "C:\JANES\FA" -o FILE.PIC file.png
///
///   Use the palette from an existing game asset (only PICs are supported at the
///   moment) with -r (--palette-resource) and write the palette that was used into
///   the new image by passing the -p (--include-palette) flag.
///
///   > ofa-tools pic pack -r CHOOSEAC.PIC -p -o FILE.PIC file.png
///
///   Use the -w (--include-row-headers) option flag to create a PIC file suitable
///   for use as a texture. Texture PICs in FA contain a blob of pre-multiplied row
///   offsets, presumably to make texturing operations faster to compute on older
///   computers.
///
///   > ofa-tools pic pack -o TEXTURE.PIC texture.png
///
///   Use a palette from a file in the file system with -f (--palette-file). You
///   can use `picdump` to extract palette data from existing PIC files and store
///   them into new palette files.
///
///   > ofa-tools pic pack -f paldump.PAL -o FILE.PIC file.png
///
/// This tool supports many source image formats. See the complete list at:
///   https://docs.rs/image/0.21.0/image/
#[derive(Debug, Args)]
struct PicPackCommand {
    /// Save the new PIC file here
    #[arg(short = 'o', long = "output", default_value = "OUT.PIC")]
    output: PathBuf,

    /// Dithering "quality" adjustment. Increasing this will give better color
    /// accuracy at the cost of increased grainyness.
    #[arg(short = 'q', long = "quality", default_value = "5")]
    dither_quality: u8,

    /// Use a palette from a PAL file
    #[arg(short = 'f', long = "palette-file")]
    palette_file: Option<PathBuf>,

    /// Use a palette from the given resource
    #[arg(short = 'r', long = "palette-resource")]
    palette_resource: Option<String>,

    /// A source image to encode into a PIC
    source_image: PathBuf,
}

#[derive(Debug, Subcommand)]
enum PicCommands {
    #[command()]
    Unpack(PicUnpackCommand),

    #[command()]
    Pack(PicPackCommand),
}

pub fn handle_pic_command(
    cmd: &PicCommand,
    installs: &Installations,
    catalog: &AssetCatalog,
) -> Result<()> {
    match &cmd.command {
        PicCommands::Unpack(unpack) => handle_pic_unpack_command(unpack, installs, catalog),
        PicCommands::Pack(pack) => handle_pic_pack_command(pack, installs, catalog),
    }
}

fn handle_pic_unpack_command(
    unpack: &PicUnpackCommand,
    installs: &Installations,
    catalog: &AssetCatalog,
) -> Result<()> {
    for input in &unpack.inputs {
        for info in catalog.search(Search::for_input("PIC", input))? {
            let long_name = info.to_string();
            println!("{long_name}");
            println!("{}", "=".repeat(long_name.len()));
            show_pic(
                info.name(),
                &info.data()?,
                installs.palette(info.collection_name())?,
                unpack,
            )?;
        }
    }
    Ok(())
}

fn handle_pic_pack_command(
    opt: &PicPackCommand,
    installs: &Installations,
    catalog: &AssetCatalog,
) -> Result<()> {
    ensure!(
        opt.dither_quality >= 1,
        "dither quality must be between 1 and 255"
    );

    let dynamic_image = image::open(&opt.source_image)?;
    let pic_data = Pic::make_texture(
        dynamic_image.to_rgba8(),
        load_encode_palette(opt, installs, catalog)?,
        opt.dither_quality,
    );

    let mut fp = File::create(&opt.output)?;
    fp.write_all(&pic_data)?;

    Ok(())
}

fn load_encode_palette<'a>(
    opt: &PicPackCommand,
    installs: &'a Installations,
    catalog: &'a AssetCatalog,
) -> Result<PicPalette<'a>> {
    if let Some(ref filename) = opt.palette_file {
        let pal_data = fs::read(filename)?;
        return Ok(PicPalette::Inline(Palette::from_bytes(&pal_data)?));
    }

    if let Some(ref s) = opt.palette_resource {
        let info = catalog.lookup(s)?;
        let data = info.data()?;
        let pic = Pic::from_bytes(&data)?;
        if let Some(frag) = pic.palette_fragment() {
            return Ok(PicPalette::Inline(frag.to_palette()?));
        }
    }

    Ok(PicPalette::System(installs.primary_palette()))
}

pub fn pic_to_png(path: &Path, palette: &Palette) -> Result<()> {
    let data = fs::read(path)?;
    let pic = Pic::from_bytes(&data)?;
    let image = pic.image(palette)?;
    image.save(path.with_extension("png"))?;
    Ok(())
}

// Note: there are many different flavors of PIC. The usage of the file determines
// what elements of the header must be present. The most common usage currently is
// for creating new textures, so we always try to create that kind of image at present.
pub fn png_to_pic(path: &Path, installs: &Installations, catalog: &AssetCatalog) -> Result<()> {
    handle_pic_pack_command(
        &PicPackCommand {
            output: path.with_extension("PIC"),
            source_image: path.to_owned(),
            dither_quality: 5,
            palette_resource: None,
            palette_file: None,
        },
        installs,
        catalog,
    )
}

fn show_pic(
    name: &str,
    content: &[u8],
    system_palette: &Palette,
    cmd: &PicUnpackCommand,
) -> Result<()> {
    let pic = Pic::from_bytes(content)?;

    if pic.is_jpeg() {
        println!("JPEG");
        return Ok(());
    }

    println!("width:  {}px", pic.width());
    println!("height: {}px", pic.height());
    if let Some(frag) = pic.palette_fragment() {
        println!("colors: {:?}", frag.color_count());

        if let Some(ref path) = cmd.show_palette {
            frag.to_palette()?.dump_png(path)?;
        }

        if let Some(ref path) = cmd.dump_palette {
            frag.to_palette()?.dump_pal(path)?;
        }
    }

    if let Some(target) = &cmd.write_image {
        let palette = if let Some(path) = &cmd.use_palette {
            Palette::from_bytes(&fs::read(path)?)?
        } else if cmd.grayscale {
            Palette::grayscale()?
        } else {
            system_palette.to_owned()
        };
        let pic = Pic::from_bytes(content)?;
        let image = pic.image(&palette)?;
        let mut path = PathBuf::from(target);
        if path.is_dir() {
            path.push(name.replace("PIC", "PNG"));
            image.save(path)?;
        } else {
            image.save(path)?;
        }
    }

    if cmd.show_ascii {
        let palette = Palette::grayscale()?;
        let image = Pic::from_bytes(content)?.image(&palette)?;
        let (width, height) = image.dimensions();
        for h in 0..height {
            for w in 0..width {
                print!("{:02X} ", image.get_pixel(w, h)[0]);
            }
            println!();
        }
    }

    Ok(())
}

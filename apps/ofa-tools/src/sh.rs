// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::Result;
use catalog::{AssetCatalog, FileSystem, Search};
use clap::Args;
use installations::Installations;
use sh::{RawShape, ShCode};
use std::{
    fs::{self, File},
    io::Write,
    path::Path,
};

/// SH format slicing and discovery tooling
#[derive(Debug, Args)]
pub struct ShCommand {
    /// Show all instructions
    #[arg(short = 'a', long = "all")]
    show_all: bool,

    /// Show the min and max coordinates
    #[arg(short = 'e', long = "extents")]
    show_extents: bool,

    /// Write instructions to a CSV file
    #[arg(long, name = "out.csv")]
    csv: Option<String>,

    /// Write instructions to a YAML file
    #[arg(long, name = "out.yaml")]
    yaml: Option<String>,

    #[arg(long)]
    custom: bool,

    /// Shape files to display
    #[arg()]
    inputs: Vec<String>,
}

pub fn handle_sh_command(
    cmd: &ShCommand,
    _installs: &Installations,
    catalog: &AssetCatalog,
) -> Result<()> {
    for input in &cmd.inputs {
        for info in catalog.search(Search::for_input("SH", input))? {
            let long_name = info.to_string();
            println!("{long_name}");
            println!("{}", "=".repeat(long_name.len()));
            show_sh(info.name(), &info.data()?, cmd)?;
        }
    }
    Ok(())
}

pub fn sh_to_yaml(path: &Path) -> Result<()> {
    let outpath = path.with_extension("SH.yaml");
    let data = fs::read(path)?;
    let shape = ShCode::from_bytes(&data)?;
    let yaml = shape.serialize_yaml()?;
    fs::write(outpath, yaml)?;
    Ok(())
}

pub fn yaml_to_sh(path: &Path) -> Result<()> {
    let yaml = fs::read_to_string(path)?;
    let code = ShCode::deserialize_yaml(&yaml)?;
    let pe_data = code.compile_pe()?;
    let outpath = path.with_extension("");
    fs::write(outpath, pe_data)?;
    Ok(())
}

fn show_sh(name: &str, data: &[u8], cmd: &ShCommand) -> Result<()> {
    if let Some(filename) = cmd.yaml.as_ref() {
        let shape = ShCode::from_bytes(data)?;
        let content = shape.serialize_yaml()?;
        if filename == "-" {
            println!("{content}");
        } else {
            let mut fp = File::create(filename)?;
            fp.write_all(content.as_bytes())?;
        }
    } else if let Some(filename) = cmd.csv.as_ref() {
        let shape = RawShape::from_bytes(name, data)?;
        sh::export_csv(&shape, filename)?;
    } else if cmd.show_all {
        let shape = RawShape::from_bytes(name, data)?;
        for (i, instr) in shape.instrs.iter().enumerate() {
            println!("{:3}: {}", i, instr.show());
        }
    } else if cmd.show_extents {
        let shape = RawShape::from_bytes(name, data)?;
        let mut min = [i16::MAX; 3];
        let mut max = [i16::MIN; 3];
        for instr in &shape.instrs {
            if let sh::Instr::VertexBuf(buf) = instr {
                for v in &buf.verts {
                    for i in 0..3 {
                        if v[i] > max[i] {
                            max[i] = v[i];
                        }
                        if v[i] < min[i] {
                            min[i] = v[i];
                        }
                    }
                }
            }
        }
        println!("MIN: {min:?}");
        println!("MAX: {max:?}");
        let mut span = [0i16; 3];
        for i in 0..3 {
            span[i] = max[i] - min[i];
        }
        println!("SPAN: {span:?}");
    } else if cmd.custom {
        println!("unused");
    } else {
        println!("Showing all instructions...");
        let shape = ShCode::from_bytes(data)?;
        println!("{}", shape.display_content()?);
    }
    Ok(())
}

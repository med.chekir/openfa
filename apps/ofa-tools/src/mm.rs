// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::Result;
use catalog::{AssetCatalog, FileInfo, FileSystem, Search};
use clap::Args;
use installations::{from_dos_string, Installations};
use mmm::MissionMap;
use std::time::Instant;
use xt::TypeManager;

/// Show contents of MM files
#[derive(Debug, Args)]
pub struct MMCommand {
    /// Back of the envelop profiling.
    #[arg(long)]
    profile: bool,

    /// One or more MM files to process
    inputs: Vec<String>,
}

const PROFILE_COUNT: usize = 10000;

pub fn handle_mm_command(
    cmd: &MMCommand,
    _installs: &Installations,
    catalog: &AssetCatalog,
) -> Result<()> {
    let type_manager = TypeManager::empty();
    for input in &cmd.inputs {
        for info in catalog.search(Search::for_input("MM", input))? {
            let long_name = info.to_string();
            println!("{long_name}");
            println!("{}", "=".repeat(long_name.len()));
            show_mm(info, &type_manager, catalog, cmd)?;
        }
    }
    Ok(())
}

fn show_mm(
    info: FileInfo,
    type_manager: &TypeManager,
    catalog: &AssetCatalog,
    cmd: &MMCommand,
) -> Result<()> {
    let content = from_dos_string(info.data()?);

    if cmd.profile {
        let start = Instant::now();
        for _ in 0..PROFILE_COUNT {
            let _ = MissionMap::from_str(&content, type_manager, catalog)?;
        }
        println!(
            "load time: {}ms",
            (start.elapsed().as_micros() / PROFILE_COUNT as u128) as f64 / 1000.0
        );
        return Ok(());
    }
    match MissionMap::from_str(&content, type_manager, catalog) {
        Ok(mm) => {
            println!("map name:    {}", mm.map_name().meta_name());
            println!("t2 name:     {}", mm.map_name().t2_name());
            println!("layer name:  {}", mm.layer_name());
            println!("layer index: {}", mm.layer_index());
            println!("tmap count:  {}", mm.texture_maps().len());
            println!("tdic count:  {}", mm.texture_dictionary().len());
            println!("obj count:   {}", mm.objects().count());
            println!();
        }
        Err(e) => println!("Load failed: {e}\n"),
    }

    Ok(())
}

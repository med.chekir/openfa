{ pkgs ? import <nixpkgs>
  {
    overlays = [];
  }
}:
let
in
  pkgs.mkShell {
    nativeBuildInputs = [
      pkgs.awscli2
      pkgs.gnumake
      pkgs.pkg-config
      pkgs.fontconfig
      pkgs.libxkbcommon
      pkgs.gmock
      pkgs.glxinfo
      pkgs.libxkbcommon
      pkgs.lzma
      pkgs.sccache
      pkgs.vulkan-tools
      pkgs.xorg.libX11
      pkgs.xorg.libXrandr
      pkgs.udev
    ];
    LD_LIBRARY_PATH = with pkgs.xorg; "${pkgs.vulkan-loader}/lib:${pkgs.mesa}/lib:${libX11}/lib:${libXcursor}/lib:${libXxf86vm}/lib:${libXi}/lib:${libXrandr}/lib:${pkgs.libxkbcommon}/lib";
    DISPLAY = ":0";
    RUSTC_WRAPPER = "${pkgs.sccache}/bin/sccache";
    SCCACHE_CACHE_SIZE = "120G";
  }

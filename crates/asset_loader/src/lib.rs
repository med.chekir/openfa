// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use anyhow::{anyhow, bail, Context, Result};
use arcball::ArcBallController;
use bevy_ecs::prelude::*;
use camera::ScreenCameraController;
use catalog::{AssetCatalog, FileSystem};
use fa_vehicle::Turbojet;
use flight_dynamics::ClassicFlightModel;
use geodb::GeoDb;
use geodesy::{Bearing, Geodetic};
use installations::{from_dos_string, Installations};
use mantle::Gpu;
use mmm::{Mission, MissionMap, ObjectInfo};
use nitrous::{
    inject_nitrous_component, inject_nitrous_resource, make_symbol, method, HeapMut,
    NitrousComponent, NitrousResource, Value,
};
use ot::ObjectFlags;
use phase::{BodyMotion, Frame};
use player::PlayerCameraController;
use runtime::{Extension, PlayerMarker, Runtime, RuntimeResource};
use sh::GearFootprintKind;
use shape::{Shape, ShapeIds, ShapeScale};
use std::{collections::HashMap, iter, time::Duration};
use t2_terrain::{T2TerrainBuffer, T2TileSet};
use vehicle::{
    AirbrakeControl, AirbrakeEffector, Airframe, BayControl, BayEffector, FlapsControl,
    FlapsEffector, FuelSystem, FuelTank, FuelTankKind, GearControl, GearEffector, HookControl,
    HookEffector, PitchInceptor, PowerSystem, RollInceptor, ThrottleInceptor,
    ThrustVectorPitchControl, ThrustVectorPitchEffector, ThrustVectorYawControl,
    ThrustVectorYawEffector, VtolAngleControl, YawInceptor,
};
use xt::TypeManager;

#[derive(NitrousComponent, Debug, Default)]
pub struct MissionMarker;

#[inject_nitrous_component]
impl MissionMarker {}

#[derive(Debug, Default, NitrousResource)]
#[resource(name = "game")]
pub struct AssetLoader;

impl Extension for AssetLoader {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        let asset_loader = Self::new();
        runtime.inject_resource(asset_loader)?;
        Ok(())
    }
}

#[inject_nitrous_resource]
impl AssetLoader {
    fn new() -> Self {
        Self
    }

    // FIXME: make this actually work
    // #[method]
    // fn boresight(&self, mut heap: HeapMut) {
    //     heap.resource_scope(|mut heap, camera: Mut<ScreenCamera>| {
    //         heap.resource_scope(|mut heap, shapes: Mut<Shape>| {
    //             let mut intersects = Vec::new();
    //             let view_ray = Ray::new(
    //                 camera.position::<Meters>().point64(),
    //                 camera.forward().to_owned(),
    //             );
    //             for (name, shape_id, frame, scale) in heap
    //                 .query::<(&EntityName, &ShapeId, &Frame, &ShapeScale)>()
    //                 .iter(heap.world())
    //             {
    //                 let metadata = shapes.metadata(*shape_id);
    //                 if let Some(intersect) = metadata.read().extent().intersect_ray(
    //                     frame.position(),
    //                     scale.scale() as f64,
    //                     &view_ray,
    //                 ) {
    //                     intersects.push((
    //                         name.name().to_owned(),
    //                         OrderedFloat(intersect.coords.magnitude()),
    //                     ));
    //                 };
    //             }
    //             intersects.sort_by_key(|(_, d)| -*d);
    //             for (name, d) in intersects {
    //                 println!("{name}: at {d:?}");
    //             }
    //         })
    //     });
    // }

    /// Despawn whatever camera is current and re-spawn as a generic arcball controlled
    /// camera, wherever the prior camera was located, if it existed.
    #[method]
    pub fn detach_camera(&self, mut heap: HeapMut) -> Result<()> {
        let mut loc = None;
        if let Some(id) = heap.maybe_entity_by_name("camera") {
            loc = heap.maybe_get_by_id::<Frame>(id).map(|v| v.to_owned());
            heap.despawn_by_id(id);
        }
        let frame = if let Some(loc) = loc {
            loc
        } else {
            Frame::default()
        };

        heap.spawn("camera")?
            .inject(frame)?
            .inject(ArcBallController::default())?
            .inject(ScreenCameraController)?;

        Ok(())
    }

    /// Despawn whatever camera is current and re-spawn as a player camera.
    /// A PlayerMarker should be added to some entity before the next frame.
    #[method]
    pub fn attach_camera(&self, mut heap: HeapMut) -> Result<()> {
        let mut loc = None;
        if let Some(id) = heap.maybe_entity_by_name("camera") {
            loc = heap.maybe_get_by_id::<Frame>(id).map(|v| v.to_owned());
            heap.despawn_by_id(id);
        }
        let frame = if let Some(loc) = loc {
            loc
        } else {
            Frame::default()
        };

        heap.spawn("camera")?
            .inject(frame)?
            .inject(PlayerCameraController::new("external")?)?
            .inject(ScreenCameraController)?;
        Ok(())
    }

    #[method]
    pub fn take_control(&self, name: &str, mut heap: HeapMut) -> Result<()> {
        let player_id = heap.entity_by_name("Player");
        if let Some(target_id) = heap.maybe_entity_by_name(name) {
            heap.entity_by_id_mut(player_id)?
                .remove::<PlayerMarker>()?
                .rename_numbered("Player_prior_");
            heap.entity_by_id_mut(target_id)?
                .inject(PlayerMarker)?
                .rename("Player");
        }
        Ok(())
    }

    fn frame_for_interactive(heap: &mut HeapMut) -> Frame {
        let target = if heap
            .maybe_get::<ScreenCameraController>("fallback_camera")
            .is_some()
        {
            heap.get::<ArcBallController>("fallback_camera").target()
        } else if heap.maybe_get::<ScreenCameraController>("camera").is_some() {
            let mut geo = heap.get::<Frame>("camera").geodetic();
            *geo.lat_mut() += degrees!(1_f64 / 60_f64);
            geo
        } else {
            Geodetic::new(degrees!(0f32), degrees!(0f32), meters!(10f32))
        };
        Frame::from_geodetic_and_bearing(target, Bearing::north())
    }

    #[method]
    pub fn spawn(&self, name: &str, filename: &str, mut heap: HeapMut) -> Result<Value> {
        // Please don't force me to type in all-caps all the time.
        let filename = filename.to_uppercase();

        // Make sure we can load the thing, before creating the entity.
        let xt = {
            let libs = heap.resource::<AssetCatalog>();
            let info = libs.lookup(&filename)?;
            heap.resource::<TypeManager>().load(info)?
        };
        let shape_file_name = xt
            .ot()
            .shape_file()
            .as_ref()
            .ok_or_else(|| anyhow!("no shape"))?
            .to_owned();

        // Fake up an info.
        let info = ObjectInfo::from_xt(xt.clone(), name);

        // Preload the one shape
        let shape_ids = heap.resource_scope(|heap, mut shapes: Mut<Shape>| {
            shapes.upload_shape(
                heap.resource::<Installations>().primary_palette(),
                heap.resource::<AssetCatalog>().lookup(&shape_file_name)?,
                heap.resource::<AssetCatalog>(),
                heap.resource::<Gpu>(),
            )
        })?;

        // Use the spawn routing shared by mission loading
        let id = Self::spawn_entity_common(
            name,
            Some((shape_file_name, shape_ids)),
            &info,
            None,
            heap.as_mut(),
        )?;
        if name == "Player" {
            heap.entity_by_id_mut(id)?.inject(PlayerMarker)?;
        }

        Ok(Value::True())
    }

    /// Spawn with a temp name, then game.take_control of the new entity.
    #[method]
    pub fn spawn_in(&self, filename: &str, mut heap: HeapMut) -> Result<Value> {
        let name: String = iter::repeat_with(fastrand::alphanumeric).take(6).collect();
        self.spawn(&name, filename, heap.as_mut())?;
        self.take_control(&name, heap)?;
        Ok(Value::True())
    }

    fn spawn_entity_common<S: AsRef<str>>(
        inst_name: S,
        shape_info: Option<(String, ShapeIds)>,
        info: &ObjectInfo,
        tile_id: Option<Entity>,
        mut heap: HeapMut,
    ) -> Result<Entity> {
        let id = heap.spawn(inst_name.as_ref())?.inject(MissionMarker)?.id();

        // Load the shape if it has one.
        let ground_offset = if let Some((shape_name, shape_ids)) = shape_info {
            // We need to include info about the expected strut (or wheel) layout from
            // our information about the plane/car/whatever.
            let gear_layout = if let Some(pt) = info.xt().pt() {
                if pt.pt_flags().is_helicopter() {
                    Some(GearFootprintKind::Skid)
                } else {
                    // gear_pitch is used for SeaHarrier... does FA have any tail draggers at all?
                    Some(GearFootprintKind::Nose)
                }
            } else {
                // TODO: detect tracked vehicles?
                info.xt().nt().map(|_| GearFootprintKind::Car)
            };
            let (base_bundle, flag_bundle, xform_bundle) =
                heap.resource::<Shape>()
                    .shape_bundle(shape_name, shape_ids, gear_layout)?;
            let mut ent = heap.entity_by_id_mut(id)?;
            ent.inject(base_bundle)?;
            if let Some(flag_bundle) = flag_bundle {
                ent.inject(flag_bundle)?;
            }
            if let Some(xform_bundle) = xform_bundle {
                ent.inject(xform_bundle)?;
            }
            // FIXME: analyze bounding boxes
            // heap.resource::<Shape>()
            //     .metadata(*heap.get_by_id::<ShapeId>(id))
            //     .read()
            //     .extent()
            //     .offset_to_ground()
            meters!(0)
        } else {
            meters!(0)
        };

        let scale = if info.xt().ot().ot_flags().contains(ObjectFlags::RUNWAY) {
            4f64
        } else {
            1f64
        };
        if let Ok(mut comp) = heap.get_by_id_mut::<ShapeScale>(id) {
            comp.set_scale(scale);
        }

        let frame = if let Some(tile_id) = tile_id {
            let tile_mapper = heap.get_by_id::<T2TileSet>(tile_id).mapper();
            // FIXME: figure out the terrain height here and raise to at least that level
            let mut position = tile_mapper.fa_world_to_geodetic(info.position());
            // FIXME: use size of object as precision
            let frame_number = heap.resource::<RuntimeResource>().frame_number();
            let ground_height = heap
                .resource_mut::<GeoDb>()
                .get_height_immediate(position.geode(), meters!(10.), frame_number)
                .unwrap_or(meters!(0));

            *position.asl_mut() += ground_height - ground_offset;
            let bearing = Bearing::new(-info.angle().yaw());
            // FIXME: manually re-align strip halfs
            Frame::from_geodetic_and_bearing(position, bearing)
        } else {
            Self::frame_for_interactive(&mut heap)
        };

        let facing = frame.facing();
        heap.entity_by_id_mut(id)?
            .inject(frame)?
            .inject(info.xt())?;

        if info.xt().is_jt() || info.xt().is_nt() || info.xt().is_pt() {
            heap.entity_by_id_mut(id)?.inject(BodyMotion::new_forward(
                feet_per_second!(info.speed()),
                facing,
            ))?;

            // TODO: use audio effect time for timing the animation and effector deployment times?
            // heap.entity_by_id_mut(id)
            // TODO: fuel overrides
            // if let Some(fuel_override) = info.fuel_override() {
            //     heap.get_mut::<VehicleState>(id)
            //         .set_internal_fuel_lbs(fuel_override);
            // }
            // TODO: hardpoint overrides
        }

        // If the type is a plane, install a flight model
        if let Some(pt) = info.xt().pt() {
            let on_ground = info.position().y() == feet!(0);

            let fuel = FuelSystem::default().with_internal_tank(FuelTank::new(
                FuelTankKind::Center,
                kilograms!(*pt.internal_fuel()),
            ))?;

            let power = PowerSystem::default().with_engine(Turbojet::new_min_power(info.xt())?);

            heap.entity_by_id_mut(id)?
                .inject(ClassicFlightModel::default())?
                .inject(Airframe::new(kilograms!(*info.xt().ot().empty_weight())))?
                .inject(fuel)?
                .inject(power)?
                .inject(PitchInceptor::default())?
                .inject(RollInceptor::default())?
                .inject(YawInceptor::default())?
                .inject(ThrustVectorPitchControl::new(
                    degrees!(-30),
                    degrees!(30),
                    degrees!(1),
                ))?
                .inject(ThrustVectorYawControl::new(
                    degrees!(-30),
                    degrees!(30),
                    degrees!(1),
                ))?
                .inject(VtolAngleControl::new(
                    degrees!(*pt.vt_limit_down()),
                    degrees!(*pt.vt_limit_down()),
                    degrees!(10),
                ))?
                .inject(ThrottleInceptor::new_min_power())?
                .inject(AirbrakeControl::default())?
                .inject(AirbrakeEffector::new(0., Duration::from_millis(1)))?
                .inject(BayControl::default())?
                .inject(ThrustVectorPitchEffector::new(0., Duration::from_millis(1)))?
                .inject(ThrustVectorYawEffector::new(0., Duration::from_millis(1)))?
                .inject(BayEffector::new(0., Duration::from_secs(2)))?
                .inject(FlapsControl::default())?
                .inject(FlapsEffector::new(0., Duration::from_millis(1)))?
                .inject(GearControl::new(on_ground))?
                .inject(GearEffector::new(
                    if on_ground { 1. } else { 0. },
                    Duration::from_secs(4),
                ))?
                .inject(HookControl::default())?
                .inject(HookEffector::new(0., Duration::from_millis(1)))?;
        }

        Ok(id)
    }

    fn load_mmm_common<'a, I>(
        &self,
        map_name: &str,
        mission_map: &MissionMap,
        objects_pass_1: I,
        heap: &mut HeapMut,
    ) -> Result<()>
    where
        I: Iterator<Item = &'a ObjectInfo>,
    {
        let tile_set = heap.resource_scope(|heap, mut t2_terrain: Mut<T2TerrainBuffer>| {
            t2_terrain.add_map(
                heap.resource::<Installations>().primary_palette(),
                mission_map,
                heap.resource::<AssetCatalog>(),
                heap.resource::<Gpu>(),
            )
        })?;
        let tile_id = heap
            .spawn(make_symbol(map_name))?
            .inject(tile_set)?
            .inject(MissionMarker)?
            .id();

        // Pre-load the shapes into as few chunks as possible.
        let install_name = heap.resource::<Installations>().primary().name().to_owned();
        let mut duplicates: HashMap<String, i32> = HashMap::new();

        for info in objects_pass_1 {
            let shape_info = if let Some(shape_file) = info.xt().ot().shape_file().as_ref() {
                let shape_ids = heap
                    .resource_scope(|heap, mut shapes: Mut<Shape>| {
                        shapes.upload_shape(
                            heap.resource::<Installations>().primary_palette(),
                            heap.resource::<AssetCatalog>().lookup(shape_file)?,
                            heap.resource::<AssetCatalog>(),
                            heap.resource::<Gpu>(),
                        )
                    })
                    .with_context(|| format!("loading map {map_name}"))?;
                Some((shape_file.to_owned(), shape_ids))
            } else {
                None
            };

            let base_inst_name = info.name().unwrap_or_else(|| {
                let shape_name = info.xt().ot().shape_file().clone().unwrap_or_default();
                format!(
                    "{}_{}_{}",
                    install_name,
                    make_symbol(&map_name[0..map_name.len() - 3]),
                    make_symbol(if !shape_name.is_empty() {
                        &shape_name[0..shape_name.len() - 3]
                    } else {
                        "none"
                    }),
                )
                .to_lowercase()
            });
            let (inst_name, next_count) =
                if let Some(current_count) = duplicates.get(&base_inst_name) {
                    (
                        format!("{}_{}", base_inst_name, current_count + 1),
                        current_count + 1,
                    )
                } else {
                    (base_inst_name.clone(), 0)
                };
            duplicates.insert(base_inst_name, next_count);

            let id = Self::spawn_entity_common(
                &inst_name,
                shape_info,
                info,
                Some(tile_id),
                heap.as_mut(),
            )?;
            if inst_name == "Player" {
                heap.entity_by_id_mut(id)?.inject(PlayerMarker)?;
            }
        }

        Ok(())
    }

    #[method]
    pub fn unload_mission(&self, mut heap: HeapMut) -> Result<()> {
        let mut work = Vec::new();
        for (entity, _) in heap.query::<(Entity, &MissionMarker)>().iter(heap.world()) {
            work.push(entity);
        }
        for entity in work.drain(..) {
            heap.despawn_by_id(entity);
        }
        Ok(())
    }

    #[method]
    pub fn load_map(&self, name: &str, mut heap: HeapMut) -> Result<()> {
        let name = name.to_uppercase();
        if name.starts_with('~') || name.starts_with('$') {
            bail!("cannot load {name}; it is a template (note the ~ or $ prefix)");
        }
        // let game_name = heap.resource::<Installations>().primary().name().to_owned();

        let mm = {
            let libs = heap.resource::<AssetCatalog>();
            // FIXME: make libs it's own FS abstraction.
            let mm_cfn = libs.lookup(&name)?;
            let mm_content = from_dos_string(mm_cfn.data()?);
            MissionMap::from_str(mm_content.as_ref(), heap.resource::<TypeManager>(), libs)?
        };

        self.load_mmm_common(&name, &mm, mm.objects(), &mut heap)?;

        Ok(())
    }

    #[method]
    pub fn load_mission(&self, name: &str, mut heap: HeapMut) -> Result<()> {
        // Unload prior mission
        self.unload_mission(heap.as_mut())?;

        let name = name.to_uppercase();
        let install_name = heap.resource::<Installations>().primary().name().to_owned();

        // FIXME: can we print this in a useful way?
        println!("Loading {install_name}:{name}...");

        let mission = {
            let libs = heap.resource::<AssetCatalog>();
            let m_cfn = libs.lookup(&name)?;
            let m_content = from_dos_string(m_cfn.data()?);
            Mission::from_str(m_content.as_ref(), heap.resource::<TypeManager>(), libs)?
        };

        self.load_mmm_common(
            &name,
            mission.mission_map(),
            mission.all_objects(),
            &mut heap,
        )?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use atmosphere::Atmosphere;
    use camera::ScreenCamera;
    use catalog::{CatalogOpts, Search};
    use installations::LibsOpts;
    use mantle::{Core, CpuDetailLevel, GpuDetailLevel};
    use nitrous::make_partial_test;
    use orrery::Orrery;
    use player::PlayerCameraController;
    use runtime::{StdPaths, StdPathsOpts};
    use terrain::{Terrain, TerrainOpts};

    fn can_load_all_missions(section: usize, total_sections: usize) -> Result<()> {
        // Some missions are templates, apparently
        const SKIP: [&str; 18] = [
            // All these contain `type <selected>`
            "~AA7.M",
            "~AA7V.M",
            "~AAC130.M",
            "~AAV8.M",
            "~AF104.M",
            "~AF14.M",
            "~AF18.M",
            "~AF4B.M",
            "~AF4J.M",
            "~AF8J.M",
            "~AMIG17F.M",
            "~AMIG21F.M",
            "~ASEAHAR.M",
            "~ASU33.M",
            "~AYAK141.M",
            "~INFO.M",
            // Explicity commented as a fake mission for the creator
            "~MC_NATO.M",
            "quick.M",
        ];

        let mut runtime = Core::for_test()?;
        runtime
            .load_extension::<AssetLoader>()?
            .load_extension_with::<StdPaths>(StdPathsOpts::new("openfa-test"))?
            .load_extension_with::<AssetCatalog>(CatalogOpts::default())?
            .load_extension_with::<Installations>(LibsOpts::for_testing())?
            .load_extension::<TypeManager>()?
            .load_extension::<GeoDb>()?
            .load_extension::<Orrery>()?
            .load_extension::<Atmosphere>()?
            .load_extension_with::<Terrain>(TerrainOpts::from_detail(
                CpuDetailLevel::Low,
                GpuDetailLevel::Low,
            ))?
            .load_extension::<T2TerrainBuffer>()?
            .load_extension::<Shape>()?
            .load_extension::<ScreenCamera>()?
            .load_extension::<PlayerCameraController>()?;

        let _fallback_camera_ent = runtime
            .spawn("fallback_camera")?
            .inject(Frame::default())?
            .inject(ArcBallController::default())?
            .id();

        let missions = runtime
            .resource::<AssetCatalog>()
            .search(Search::for_extension("M").in_collection("FA").must_match())?
            .into_iter()
            .filter(|info| !SKIP.contains(&info.name()))
            .filter(|info| !info.name().starts_with("~F"))
            .filter(|info| !info.name().starts_with("~Q"))
            .map(|info| info.name().to_owned())
            .collect::<Vec<String>>();

        let missions = nitrous::segment_tests(section, total_sections, &missions);

        for mission in missions {
            runtime.resource_scope(|heap, assets: Mut<AssetLoader>| {
                assets.load_mission(mission, heap)
            })?;
        }

        Ok(())
    }
    make_partial_test!(can_load_all_missions, 15);
}

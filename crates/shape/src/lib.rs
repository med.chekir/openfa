// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
mod chunk;
mod components;
mod instance_block;

pub use crate::{
    chunk::{
        upload::{ExtractTarget, FaceInfo, ShapeExtractor},
        ShapeId, ShapeIds, ShapeName, ShapeVertex,
    },
    components::*,
};

use crate::{
    chunk::{ChunkManager, VertexFlags},
    instance_block::{InstanceBlock, TransformType},
};
use absolute_unit::{Meters, Radians};
use anyhow::{anyhow, ensure, Context, Result};
use atlas::{AtlasPacker, Frame as AtlasFrame};
use atmosphere::Atmosphere;
use bevy_ecs::prelude::*;
use camera::{CameraStep, ScreenCamera};
use catalog::{AssetCatalog, FileInfo, FileSystem, Search};
use composite::CompositeStep;
use gui::GuiStep;
use image::Rgba;
use installations::Installations;
use log::{trace, warn};
use mantle::{BindingsBuilder, CurrentEncoder, Gpu, GpuStep, LayoutBuilder, TimeStep};
use marker::MarkersStep;
use nitrous::{inject_nitrous_resource, NitrousResource};
use orrery::{Orrery, OrreryStep};
use pal::Palette;
use phase::Frame;
use pic::Pic;
use pic_uploader::PicUploader;
use runtime::{Extension, Runtime};
use sh::{
    AfterBurnerState, AirbrakeState, BayState, DrawSelection, DrawState, FlapState,
    GearFootprintKind, GearState, HookState, ShAnalysis, ShCapabilities, ShCode, ShExtent,
    ShXforms, SlatsState,
};
use smallvec::SmallVec;
use std::collections::HashMap;
use t2_terrain::T2TerrainStep;
use terrain::TerrainStep;
use uuid::Uuid;
use vehicle::{
    AirbrakeEffector, BayEffector, FlapsEffector, GearEffector, HookEffector, PowerSystem,
    ThrustVectorPitchEffector, VtolAngleEffector,
};
use world::WorldStep;

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub enum PlaneArtKind {
    Left,
    Right,
    Nose,
    Round,
}

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum ShapeStep {
    UploadArt,
    ResetUploadCursor,
    AnimateDrawState,
    ApplyTransforms,
    ApplyFlags,
    ApplyXforms,
    PushToBlock,
    UploadChunks,
    UploadBlocks,
    Render,
    CleanupOpenChunks,
}

#[derive(Debug, NitrousResource)]
pub struct Shape {
    chunk_man: ChunkManager,

    // Map from any particular shape name to the ShapeIds we have on file for it.
    shapes_cache: HashMap<String, ShapeIds>,

    // Each instance needs its own copy of the xformers VMs
    analysis_cache: HashMap<ShapeId, ShAnalysis>,

    combined_group: wgpu::BindGroup,
    block_bind_group_layout: wgpu::BindGroupLayout,
    pipeline: wgpu::RenderPipeline,
}

impl Extension for Shape {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        let shapes = runtime.resource_scope(|heap, mut gpu: Mut<Gpu>| {
            Shape::new(
                heap.resource::<AssetCatalog>(),
                heap.resource::<Installations>(),
                heap.resource::<Atmosphere>(),
                heap.resource::<Orrery>(),
                &mut gpu,
            )
        })?;

        runtime.add_frame_system(
            Self::sys_ts_reset_upload_cursor.in_set(ShapeStep::ResetUploadCursor),
        );
        runtime
            .add_frame_system(Self::sys_ts_animate_draw_state.in_set(ShapeStep::AnimateDrawState));
        runtime.add_frame_system(
            Self::sys_ts_apply_transforms
                .in_set(ShapeStep::ApplyTransforms)
                .after(CameraStep::MoveCameraToFrame)
                // Not ordered for camera users that only care about the backing store
                .ambiguous_with(TerrainStep::RenderDeferredTexture)
                .ambiguous_with(TerrainStep::AccumulateNormalsAndColor)
                .ambiguous_with(T2TerrainStep::AccumulateNormalsAndColor)
                .ambiguous_with(WorldStep::Render),
        );
        runtime.add_frame_system(
            Self::sys_ts_build_flag_mask
                .in_set(ShapeStep::ApplyFlags)
                .after(ShapeStep::AnimateDrawState),
        );
        runtime.add_frame_system(
            Self::sys_ts_apply_xforms
                .in_set(ShapeStep::ApplyXforms)
                .after(ShapeStep::AnimateDrawState),
        );
        runtime.add_frame_system(
            Self::sys_ts_push_values_to_block
                .in_set(ShapeStep::PushToBlock)
                .after(ShapeStep::ApplyTransforms)
                .after(ShapeStep::ApplyFlags)
                .after(ShapeStep::ApplyXforms)
                .after(ShapeStep::ResetUploadCursor),
        );

        runtime.add_frame_system(
            Self::sys_close_open_chunks
                .in_set(ShapeStep::UploadChunks)
                .after(GpuStep::CreateCommandEncoder)
                .before(ShapeStep::PushToBlock)
                .before(GpuStep::SubmitCommands)
                .ambiguous_with(ShapeStep::ResetUploadCursor)
                // Does encoder ordering matter? They all serialize on mutable access, so order
                // them as would make sense for drawing, to minimize inter-run variations.
                .after(OrreryStep::UploadToGpu)
                .after(CameraStep::UploadToGpu)
                .after(TerrainStep::EncodeUploads)
                .after(TerrainStep::RenderDeferredTexture)
                .after(T2TerrainStep::Tessellate)
                .after(T2TerrainStep::EncodeUploads)
                .after(T2TerrainStep::FinishUploads)
                .after(T2TerrainStep::AccumulateNormalsAndColor)
                .after(WorldStep::Render)
                .before(MarkersStep::UploadGeometry)
                .before(GuiStep::EndFrame),
        );
        runtime.add_frame_system(
            Self::sys_upload_block_frame_data
                .in_set(ShapeStep::UploadBlocks)
                .after(ShapeStep::PushToBlock)
                .after(GpuStep::CreateCommandEncoder)
                .before(GpuStep::SubmitCommands),
        );
        runtime.add_frame_system(
            Self::sys_draw_shapes
                .in_set(ShapeStep::Render)
                .after(ShapeStep::UploadChunks)
                .after(ShapeStep::UploadBlocks)
                .after(OrreryStep::UploadToGpu)
                .after(WorldStep::Render)
                .before(MarkersStep::Render)
                .before(CompositeStep::Render)
                // Does encoder ordering matter? They all serialize on mutable access, so order
                // them as would make sense for drawing, to minimize inter-run variations.
                .before(GuiStep::EndFrame)
                .before(MarkersStep::UploadGeometry),
        );

        runtime.inject_resource(shapes)?;
        Ok(())
    }
}

#[inject_nitrous_resource]
impl Shape {
    pub fn new(
        catalog: &AssetCatalog,
        installations: &Installations,
        atmosphere: &Atmosphere,
        orrery: &Orrery,
        gpu: &mut Gpu,
    ) -> Result<Self> {
        // An atlas with all nose, tail, wing, and country art.
        let mut plane_art_map = HashMap::new();
        let _atlas = {
            let mut pic_uploader = PicUploader::new(gpu)?;
            let mut atlas_packer = AtlasPacker::<Rgba<u8>>::new(
                "plane-art",
                gpu,
                Gpu::stride_for_row_size(12 * 129 * 4) / 4,
                Gpu::stride_for_row_size(12 * 129 * 4) / 4,
                wgpu::TextureFormat::Rgba8Unorm,
                wgpu::FilterMode::Nearest, // TODO: see if we can "improve" things with filtering?
            );
            let pairs = vec![
                (PlaneArtKind::Round, 5, "ROUND??.PIC"), // 60 images at 128x128
                (PlaneArtKind::Left, 4, "LEFT??.PIC"),   // 25 images at 128x128
                (PlaneArtKind::Right, 5, "RIGHT??.PIC"), // 25 images at 128x128
                (PlaneArtKind::Nose, 4, "NOSE??.PIC"),   // 24 images at 64x64
            ];
            let usage = wgpu::BufferUsages::COPY_SRC;
            for installation_name in installations.installation_names() {
                pic_uploader.set_shared_palette(installations.palette(installation_name)?, gpu);
                plane_art_map.insert(installation_name.to_owned(), HashMap::new());
                // FIXME: load all of these, in each collection
                for (kind, offset, glob) in &pairs {
                    plane_art_map
                        .get_mut(installation_name)
                        .unwrap()
                        .insert(*kind, HashMap::new());
                    for info in catalog.search(Search::for_extension("PIC").with_glob(glob))? {
                        let number = info.name()[*offset..*offset + 2].parse::<u32>()?;
                        let data = info.data()?;
                        let pic = Pic::from_bytes(&data)?;
                        let (buffer, width, height, stride_bytes) =
                            pic_uploader.upload(&pic, gpu, usage)?;
                        let frame =
                            atlas_packer.push_buffer(buffer, width, height, stride_bytes)?;
                        plane_art_map
                            .get_mut(installation_name)
                            .unwrap()
                            .get_mut(kind)
                            .unwrap()
                            .insert(number, frame);
                    }
                }
            }
            let mut encoder =
                gpu.device()
                    .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                        label: Some("plane-art-upload-encoder"),
                    });
            pic_uploader.expand_pics(&mut encoder);
            atlas_packer.encode_frame_uploads(gpu, &mut encoder);
            pic_uploader.finish_expand_pass();
            gpu.queue_mut().submit(vec![encoder.finish()]);
            atlas_packer
        };

        let combined_group_layout = LayoutBuilder::default()
            .with_label("shape-combined-layout")
            .with_layout::<Atmosphere>()
            .with_layout::<Orrery>()
            .build(gpu);
        let combined_group = BindingsBuilder::new(&combined_group_layout)
            .with_bindings(atmosphere)
            .with_bindings(orrery)
            .build(gpu);

        let block_bind_group_layout =
            gpu.device()
                .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                    label: Some("shape-instance-bind-group-layout"),
                    entries: &[wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStages::VERTEX,
                        ty: wgpu::BindingType::Buffer {
                            ty: wgpu::BufferBindingType::Storage { read_only: true },
                            has_dynamic_offset: false,
                            min_binding_size: InstanceBlock::instance_info_buffer_size(),
                        },
                        count: None,
                    }],
                });

        let chunk_man = ChunkManager::new(gpu)?;

        let shader = gpu.compile_shader("shape.wgsl", include_str!("../target/shape.wgsl"));

        let pipeline_layout =
            gpu.device()
                .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                    label: Some("shape-render-pipeline-layout"),
                    push_constant_ranges: &[],
                    bind_group_layouts: &[
                        &ScreenCamera::info_bind_group_layout(gpu),
                        &combined_group_layout,
                        chunk_man.bind_group_layout(),
                        &block_bind_group_layout,
                    ],
                });

        let pipeline = gpu
            .device()
            .create_render_pipeline(&wgpu::RenderPipelineDescriptor {
                label: Some("shape-render-pipeline"),
                layout: Some(&pipeline_layout),
                vertex: wgpu::VertexState {
                    module: &shader,
                    entry_point: "vert_main",
                    buffers: &[ShapeVertex::descriptor()],
                },
                fragment: Some(wgpu::FragmentState {
                    module: &shader,
                    entry_point: "frag_main",
                    targets: &[Some(wgpu::ColorTargetState {
                        format: ScreenCamera::COLOR_FORMAT,
                        blend: None,
                        write_mask: wgpu::ColorWrites::ALL,
                    })],
                }),
                primitive: wgpu::PrimitiveState {
                    topology: wgpu::PrimitiveTopology::TriangleList,
                    strip_index_format: None,
                    front_face: wgpu::FrontFace::Cw,
                    // Note: showing backfaces would minimize the impact of the many seams and gaps
                    //       in FA models; however, many surfaces are co-planar, resulting in
                    //       massive z-fighting between front and reversed back faces.
                    cull_mode: Some(wgpu::Face::Back),
                    unclipped_depth: true,
                    polygon_mode: wgpu::PolygonMode::Fill,
                    conservative: false,
                },
                depth_stencil: Some(wgpu::DepthStencilState {
                    format: Gpu::DEPTH_FORMAT,
                    depth_write_enabled: true,
                    depth_compare: wgpu::CompareFunction::Greater,
                    stencil: wgpu::StencilState {
                        front: wgpu::StencilFaceState::IGNORE,
                        back: wgpu::StencilFaceState::IGNORE,
                        read_mask: 0,
                        write_mask: 0,
                    },
                    bias: wgpu::DepthBiasState {
                        constant: 0,
                        slope_scale: 0.0,
                        clamp: 0.0,
                    },
                }),
                multisample: wgpu::MultisampleState {
                    count: 1,
                    mask: !0,
                    alpha_to_coverage_enabled: false,
                },
                multiview: None,
            });

        Ok(Self {
            chunk_man,
            shapes_cache: HashMap::new(),
            analysis_cache: HashMap::new(),
            combined_group,
            block_bind_group_layout,
            pipeline,
        })
    }

    pub fn upload_shape(
        &mut self,
        palette: &Palette,
        shape_file_info: FileInfo,
        catalog: &dyn FileSystem,
        gpu: &Gpu,
    ) -> Result<ShapeIds> {
        // Load all SH files, including associated damage models (but not shadow shapes, those have
        // their own section in the OT files, for some reason).
        let full_shape_file_name = shape_file_info.to_string();
        if let Some(shape_ids) = self.shapes_cache.get(&full_shape_file_name) {
            return Ok(shape_ids.to_owned());
        }

        trace!("Loading: {shape_file_info}");
        // Parse instructions out of the PE.
        let sh_code = ShCode::from_bytes(&shape_file_info.data()?)
            .with_context(|| format!("raw shape {shape_file_info}"))?;

        // FIXME: We now store teh full ShCode, so no need to cache the analysis separately.

        // Transpile to GPU
        let (normal_shape_id, maybe_damage_shape_id) = self.chunk_man.upload_shape(
            &shape_file_info.to_string(),
            &sh_code,
            palette,
            catalog,
            gpu,
        )?;

        // Capture what we need to manage the shape
        self.analysis_cache.insert(
            normal_shape_id,
            sh_code.analysis(DrawSelection::NormalModel)?.to_owned(),
        );

        // Look up our damage shape(s)
        let mut damage_shape_ids = SmallVec::new();
        if let Some(damage_shape_id) = maybe_damage_shape_id {
            damage_shape_ids.push(damage_shape_id);
            self.analysis_cache.insert(
                damage_shape_id,
                sh_code.analysis(DrawSelection::DamageModel)?.to_owned(),
            );
        } else {
            let (base_name, _sh) = shape_file_info.name().rsplit_once('.').unwrap();
            for suffix in ["_A", "_B", "_C", "_D"] {
                let assoc_name = format!("{base_name}{suffix}.SH");
                if let Ok(shape_dam_info) = catalog.lookup(&assoc_name) {
                    trace!("Loading: {shape_dam_info}");
                    let sh_code = ShCode::from_bytes(&shape_dam_info.data()?)
                        .with_context(|| format!("raw damage shape {shape_dam_info}"))?;
                    let (damage_shape_id, maybe_damage_shape_id) = self.chunk_man.upload_shape(
                        &shape_dam_info.to_string(),
                        &sh_code,
                        palette,
                        catalog,
                        gpu,
                    )?;
                    ensure!(
                        maybe_damage_shape_id.is_none(),
                        "dedicated damage shape has a damage sub-model! {shape_dam_info}"
                    );
                    damage_shape_ids.push(damage_shape_id);
                    self.analysis_cache.insert(
                        damage_shape_id,
                        sh_code.analysis(DrawSelection::NormalModel)?.to_owned(),
                    );
                }
            }
        }

        let shape_ids = ShapeIds::new(normal_shape_id, damage_shape_ids);
        self.shapes_cache
            .insert(full_shape_file_name, shape_ids.clone());

        Ok(shape_ids)
    }

    pub fn shape_bundle<S: AsRef<str>>(
        &self,
        name: S,
        shape_set: ShapeIds,
        gear_layout: Option<GearFootprintKind>,
    ) -> Result<(
        (
            ShapeId,
            ShapeIds,
            ShapeName,
            ShapeScale,
            ShCapabilities,
            ShExtent,
            ShapeTransformBuffer,
        ),
        Option<(DrawState, ShapeFlagBuffer)>,
        Option<(ShXforms, ShapeXformBuffer)>,
    )> {
        let mut flags_bundle = None;
        let mut xforms_bundle = None;

        let shape_name = ShapeName::new(name.as_ref());
        let shape_id = shape_set.normal();
        let analysis = self.analysis_cache[&shape_id].to_owned();
        let mut extent = analysis.extent().to_owned();
        if let Some(gear_layout) = gear_layout {
            extent.lay_out_gear(gear_layout);
        }
        let base_bundle = (
            shape_id,
            shape_set,
            shape_name,
            ShapeScale::new(1.),
            analysis.capabilities(),
            extent,
            ShapeTransformBuffer::default(),
        );
        if analysis.capabilities().needs_draw_state() {
            let draw_state = DrawState::empty(analysis.capabilities());
            flags_bundle = Some((draw_state, ShapeFlagBuffer::default()));
            if let Some(shape_xforms) = analysis.xforms_for_shape_bundle() {
                xforms_bundle = Some((shape_xforms, ShapeXformBuffer::default()));
            }
        }
        Ok((base_bundle, flags_bundle, xforms_bundle))
    }

    pub fn analysis(&self, shape_id: ShapeId) -> Result<&ShAnalysis> {
        self.analysis_cache
            .get(&shape_id)
            .ok_or_else(|| anyhow!("no analysis for shape_id {shape_id:?}"))
    }

    pub fn analysis_mut(&mut self, shape_id: ShapeId) -> Result<&mut ShAnalysis> {
        self.analysis_cache
            .get_mut(&shape_id)
            .ok_or_else(|| anyhow!("no analysis for shape_id {shape_id:?}"))
    }

    // Finish any uploads, queue work to depalettize, etc.
    fn sys_close_open_chunks(
        mut shapes: ResMut<Shape>,
        gpu: Res<Gpu>,
        mut maybe_encoder: ResMut<CurrentEncoder>,
    ) {
        if let Some(encoder) = maybe_encoder.get_encoder_mut() {
            shapes.chunk_man.close_open_chunks(&gpu, encoder)
        }
    }

    fn sys_ts_reset_upload_cursor(mut shapes: ResMut<Shape>) {
        shapes.chunk_man.reset_upload_cursor();
    }

    fn sys_ts_animate_draw_state(
        mut query: Query<(
            &mut DrawState,
            Option<&PowerSystem>,
            Option<&ThrustVectorPitchEffector>,
            Option<&VtolAngleEffector>,
            Option<&AirbrakeEffector>,
            Option<&BayEffector>,
            Option<&FlapsEffector>,
            Option<&GearEffector>,
            Option<&HookEffector>,
        )>,
    ) {
        // TODO: swing wing state based upon flight model state

        for (mut draw_state, power, tv_pitch, vtol_angle, airbrake, bay, flaps, gear, hook) in
            query.iter_mut()
        {
            if let Some(power) = power {
                draw_state.set_afterburner(AfterBurnerState::for_power(power));
            }
            if let Some(tv_pitch) = tv_pitch {
                draw_state.set_thrust_vector_pitch(tv_pitch.angle::<Radians>());
            }
            if let Some(vtol_angle) = vtol_angle {
                draw_state.set_vtol_angle(vtol_angle.angle::<Radians>());
            }
            if let Some(airbrake) = airbrake {
                draw_state.set_airbrake(AirbrakeState::for_effector(airbrake));
            }
            if let Some(bay) = bay {
                draw_state.set_bay_state(BayState::for_effector(bay));
                draw_state.set_bay_position(bay.position() as f32);
            }
            if let Some(flaps) = flaps {
                draw_state.set_flaps(FlapState::for_effector(flaps));
                draw_state.set_slats(SlatsState::for_effector(flaps));
            }
            if let Some(gear) = gear {
                draw_state.set_gear_state(GearState::for_effector(gear));
                draw_state.set_gear_position(gear.position() as f32);
            }
            if let Some(hook) = hook {
                draw_state.set_hook(HookState::for_effector(hook));
            }
        }
    }

    fn sys_ts_apply_transforms(
        camera: Res<ScreenCamera>,
        mut query: Query<(&Frame, &ShapeScale, &mut ShapeTransformBuffer)>,
    ) {
        let view = camera.world_to_eye::<Meters>();
        query
            .par_iter_mut()
            .for_each(|(frame, scale, mut transform_buffer)| {
                // Models are uploaded (shared in the chunk) in model space. In order to present them
                // in the correct spot we need to know the eye-space location of the world-space frame.
                //
                // Precision matters here because the frame is in world space coords. We have to
                // do the transform in f64 to have reasonable precision.
                let pos_view = view.transform_point3(frame.position().dvec3()).as_vec3();
                transform_buffer.buffer[0] = pos_view.x;
                transform_buffer.buffer[1] = pos_view.y;
                transform_buffer.buffer[2] = pos_view.z;

                // We transformed from FA model space to native nitrogen (+Z forward, RH)
                // as we uploaded the coordinates initially. We still need to transform from
                // model space to the world space normals. Encode the rotation in the same way
                // as we encode the xform rotation.
                let facing = frame.facing().as_quat();
                transform_buffer.buffer[3] = facing.x;
                transform_buffer.buffer[4] = facing.y;
                transform_buffer.buffer[5] = facing.z;
                transform_buffer.buffer[6] = facing.w;

                transform_buffer.buffer[7] = scale.scale();
            });
    }

    fn sys_ts_build_flag_mask(
        step: Res<TimeStep>,
        mut query: Query<(&DrawState, &mut ShapeFlagBuffer)>,
    ) {
        let start = step.start_time();
        query
            .par_iter_mut()
            .for_each(|(draw_state, mut flag_buffer)| {
                flag_buffer
                    .buffer
                    .copy_from_slice(&VertexFlags::build_mask(draw_state, start).as_gpu());
            });
    }

    fn sys_ts_apply_xforms(
        step: Res<TimeStep>,
        mut query: Query<(&ShapeName, &DrawState, &mut ShXforms, &mut ShapeXformBuffer)>,
    ) {
        let start = step.start_time();
        let now = step.sim_time();
        assert!(now >= start);

        // Serial version for debugging
        // for (shape_name, draw_state, mut xforms, mut xform_buffer) in query.iter_mut() {
        //     println!("Name: {}", shape_name.name());
        //     xforms
        //         .run_xforms(draw_state, start, now, &mut xform_buffer.buffer)
        //         .unwrap();
        // }

        query
            .par_iter_mut()
            .for_each(|(name, draw_state, mut xforms, mut xform_buffer)| {
                // Draw state is updated with the current desired positions / input values
                // Run the x86 xform fragments to transform those inputs into xform data.
                if let Err(err) =
                    xforms.run_xforms(draw_state, start, now, &mut xform_buffer.buffer)
                {
                    warn!("ERR in {}: {err}", name.name());
                }
            });
    }

    fn sys_ts_push_values_to_block(
        mut shapes: ResMut<Shape>,
        gpu: Res<Gpu>,
        query: Query<(
            &ShapeId,
            &ShapeTransformBuffer,
            Option<&ShapeFlagBuffer>,
            Option<&ShapeXformBuffer>,
        )>,
    ) {
        shapes.push_values_to_block(&gpu, query);
    }

    fn push_values_to_block(
        &mut self,
        gpu: &Gpu,
        query: Query<(
            &ShapeId,
            &ShapeTransformBuffer,
            Option<&ShapeFlagBuffer>,
            Option<&ShapeXformBuffer>,
        )>,
    ) {
        for (shape_id, transform_buffer, flag_buffer, xform_buffer) in query.iter() {
            let flags = VertexFlags::base().as_gpu();
            let flags = if let Some(flags_ref) = flag_buffer {
                &flags_ref.buffer
            } else {
                &flags
            };
            self.chunk_man.chunk_mut(shape_id.chunk()).push_values(
                shape_id,
                &self.block_bind_group_layout,
                gpu,
                &transform_buffer.buffer,
                flags,
                &xform_buffer.map(|v| v.buffer),
            );
        }
    }

    fn sys_upload_block_frame_data(
        shapes: Res<Shape>,
        gpu: Res<Gpu>,
        mut maybe_encoder: ResMut<CurrentEncoder>,
    ) {
        if let Some(encoder) = maybe_encoder.get_encoder_mut() {
            shapes.chunk_man.upload_block_data(&gpu, encoder);
        }
    }

    pub fn get_texture_frames(&self, shape_id: ShapeId) -> &[(String, AtlasFrame)] {
        let chunk = self.chunk_man.chunk(self.chunk_man.shape_chunk(shape_id));
        let part = chunk.part(shape_id);
        part.frames()
    }

    pub fn get_vertices(&self, shape_id: ShapeId) -> &[ShapeVertex] {
        let chunk = self.chunk_man.chunk(self.chunk_man.shape_chunk(shape_id));
        chunk.vertices(shape_id)
    }

    pub fn get_vertices_mut(&mut self, shape_id: ShapeId) -> &mut [ShapeVertex] {
        let chunk = self
            .chunk_man
            .chunk_mut(self.chunk_man.shape_chunk(shape_id));
        chunk.vertices_mut(shape_id)
    }

    pub fn get_faces(&self, shape_id: ShapeId) -> &HashMap<Uuid, FaceInfo> {
        let chunk = self.chunk_man.chunk(self.chunk_man.shape_chunk(shape_id));
        chunk.faces(shape_id)
    }

    pub fn get_code(&self, shape_id: ShapeId) -> &ShCode {
        let chunk = self.chunk_man.chunk(self.chunk_man.shape_chunk(shape_id));
        chunk.code(shape_id)
    }

    pub fn get_code_mut(&mut self, shape_id: ShapeId) -> &mut ShCode {
        let chunk = self
            .chunk_man
            .chunk_mut(self.chunk_man.shape_chunk(shape_id));
        chunk.code_mut(shape_id)
    }

    fn sys_draw_shapes(
        shapes: Res<Shape>,
        camera: Res<ScreenCamera>,
        mut maybe_encoder: ResMut<CurrentEncoder>,
    ) {
        if let Some(encoder) = maybe_encoder.get_encoder_mut() {
            let mut rpass = camera.begin_render_pass("color", Some("depth"), false, encoder);
            rpass.set_pipeline(&shapes.pipeline);
            rpass.set_bind_group(0, camera.info_bind_group(), &[]);
            rpass.set_bind_group(1, &shapes.combined_group, &[]);

            for chunk in shapes.chunk_man.chunks() {
                rpass.set_bind_group(2, chunk.bind_group(), &[]);
                for block in chunk.blocks() {
                    rpass.set_bind_group(3, block.bind_group(), &[]);
                    rpass.set_vertex_buffer(0, chunk.vertex_buffer());
                    rpass.set_index_buffer(chunk.index_buffer(), wgpu::IndexFormat::Uint32);

                    // rpass.draw_indexed_indirect(block.command_buffer(), 0 as wgpu::BufferAddress);
                    for cmd in block.commands() {
                        rpass.draw_indexed(
                            cmd.base_index..cmd.base_index + cmd.index_count,
                            cmd.vertex_offset,
                            cmd.base_instance..cmd.base_instance + cmd.instance_count,
                        );
                    }
                }
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use absolute_unit::meters;
    use camera::ScreenCameraController;
    use catalog::{AssetCatalog, CatalogOpts, Search};
    use geodb::{GeoDb, GeoDbOpts};
    use installations::{Installations, LibsOpts};
    use mantle::{Core, CpuDetailLevel, GpuDetailLevel};
    use marker::Markers;
    use orrery::Orrery;
    use runtime::{StdPaths, StdPathsOpts};
    use stars::Stars;
    use terrain::{Terrain, TerrainOpts};
    use world::World;

    #[test]
    fn test_creation() -> Result<()> {
        let mut runtime = Core::for_test()?;
        runtime
            .load_extension_with::<StdPaths>(StdPathsOpts::new("openfa"))?
            .load_extension_with::<AssetCatalog>(CatalogOpts::default())?
            .load_extension_with::<Installations>(LibsOpts::for_testing())?
            .load_extension::<Orrery>()?
            .load_extension::<ScreenCamera>()?
            .load_extension_with::<GeoDb>(GeoDbOpts::default())?
            .load_extension::<Markers>()?
            .load_extension::<Atmosphere>()?
            .load_extension::<Shape>()?
            .load_extension::<Stars>()?
            .load_extension_with::<Terrain>(TerrainOpts::from_detail(
                CpuDetailLevel::Low,
                GpuDetailLevel::Low,
            ))?
            .load_extension::<World>()?;

        let skipped = vec![
            "CATGUY.SH",  // 640
            "MOON.SH",    // 41
            "SOLDIER.SH", // 320
            "CHAFF.SH",
            "CRATER.SH",
            "DEBRIS.SH",
            "EXP.SH",
            "FIRE.SH",
            "FLARE.SH",
            "SMOKE.SH",
            "WAVE1.SH",
            "WAVE2.SH",
            // Missing xform call args support
            "KRIV.SH",
            "KRIVAK.SH",
            "MOTH.SH",
            "MOTHB.SH",
            "QUE.SH",
            "SOVR.SH",
            "TEST2.SH",
        ];

        let (libs, installs) = Installations::for_testing()?;
        for info in libs.search(Search::for_extension("SH").with_glob("*_?.SH").must_match())? {
            // Note: damage shapes will get auto-loaded
            // Note: we're not dealing with shadow shapes yet
            if info.name().ends_with("_S.SH")
                || info.name().ends_with("_A.SH")
                || info.name().ends_with("_B.SH")
                || info.name().ends_with("_C.SH")
                || info.name().ends_with("_D.SH")
            {
                continue;
            }

            // FIXME: re-try all of these
            if skipped.contains(&info.name()) {
                println!("SKIP: {info}");
                continue;
            }

            let shape_ids = runtime.resource_scope(|heap, mut shapes: Mut<Shape>| {
                shapes.upload_shape(
                    installs.palette(info.collection_name())?,
                    info,
                    libs.collection(info.collection_name())?,
                    heap.resource::<Gpu>(),
                )
            })?;
            let (shape_bundle, flags_bundle, xforms_bundle) = runtime
                .resource::<Shape>()
                .shape_bundle(info.name(), shape_ids, None)?;
            let mut ent = runtime.spawn(&info.to_string())?;
            ent.inject(shape_bundle)?;
            if let Some(flags_bundle) = flags_bundle {
                ent.inject(flags_bundle)?;
            }
            if let Some(xforms_bundle) = xforms_bundle {
                ent.inject(xforms_bundle)?;
            }
        }

        // Crank frame to upload all the shapes we just loaded.
        let mut frame = Frame::default();
        let mut pos = frame.geodetic();
        *pos.asl_mut() += meters!(1);
        frame.set_position(pos.pt3());
        let _player_ent = runtime
            .spawn("player")?
            .inject(frame)?
            .inject(ScreenCameraController)?
            .id();
        runtime.run_sim_once();
        runtime.run_frame_once();

        Ok(())
    }
}

// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::{Meters, Pt3};
use anyhow::{anyhow, bail, Result};
use bitflags::bitflags;
use glam::{Vec3, Vec3A};
use memoffset::offset_of;
use sh::{
    fa_norm_to_vec, fa_pos_to_pt3, AfterBurnerState, AileronPosition, AirbrakeState, BayState,
    DrawState, EjectState, FlapState, GearState, HookState, PlayerState, RudderPosition,
    SlatsState, VertBufUsage, XformId,
};
use std::{mem, time::Instant};
use zerocopy::{AsBytes, FromBytes, FromZeroes, Ref};

const ANIMATION_FRAME_TIME: usize = 166; // ms

bitflags! {
    #[derive(Clone, Copy, Debug, Eq, PartialEq)]
    pub struct VertexFlags: u64 {
        const NONE                 = 0x0000_0000_0000_0000;

        // If set, manually blend the face color with the texture color.
        const BLEND_TEXTURE        = 0x0000_0000_0000_0001;

        // For each frame, the expected configuration of what we expect to be
        // drawn is set to true is set in PushConstants. Each vertex is tagged
        // with the part it is part of based on the reference in a controlling
        // script, if present.
        const STATIC               = 0x0000_0000_0000_0002;
        const AFTERBURNER_ON       = 0x0000_0000_0000_0004;
        const AFTERBURNER_OFF      = 0x0000_0000_0000_0008;
        const RIGHT_FLAP_DOWN      = 0x0000_0000_0000_0010;
        const RIGHT_FLAP_UP        = 0x0000_0000_0000_0020;
        const RIGHT_FLAP_HOVER     = 0x0800_0000_0000_0000;
        const RIGHT_FLAP_BRAKE     = 0x1000_0000_0000_0000;
        const LEFT_FLAP_DOWN       = 0x0000_0000_0000_0040;
        const LEFT_FLAP_UP         = 0x0000_0000_0000_0080;
        const LEFT_FLAP_HOVER      = 0x2000_0000_0000_0000;
        const LEFT_FLAP_BRAKE      = 0x4000_0000_0000_0000;
        const HOOK_EXTENDED        = 0x0000_0000_0000_0100;
        const HOOK_RETRACTED       = 0x0000_0000_0000_0200;
        const GEAR_UP              = 0x0000_0000_0000_0400;
        const GEAR_DOWN            = 0x0000_0000_0000_0800;
        const GEAR_STRUT           = 0x0200_0000_0000_0000;
        const BRAKE_EXTENDED       = 0x0000_0000_0000_1000;
        const BRAKE_RETRACTED      = 0x0000_0000_0000_2000;
        const BRAKE_INVERTED       = 0x0400_0000_0000_0000;
        const BAY_CLOSED           = 0x0000_0000_0000_4000;
        const BAY_OPEN             = 0x0000_0000_0000_8000;
        const RUDDER_CENTER        = 0x0000_0000_0001_0000;
        const RUDDER_LEFT          = 0x0000_0000_0002_0000;
        const RUDDER_RIGHT         = 0x0000_0000_0004_0000;
        const LEFT_AILERON_CENTER  = 0x0000_0000_0008_0000;
        const LEFT_AILERON_UP      = 0x0000_0000_0010_0000;
        const LEFT_AILERON_DOWN    = 0x0000_0000_0020_0000;
        const RIGHT_AILERON_CENTER = 0x0000_0000_0040_0000;
        const RIGHT_AILERON_UP     = 0x0000_0000_0080_0000;
        const RIGHT_AILERON_DOWN   = 0x0000_0000_0100_0000;
        const SLATS_DOWN           = 0x0000_0000_0200_0000;
        const SLATS_UP             = 0x0000_0000_0400_0000;

        const PLAYER_ALIVE         = 0x0000_0000_2000_0000;
        const PLAYER_DEAD          = 0x0000_0000_4000_0000;

        const ANIM_FRAME_0_2       = 0x0000_0001_0000_0000;
        const ANIM_FRAME_1_2       = 0x0000_0002_0000_0000;

        const ANIM_FRAME_0_3       = 0x0000_0004_0000_0000;
        const ANIM_FRAME_1_3       = 0x0000_0008_0000_0000;
        const ANIM_FRAME_2_3       = 0x0000_0010_0000_0000;

        const ANIM_FRAME_0_4       = 0x0000_0020_0000_0000;
        const ANIM_FRAME_1_4       = 0x0000_0040_0000_0000;
        const ANIM_FRAME_2_4       = 0x0000_0080_0000_0000;
        const ANIM_FRAME_3_4       = 0x0000_0100_0000_0000;

        const ANIM_FRAME_0_6       = 0x0000_0200_0000_0000;
        const ANIM_FRAME_1_6       = 0x0000_0400_0000_0000;
        const ANIM_FRAME_2_6       = 0x0000_0800_0000_0000;
        const ANIM_FRAME_3_6       = 0x0000_1000_0000_0000;
        const ANIM_FRAME_4_6       = 0x0000_2000_0000_0000;
        const ANIM_FRAME_5_6       = 0x0000_4000_0000_0000;

        const SAM_COUNT_0          = 0x0000_8000_0000_0000;
        const SAM_COUNT_1          = 0x0001_0000_0000_0000;
        const SAM_COUNT_2          = 0x0002_0000_0000_0000;
        const SAM_COUNT_3          = 0x0004_0000_0000_0000;

        const EJECT_STATE_0        = 0x0008_0000_0000_0000;
        const EJECT_STATE_1        = 0x0010_0000_0000_0000;
        const EJECT_STATE_2        = 0x0020_0000_0000_0000;
        const EJECT_STATE_3        = 0x0040_0000_0000_0000;
        const EJECT_STATE_4        = 0x0080_0000_0000_0000;

        const IS_VERTEX_NORMAL     = 0x0100_0000_0000_0000;

        const AILERONS_DOWN        = Self::LEFT_AILERON_DOWN.bits() | Self::RIGHT_AILERON_DOWN.bits();
        const AILERONS_UP          = Self::LEFT_AILERON_UP.bits() | Self::RIGHT_AILERON_UP.bits();
    }
}

impl VertexFlags {
    pub fn base() -> Self {
        Self::STATIC | Self::BLEND_TEXTURE
    }

    pub fn as_gpu(&self) -> [u32; 2] {
        let flags = self.bits();
        [(flags & 0xFFFF_FFFF) as u32, (flags >> 32) as u32]
    }

    pub fn from_usage(usage: VertBufUsage) -> Self {
        type V = VertBufUsage;
        match usage {
            V::Afterburner(AfterBurnerState::On) => Self::AFTERBURNER_ON,
            V::Afterburner(AfterBurnerState::Off) => Self::AFTERBURNER_OFF,
            V::AileronLeft(AileronPosition::Up) => Self::LEFT_AILERON_UP,
            V::AileronLeft(AileronPosition::Down) => Self::LEFT_AILERON_DOWN,
            V::AileronLeft(AileronPosition::Center) => Self::LEFT_AILERON_CENTER,
            V::AileronRight(AileronPosition::Up) => Self::RIGHT_AILERON_UP,
            V::AileronRight(AileronPosition::Down) => Self::RIGHT_AILERON_DOWN,
            V::AileronRight(AileronPosition::Center) => Self::RIGHT_AILERON_CENTER,
            V::Bay(BayState::Open) => Self::BAY_OPEN,
            V::Bay(BayState::Closed) => Self::BAY_CLOSED,
            V::Brake(AirbrakeState::Extended) => Self::BRAKE_EXTENDED,
            V::Brake(AirbrakeState::Retracted) => Self::BRAKE_RETRACTED,
            V::Brake(AirbrakeState::Inverted) => Self::BRAKE_INVERTED,
            V::Eject(EjectState::Unk11 | EjectState::Unk1A | EjectState::Unk22) => {
                Self::EJECT_STATE_0
            }
            V::Eject(EjectState::Unk12 | EjectState::Unk1B | EjectState::Unk23) => {
                Self::EJECT_STATE_1
            }
            V::Eject(EjectState::Unk13 | EjectState::Unk1C | EjectState::Unk24) => {
                Self::EJECT_STATE_2
            }
            V::Eject(EjectState::Unk14 | EjectState::Unk1D | EjectState::Unk25) => {
                Self::EJECT_STATE_3
            }
            V::Eject(EjectState::Unk15 | EjectState::Unk1E | EjectState::Unk26) => {
                Self::EJECT_STATE_4
            }
            V::FlapLeft(FlapState::Up) => Self::LEFT_FLAP_UP,
            V::FlapLeft(FlapState::Down) => Self::LEFT_FLAP_DOWN,
            V::FlapLeft(FlapState::Hover) => Self::LEFT_FLAP_HOVER,
            V::FlapLeft(FlapState::Brake) => Self::LEFT_FLAP_BRAKE,
            V::FlapRight(FlapState::Up) => Self::RIGHT_FLAP_UP,
            V::FlapRight(FlapState::Down) => Self::RIGHT_FLAP_DOWN,
            V::FlapRight(FlapState::Hover) => Self::RIGHT_FLAP_HOVER,
            V::FlapRight(FlapState::Brake) => Self::RIGHT_FLAP_BRAKE,
            V::Gear(GearState::Extended) => Self::GEAR_DOWN,
            V::Gear(GearState::Retracted) => Self::GEAR_UP,
            V::Gear(GearState::Strut) => Self::GEAR_STRUT,
            V::Hook(HookState::Extended) => Self::HOOK_EXTENDED,
            V::Hook(HookState::Retracted) => Self::HOOK_RETRACTED,
            V::PlayerDead(PlayerState::Dead) => Self::PLAYER_DEAD,
            V::PlayerDead(PlayerState::Alive) => Self::PLAYER_ALIVE,
            V::Rudder(RudderPosition::Left) => Self::RUDDER_LEFT,
            V::Rudder(RudderPosition::Center) => Self::RUDDER_CENTER,
            V::Rudder(RudderPosition::Right) => Self::RUDDER_RIGHT,
            V::SamCount(cnt) => {
                Self::from_bits(Self::SAM_COUNT_0.bits() << cnt.num()).expect("out of range num")
            }
            V::Slats(SlatsState::Retracted) => Self::SLATS_UP,
            V::Slats(SlatsState::Extended) => Self::SLATS_DOWN,
            V::Static => Self::STATIC,
        }
    }

    pub fn from_animation(frame_number: u8, num_frames: u8) -> Result<Self> {
        Ok(match num_frames {
            2 => Self::ANIM_FRAME_0_2,
            3 => Self::ANIM_FRAME_0_3,
            4 => Self::ANIM_FRAME_0_4,
            6 => Self::ANIM_FRAME_0_6,
            _ => bail!("unexpected num_frames: {num_frames}"),
        }
        .displacement(frame_number as usize))
    }

    // Panics if offset is out of range
    fn displacement(self, offset: usize) -> Self {
        VertexFlags::from_bits(self.bits() << offset).unwrap()
    }

    pub fn build_mask(state: &DrawState, start: &Instant) -> Self {
        let mut mask = VertexFlags::base();

        let elapsed = start.elapsed().as_millis() as usize;
        let frame_off = elapsed / ANIMATION_FRAME_TIME;
        mask |= VertexFlags::ANIM_FRAME_0_2.displacement(frame_off % 2);
        mask |= VertexFlags::ANIM_FRAME_0_3.displacement(frame_off % 3);
        mask |= VertexFlags::ANIM_FRAME_0_4.displacement(frame_off % 4);
        mask |= VertexFlags::ANIM_FRAME_0_6.displacement(frame_off % 6);

        mask |= match state.afterburner_state() {
            AfterBurnerState::Off => Self::AFTERBURNER_OFF,
            AfterBurnerState::On => Self::AFTERBURNER_ON,
        };

        mask |= match state.airbrake_state() {
            AirbrakeState::Retracted => Self::BRAKE_RETRACTED,
            AirbrakeState::Extended => Self::BRAKE_EXTENDED,
            AirbrakeState::Inverted => panic!("no bit for inverted airbrake"),
        };

        mask |= match state.bay_state() {
            BayState::Closed => VertexFlags::BAY_CLOSED,
            BayState::Open => VertexFlags::BAY_OPEN,
        };

        mask |= match state.flaps_state() {
            FlapState::Up => Self::LEFT_FLAP_UP | Self::RIGHT_FLAP_UP,
            FlapState::Down => Self::LEFT_FLAP_DOWN | Self::RIGHT_FLAP_DOWN,
            FlapState::Brake => panic!("no bit for flap brake"),
            FlapState::Hover => panic!("no bit for flap hover pos"),
        };

        mask |= match state.gear_state() {
            GearState::Retracted => VertexFlags::GEAR_UP,
            GearState::Extended => VertexFlags::GEAR_DOWN,
            GearState::Strut => VertexFlags::GEAR_STRUT,
        };

        mask |= match state.hook_state() {
            HookState::Retracted => Self::HOOK_RETRACTED,
            HookState::Extended => Self::HOOK_EXTENDED,
        };

        mask |= match state.slats_state() {
            SlatsState::Retracted => Self::SLATS_UP,
            SlatsState::Extended => Self::SLATS_DOWN,
        };

        // mask |= if state.rudder_right() {
        //     VertexFlags::RUDDER_RIGHT
        // } else if state.rudder_left() {
        //     VertexFlags::RUDDER_LEFT
        // } else {
        //     VertexFlags::RUDDER_CENTER
        // };
        //
        // mask |= if state.left_aileron_down() {
        //     VertexFlags::LEFT_AILERON_DOWN
        // } else if state.left_aileron_up() {
        //     // FIXME: handle divergent aileron caps
        //     // if state.errata.no_upper_aileron {
        //     //     VertexFlags::LEFT_AILERON_CENTER
        //     // } else {
        //     VertexFlags::LEFT_AILERON_UP
        //     // }
        // } else {
        //     VertexFlags::LEFT_AILERON_CENTER
        // };
        //
        // mask |= if state.right_aileron_down() {
        //     VertexFlags::RIGHT_AILERON_DOWN
        // } else if state.right_aileron_up() {
        //     // if state.errata.no_upper_aileron {
        //     //     VertexFlags::RIGHT_AILERON_CENTER
        //     // } else {
        //     VertexFlags::RIGHT_AILERON_UP
        //     // }
        // } else {
        //     VertexFlags::RIGHT_AILERON_CENTER
        // };
        //
        // mask |= match state.sam_count() {
        //     0 => VertexFlags::SAM_COUNT_0,
        //     1 => VertexFlags::SAM_COUNT_0 | VertexFlags::SAM_COUNT_1,
        //     2 => VertexFlags::SAM_COUNT_0 | VertexFlags::SAM_COUNT_1 | VertexFlags::SAM_COUNT_2,
        //     3 => {
        //         VertexFlags::SAM_COUNT_0
        //             | VertexFlags::SAM_COUNT_1
        //             | VertexFlags::SAM_COUNT_2
        //             | VertexFlags::SAM_COUNT_3
        //     }
        //     _ => panic!("expected sam count < 3"),
        // };
        //
        // mask |= match state.eject_state() {
        //     0 => VertexFlags::EJECT_STATE_0,
        //     1 => VertexFlags::EJECT_STATE_1,
        //     2 => VertexFlags::EJECT_STATE_2,
        //     3 => VertexFlags::EJECT_STATE_3,
        //     4 => VertexFlags::EJECT_STATE_4,
        //     _ => panic!("expected eject state in 0..4"),
        // };
        //
        // mask |= if state.player_dead() {
        //     VertexFlags::PLAYER_DEAD
        // } else {
        //     VertexFlags::PLAYER_ALIVE
        // };

        mask
    }
}

#[repr(C)]
#[derive(AsBytes, FromBytes, FromZeroes, Copy, Clone, Debug)]
pub struct ShapeVertex {
    position: [f32; 3],
    normal: [f32; 3],
    color: [f32; 4],
    tex_coord: [u32; 2],
    // pixel offset
    flags0: u32,
    flags1: u32,
    xform_id: u32,
}

impl ShapeVertex {
    #[allow(clippy::unneeded_field_pattern)]
    pub fn descriptor() -> wgpu::VertexBufferLayout<'static> {
        let tmp = wgpu::VertexBufferLayout {
            array_stride: mem::size_of::<Self>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &[
                // position
                wgpu::VertexAttribute {
                    format: wgpu::VertexFormat::Float32x3,
                    offset: 0,
                    shader_location: 0,
                },
                // normal
                wgpu::VertexAttribute {
                    format: wgpu::VertexFormat::Float32x3,
                    offset: 12,
                    shader_location: 1,
                },
                // color
                wgpu::VertexAttribute {
                    format: wgpu::VertexFormat::Float32x4,
                    offset: 24,
                    shader_location: 2,
                },
                // tex_coord
                wgpu::VertexAttribute {
                    format: wgpu::VertexFormat::Uint32x2,
                    offset: 40,
                    shader_location: 3,
                },
                // flags0
                wgpu::VertexAttribute {
                    format: wgpu::VertexFormat::Uint32,
                    offset: 48,
                    shader_location: 4,
                },
                // flags1
                wgpu::VertexAttribute {
                    format: wgpu::VertexFormat::Uint32,
                    offset: 52,
                    shader_location: 5,
                },
                // xform_id
                wgpu::VertexAttribute {
                    format: wgpu::VertexFormat::Uint32,
                    offset: 56,
                    shader_location: 6,
                },
            ],
        };

        assert_eq!(
            tmp.attributes[0].offset,
            offset_of!(ShapeVertex, position) as wgpu::BufferAddress
        );
        assert_eq!(
            tmp.attributes[1].offset,
            offset_of!(ShapeVertex, normal) as wgpu::BufferAddress
        );
        assert_eq!(
            tmp.attributes[2].offset,
            offset_of!(ShapeVertex, color) as wgpu::BufferAddress
        );
        assert_eq!(
            tmp.attributes[3].offset,
            offset_of!(ShapeVertex, tex_coord) as wgpu::BufferAddress
        );
        assert_eq!(
            tmp.attributes[4].offset,
            offset_of!(ShapeVertex, flags0) as wgpu::BufferAddress
        );
        assert_eq!(
            tmp.attributes[5].offset,
            offset_of!(ShapeVertex, flags1) as wgpu::BufferAddress
        );
        assert_eq!(
            tmp.attributes[6].offset,
            offset_of!(ShapeVertex, xform_id) as wgpu::BufferAddress
        );

        tmp
    }

    pub(crate) fn new(position: &[i16; 3], usage: VertBufUsage, xform_id: XformId) -> Self {
        let flags = VertexFlags::from_usage(usage).as_gpu();
        Self {
            // Color and Tex Coords will be filled out by the
            // face when we move this into the verts list.
            color: [0.75f32, 0.5f32, 0f32, 1f32],
            tex_coord: [0u32; 2],
            // Normal may be a vertex normal or face normal, depending.
            // It will also be filled out as we discover more.
            normal: [0f32; 3],
            // Base position, flags, and the xform are constant
            // for this entire buffer, independent of the face.
            position: fa_pos_to_pt3(position).dvec3().as_vec3().to_array(),
            flags0: flags[0],
            flags1: flags[1],
            // FIXME:
            // xform_id: props.xform_id,
            xform_id: xform_id.as_gpu(),
        }
    }

    pub fn set_fa_position(&mut self, position: &[i16; 3]) {
        self.position = fa_pos_to_pt3(position).dvec3().as_vec3().to_array();
    }

    pub fn overlay_slice(buf: &[u8]) -> Result<&[Self]> {
        Ref::<&[u8], [Self]>::new_slice(buf)
            .map(|v| v.into_slice())
            .ok_or_else(|| anyhow!("cannot overlay slice"))
    }

    pub fn set_color(&mut self, color: [f32; 4]) {
        self.color = color;
    }

    pub fn set_raw_tex_coords(&mut self, s: u32, t: u32) {
        self.tex_coord = [s, t];
    }

    pub fn flags(&self) -> VertexFlags {
        VertexFlags::from_bits(self.flags0 as u64 | (self.flags1 as u64) << 32).unwrap()
    }

    pub fn set_flags(&mut self, flags: VertexFlags) {
        self.flags0 = (flags.bits() & 0xFFFF_FFFF) as u32;
        self.flags1 = (flags.bits() >> 32) as u32;
    }

    pub fn set_is_blend_texture(&mut self) {
        self.flags0 |= (VertexFlags::BLEND_TEXTURE.bits() & 0xFFFF_FFFF) as u32;
    }

    pub fn unset_is_blend_texture(&mut self) {
        self.flags0 &= (!VertexFlags::BLEND_TEXTURE.bits() & 0xFFFF_FFFF) as u32;
    }

    pub fn set_is_vertex_normal(&mut self) {
        self.flags1 |= (VertexFlags::IS_VERTEX_NORMAL.bits() >> 32) as u32;
    }

    pub fn position(&self) -> Pt3<Meters> {
        Pt3::new_dvec3(Vec3::from_array(self.position).as_dvec3())
    }

    pub fn set_position(&mut self, pt: Pt3<Meters>) {
        self.position = pt.dvec3().as_vec3().to_array();
    }

    pub fn normal(&self) -> Vec3A {
        Vec3A::from_array(self.normal)
    }

    pub fn set_computed_normal(&mut self, normal: Vec3) {
        self.normal = normal.to_array();
    }

    pub fn set_fa_face_normal(&mut self, normal: &[i8; 3]) {
        self.normal = fa_norm_to_vec(normal).to_array()
    }

    pub fn set_fa_vert_normal(&mut self, normal: &[i8; 3]) {
        self.normal = fa_norm_to_vec(normal).to_array()
    }

    pub fn is_vertex_normal(&self) -> bool {
        self.flags1 & (VertexFlags::IS_VERTEX_NORMAL.bits() >> 32) as u32 != 0
    }
}

impl Default for ShapeVertex {
    fn default() -> Self {
        Self {
            position: [0f32; 3],
            normal: [0f32; 3],
            color: [0.75f32, 0.5f32, 0f32, 1f32],
            tex_coord: [0u32; 2],
            flags0: 0,
            flags1: 0,
            xform_id: 0,
        }
    }
}

// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::chunk::shape_vertex::{ShapeVertex, VertexFlags};
use absolute_unit::prelude::*;
use anyhow::{bail, ensure, Result};
use atlas::{AtlasPacker, Frame as AtlasFrame};
use catalog::FileSystem;
use geometry::algorithm::compute_normal;
use glam::{DMat4, Vec3};
use image::Rgba;
use log::{trace, warn};
use mantle::Gpu;
use pal::Palette;
use phase::Frame;
use pic::Pic;
use pic_uploader::PicUploader;
use sh::{
    i82_VertexBuffer, iFC_Face, DrawSelection, FaceContentFlags, FaceUsage, InstrDetail, ShCode,
    ShInstr,
};
use std::{collections::HashMap, ops::Range};
use uuid::Uuid;

// #[derive(Copy, Clone, Debug, Default, Eq, PartialEq)]
// pub struct ShapeErrata {
//     pub no_upper_aileron: bool,
// }
// impl ShapeErrata {
//     fn from_flags(analysis: &AnalysisResults) -> Self {
//         let flags = analysis.prop_man.seen_flags;
//         Self {
//             // ERRATA: ATFNATO:F22.SH is missing aileron up meshes.
//             no_upper_aileron: !(flags & VertexFlags::AILERONS_DOWN).is_empty()
//                 && (flags & VertexFlags::AILERONS_UP).is_empty(),
//         }
//     }
// }

// In FA, there seems to be a fixed size vertex staging area that vxbuf commands put
// vertices into so that faces can index into them. Sometimes vxbuf commands append,
// other times they overwrite from the beginning, other times they plop vertices past
// the end or randomly in the middle. In order to determine which vxbuf a read comes
// from we need a map of what pushes have happened. We need this to figure out what
// vxbuf created a face so that we can find the right xform and know the actual source
// position in the file to update.
#[derive(Clone, Debug)]
pub struct VxBufStackEntry {
    uuid: Uuid,
    range: Range<usize>,
}

pub struct ShapeExtractor<'a, Target: ExtractTarget> {
    name: &'a str,
    palette: &'a Palette,
    vert_pool: Vec<ShapeVertex>,
    vxbuf_stack: Vec<VxBufStackEntry>,
    active_frame: Option<AtlasFrame>,
    target: &'a mut Target,
}

impl<'a, Target: ExtractTarget> ShapeExtractor<'a, Target> {
    pub fn new(name: &'a str, palette: &'a Palette, target: &'a mut Target) -> Self {
        Self {
            name,
            palette,
            vert_pool: Vec::new(),
            vxbuf_stack: Vec::new(),
            active_frame: None,
            target,
        }
    }

    fn load_vertex_buffer(&mut self, vert_buf: &i82_VertexBuffer) {
        // Emulate a fixed size vertex by creating an arbitrary size pool
        self.vert_pool
            .resize(vert_buf.pool_range().end, Default::default());

        self.vert_pool[vert_buf.pool_range()].copy_from_slice(
            &vert_buf
                .vertices()
                .map(|v| ShapeVertex::new(v, *vert_buf.usage(), vert_buf.xform_id()))
                .collect::<Vec<_>>(),
        );

        trace!(
            "Loaded VxBuf {} with usage: {:?}",
            vert_buf.vertices().count(),
            vert_buf.usage()
        );
    }

    fn find_vxbuf_at_offset(&self, offset: usize) -> Result<&VxBufStackEntry> {
        for entry in &self.vxbuf_stack {
            if entry.range.contains(&offset) {
                return Ok(entry);
            }
        }
        bail!("read of uninitialized vertex at offset {offset}");
    }

    fn draw_facet_vertex(
        &mut self,
        facet_vertex_offset: usize,
        computed_normal: Vec3,
        face: &iFC_Face,
    ) -> Result<()> {
        let index = face.index(facet_vertex_offset);
        if index >= self.vert_pool.len() {
            bail!(
                "Face tried to read past end of vertex buffer {} of {}",
                index,
                self.vert_pool.len(),
            );
        }
        let mut v = self.vert_pool[index];
        let vxbuf_entry = self.find_vxbuf_at_offset(index)?.to_owned();

        if let FaceUsage::Animation {
            frame_number,
            num_frames,
        } = face.usage()
        {
            v.set_flags(VertexFlags::from_animation(frame_number, num_frames)?);
        }
        // Set normal if not set by vertex normals
        if !v.is_vertex_normal() {
            if let Some(norm) = face.normal() {
                v.set_fa_face_normal(norm);
            } else {
                // Rare, but plenty of shapes fall through to this block.
                v.set_computed_normal(computed_normal);
            }
        }
        v.set_color(self.palette.rgba_f32(face.color() as usize)?);

        if face
            .content_flags()
            .contains(FaceContentFlags::FILL_BACKGROUND)
            || face.content_flags().contains(FaceContentFlags::UNK1)
            || face.content_flags().contains(FaceContentFlags::UNK5)
        {
            v.set_is_blend_texture();
        }

        if face
            .content_flags()
            .contains(FaceContentFlags::HAVE_TEXCOORDS)
        {
            ensure!(
                self.active_frame.is_some(),
                "no frame active at facet with texcoords defined"
            );
            let frame = self.active_frame.as_ref().expect("active frame");
            let (base_s, base_t) = frame.raw_base();
            let tex_coord = face.tex_coord(facet_vertex_offset);
            v.set_raw_tex_coords(
                base_s + tex_coord[0] as u32,
                base_t.saturating_sub(tex_coord[1] as u32),
            );
        }

        self.target
            .handle_vertex(&vxbuf_entry, facet_vertex_offset, v, face)?;

        Ok(())
    }

    fn draw_facet(&mut self, face: &iFC_Face) -> Result<()> {
        // Compute face normal once
        let p0 = &self.vert_pool[face.index(0)].position();
        let p1 = &self.vert_pool[face.index(1)].position();
        let p2 = &self.vert_pool[face.index(2)].position();
        let computed_normal = compute_normal(p0, p1, p2);

        // Upload each vertex in the face loop once
        for i in 0..face.indices().len() {
            self.draw_facet_vertex(i, computed_normal.as_vec3(), face)?;
        }

        // Compute vertex indices of triangles;
        // At the moment we are using a fan off of the first vertex. Is there a better way?
        for i in 2..face.indices().len() {
            let ids_rel_facet = [0, i - 1, i];
            self.target.handle_triangle(&ids_rel_facet)?;
        }
        Ok(())
    }

    pub fn draw_model(&mut self, sh: &ShCode, selection: &DrawSelection) -> Result<()> {
        trace!("ShapeUploader::draw_model: {}", self.name);
        for instr in sh.iter_draw_order(*selection) {
            match instr.detail() {
                InstrDetail::TextureFile(texture) => {
                    let filename = texture.filename().to_uppercase();
                    self.active_frame = Some(self.target.handle_texture(&filename)?);
                }

                InstrDetail::TextureIndex(_texture) => {
                    // println!("TEX: {:#?}", texture);
                }

                InstrDetail::VertexBuffer(vert_buf) => {
                    self.vxbuf_stack.push(VxBufStackEntry {
                        uuid: *instr.uuid(),
                        range: vert_buf.pool_range(),
                    });
                    self.load_vertex_buffer(vert_buf);
                }

                InstrDetail::VertexInfo(info) => {
                    self.vert_pool[info.index()].set_is_vertex_normal();
                    self.vert_pool[info.index()].set_fa_vert_normal(info.normal());
                }

                InstrDetail::Face(face) => {
                    self.target.begin_face(instr, face);
                    self.draw_facet(face)?;
                    self.target.end_face(instr, face);
                }

                InstrDetail::JumpToFrame(frameset) => {
                    // Note: faces after first from jump-to-frame are jumped past
                    for frame_ptr in frameset.frame_pointers().skip(1) {
                        let face_instr = sh.ptr_to_instr(frame_ptr)?;
                        let InstrDetail::Face(face) = face_instr.detail() else {
                            bail!("anim frame pointers must point to faces");
                        };
                        self.target.begin_face(face_instr, face);
                        self.draw_facet(face)?;
                        self.target.end_face(face_instr, face);
                    }
                }

                _ => {}
            }
        }
        Ok(())
    }
}

#[derive(Clone, Debug, Default)]
pub struct FaceInfo {
    // The vertex buffer index of each vertex in the face loop, one point point on the loop.
    face_loop: Vec<usize>,

    // One set of three indices for each triangle that comprises the face, when rendered.
    // Note that this is an artifact of uploading to the GPU and not part of the base games.
    // As such, these indices are into the face loop and should be in-directed to get the
    // index into the vertex buffer.
    tri_list: Vec<[usize; 3]>,

    // The vxbuf from which the vertices came from
    vxbuf_uuid: Uuid,

    // The load base of the vxbuf. If a face index refers to offset N, that is into the
    // full vertex pool, which may have been loaded from the VxBuf at some offset M. To
    // get the vert offset into the vxbuf instruction data, we need to take `N - M`.
    vxbuf_load_base: usize,
}

impl FaceInfo {
    pub fn tri_list(&self) -> &[[usize; 3]] {
        &self.tri_list
    }

    pub fn face_loop(&self) -> &[usize] {
        &self.face_loop
    }

    pub fn vxbuf_uuid(&self) -> &Uuid {
        &self.vxbuf_uuid
    }

    pub fn vxbuf_offset(&self, loop_offset: usize, face: &iFC_Face) -> usize {
        let pool_offset = face.index(loop_offset);
        pool_offset
            .checked_sub(self.vxbuf_load_base)
            .expect("pool address must be larger than the load base")
    }

    pub fn triangles<'a, 'b>(
        &'a self,
        verts: &'b [ShapeVertex],
        frame: &'b Frame,
        xform: &'b DMat4,
    ) -> impl Iterator<Item = [Pt3<Meters>; 3]> + 'a
    where
        'b: 'a,
    {
        let p = frame.position();
        let q = frame.facing();
        self.tri_list.iter().map(move |tri| {
            let pt0 = verts[self.face_loop[tri[0]]].position();
            let pt1 = verts[self.face_loop[tri[1]]].position();
            let pt2 = verts[self.face_loop[tri[2]]].position();
            [
                p + q * pt0.transform_by(xform),
                p + q * pt1.transform_by(xform),
                p + q * pt2.transform_by(xform),
            ]
        })
    }
}

pub trait ExtractTarget {
    fn handle_texture(&mut self, filename: &str) -> Result<AtlasFrame>;
    fn begin_face(&mut self, _instr: &ShInstr, _face: &iFC_Face) {}
    fn handle_vertex(
        &mut self,
        vxbuf: &VxBufStackEntry,
        facet_vertex_offset: usize,
        vertex: ShapeVertex,
        face: &iFC_Face,
    ) -> Result<()>;
    fn handle_triangle(&mut self, _face_indices: &[usize; 3]) -> Result<()>;
    fn end_face(&mut self, _instr: &ShInstr, _face: &iFC_Face) {}
}

pub struct GpuUploader<'a> {
    // Pixmap upload
    fs: &'a dyn FileSystem,
    pic_uploader: &'a mut PicUploader,
    atlas_packer: &'a mut AtlasPacker<Rgba<u8>>,
    gpu: &'a Gpu,
    loaded_frames: HashMap<String, AtlasFrame>,

    // Geometry upload
    base_vertex_index: usize,
    vertices: Vec<ShapeVertex>,
    indices: Vec<u32>,

    // What do we need to map from clicks in a UX to vertices to update?
    //   1) The list of faces for hit detection.
    //   2) A map from those faces to the vertices each face uploaded.
    //   3) (nonfunctional) The ShCode that was used for the upload so that we
    //      only need to store the Uuid of the face and can look up the actual values
    //      easily, because any editing we do is going to need to flow back there too.
    current_face: Option<Uuid>,
    face_map: HashMap<Uuid, FaceInfo>,
}

impl<'a> GpuUploader<'a> {
    pub fn new(
        fs: &'a dyn FileSystem,
        pic_uploader: &'a mut PicUploader,
        atlas_packer: &'a mut AtlasPacker<Rgba<u8>>,
        gpu: &'a Gpu,
    ) -> Self {
        Self {
            fs,
            pic_uploader,
            atlas_packer,
            gpu,
            loaded_frames: HashMap::new(),
            base_vertex_index: 0,
            vertices: Vec::new(),
            indices: Vec::new(),
            current_face: None,
            face_map: HashMap::new(),
        }
    }

    pub fn finish(
        self,
    ) -> (
        Vec<(String, AtlasFrame)>,
        Vec<ShapeVertex>,
        Vec<u32>,
        HashMap<Uuid, FaceInfo>,
    ) {
        (
            self.loaded_frames.into_iter().collect::<Vec<_>>(),
            self.vertices,
            self.indices,
            self.face_map,
        )
    }
}

impl<'a> ExtractTarget for GpuUploader<'a> {
    fn handle_texture(&mut self, filename: &str) -> Result<AtlasFrame> {
        Ok(if let Some(frame) = self.loaded_frames.get(filename) {
            *frame
        } else {
            let info = self.fs.lookup(filename)?;
            let data = info.data()?;
            let pic = Pic::from_bytes(&data)?;
            let (buffer, w, h, stride) =
                self.pic_uploader
                    .upload(&pic, self.gpu, wgpu::BufferUsages::COPY_SRC)?;
            let frame = self.atlas_packer.push_buffer(buffer, w, h, stride)?;
            self.loaded_frames.insert(filename.to_string(), frame);
            frame
        })
    }

    fn begin_face(&mut self, instr: &ShInstr, _face: &iFC_Face) {
        assert!(!self.face_map.contains_key(instr.uuid()));

        // Store the base vertex index before we upload the loop.
        self.base_vertex_index = self.vertices.len();

        // Track the uuid of the face so we can associate it later
        self.current_face = Some(*instr.uuid());
        self.face_map.insert(*instr.uuid(), FaceInfo::default());
    }

    fn handle_triangle(&mut self, loop_indices: &[usize; 3]) -> Result<()> {
        // Extend the index buffer with each triangle, relocating into the vertex buffer.
        for facet_rel in loop_indices {
            self.indices
                .push((self.base_vertex_index + *facet_rel).try_into()?);
        }

        let info = self
            .face_map
            .get_mut(&self.current_face.expect("call order"))
            .expect("call order");
        info.tri_list.push(*loop_indices);

        Ok(())
    }

    fn handle_vertex(
        &mut self,
        vxbuf: &VxBufStackEntry,
        facet_vertex_offset: usize,
        vertex: ShapeVertex,
        _face: &iFC_Face,
    ) -> Result<()> {
        // Capture the current index of the vertex: e.g. where it will be pushed to (0, 1, 2, ...)
        let current_index = self.vertices.len();

        let info = self
            .face_map
            .get_mut(&self.current_face.expect("call order"))
            .expect("call order");

        // Capture the index into the face loop, if not already captured
        assert_eq!(info.face_loop.len(), facet_vertex_offset);
        info.face_loop.push(current_index);

        // Note that the vxbuf_uuid is best effort. If a face crosses vxbuf (as it appears to be
        // the case in EJECT.SH), this will capture the _last_ vxbuf.
        if info.vxbuf_uuid != Uuid::default() && info.vxbuf_uuid != vxbuf.uuid {
            warn!("discovered a face that crosses vertex buffers");
        }
        info.vxbuf_uuid = vxbuf.uuid;
        info.vxbuf_load_base = vxbuf.range.start;

        self.vertices.push(vertex);

        Ok(())
    }

    fn end_face(&mut self, _instr: &ShInstr, _face: &iFC_Face) {
        self.current_face = None;
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_vertex_offsets() {
        let _ = ShapeVertex::descriptor();
    }
}

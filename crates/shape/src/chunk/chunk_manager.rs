// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::chunk::chunks::{ChunkFlags, ChunkId, OpenChunk, ShapeId};
use anyhow::{Context, Result};
use catalog::FileSystem;
use mantle::Gpu;
use pal::Palette;
use pic_uploader::PicUploader;
use sh::{DrawSelection, ShCode};
use std::{collections::HashMap, mem, num::NonZeroU64};
use zerocopy::{AsBytes, FromBytes, FromZeroes};

#[repr(C)]
#[derive(AsBytes, FromBytes, FromZeroes, Copy, Clone, Debug)]
pub struct TextureAtlasProperties {
    width: u32,
    height: u32,
    pad: [u32; 2],
}

impl TextureAtlasProperties {
    pub fn new(width: u32, height: u32) -> Self {
        Self {
            width,
            height,
            pad: [0; 2],
        }
    }
}

#[derive(Debug)]
pub(crate) struct ChunkManager {
    chunk_bind_group_layout: wgpu::BindGroupLayout,
    shared_sampler: wgpu::Sampler,

    name_to_shape_map: HashMap<String, ShapeId>,
    shape_to_chunk_map: HashMap<ShapeId, ChunkId>,

    pic_uploader: PicUploader,
    // TODO: does this need to be a vec of OpenChunk? Where do we actually stop having "enough" room.
    // TODO: why do we need to sort by chunk flags? Is that legacy since we have an ECS now?
    open_chunks: HashMap<ChunkFlags, OpenChunk>,
}

impl ChunkManager {
    pub fn new(gpu: &Gpu) -> Result<Self> {
        let chunk_bind_group_layout =
            gpu.device()
                .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                    label: Some("shape-chunk-bind-group-layout"),
                    entries: &[
                        // FIXME: use layout entries from the atlas?
                        // Texture Atlas
                        wgpu::BindGroupLayoutEntry {
                            binding: 0,
                            visibility: wgpu::ShaderStages::FRAGMENT,
                            ty: wgpu::BindingType::Texture {
                                multisampled: false,
                                sample_type: wgpu::TextureSampleType::Float { filterable: true },
                                view_dimension: wgpu::TextureViewDimension::D2,
                            },
                            count: None,
                        },
                        // Texture Atlas Sampler
                        wgpu::BindGroupLayoutEntry {
                            binding: 1,
                            visibility: wgpu::ShaderStages::FRAGMENT,
                            ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
                            count: None,
                        },
                        // Texture Atlas Properties
                        wgpu::BindGroupLayoutEntry {
                            binding: 2,
                            visibility: wgpu::ShaderStages::VERTEX,
                            ty: wgpu::BindingType::Buffer {
                                ty: wgpu::BufferBindingType::Uniform,
                                has_dynamic_offset: false,
                                min_binding_size: NonZeroU64::new(mem::size_of::<
                                    TextureAtlasProperties,
                                >(
                                )
                                    as u64),
                            },
                            count: None,
                        },
                    ],
                });
        let shared_sampler = gpu.device().create_sampler(&wgpu::SamplerDescriptor {
            label: Some("shape-chunk-atlas-sampler"),
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Nearest,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            lod_min_clamp: 0f32,
            lod_max_clamp: 9_999_999f32,
            anisotropy_clamp: 1,
            compare: None,
            border_color: None,
        });
        Ok(Self {
            chunk_bind_group_layout,
            shared_sampler,
            name_to_shape_map: HashMap::new(),
            shape_to_chunk_map: HashMap::new(),
            pic_uploader: PicUploader::new(gpu)?,
            open_chunks: HashMap::new(),
        })
    }

    pub(crate) fn close_open_chunks(&mut self, gpu: &Gpu, encoder: &mut wgpu::CommandEncoder) {
        //   ScriptRun phase causes various chunk.upload_shape calls
        //     pic_uploader.upload -> creates a buffer, but not filled yet
        //     atlas.upload_buffer(unfilled buffer) into the blit_list of the atlas
        //   Render Phase:
        //     PicUploader::expand_pics fills out the buffers we pushed into the blit list above
        //     AtlasPacker::encode_frame_uploads copies and/or blits those buffers into the atlas
        // Upshot: we need the open chunks to stay live through the frame
        self.pic_uploader.expand_pics(encoder);
        for open_chunk in self.open_chunks.values_mut() {
            assert!(
                !open_chunk.chunk_is_empty(),
                "opened a chunk that didn't get written to"
            );
            open_chunk.begin_frame(
                &self.chunk_bind_group_layout,
                &self.shared_sampler,
                gpu,
                encoder,
            );
        }
        self.pic_uploader.finish_expand_pass();
    }

    pub(crate) fn reset_upload_cursor(&mut self) {
        for chunk in self.open_chunks.values_mut() {
            chunk.reset_upload_cursor();
        }
    }

    pub fn push_one_shape(
        &mut self,
        palette: &Palette,
        shape_file_name: &str, // for caching and tracing
        sh_code: &ShCode,
        selection: DrawSelection,
        fs: &dyn FileSystem,
        gpu: &Gpu,
    ) -> Result<ShapeId> {
        let cache_key = format!("{}:{:?}", shape_file_name, selection);
        if let Some(&shape_id) = self.name_to_shape_map.get(&cache_key) {
            // Note: if we are cached, we would already have loaded the damage model
            return Ok(shape_id);
        }

        // NOTE: The analysis should always detect the damage model, even when visiting
        //       normal model instructions: we have to jump the other direction after all.
        //       This let's us save a costly re-analysis if there's no model there anyway.
        let chunk_flags = ChunkFlags::for_analysis(sh_code.analysis(selection)?);
        let chunk = self.open_chunks.entry(chunk_flags).or_insert_with(|| {
            OpenChunk::new(
                chunk_flags,
                &self.chunk_bind_group_layout,
                &self.shared_sampler,
                gpu,
            )
        });
        let shape_id = chunk.upload_shape(
            shape_file_name,
            sh_code,
            &selection,
            palette,
            fs,
            &mut self.pic_uploader,
            gpu,
        )?;

        self.name_to_shape_map.insert(cache_key, shape_id);
        self.shape_to_chunk_map.insert(shape_id, chunk.chunk_id());
        Ok(shape_id)
    }

    pub fn upload_shape(
        &mut self,
        shape_file_name: &str,
        sh_code: &ShCode,
        palette: &Palette,
        fs: &dyn FileSystem,
        gpu: &Gpu,
    ) -> Result<(ShapeId, Option<ShapeId>)> {
        // FIXME: check that this is not overwritten if we upload with more than 1 palette per frame
        self.pic_uploader.set_shared_palette(palette, gpu);

        let normal_shape_id = self
            .push_one_shape(
                palette,
                shape_file_name,
                sh_code,
                DrawSelection::NormalModel,
                fs,
                gpu,
            )
            .with_context(|| format!("normal shape file {shape_file_name}"))?;

        let damage_shape_id = if sh_code.has_damage_model() {
            Some(
                self.push_one_shape(
                    palette,
                    shape_file_name,
                    sh_code,
                    DrawSelection::DamageModel,
                    fs,
                    gpu,
                )
                .with_context(|| format!("damage shape file {shape_file_name}"))?,
            )
        } else {
            None
        };

        Ok((normal_shape_id, damage_shape_id))
    }

    // pub fn upload_shapes(
    //     &mut self,
    //     palette: &Palette,
    //     shapes: &HashMap<String, ShCode>,
    //     catalog: &dyn FileSystem,
    //     gpu: &Gpu,
    // ) -> Result<HashMap<String, HashMap<DrawSelection, ShapeId>>> {
    //     self.pic_uploader.set_shared_palette(palette, gpu);
    //
    //     let mut out = HashMap::new();
    //     for (shape_file_name, sh) in shapes.iter() {
    //         out.insert(shape_file_name.to_owned(), HashMap::new());
    //
    //         let normal_shape_id = self
    //             .push_one_shape(
    //                 palette,
    //                 shape_file_name,
    //                 sh,
    //                 DrawSelection::NormalModel,
    //                 catalog,
    //                 gpu,
    //             )
    //             .with_context(|| format!("normal shape file {shape_file_name}"))?;
    //         out.get_mut(shape_file_name)
    //             .unwrap()
    //             .insert(DrawSelection::NormalModel, normal_shape_id);
    //
    //         if sh.has_damage_model() {
    //             let damage_shape_id = self
    //                 .push_one_shape(
    //                     palette,
    //                     shape_file_name,
    //                     sh,
    //                     DrawSelection::DamageModel,
    //                     catalog,
    //                     gpu,
    //                 )
    //                 .with_context(|| format!("damage shape file {shape_file_name}"))?;
    //             out.get_mut(shape_file_name)
    //                 .unwrap()
    //                 .insert(DrawSelection::DamageModel, damage_shape_id);
    //         }
    //     }
    //
    //     Ok(out)
    // }

    pub(crate) fn upload_block_data(&self, gpu: &Gpu, encoder: &mut wgpu::CommandEncoder) {
        for chunk in self.open_chunks.values() {
            chunk.upload_block_data(gpu, encoder);
        }
    }

    pub fn shape_chunk(&self, shape_id: ShapeId) -> ChunkId {
        self.shape_to_chunk_map[&shape_id]
    }

    pub(crate) fn chunks(&self) -> impl Iterator<Item = &OpenChunk> {
        self.open_chunks.values()
    }

    // NOTE: The chunk must be closed.
    pub fn chunk(&self, chunk_id: ChunkId) -> &OpenChunk {
        // FIXME: organize by chunk_id
        for chunk in self.open_chunks.values() {
            if chunk_id == chunk.chunk_id() {
                return chunk;
            }
        }
        panic!("no such chunk")
    }

    pub fn chunk_mut(&mut self, chunk_id: ChunkId) -> &mut OpenChunk {
        // FIXME: organize by chunk_id
        for chunk in self.open_chunks.values_mut() {
            if chunk_id == chunk.chunk_id() {
                return chunk;
            }
        }
        panic!("no such chunk")
    }

    pub fn bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        &self.chunk_bind_group_layout
    }
}

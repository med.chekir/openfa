// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
mod chunk_manager;
mod chunks;
mod shape_vertex;
pub(crate) mod upload;

pub(crate) use chunk_manager::ChunkManager;
pub use chunks::{ShapeId, ShapeIds, ShapeName};
pub use shape_vertex::{ShapeVertex, VertexFlags};

#[cfg(test)]
mod test {
    use super::*;
    use anyhow::Result;
    use catalog::{FileSystem, Order, Search};
    use installations::Installations;
    use mantle::{Core, Gpu};
    use sh::ShCode;

    fn upload_all_sh(section: usize, total_sections: usize) -> Result<()> {
        let skipped = vec![
            "CATGUY.SH",  // 640
            "MOON.SH",    // 41
            "SOLDIER.SH", // 320
            "CHAFF.SH",
            "CRATER.SH",
            "DEBRIS.SH",
            "EXP.SH",
            "FIRE.SH",
            "FLARE.SH",
            "MOTHB.SH",
            "SMOKE.SH",
            "WAVE1.SH",
            "WAVE2.SH",
        ];

        let runtime = Core::for_test()?;
        let mut chunk_man = ChunkManager::new(runtime.resource::<Gpu>())?;

        let (libs, installs) = Installations::for_testing()?;

        let all_files = libs.search(Search::for_extension("SH").sort(Order::Asc).must_match())?;
        let files = nitrous::segment_tests(section, total_sections, &all_files);

        for info in files {
            if skipped.contains(&info.name()) {
                continue;
            }

            println!("At: {info}");
            let sh_code = ShCode::from_bytes(&info.data()?)?;
            let (_shape_id, _damage_id) = chunk_man.upload_shape(
                &info.to_string(),
                &sh_code,
                installs.palette(info.collection_name())?,
                &libs,
                runtime.resource::<Gpu>(),
            )?;
        }

        // Manually crank a frame
        let mut encoder = runtime.resource::<Gpu>().device().create_command_encoder(
            &wgpu::CommandEncoderDescriptor {
                label: Some("test-chunk-encoder"),
            },
        );
        chunk_man.close_open_chunks(runtime.resource::<Gpu>(), &mut encoder);
        runtime
            .resource::<Gpu>()
            .device()
            .poll(wgpu::Maintain::Wait);

        Ok(())
    }
    nitrous::make_partial_test!(upload_all_sh, 10);
}

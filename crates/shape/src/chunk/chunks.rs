// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    chunk::{
        chunk_manager::TextureAtlasProperties,
        shape_vertex::ShapeVertex,
        upload::{FaceInfo, GpuUploader, ShapeExtractor},
    },
    instance_block::InstanceBlock,
};
use anyhow::Result;
use atlas::{AtlasPacker, Frame};
use bevy_ecs::prelude::*;
use catalog::FileSystem;
use image::Rgba;
use log::trace;
use mantle::Gpu;
use nitrous::{inject_nitrous_component, NitrousComponent};
use once_cell::sync::Lazy;
use pal::Palette;
use pic_uploader::PicUploader;
use sh::{DrawSelection, ShAnalysis, ShCode};
use shader_shared::DrawIndexedIndirect;
use smallvec::SmallVec;
use std::{
    collections::HashMap,
    fmt::{Display, Formatter},
    mem,
    ops::Range,
    sync::Mutex,
};
use uuid::Uuid;
use zerocopy::AsBytes;

const AVERAGE_VERTEX_BYTES: usize = 24_783;
const VERTEX_CHUNK_COUNT: usize = AVERAGE_VERTEX_BYTES / mem::size_of::<ShapeVertex>();

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub struct ChunkId(u32);

impl Display for ChunkId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[derive(NitrousComponent, Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub struct ShapeId((ChunkId, u32));

#[inject_nitrous_component]
impl ShapeId {
    pub(crate) fn chunk(&self) -> ChunkId {
        self.0 .0
    }
}

#[derive(NitrousComponent, Clone, Debug)]
pub struct ShapeIds {
    normal: ShapeId,
    damage: SmallVec<[ShapeId; 4]>,
}

#[inject_nitrous_component]
impl ShapeIds {
    pub(crate) fn new(normal_shape_id: ShapeId, damage_shape_ids: SmallVec<[ShapeId; 4]>) -> Self {
        Self {
            normal: normal_shape_id,
            damage: damage_shape_ids,
        }
    }

    pub fn normal(&self) -> ShapeId {
        self.normal
    }

    pub fn damage(&self) -> &[ShapeId] {
        &self.damage
    }
}

#[derive(NitrousComponent, Clone, Debug)]
pub struct ShapeName {
    name: String,
}

#[inject_nitrous_component]
impl ShapeName {
    pub fn new<S: ToString>(name: S) -> Self {
        Self {
            name: name.to_string(),
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }
}

static GLOBAL_CHUNK_ID: Lazy<Mutex<u32>> = Lazy::new(|| Mutex::new(0));

fn allocate_chunk_id() -> ChunkId {
    let mut global = GLOBAL_CHUNK_ID.lock().unwrap();
    let next_id = *global;
    assert!(next_id < std::u32::MAX, "overflowed chunk id");
    *global += 1;
    ChunkId(next_id)
}

// FIXME: do we need segmentation? I think we just hit whatever has a component these days?
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct ChunkFlags {
    // Discovered in the model analysis phase
    has_flags: bool,
    has_anim: bool,
    has_xform: bool,
}

impl ChunkFlags {
    pub(crate) fn for_analysis(analysis: &ShAnalysis) -> Self {
        Self {
            has_flags: analysis.has_flags(),
            has_anim: analysis.has_animation(),
            has_xform: analysis.has_xforms(),
        }
    }
}

// Where a shape lives in a chunk.
#[derive(Clone, Debug)]
pub struct ChunkPart {
    // FIXME: index start/end
    vertex_start: usize,
    vertex_count: usize,
    index_start: usize,
    index_count: usize,
    pic_frames: Vec<(String, Frame)>,
    faces: HashMap<Uuid, FaceInfo>,
    code: ShCode,
}

impl ChunkPart {
    fn new(
        vertex_start: usize,
        vertex_end: usize,
        index_start: usize,
        index_end: usize,
        pic_frames: Vec<(String, Frame)>,
        faces: HashMap<Uuid, FaceInfo>,
        code: ShCode,
    ) -> Self {
        // Note: there are shapes with no vertices
        ChunkPart {
            vertex_start,
            vertex_count: vertex_end - vertex_start,
            index_start,
            index_count: index_end - index_start,
            pic_frames,
            faces,
            code,
        }
    }

    pub fn draw_command(&self, first_instance: u32, instance_count: u32) -> DrawIndexedIndirect {
        DrawIndexedIndirect {
            // The number of vertices to draw.
            index_count: self.index_count as u32,
            // The number of instances to draw.
            instance_count,
            // The base index within the index buffer.
            base_index: self.index_start as u32,
            // The value added to the vertex index before indexing into the vertex buffer.
            vertex_offset: self.vertex_start as i32,
            // The instance ID of the first instance to draw.
            // Has to be 0, unless [`Features::INDIRECT_FIRST_INSTANCE`](crate::Features::INDIRECT_FIRST_INSTANCE) is enabled.
            base_instance: first_instance,
        }
    }

    pub fn vertex_range(&self) -> Range<usize> {
        self.vertex_start..self.vertex_start + self.vertex_count
    }

    pub fn frames(&self) -> &[(String, Frame)] {
        &self.pic_frames
    }

    pub fn faces(&self) -> &HashMap<Uuid, FaceInfo> {
        &self.faces
    }

    pub fn code(&self) -> &ShCode {
        &self.code
    }

    pub fn code_mut(&mut self) -> &mut ShCode {
        &mut self.code
    }
}

#[derive(Debug)]
pub struct OpenChunk {
    chunk_id: ChunkId,

    // The flags will be useful if we want to optimize our CPU use later
    #[allow(unused)]
    chunk_flags: ChunkFlags,

    // So we can give out unique ids to each shape in this chunk.
    last_shape_id: u32,

    // Track where each shape lands in our buffers so we can read and write it.
    chunk_parts: HashMap<ShapeId, ChunkPart>,

    // CPU side resources
    vertex_upload_buffer: Vec<ShapeVertex>,
    index_upload_buffer: Vec<u32>,
    atlas_packer: AtlasPacker<Rgba<u8>>,
    dirty: bool,

    // GPU side resources
    vertex_buffer: wgpu::Buffer,
    index_buffer: wgpu::Buffer,
    atlas_bind_group: wgpu::BindGroup,

    // Instances being rendered that live in this chunk, pieced up via blocks, so that we can
    // balance the excess space in a block vs the number of indirect draw calls we need.
    blocks: Vec<InstanceBlock>,
    block_cursor: usize,
}

impl OpenChunk {
    pub(crate) fn new(
        chunk_flags: ChunkFlags,
        layout: &wgpu::BindGroupLayout,
        sampler: &wgpu::Sampler,
        gpu: &Gpu,
    ) -> Self {
        let atlas_size0 = 1024 + 4;
        let atlas_stride = Gpu::stride_for_row_size(atlas_size0 * 4);
        let atlas_size = atlas_stride / 4;
        let atlas_packer = AtlasPacker::<Rgba<u8>>::new(
            "open-shape-chunk",
            gpu,
            atlas_size,
            atlas_size,
            wgpu::TextureFormat::Rgba8Unorm,
            wgpu::FilterMode::Nearest, // TODO: see if we can "improve" things with filtering?
        );

        let vertex_upload_buffer = Vec::with_capacity(VERTEX_CHUNK_COUNT);
        let vertex_buffer = gpu.push_slice(
            "shape-chunk-vertices",
            &[ShapeVertex::default()], // Note: non-empty upload
            wgpu::BufferUsages::VERTEX,
        );

        let index_upload_buffer = Vec::with_capacity(VERTEX_CHUNK_COUNT * 2);
        let index_buffer = gpu.push_slice(
            "shape-chunk-indices",
            &[0u32], // Note: non-empty upload
            wgpu::BufferUsages::INDEX,
        );

        let atlas_bind_group = Self::make_atlas_bind_group(
            atlas_packer.width(),
            atlas_packer.height(),
            atlas_packer.texture_view(),
            layout,
            sampler,
            gpu,
        );

        Self {
            chunk_id: allocate_chunk_id(),
            chunk_flags,
            last_shape_id: 0,
            chunk_parts: HashMap::new(),
            vertex_upload_buffer,
            index_upload_buffer,
            atlas_packer,
            dirty: false,
            vertex_buffer,
            index_buffer,
            atlas_bind_group,
            blocks: Vec::new(),
            block_cursor: 0,
        }
    }

    fn make_atlas_bind_group(
        atlas_width: u32,
        atlas_height: u32,
        atlas_view: &wgpu::TextureView,
        layout: &wgpu::BindGroupLayout,
        sampler: &wgpu::Sampler,
        gpu: &Gpu,
    ) -> wgpu::BindGroup {
        let atlas_properties = TextureAtlasProperties::new(atlas_width, atlas_height);
        let atlas_properties = gpu.push_buffer(
            "chunk-atlas-properties",
            atlas_properties.as_bytes(),
            wgpu::BufferUsages::UNIFORM,
        );
        gpu.device().create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("shape-chunk-atlas-bind-group"),
            layout,
            entries: &[
                // atlas texture
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: wgpu::BindingResource::TextureView(atlas_view),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: wgpu::BindingResource::Sampler(sampler),
                },
                wgpu::BindGroupEntry {
                    binding: 2,
                    resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                        buffer: &atlas_properties,
                        offset: 0,
                        size: None,
                    }),
                },
            ],
        })
    }

    pub(crate) fn reset_upload_cursor(&mut self) {
        for block in self.blocks.iter_mut() {
            block.begin_frame();
        }
        self.block_cursor = 0;
    }

    pub(crate) fn upload_block_data(&self, gpu: &Gpu, encoder: &mut wgpu::CommandEncoder) {
        for block in &self.blocks {
            block.make_upload_buffer(gpu, encoder);
        }
    }

    pub(crate) fn begin_frame(
        &mut self,
        layout: &wgpu::BindGroupLayout,
        sampler: &wgpu::Sampler,
        gpu: &Gpu,
        encoder: &mut wgpu::CommandEncoder,
    ) {
        // Atlas packer has its own dirty-tracking mechanism.
        // We have to call it first, because it might create new textures.
        self.atlas_packer.encode_frame_uploads(gpu, encoder);

        if self.dirty {
            trace!(
                "uploading {} verts to {} / {:?}",
                self.vertex_upload_buffer.len(),
                self.chunk_id,
                self.chunk_flags
            );
            self.vertex_buffer = gpu.push_slice(
                "shape-chunk-vertices",
                &self.vertex_upload_buffer,
                wgpu::BufferUsages::VERTEX,
            );
            self.index_buffer = gpu.push_slice(
                "shape-chunk-indices",
                &self.index_upload_buffer,
                wgpu::BufferUsages::INDEX,
            );
            self.atlas_bind_group = Self::make_atlas_bind_group(
                self.atlas_packer.width(),
                self.atlas_packer.height(),
                self.atlas_packer.texture_view(),
                layout,
                sampler,
                gpu,
            );
            self.dirty = false;
        }
    }

    // QUERIES
    //////////////////////

    pub fn bind_group(&self) -> &wgpu::BindGroup {
        &self.atlas_bind_group
    }

    pub fn vertex_buffer(&self) -> wgpu::BufferSlice {
        self.vertex_buffer.slice(..)
    }

    pub fn index_buffer(&self) -> wgpu::BufferSlice {
        self.index_buffer.slice(..)
    }

    pub(crate) fn blocks(&self) -> &[InstanceBlock] {
        &self.blocks
    }

    pub(crate) fn chunk_is_empty(&self) -> bool {
        self.vertex_upload_buffer.is_empty()
    }

    pub fn part(&self, shape_id: ShapeId) -> &ChunkPart {
        &self.chunk_parts[&shape_id]
    }

    pub fn part_mut(&mut self, shape_id: ShapeId) -> &mut ChunkPart {
        self.chunk_parts.get_mut(&shape_id).expect("valid shape")
    }

    pub fn chunk_id(&self) -> ChunkId {
        self.chunk_id
    }

    pub fn vertices(&self, shape_id: ShapeId) -> &[ShapeVertex] {
        &self.vertex_upload_buffer[self.chunk_parts[&shape_id].vertex_range()]
    }

    pub fn vertices_mut(&mut self, shape_id: ShapeId) -> &mut [ShapeVertex] {
        self.dirty = true;
        &mut self.vertex_upload_buffer[self.chunk_parts[&shape_id].vertex_range()]
    }

    pub fn faces(&self, shape_id: ShapeId) -> &HashMap<Uuid, FaceInfo> {
        self.part(shape_id).faces()
    }

    pub fn code(&self, shape_id: ShapeId) -> &ShCode {
        self.part(shape_id).code()
    }

    pub fn code_mut(&mut self, shape_id: ShapeId) -> &mut ShCode {
        self.part_mut(shape_id).code_mut()
    }

    // SHAPE UPLOAD
    //////////////////////

    fn allocate_shape_id(&mut self) -> ShapeId {
        let shape_index = self.last_shape_id + 1;
        self.last_shape_id = shape_index;
        ShapeId((self.chunk_id, shape_index))
    }

    #[allow(clippy::too_many_arguments)]
    pub(crate) fn upload_shape(
        &mut self,
        name: &str, // for tracing purposes
        sh_code: &ShCode,
        selection: &DrawSelection,
        palette: &Palette,
        catalog: &dyn FileSystem, // for loading textures
        pic_uploader: &mut PicUploader,
        gpu: &Gpu,
    ) -> Result<ShapeId> {
        self.dirty = true;

        assert!(*selection != DrawSelection::DamageModel || sh_code.has_damage_model());
        let start_vertex = self.vertex_upload_buffer.len();
        let start_index = self.index_upload_buffer.len();
        let mut uploader = GpuUploader::new(catalog, pic_uploader, &mut self.atlas_packer, gpu);
        ShapeExtractor::new(name, palette, &mut uploader).draw_model(sh_code, selection)?;
        let (frames, mut verts, mut indices, faces) = uploader.finish();
        trace!(
            "Rendered {name} ({} verts, {} indices, {} faces) to {}",
            verts.len(),
            faces.len(),
            indices.len(),
            self.chunk_id
        );
        self.vertex_upload_buffer.append(&mut verts);
        self.index_upload_buffer.append(&mut indices);

        let part = ChunkPart::new(
            start_vertex,
            self.vertex_upload_buffer.len(),
            start_index,
            self.index_upload_buffer.len(),
            frames,
            faces,
            sh_code.to_owned(),
        );
        let shape_id = self.allocate_shape_id();
        self.chunk_parts.insert(shape_id, part);
        Ok(shape_id)
    }

    // INSTANCE MANAGEMENT
    //////////////////////

    pub(crate) fn ensure_instance_slot(
        &mut self,
        block_layout: &wgpu::BindGroupLayout,
        gpu: &Gpu,
    ) -> &mut InstanceBlock {
        if self.blocks.is_empty() || self.blocks.len() < self.block_cursor + 1 {
            self.blocks
                .push(InstanceBlock::new(block_layout, gpu.device()));
        }
        if !self.blocks[self.block_cursor].has_open_slot() {
            self.blocks
                .push(InstanceBlock::new(block_layout, gpu.device()));
            self.block_cursor += 1;
        }
        &mut self.blocks[self.block_cursor]
    }

    #[inline]
    pub(crate) fn push_values(
        &mut self,
        shape_id: &ShapeId,
        block_layout: &wgpu::BindGroupLayout,
        gpu: &Gpu,
        transform: &[f32; 8],
        flags: &[u32; 2],
        xforms: &Option<[[f32; 6]; 14]>,
    ) {
        let cmd = self.part(*shape_id).draw_command(0, 1);
        self.ensure_instance_slot(block_layout, gpu)
            .push_values(cmd, transform, flags, xforms);
    }
}

// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use mantle::Gpu;
use shader_shared::DrawIndexedIndirect;
use std::{mem, num::NonZeroU64};
use zerocopy::AsBytes;

const BLOCK_SIZE: usize = 1 << 11;

const MAX_XFORMS: usize = 14;

pub(crate) type TransformType = [f32; 8];

#[allow(unused)] // Usage is in shader
#[derive(AsBytes, Clone, Copy, Debug, Default)]
#[repr(packed)]
pub(crate) struct InstanceInfo {
    transform: [f32; 8],
    flags0: u32,
    flags1: u32,
    xforms: [[f32; 6]; MAX_XFORMS],
}

// Fixed reservation blocks for upload of a number of entities. Unfortunately, because of
// xforms, we don't know exactly how many instances will fit in any given block.
#[derive(Debug)]
pub struct InstanceBlock {
    command_buffer_scratch: Vec<DrawIndexedIndirect>,
    // [DrawIndirectCommand; BLOCK_SIZE],
    instance_info_buffer_scratch: Vec<InstanceInfo>, //Box<[InstanceInfo; BLOCK_SIZE]>,

    command_buffer: wgpu::Buffer,
    instance_info_buffer: wgpu::Buffer,

    bind_group: wgpu::BindGroup,
}

impl InstanceBlock {
    const INSTANCE_INFO_BUFFER_SIZE: wgpu::BufferAddress =
        (mem::size_of::<InstanceInfo>() * BLOCK_SIZE) as wgpu::BufferAddress;

    pub(crate) const fn instance_info_buffer_size() -> Option<NonZeroU64> {
        NonZeroU64::new(Self::INSTANCE_INFO_BUFFER_SIZE)
    }

    pub(crate) fn new(layout: &wgpu::BindGroupLayout, device: &wgpu::Device) -> Self {
        let command_buffer_size =
            (mem::size_of::<DrawIndexedIndirect>() * BLOCK_SIZE) as wgpu::BufferAddress;
        let command_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("shape-instance-command-buffer"),
            size: command_buffer_size,
            usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::INDIRECT,
            mapped_at_creation: false,
        });

        let instance_info_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("shape-instance-info-buffer"),
            size: Self::INSTANCE_INFO_BUFFER_SIZE,
            usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::STORAGE,
            mapped_at_creation: false,
        });

        let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("shape-instance-bind-group"),
            layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                    buffer: &instance_info_buffer,
                    offset: 0,
                    size: None,
                }),
            }],
        });

        Self {
            command_buffer_scratch: Vec::new(),
            instance_info_buffer_scratch: Vec::new(),
            command_buffer,
            instance_info_buffer,
            bind_group,
        }
    }

    pub(crate) fn bind_group(&self) -> &wgpu::BindGroup {
        &self.bind_group
    }

    pub(crate) fn commands(&self) -> &[DrawIndexedIndirect] {
        &self.command_buffer_scratch
    }

    pub(crate) fn has_open_slot(&self) -> bool {
        // self.free_slot < BLOCK_SIZE
        self.instance_info_buffer_scratch.len() < BLOCK_SIZE
    }

    #[inline]
    pub(crate) fn begin_frame(&mut self) {
        self.command_buffer_scratch.clear();
        self.instance_info_buffer_scratch.clear();
    }

    #[inline]
    pub(crate) fn push_values(
        &mut self,
        mut cmd: DrawIndexedIndirect,
        transform: &TransformType,
        flags: &[u32; 2],
        xforms: &Option<[[f32; 6]; MAX_XFORMS]>,
    ) {
        assert!(self.instance_info_buffer_scratch.len() < BLOCK_SIZE);
        cmd.base_instance = self.instance_info_buffer_scratch.len() as u32;
        // cmd.base_instance = 0;
        cmd.instance_count = 1;
        self.command_buffer_scratch.push(cmd);
        self.instance_info_buffer_scratch.push(InstanceInfo {
            transform: *transform,
            flags0: flags[0],
            flags1: flags[1],
            xforms: xforms.unwrap_or_else(|| [[0f32; 6]; MAX_XFORMS]),
        });
    }

    pub(crate) fn make_upload_buffer(&self, gpu: &Gpu, encoder: &mut wgpu::CommandEncoder) {
        gpu.upload_slice_to(&self.command_buffer_scratch, &self.command_buffer, encoder);
        gpu.upload_slice_to(
            &self.instance_info_buffer_scratch,
            &self.instance_info_buffer,
            encoder,
        );
    }
}

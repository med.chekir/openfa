// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.

//float fa2r(float d) {
//    return d * PI / 8192.0;
//}

fn from_euler_angles(roll: f32, pitch: f32, yaw: f32) -> mat4x4<f32> {
    let sr = sin(-roll);
    let cr = cos(-roll);
    let sp = sin(pitch);
    let cp = cos(pitch);
    let sy = sin(yaw);
    let cy = cos(yaw);

    return mat4x4(
        vec4(
            cy * cp,
            sy * cp,
            -sp,
            0.
        ),

        vec4(
            cy * sp * sr - sy * cr,
            sy * sp * sr + cy * cr,
            cp * sr,
            0.
        ),

        vec4(
            cy * sp * cr + sy * sr,
            sy * sp * cr - cy * sr,
            cp * cr,
            0.
        ),

        vec4(0., 0., 0., 1.),
    );
}

fn rotation_for_xform(xform: array<f32,8>) -> mat4x4<f32> {
    let r0 = xform[3];
    let r1 = xform[4];
    let r2 = xform[5];
    return from_euler_angles(r0, r1, r2);
}

fn matrix_for_transform_and_scale(transform: vec3<f32>, scale: f32) -> mat4x4<f32> {
    let t0 = transform.x;
    let t1 = transform.y;
    let t2 = transform.z;
    let s = scale;
    return mat4x4(
        vec4(  s, 0.0, 0.0, 0.0),
        vec4(0.0,   s, 0.0, 0.0),
        vec4(0.0, 0.0,   s, 0.0),
        vec4( t0,  t1,  t2, 1.0)
    );
}

fn matrix_for_xform(xform: array<f32,8>) -> mat4x4<f32> {
    let t0 = xform[0];
    let t1 = xform[1];
    let t2 = xform[2];
    let r0 = xform[3];
    let r1 = xform[4];
    let r2 = xform[5];
    let s = xform[7];
    let trans = mat4x4(
        vec4(s,   0.0, 0.0, 0.0),
        vec4(0.0,   s, 0.0, 0.0),
        vec4(0.0, 0.0,   s, 0.0),
        vec4(t0,   t1,  t2, 1.0)
    );
    let rot = from_euler_angles(r0, r1, r2);
    return trans * rot;
}

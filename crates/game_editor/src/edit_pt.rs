// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    widget::{FieldDisplay, FieldRef, Form, FormEvent, FormField, TabSet, Widget},
    DocumentEditProvider, DocumentImportProvider, EditState, EditableDocument,
};
use anyhow::{bail, Result};
use catalog::{AssetCollection, FileInfo, FileSystem, Search};
use egui::Ui;
use enum_iterator::{all, Sequence};
use installations::GameInstallation;
use nitrous::HeapMut;
use ot::{ExplosionType, ObjectKind};
use pt::{PlaneFlags, PlaneType, PlaneTypeIo};
use smallvec::{smallvec, SmallVec};
use std::str::FromStr;
use xt::TypeManager;

#[derive(Clone, Debug, Default)]
pub struct PlaneTypeEdit;

impl EditableDocument for PlaneTypeEdit {
    fn make_importer(&self) -> Box<dyn DocumentImportProvider> {
        Box::<PlaneTypeImporter>::default() as Box<dyn DocumentImportProvider>
    }
    fn object_menu(&self, ui: &mut Ui) -> Option<EditState> {
        self.waiting_menu(ui)
    }
    fn graphics_menu(&self, _ui: &mut Ui) -> Option<EditState> {
        None
    }
    fn waiting_menu(&self, ui: &mut Ui) -> Option<EditState> {
        if ui.button(".PT [Plane Type]").clicked() {
            return Some(EditState::ImportDocument(self.make_importer()));
        }
        None
    }
    fn document_actions(
        &self,
        ui: &mut Ui,
        info: FileInfo,
        install: &GameInstallation,
        collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<Option<EditState>> {
        if info.name().ends_with(".PT") && ui.button("Edit").clicked() {
            return Ok(Some(
                PlaneTypeImporter.import(info, install, collection, heap)?,
            ));
        }
        Ok(None)
    }
}

// Note: currently leaning on the TypeManager cache
#[derive(Debug, Default)]
pub struct PlaneTypeImporter;

impl DocumentImportProvider for PlaneTypeImporter {
    fn all_documents<'a>(&self, collection: &'a AssetCollection) -> Result<Vec<FileInfo<'a>>> {
        collection.search(Search::for_extension("PT"))
    }

    fn column_headers(&self) -> SmallVec<[&'static str; 8]> {
        smallvec!["Name", "Description"]
    }

    fn column_values(
        &mut self,
        info: FileInfo,
        _install: &GameInstallation,
        _collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<SmallVec<[String; 8]>> {
        let xt = heap.resource::<TypeManager>().load(info)?;
        Ok(smallvec![
            info.name().to_owned(),
            xt.ot().ot_names().long_name().to_owned()
        ])
    }

    fn import(
        &mut self,
        info: FileInfo,
        _install: &GameInstallation,
        _collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<EditState> {
        let xt = heap.resource::<TypeManager>().load(info)?;
        Ok(EditState::EditDocument(
            Box::new(PlaneTypeEditState::new(xt.xpt().to_owned())) as Box<dyn DocumentEditProvider>,
        ))
    }
}

#[derive(Clone, Debug, Sequence)]
enum PlaneTypeEditTab {
    ObjectInfo,
    Class,
    General,
    Damage,
    Movement,
    HardPoints,
    Controls,
    Engine,
    Sounds,
    Envelope,
    Aerodynamics,
}

impl PlaneTypeEditTab {
    pub fn label(&self) -> &'static str {
        match self {
            Self::ObjectInfo => "Object Info",
            Self::Class => "Class",
            Self::General => "General",
            Self::Damage => "Damage",
            Self::Movement => "Movement",
            Self::HardPoints => "HardPoints",
            Self::Controls => "Controls",
            Self::Engine => "Engine",
            Self::Sounds => "Sounds",
            Self::Envelope => "Envelope",
            Self::Aerodynamics => "Aerodynamics",
        }
    }
}

impl FromStr for PlaneTypeEditTab {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        Ok(match s {
            "Object Info" => Self::ObjectInfo,
            "Class" => Self::Class,
            "General" => Self::General,
            "Damage" => Self::Damage,
            "Movement" => Self::Movement,
            "HardPoints" => Self::HardPoints,
            "Controls" => Self::Controls,
            "Engine" => Self::Engine,
            "Sounds" => Self::Sounds,
            "Envelope" => Self::Envelope,
            "Aerodynamics" => Self::Aerodynamics,
            _ => bail!("not a PlaneTypeEditTab value"),
        })
    }
}

#[derive(Clone, Debug)]
pub(crate) struct PlaneTypeEditState {
    pt: PlaneTypeIo,
    active_tab: PlaneTypeEditTab,
}

impl DocumentEditProvider for PlaneTypeEditState {
    fn plugin(&self) -> &'static str {
        "PT"
    }

    fn get_edit_widgets(&mut self, _heap: HeapMut) -> Result<Vec<Widget>> {
        use FormField as FF;
        let tabs = TabSet::from_iter(
            "tabs",
            self.active_tab.label(),
            all::<PlaneTypeEditTab>().map(|v| v.label()),
        );

        let form = match &self.active_tab {
            PlaneTypeEditTab::ObjectInfo => Form::from_table(
                "info_form",
                vec![
                    FF::new("info_file_name", self)?
                        .with_label("Object Name")
                        .with_hover_text("FA Resource Name (filename)"),
                    FF::new("info_short_name", self)?
                        .with_label("Internal Name")
                        .with_hover_text("Value shown in aircraft lists"),
                    FF::new("info_desc", self)?.with_label("Description"),
                    FF::new("info_shape", self)?.with_label("3D Shape"),
                    FF::new("info_shadow", self)?.with_label("Shadow Shape"),
                    FF::new("info_ai", self)?.with_label("AI Profile"),
                    FF::new("info_hud", self)?.with_label("HUD"),
                ],
            ),
            PlaneTypeEditTab::Class => {
                let cls: ObjectKind = (*self.pt.nt().ot().obj_class()).try_into()?;
                Form::from_table(
                    "class_form",
                    vec![
                        FF::new_missing("class_fighter")?
                            .with_label("Fighter")
                            .with_display(FieldDisplay::RadioButton(cls == ObjectKind::Fighter))
                            .inactive(),
                        FF::new_missing("class_bomber")?
                            .with_label("Bomber")
                            .with_display(FieldDisplay::RadioButton(cls == ObjectKind::Bomber))
                            .inactive(),
                        FF::new_missing("class_ship")?
                            .with_label("Ship")
                            .with_display(FieldDisplay::RadioButton(cls == ObjectKind::Ship))
                            .inactive(),
                        FF::new_missing("class_sam")?
                            .with_label("SAM")
                            .with_display(FieldDisplay::RadioButton(cls == ObjectKind::Sam))
                            .inactive(),
                        FF::new_missing("class_aaa")?
                            .with_label("AAA")
                            .with_display(FieldDisplay::RadioButton(cls == ObjectKind::Aaa))
                            .inactive(),
                        FF::new_missing("class_vehicle")?
                            .with_label("Vehicle")
                            .with_display(FieldDisplay::RadioButton(cls == ObjectKind::Vehicle))
                            .inactive(),
                        FF::new_missing("class_tank")?
                            .with_label("Tank")
                            .with_display(FieldDisplay::RadioButton(cls == ObjectKind::Tank))
                            .inactive(),
                        FF::new_missing("class_structure_1")?
                            .with_label("Structure1")
                            .with_display(FieldDisplay::RadioButton(cls == ObjectKind::Structure1))
                            .inactive(),
                        FF::new_missing("class_projectile")?
                            .with_label("Projectile")
                            .with_display(FieldDisplay::RadioButton(cls == ObjectKind::Projectile))
                            .inactive(),
                        FF::new_missing("class_structure_2")?
                            .with_label("Structure2")
                            .with_display(FieldDisplay::RadioButton(cls == ObjectKind::Structure2))
                            .inactive(),
                    ],
                )
            }
            PlaneTypeEditTab::General => Form::from_table(
                "general_form",
                vec![
                    FF::new("general_has_jet_engine", self)?.with_label(PlaneFlags::HAS_JET_ENGINE.label()),
                    FF::new("general_has_hook", self)?.with_label(PlaneFlags::HAS_HOOK.label()),
                    FF::new("general_two_seat", self)?.with_label(PlaneFlags::TWO_SEAT_COCKPIT.label()),
                    FF::new("general_is_helicopter", self)?.with_label(PlaneFlags::IS_HELICOPTER.label()),
                    FF::new("general_can_eject", self)?.with_label(PlaneFlags::HAS_EJECTION_SEATS.label()),
                    FF::new("general_vtol_capable", self)?.with_label(PlaneFlags::VTOL_CAPABLE.label()),
                    FF::new("general_carrier", self)?.with_label(PlaneFlags::CARRIER_CAPABLE.label()),
                    FF::new("general_has_bay", self)?.with_label(PlaneFlags::HAS_BAY.label()),
                    FF::new("general_unk9", self)?
                        .with_label(PlaneFlags::UNK_BIT9.label())
                        .with_hover_text("On F31{EFV} only. Horizontal Vectoring?"),
                    FF::new("general_unk11", self)?
                        .with_label(PlaneFlags::UNK_BIT11.label())
                        .with_hover_text("On ASTOV{LVF}, F31{EFV}, F29. Maybe X planes? Would say thrust vectoring, but where is F22?"),
                    FF::new("general_decoy", self)?
                        .with_label(PlaneFlags::IS_UNMANNED_DECOY.label())
                        .with_hover_text("An unmanned decoy aircraft"),
                    FF::new("general_unk14", self)?
                        .with_label(PlaneFlags::UNK_BIT14.label())
                        .with_hover_text("ATL, E2, IL76, E3, P3. Prop aircraft?"),
                    FF::new("general_unk15", self)?
                        .with_label(PlaneFlags::UNK_BIT15.label())
                        .with_hover_text("Recon drones + E8. JSTARS?"),
                ],
            ),
            PlaneTypeEditTab::Damage => Form::from_table(
                "damage_form",
                vec![
                    FF::new("damage_explosion", self)?.with_label("Explosion if Destroyed"),
                    FF::new("damage_crater_size", self)?.with_label("Crater Size"),
                    FF::new("damage_hit_points", self)?.with_label("Object Hit Points"),
                    FF::new_missing("damage_on_")?.with_label("Damage Inflicted"),
                    FF::new("damage_on_planes", self)?.with_label("Planes:"),
                    FF::new("damage_on_ships", self)?.with_label("Ships:"),
                    FF::new("damage_on_structures", self)?.with_label("Structures:"),
                    FF::new("damage_on_armor", self)?.with_label("Armor:"),
                    FF::new("damage_on_other", self)?.with_label("Other:"),
                ]),
            PlaneTypeEditTab::Movement => Form::from_table("movement_form", vec![
                FF::new_missing("move_maneuvering")?.with_label("Maneuvering:"),
                FF::new("move_bank_rate", self)?.with_label("Bank Rate:"),
                FF::new("move_max_climb", self)?.with_label("Max Climb:"),
                FF::new("move_max_dive", self)?.with_label("Max Dive:"),
                FF::new("move_max_bank", self)?.with_label("Max Bank:"),
                FF::new_missing("move_alt")?.with_label("Altitude Limits:"),
                FF::new("move_min_alt", self)?.with_label("Min Altitude:"),
                FF::new("move_max_alt", self)?.with_label("Max Altitude:"),
            ]),
            PlaneTypeEditTab::HardPoints => Form::from_table("hardpoints_form", vec![
                FF::new_missing("hardpoints_todo")?.with_label("TODO!"),
            ]),
            PlaneTypeEditTab::Controls => Form::from_table("controls_form", vec![
                FF::new_missing("controls_air_drag_characteristics")?.with_label("Drag Characteristics:"),
                FF::new("controls_air_brake_drag", self)?
                    .with_label("Airbrake Drag")
                    .with_hover_text("Value added to drag coefficient when airbrakes are deployed"),
                FF::new("controls_flap_drag", self)?
                    .with_label("Flap Drag")
                    .with_hover_text("Value added to drag coefficient when flaps are down"),
                FF::new("controls_bay_drag", self)?
                    .with_label("Bay Drag")
                    .with_hover_text("Value added to drag coefficient when bay doors are open"),
                FF::new("controls_rudder_drag", self)?
                    .with_label("Rudder Drag")
                    .with_hover_text("Value added to drag coefficient when using the rudder to slip"),
                FF::new("controls_gear_drag", self)?
                    .with_label("Gear Drag")
                    .with_hover_text("Value added to drag coefficient when the landing gear is down"),
                FF::new("controls_loaded_drag", self)?
                    .with_label("Ordinance Drag")
                    .with_hover_text("Value added to drag coefficient when the plane is carrying missiles"),
                FF::new("controls_gpull_drag", self)?
                    .with_label("G-Pull Drag")
                    .with_hover_text("Value added to drag coefficient when the plane is taking G's"),
                FF::new("controls_loaded_gpull_drag", self)?
                    .with_label("Loaded G-Pull Drag")
                    .with_hover_text("Value added to drag coefficient when the plane is taking G's while carrying missiles"),
                FF::new_missing("controls_ground_drag_characteristics")?.with_label("Drag Characteristics:"),
                FF::new("controls_wheel_brake_drag", self)?
                    .with_label("Drag")
                    .with_hover_text("Value added to drag coefficient when the brakes are deployed on the ground"),
                FF::new_missing("controls_structure_speed_limits")?.with_label("Structural Speed Limits:"),
                FF::new("controls_0angels_limit", self)?
                    .with_label("Sea Level")
                    .with_hover_text("Do Not Exceed speed at sea level. (The plane's flight model will clamp to this value.)"),
                FF::new("controls_36angels_limit", self)?
                    .with_label("36,000'")
                    .with_hover_text("Do Not Exceed speed at 36,000'. (The plane's flight model will clamp to this value.)"),
                FF::new_missing("controls_structural_effects")?.with_label("Structural Effects:"),
                FF::new("controls_turbulence_factor", self)?
                    .with_label("Turbulence Factor"),
            ]),
            PlaneTypeEditTab::Engine => Form::from_table("engine_form", vec![]),
            PlaneTypeEditTab::Sounds => Form::from_table("sounds_form", vec![]),
            PlaneTypeEditTab::Envelope => Form::from_table("envelope_form", vec![]),
            PlaneTypeEditTab::Aerodynamics => Form::from_table("aerodynamics_form", vec![]),
        };
        Ok(vec![
            Widget::Tabs(tabs),
            Widget::H1(format!(
                "{}: {}",
                self.pt.nt().ot().ot_names().file_name(),
                self.pt.nt().ot().ot_names().long_name()
            )),
            Widget::Form(form),
        ])
    }

    fn get_field_ref(&mut self, name: &str) -> Result<FieldRef> {
        use FieldRef as FR;
        use PlaneFlags as PF;
        let pt = &mut self.pt;
        Ok(match name {
            "tabs" => FieldRef::Tab(self.active_tab.label().to_owned()),
            // info
            "info_file_name" => FR::Line(pt.nt_mut().ot_mut().ot_names_mut().file_name_mut()),
            "info_short_name" => FR::Line(pt.nt_mut().ot_mut().ot_names_mut().short_name_mut()),
            "info_desc" => FR::Line(pt.nt_mut().ot_mut().ot_names_mut().long_name_mut()),
            "info_shape" => FR::MaybeLine(pt.nt_mut().ot_mut().shape_mut()),
            "info_shadow" => FR::MaybeLine(pt.nt_mut().ot_mut().shadow_shape_mut()),
            "info_ai" => FR::DropdownNullableExtensionSearch {
                value: pt.nt_mut().ct_name_mut(),
                ext: "BI",
            },
            "info_hud" => FR::DropdownNullableExtensionSearch {
                value: pt.nt_mut().ot_mut().hud_name_mut(),
                ext: "HUD",
            },
            "info_year" => FR::U32(pt.nt_mut().ot_mut().year_available_mut()),
            // class - not editable
            // general
            "general_has_jet_engine" => FR::CheckBit32(pt.pt_flags_mut(), PF::HAS_JET_ENGINE.off()),
            "general_has_hook" => FR::CheckBit32(pt.pt_flags_mut(), PF::HAS_HOOK.off()),
            "general_two_seat" => FR::CheckBit32(pt.pt_flags_mut(), PF::TWO_SEAT_COCKPIT.off()),
            "general_is_helicopter" => FR::CheckBit32(pt.pt_flags_mut(), PF::IS_HELICOPTER.off()),
            "general_can_eject" => FR::CheckBit32(pt.pt_flags_mut(), PF::HAS_EJECTION_SEATS.off()),
            "general_vtol_capable" => FR::CheckBit32(pt.pt_flags_mut(), PF::VTOL_CAPABLE.off()),
            "general_carrier" => FR::CheckBit32(pt.pt_flags_mut(), PF::CARRIER_CAPABLE.off()),
            "general_has_bay" => FR::CheckBit32(pt.pt_flags_mut(), PF::HAS_BAY.off()),
            "general_unk9" => FR::CheckBit32(pt.pt_flags_mut(), PF::UNK_BIT9.off()),
            "general_unk11" => FR::CheckBit32(pt.pt_flags_mut(), PF::UNK_BIT11.off()),
            "general_decoy" => FR::CheckBit32(pt.pt_flags_mut(), PF::IS_UNMANNED_DECOY.off()),
            "general_unk14" => FR::CheckBit32(pt.pt_flags_mut(), PF::UNK_BIT14.off()),
            "general_unk15" => FR::CheckBit32(pt.pt_flags_mut(), PF::UNK_BIT15.off()),
            "general_uav_attack" => FR::CheckBit32(pt.pt_flags_mut(), PF::IS_UNMANNED_ATTACK.off()),
            "general_uav_recon" => FR::CheckBit32(pt.pt_flags_mut(), PF::IS_UNMANNED_RECON.off()),
            // damage
            "damage_explosion" => FR::DropdownU8 {
                value: pt.nt_mut().ot_mut().explosion_type_mut(),
                options: all::<ExplosionType>().map(|v| v as u8).collect(),
                labels: all::<ExplosionType>().map(|v| v.label()).collect(),
            },
            "damage_crater_size" => FR::U8(pt.nt_mut().ot_mut().crater_size_mut()),
            "damage_hit_points" => FR::U16(pt.nt_mut().ot_mut().hit_points_mut()),
            "damage_on_planes" => FR::U16(pt.nt_mut().ot_mut().damage_on_planes_mut()),
            "damage_on_ships" => FR::U16(pt.nt_mut().ot_mut().damage_on_ships_mut()),
            "damage_on_structures" => FR::U16(pt.nt_mut().ot_mut().damage_on_structures_mut()),
            "damage_on_armor" => FR::U16(pt.nt_mut().ot_mut().damage_on_armor_mut()),
            "damage_on_other" => FR::U16(pt.nt_mut().ot_mut().damage_on_other_mut()),
            // Movement
            "move_bank_rate" => FR::U16(pt.nt_mut().ot_mut().bank_rate_mut()),
            "move_max_climb" => FR::I16(pt.nt_mut().ot_mut().max_climb_mut()),
            "move_max_dive" => FR::I16(pt.nt_mut().ot_mut().max_dive_mut()),
            "move_max_bank" => FR::I16(pt.nt_mut().ot_mut().max_bank_mut()),
            "move_min_alt" => FR::I32(pt.nt_mut().ot_mut().min_altitude_mut()),
            "move_max_alt" => FR::I32(pt.nt_mut().ot_mut().max_altitude_mut()),
            // Controls
            "controls_air_brake_drag" => FR::I16(pt.air_brakes_drag_mut()),
            "controls_flap_drag" => FR::I16(pt.flaps_drag_mut()),
            "controls_bay_drag" => FR::I16(pt.bay_drag_mut()),
            "controls_rudder_drag" => FR::I16(pt.rudder_drag_mut()),
            "controls_gear_drag" => FR::I16(pt.gear_drag_mut()),
            "controls_loaded_drag" => FR::I16(pt.loaded_drag_mut()),
            "controls_gpull_drag" => FR::I16(pt._gpull_drag_mut()),
            "controls_loaded_gpull_drag" => FR::I16(pt.loaded_gpull_drag_mut()),
            "controls_wheel_brake_drag" => FR::I16(pt.wheel_brakes_drag_mut()),
            "controls_0angels_limit" => FR::U16(pt.max_speed_sea_level_mut()),
            "controls_36angels_limit" => FR::U16(pt.max_speed_36a_mut()),
            "controls_turbulence_factor" => FR::I16(pt.turbulence_percent_mut()),
            name => {
                bail!("asked for missing field {name}")
            }
        })
    }

    fn apply_event(&mut self, event: &FormEvent, _heap: HeapMut) -> Result<()> {
        let next = event.next();
        match event.name() {
            "tabs" => self.active_tab = next.string()?.parse::<PlaneTypeEditTab>()?,
            name => {
                bail!("unknown event name in manual handler: {name}");
            }
        }
        Ok(())
    }

    fn serialize(&self, _heap: HeapMut) -> Result<(String, Vec<u8>)> {
        Ok((
            self.pt.nt().ot().ot_names().file_name().to_owned(),
            self.pt.to_string()?.as_bytes().to_owned(),
        ))
    }
}

impl PlaneTypeEditState {
    pub(crate) fn new(pt: PlaneType) -> Self {
        Self {
            pt: PlaneTypeIo::from(&pt),
            active_tab: PlaneTypeEditTab::ObjectInfo,
        }
    }
}

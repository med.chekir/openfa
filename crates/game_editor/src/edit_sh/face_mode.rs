// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::widget::{FieldDisplay, FormEvent, FormField};
use absolute_unit::prelude::*;
use anyhow::{bail, Result};
use bevy_ecs::prelude::*;
use camera::ScreenCamera;
use csscolorparser::Color;
use geometry::{algorithm::compute_normal, intersect::ray_vs_triangle, Plane, Ray};
use glam::DMat4;
use mantle::TimeStep;
use marker::{rgb, Markers};
use nitrous::HeapMut;
use phase::Frame;
use sh::{
    fa_norm_to_vec, iFC_Face, matrix_for_xform, DrawState, FaceContentFlags, FaceLayoutFlags,
    InstrDetail, ShInstr, ShXforms,
};
use shape::{FaceInfo, Shape, ShapeId, ShapeVertex};
use uuid::Uuid;

pub fn matrix_for_face(
    info: &FaceInfo,
    entity_id: Entity,
    shape_id: ShapeId,
    shape: &Shape,
    mut heap: HeapMut,
) -> Result<DMat4> {
    // Note: clone these to the stack to avoid the borrow on heap
    let Some(draw_state) = heap.maybe_get_by_id::<DrawState>(entity_id).copied() else {
        return Ok(DMat4::IDENTITY);
    };
    let start = *heap.resource::<TimeStep>().start_time();
    let now = *heap.resource::<TimeStep>().sim_time();

    // Takes 10-20ms to clone out of the shape.
    // let mut xforms = shape.analysis(shape_id)?.xforms_for_shape_bundle();
    // if let Some(xforms) = xforms.as_mut() {

    // Takes 100-200us to use a reference here.
    if let Ok(xforms) = &mut heap.maybe_get_by_id_mut::<ShXforms>(entity_id) {
        let InstrDetail::VertexBuffer(vxbuf) = shape
            .get_code(shape_id)
            .instruction(info.vxbuf_uuid())
            .unwrap()
            .detail()
        else {
            bail!("expected vxbuf uuid to link to a vxbuf");
        };
        if let Some(xformer) = xforms.get_mut(vxbuf.xform_id()) {
            let mut out = [0f32; 6];
            if xformer.run(&draw_state, &start, &now, &mut out).is_ok() {
                return Ok(matrix_for_xform(out).as_dmat4());
            }
        }
    }
    Ok(DMat4::IDENTITY)
}

#[derive(Debug, Default)]
pub struct FaceMode {
    selected_faces: Vec<Uuid>,
    selecting_face_color: bool,
}

impl FaceMode {
    // Use point intersection on mouse-press to immediately select
    // whatever is under the cursor.
    pub fn ray_select(
        &mut self,
        cursor: (f64, f64),
        shift_pressed: bool,
        (entity_id, frame): (Entity, &Frame),
        (shape_id, shape): (ShapeId, &mut Shape),
        mut heap: HeapMut,
    ) -> Result<()> {
        let camera = heap.resource::<ScreenCamera>();
        let wrld_m = camera.mouse_to_world(cursor);

        // The ray is in world space.
        let ray = Ray::new(
            camera.position::<Meters>(),
            (wrld_m.dvec3() - camera.position::<Meters>().dvec3()).normalize(),
        );

        // Get shape, but allow us to still get the xforms mutably
        let mut intersections = vec![];
        // Note: this is the slice of vertices specific to this shape.
        let verts = shape.get_vertices(shape_id);
        for (uuid, info) in shape.get_faces(shape_id) {
            let m = matrix_for_face(info, entity_id, shape_id, shape, heap.as_mut())?;
            for tri in info.triangles(verts, frame, &m) {
                if let Some(intersect) = ray_vs_triangle(&ray, &tri[0], &tri[1], &tri[2]) {
                    intersections.push((intersect, *uuid));
                }
            }
        }

        intersections.sort_by_key(|(intersect, _)| intersect.distance::<Meters>());
        if !shift_pressed {
            if let Some((_intersect, uuid)) = intersections.first() {
                self.selected_faces = vec![*uuid];
            } else {
                // FIXME: we should be able to check if the mouse is in a UX element. If we just
                //        naively deselect on click, trying to edit will unselect the thing we are
                //        trying to edit.
                //self.selected_faces = vec![];
            }
        } else if let Some((_intersect, uuid)) = intersections.first() {
            if self.selected_faces.contains(uuid) {
                self.selected_faces.retain(|v| v != uuid);
            } else {
                self.selected_faces.push(*uuid);
            }
        }

        Ok(())
    }

    // Select whatever is in the current drag region.
    pub fn region_select(
        &mut self,
        planes: &[Plane<Meters>; 4],
        shift_pressed: bool,
        (entity_id, frame): (Entity, &Frame),
        (shape_id, shape): (ShapeId, &mut Shape),
        mut heap: HeapMut,
    ) -> Result<()> {
        // Transform from model to world space for the shape.
        let p = frame.position();
        let q = frame.facing();

        // Note: this is the slice of vertices specific to this shape.
        let mut intersections = vec![];
        let verts = shape.get_vertices(shape_id);
        for (uuid, info) in shape.get_faces(shape_id) {
            // Compute the normal of the first tri to see if we are front facing or back facing.
            let xform = matrix_for_face(info, entity_id, shape_id, shape, heap.as_mut())?;
            let camera = heap.resource::<ScreenCamera>();
            let tri = info.triangles(verts, frame, &xform).next().unwrap();
            let normal = compute_normal(&tri[0], &tri[1], &tri[2]);
            if normal.dot(*camera.forward()) > 0.5 {
                continue;
            }
            // Find any point in the face loop that is fully enclosed.
            for index in info.face_loop() {
                let pt = p + q * verts[*index].position().transform_by(&xform);
                if planes.iter().all(|plane| plane.point_is_in_front(&pt)) {
                    intersections.push(*uuid);
                    break;
                }
            }
        }

        // FIXME: we should be able to check if the mouse is in a UX element. If we just
        //        naively deselect on click, trying to edit will unselect the thing we are
        //        trying to edit.
        if !shift_pressed && !intersections.is_empty() {
            self.selected_faces = intersections;
        } else {
            self.selected_faces.extend(&intersections);
        }

        Ok(())
    }

    fn instr<'a>(&self, uuid: &Uuid, shape_id: ShapeId, shape: &'a Shape) -> Result<&'a ShInstr> {
        let sh_code = shape.get_code(shape_id);
        sh_code.instruction(uuid)
    }

    fn face<'a>(&self, uuid: &Uuid, shape_id: ShapeId, shape: &'a Shape) -> Result<&'a iFC_Face> {
        let instr = self.instr(uuid, shape_id, shape)?;
        Ok(match instr.detail() {
            InstrDetail::Face(face) => face,
            _ => bail!("not a face instruction"),
        })
    }

    fn face_mut<'a>(
        &mut self,
        uuid: &Uuid,
        shape_id: ShapeId,
        shape: &'a mut Shape,
    ) -> Result<&'a mut iFC_Face> {
        let sh_code = shape.get_code_mut(shape_id);
        let InstrDetail::Face(face) = sh_code.instruction_mut(uuid)?.detail_mut() else {
            bail!("expected a face instruction");
        };
        Ok(face)
    }

    fn show_face_loops(
        &self,
        entity_id: Entity,
        frame: &Frame,
        shape_id: ShapeId,
        shape: &Shape,
        markers: &mut Markers,
        mut heap: HeapMut,
    ) -> Result<()> {
        let p = frame.position();
        let q = frame.facing();

        let verts = shape.get_vertices(shape_id);

        for face_uuid in &self.selected_faces {
            let face_info = shape.get_faces(shape_id).get(face_uuid).unwrap();
            let m = matrix_for_face(face_info, entity_id, shape_id, shape, heap.as_mut())?;

            let face_loop = &shape.get_faces(shape_id)[&face_uuid].face_loop();
            for (i, &v0i) in face_loop.iter().enumerate() {
                let j = (i + 1) % face_loop.len();
                let v1j = face_loop[j];
                let v0 = &verts[v0i];
                let v1 = &verts[v1j];
                let p0 = p + q * v0.position().transform_by(&m);
                let p1 = p + q * v1.position().transform_by(&m);
                markers.draw_point(p0, meters!(0.15), &Color::from_rgba8(0, 255, 255, 255));
                markers.draw_cylinder(&Color::from_rgba8(255, 0, 255, 255), p0, p1, meters!(0.1));
            }

            let face = self.face(face_uuid, shape_id, shape)?;
            if let Some(center) = face.center() {
                let c = Pt3::new(feet!(center[0]), feet!(center[1]), feet!(center[2]));
                let c = p + q * c;
                let color = if face.content_flags().contains(FaceContentFlags::UNK2) {
                    0x00FF00
                } else {
                    0xFF00FF
                };

                if let Some(normal) = face.normal() {
                    let n = q * fa_norm_to_vec(normal).as_dvec3();
                    markers.draw_arrow(&rgb(color), c, n, meters!(1), meters!(0.1));
                } else {
                    markers.draw_point(c, meters!(0.1), &rgb(color));
                }
            }
        }

        Ok(())
    }

    fn update_face<F0, F1>(
        &mut self,
        shape_id: ShapeId,
        shape: &mut Shape,
        mut update_face: F0,
        mut update_vert: F1,
    ) -> Result<()>
    where
        F0: FnMut(&mut iFC_Face) -> Result<()>,
        F1: FnMut(usize, &mut ShapeVertex) -> Result<()>,
    {
        for uuid in self.selected_faces.clone().into_iter() {
            // Update the code.
            let face = self.face_mut(&uuid, shape_id, shape)?;
            update_face(face)?;

            // Update the GPU representation.
            let face_loop = shape.get_faces(shape_id)[&uuid].face_loop().to_owned();
            let verts = shape.get_vertices_mut(shape_id);
            for (i, vert_index) in face_loop.iter().enumerate() {
                update_vert(i, &mut verts[*vert_index])?;
            }
        }
        Ok(())
    }

    fn edit_face_color(
        &self,
        shape_id: ShapeId,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        fields.push(if !self.selecting_face_color {
            FormField::new_missing("face_color_toggle")?
                .with_label("Face Color")
                .with_hover_text("The base color of this face.")
                .with_display(FieldDisplay::PaletteColorButton(
                    self.face(&self.selected_faces[0], shape_id, shape)?.color(),
                ))
        } else {
            FormField::new_missing("face_color_selector")?
                .with_label("Face Color")
                .with_hover_text("Select a new base color for this face.")
                .with_display(FieldDisplay::PaletteColorSelector(
                    self.face(&self.selected_faces[0], shape_id, shape)?.color(),
                ))
        });
        Ok(())
    }

    fn edit_content_flags(
        &self,
        shape_id: ShapeId,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        let flags = self
            .face(&self.selected_faces[0], shape_id, shape)?
            .content_flags();
        use FaceContentFlags as FCF;
        fn make_field(flags: FCF, flag: FCF) -> Result<FormField> {
            Ok(
                FormField::new_missing(format!("face_content_flag_{flag:?}"))?
                    .with_label(flag.to_string())
                    .with_display(FieldDisplay::Checkbox(flags.contains(flag))),
            )
        }
        fields.extend([
            FormField::new_h2("face_content_flags_header")?.with_label("Face Content Flags:"),
            make_field(flags, FCF::UNK1)?,
            make_field(flags, FCF::HAVE_FACE_NORMAL)?.inactive(),
            make_field(flags, FCF::UNK2)?,
            make_field(flags, FCF::UNK3)?,
            make_field(flags, FCF::UNK4)?,
            make_field(flags, FCF::HAVE_TEXCOORDS)?.inactive(),
            make_field(flags, FCF::FILL_BACKGROUND)?,
            make_field(flags, FCF::UNK5)?,
        ]);
        Ok(())
    }

    fn edit_face_texcoords(
        &self,
        shape_id: ShapeId,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        fields.push(
            FormField::new_h2("face_texcoords_header")?
                .with_label("Tex Coords")
                .with_hover_text(
                    "Each vertices texture coordinate in the order specified on the face",
                ),
        );
        let face = self.face(&self.selected_faces[0], shape_id, shape)?;
        for (i, (s, t)) in face.tex_coords().iter().enumerate() {
            fields.push(
                FormField::new_missing(format!("face_texcoord_{i}"))?
                    .with_label(format!("Offset {i}"))
                    .with_hover_text(format!("Texture Coordinate {i}"))
                    .with_display(FieldDisplay::U16x2([*s, *t])),
            );
        }
        Ok(())
    }

    fn edit_face_normal(
        &self,
        shape_id: ShapeId,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        let face_uuid = self.selected_faces[0];
        let Some(normal) = self.face(&face_uuid, shape_id, shape)?.normal() else {
            return Ok(());
        };
        {
            // Skip edit if VxNormal instructions override the face normal
            let face_loop = shape.get_faces(shape_id)[&face_uuid].face_loop();
            let verts = shape.get_vertices(shape_id);
            for vert_index in face_loop {
                if verts[*vert_index].is_vertex_normal() {
                    return Ok(());
                }
            }
        }
        fields.push(
            FormField::new_missing("face_normal")?
                .with_label("Face Normal")
                .with_hover_text("The normal vector of this face.")
                .with_display(FieldDisplay::I8x3(*normal)),
        );
        Ok(())
    }

    fn edit_face_center(
        &self,
        shape_id: ShapeId,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        let face_uuid = self.selected_faces[0];
        let Some(center) = self.face(&face_uuid, shape_id, shape)?.center() else {
            return Ok(());
        };
        fields.push(
            FormField::new_missing("face_center")?
                .with_label("Face Center")
                .with_hover_text("The \"center\" location for this face.")
                .with_display(FieldDisplay::I16x3(*center)),
        );
        Ok(())
    }

    fn edit_face_residual(
        &self,
        shape_id: ShapeId,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        let face_uuid = self.selected_faces[0];
        let Some(residual) = self.face(&face_uuid, shape_id, shape)?.residual() else {
            return Ok(());
        };
        fields.push(
            FormField::new_missing("face_residual")?
                .with_label("Face Residual")
                .with_hover_text("The \"residual\" of this face. I don't know either.")
                .with_display(FieldDisplay::I8x3(*residual)),
        );
        Ok(())
    }

    fn show_layout_flags(
        &self,
        shape_id: ShapeId,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        let flags = self
            .face(&self.selected_faces[0], shape_id, shape)?
            .layout_flags();
        use FaceLayoutFlags as FLF;
        fn make_field(label: &str, hover: &str, flag: FLF, flags: FLF) -> Result<FormField> {
            Ok(
                FormField::new_missing(format!("face_layout_flag_{flag:?}"))?
                    .with_label(label)
                    .with_hover_text(hover)
                    .inactive()
                    .with_display(FieldDisplay::Checkbox(flags.contains(flag))),
            )
        }
        fields.extend([
            FormField::new_h2("Face Layout Flags")?
                .with_hover_text("The layout flags are an implementation detail."),
            make_field(
                "unk0*",
                "I'm not sure what this flag controls.\nOther flags in this byte control layout,\nbut we don't seem to need this for decoding",
                FLF::UNK0,
                flags,
            )?,
            make_field(
                "Word Indices",
                "Indices are represented on disk as a u16 (instead of a u8)",
                FLF::USE_SHORT_INDICES,
                flags,
            )?,
            make_field(
                "Byte Face Center",
                "Face center coords are represented on disk as a u8 (instead of a u16)",
                FLF::USE_BYTE_FACE_CENTER,
                flags,
            )?,
            make_field(
                "Byte TexCoords",
                "Tex coords for this face are represented as u8 (instead of u16)",
                FLF::USE_BYTE_TEXCOORDS,
                flags,
            )?,
        ]);
        Ok(())
    }

    fn show_face_info(
        &self,
        shape_id: ShapeId,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        let face = self.face(&self.selected_faces[0], shape_id, shape)?;
        fields.extend([
            FormField::new_h2("Face Info")?.with_hover_text("We cannot yet insert vertices."),
            FormField::new_missing("Indices")?
                .with_hover_text("The vertex indices into the vertex pool (not the VxBuf instruction) that comprise this Face.")
                .inactive()
                .with_display(FieldDisplay::Label(format!("{:?}", face.indices()))),
            FormField::new_missing("Usage")?
                .with_hover_text("The vertex usage tracks what kind of control instruction references this face.")
                .inactive()
                .with_display(FieldDisplay::Label(format!("{:?}", face.usage()))),
        ]);
        Ok(())
    }

    fn show_instr_info(
        &self,
        shape_id: ShapeId,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        let instr = self.instr(&self.selected_faces[0], shape_id, shape)?;
        fields.extend([
            FormField::new_h2("Instruction Info")?.with_hover_text(
                "Details about the underlying 0xFC data to help with hex editing.",
            ),
            FormField::new_missing("Instr Magic")?
                .with_display(FieldDisplay::Label(format!("{:0X?}", instr.magic()))),
            FormField::new_missing("Instr Offset")?
                .with_hover_text("Offset in bytes from the start of the CODE section")
                .with_display(FieldDisplay::Label(format!(
                    "{} (0x{:04X})",
                    instr.instr_offset(),
                    instr.instr_offset()
                ))),
            FormField::new_missing("Instr Size")?
                .with_hover_text("Number of bytes that comprise this face")
                .with_display(FieldDisplay::Label(format!("{} bytes", instr.instr_size()))),
        ]);
        Ok(())
    }

    pub fn get_edit_fields(
        &self,
        (shape_id, shape): (ShapeId, &Shape),
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        match self.selected_faces.len() {
            0 => {}
            1 => {
                fields.push(FormField::new_h1(format!(
                    "Editing Face at 0x{:04X}",
                    self.instr(&self.selected_faces[0], shape_id, shape)?
                        .instr_offset()
                ))?);
                self.edit_face_color(shape_id, shape, fields)?;
                self.edit_face_texcoords(shape_id, shape, fields)?;
                self.edit_face_normal(shape_id, shape, fields)?;
                self.edit_face_center(shape_id, shape, fields)?;
                self.edit_face_residual(shape_id, shape, fields)?;
                self.edit_content_flags(shape_id, shape, fields)?;

                // Informational
                self.show_layout_flags(shape_id, shape, fields)?;
                self.show_face_info(shape_id, shape, fields)?;
                self.show_instr_info(shape_id, shape, fields)?;
            }
            _ => {
                fields.push(FormField::new_h1(format!(
                    "Editing {} Faces",
                    self.selected_faces.len(),
                ))?);
                self.edit_face_color(shape_id, shape, fields)?;
                self.edit_content_flags(shape_id, shape, fields)?;
            }
        }
        Ok(())
    }

    pub fn apply_event(
        &mut self,
        event: &FormEvent,
        (shape_id, shape): (ShapeId, &mut Shape),
    ) -> Result<()> {
        use FaceContentFlags as FCF;
        let flags0 = self
            .face(&self.selected_faces[0], shape_id, shape)?
            .content_flags();
        let mut flags = flags0;
        match event.name() {
            "face_color_toggle" => {
                // We have to be not showing the picker to be showing this.
                self.selecting_face_color = true;
            }
            "face_color_selector" => {
                self.selecting_face_color = false;
                self.update_face(
                    shape_id,
                    shape,
                    |face| {
                        *face.color_mut() = event.next().u8()?;
                        Ok(())
                    },
                    |_loop_index, _vert| {
                        // vert.set_color(event.next().u8()?);
                        Ok(())
                    },
                )?;
            }
            "face_content_flag_FaceContentFlags(UNK1)" => {
                flags.set(FCF::UNK1, event.next().bool()?)
            }
            "face_content_flag_FaceContentFlags(HAVE_FACE_NORMAL)" => {
                flags.set(FCF::HAVE_FACE_NORMAL, event.next().bool()?)
            }
            "face_content_flag_FaceContentFlags(UNK2)" => {
                flags.set(FCF::UNK2, event.next().bool()?)
            }
            "face_content_flag_FaceContentFlags(UNK3)" => {
                flags.set(FCF::UNK3, event.next().bool()?)
            }
            "face_content_flag_FaceContentFlags(UNK4)" => {
                flags.set(FCF::UNK4, event.next().bool()?)
            }
            "face_content_flag_FaceContentFlags(HAVE_TEXCOORDS)" => {
                flags.set(FCF::HAVE_TEXCOORDS, event.next().bool()?)
            }
            "face_content_flag_FaceContentFlags(FILL_BACKGROUND)" => {
                flags.set(FCF::FILL_BACKGROUND, event.next().bool()?)
            }
            "face_content_flag_FaceContentFlags(UNK5)" => {
                flags.set(FCF::UNK4, event.next().bool()?)
            }
            "face_normal" => {
                self.update_face(
                    shape_id,
                    shape,
                    |face| {
                        face.set_normal(event.next().i8x3()?);
                        Ok(())
                    },
                    |_loop_index, vert| {
                        if !vert.is_vertex_normal() {
                            vert.set_fa_face_normal(&event.next().i8x3()?);
                        }
                        Ok(())
                    },
                )?;
            }
            "face_center" => {
                self.update_face(
                    shape_id,
                    shape,
                    |face| {
                        face.set_center(event.next().i16x3()?);
                        Ok(())
                    },
                    |_loop_index, _vert| Ok(()),
                )?;
            }
            "face_residual" => {
                self.update_face(
                    shape_id,
                    shape,
                    |face| {
                        face.set_residual(event.next().i8x3()?);
                        Ok(())
                    },
                    |_loop_index, _vert| Ok(()),
                )?;
            }
            name => {
                if let Some(index_str) = name.strip_prefix("face_texcoord_") {
                    let i = index_str.parse::<usize>()?;
                    let tc = event.next().u16x2()?;
                    self.update_face(
                        shape_id,
                        shape,
                        |face| {
                            face.set_tex_coord_arr(i, tc)?;
                            Ok(())
                        },
                        |face_loop_index, vert| {
                            if face_loop_index == i {
                                vert.set_raw_tex_coords(tc[0] as u32, tc[1] as u32);
                            }
                            Ok(())
                        },
                    )?;
                }
                bail!("face mode doesn't know how to handle event {name}")
            }
        }

        if flags != flags0 {
            self.update_face(
                shape_id,
                shape,
                |face| {
                    *face.content_flags_mut() = flags;
                    Ok(())
                },
                |_loop_index, vert| {
                    if flags.contains(FCF::FILL_BACKGROUND)
                        || flags.contains(FCF::UNK1)
                        || flags.contains(FCF::UNK5)
                    {
                        vert.set_is_blend_texture();
                    } else {
                        vert.unset_is_blend_texture();
                    }
                    Ok(())
                },
            )?;
        }
        Ok(())
    }

    pub fn show_face_edit_ux(
        &mut self,
        (entity_id, frame): (Entity, &Frame),
        (shape_id, shape): (ShapeId, &Shape),
        markers: &mut Markers,
        heap: HeapMut,
    ) -> Result<()> {
        self.show_face_loops(entity_id, frame, shape_id, shape, markers, heap)?;
        Ok(())
    }
}

// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    edit_sh::face_mode::matrix_for_face,
    widget::{FieldDisplay, FormEvent, FormField},
};
use absolute_unit::prelude::*;
use anyhow::{bail, Result};
use bevy_ecs::prelude::*;
use camera::ScreenCamera;
use csscolorparser::Color;
use geometry::{intersect::sphere_vs_ray, Plane, Ray, Sphere};
use itertools::Itertools;
use marker::Markers;
use nitrous::HeapMut;
use ordered_float::OrderedFloat;
use phase::Frame;
use sh::InstrDetail;
use shape::{Shape, ShapeId};
use std::collections::{BTreeMap, HashSet};
use uuid::Uuid;

// Holds all locations where a vertex is referenced.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct VertexRef {
    vb_offset: usize,
    face: Uuid,
    vxbuf: (Uuid, usize),
}

// A set of Co-Located vertices from the vertex buffer (get_vertices).
// This collects all the faces that associate these vertices, all locations in
// the vertex buffer and all vxbuf locations. Typically one vxbuf location will
// map to many faces, so the expectation is there will be many references to
// the same vxbuf address. That's not a requirement, however: e.g. if there is
// a different vertex normal for adjacent faces.
#[derive(Clone, Debug)]
pub struct CoVertex {
    position: Pt3<Meters>,
    vertices: Vec<VertexRef>,
}

impl CoVertex {
    pub fn get_fa_vertex_position(&self, shape_id: ShapeId, shape: &Shape) -> Result<[i16; 3]> {
        let (vxbuf_uuid, vxbuf_offset) = self.vertices[0].vxbuf;
        let InstrDetail::VertexBuffer(vxbuf) =
            shape.get_code(shape_id).instruction(&vxbuf_uuid)?.detail()
        else {
            bail!("expected a vertex buffer at {vxbuf_uuid}");
        };
        Ok(*vxbuf.vertex(vxbuf_offset))
    }

    pub fn update_position(
        &mut self,
        next: [i16; 3],
        shape_id: ShapeId,
        shape: &mut Shape,
    ) -> Result<()> {
        for vxref in &self.vertices {
            let (vxbuf_uuid, vxbuf_offset) = vxref.vxbuf;
            let InstrDetail::VertexBuffer(vxbuf) = shape
                .get_code_mut(shape_id)
                .instruction_mut(&vxbuf_uuid)?
                .detail_mut()
            else {
                bail!("expected a vertex buffer at {vxbuf_uuid}");
            };
            *vxbuf.vertex_mut(vxbuf_offset) = next;

            let vert = &mut shape.get_vertices_mut(shape_id)[vxref.vb_offset];
            vert.set_fa_position(&next);
        }

        let updated = shape.get_vertices(shape_id)[self.vertices[0].vb_offset].position();
        self.position = updated;

        Ok(())
    }
}

#[derive(Debug, Default)]
pub struct VertexMode {
    vertex_group_move: [i16; 3],
    selected_vertices: Vec<CoVertex>,
}

impl VertexMode {
    fn position_to_co_vert(
        pos: &Pt3<Meters>,
        shape_id: ShapeId,
        shape: &Shape,
    ) -> Result<Option<CoVertex>> {
        let mut vert_refs = vec![];
        let verts = shape.get_vertices(shape_id);
        let sh_code = shape.get_code(shape_id);
        for (face_uuid, face_info) in shape.get_faces(shape_id) {
            let InstrDetail::Face(face_detail) = &sh_code.instruction(face_uuid)?.detail() else {
                bail!("expected face_uuid to be a face!");
            };
            for (loop_offset, index) in face_info.face_loop().iter().enumerate() {
                if verts[*index].position() == *pos {
                    vert_refs.push(VertexRef {
                        vb_offset: *index,
                        face: *face_uuid,
                        vxbuf: (
                            *face_info.vxbuf_uuid(),
                            face_info.vxbuf_offset(loop_offset, face_detail),
                        ),
                    });
                }
            }
        }
        if vert_refs.is_empty() {
            return Ok(None);
        }
        Ok(Some(CoVertex {
            position: *pos,
            vertices: vert_refs,
        }))
    }

    // Use point intersection on mouse-press to immediately select
    // whatever is under the cursor.
    pub fn ray_select(
        &mut self,
        cursor: (f64, f64),
        shift_pressed: bool,
        (entity_id, frame): (Entity, &Frame),
        (shape_id, shape): (ShapeId, &mut Shape),
        mut heap: HeapMut,
    ) -> Result<()> {
        let camera = heap.resource::<ScreenCamera>();
        let wrld_m = camera.mouse_to_world(cursor);

        // The ray is in world space.
        let ray = Ray::new(
            camera.position::<Meters>(),
            (wrld_m.dvec3() - camera.position::<Meters>().dvec3()).normalize(),
        );

        // Transform from model to world space for the shape.
        let p = frame.position();
        let q = frame.facing();

        // Note: this will select all co-located vertices, but may select multiple loci
        // of co-vertices: e.g. if multiple points intersect the ray. We only want to select
        // the closest co-vertices, so we take the closest. We have to revisit all verts
        // via the faces anyway in order to discover the associated faces.
        let mut intersections = vec![];
        let verts = shape.get_vertices(shape_id);
        let mut visited = HashSet::with_capacity(verts.len());
        for face_info in shape.get_faces(shape_id).values() {
            let m = matrix_for_face(face_info, entity_id, shape_id, shape, heap.as_mut())?;
            for index in face_info.face_loop() {
                if !visited.insert(*index) {
                    continue;
                }
                let v = &verts[*index];
                let pt = p + q * v.position().transform_by(&m);
                let sphere = Sphere::from_center_and_radius(pt, meters!(0.25));
                if sphere_vs_ray(&sphere, &ray).is_some() {
                    intersections.push((v.position(), index));
                }
            }
        }

        // FIXME: we should be able to check if the mouse is in a UX element. If we just
        //        naively deselect on click, trying to edit will unselect the thing we are
        //        trying to edit!
        if intersections.is_empty() {
            return Ok(());
        }

        // Get the closest location.
        intersections.sort_by_key(|(intersect, _)| {
            OrderedFloat((intersect.dvec3() - wrld_m.dvec3()).length())
        });
        let (pos, _) = intersections.first().unwrap();

        let Some(co_vert) = Self::position_to_co_vert(pos, shape_id, shape)? else {
            bail!("expected to find a co-vertex for an already found position");
        };

        if !shift_pressed {
            self.selected_vertices = vec![co_vert];
            self.vertex_group_move = Default::default();
        } else if self
            .selected_vertices
            .iter()
            .map(|v| v.position)
            .collect::<Vec<_>>()
            .contains(&co_vert.position)
        {
            self.selected_vertices
                .retain(|v| v.position != co_vert.position);
            self.vertex_group_move = Default::default();
        } else {
            self.selected_vertices.push(co_vert);
            self.vertex_group_move = Default::default();
        }

        Ok(())
    }

    // Select whatever is in the current drag region.
    pub fn region_select(
        &mut self,
        planes: &[Plane<Meters>; 4],
        shift_pressed: bool,
        (entity_id, frame): (Entity, &Frame),
        (shape_id, shape): (ShapeId, &mut Shape),
        mut heap: HeapMut,
    ) -> Result<()> {
        // Transform from model to world space for the shape.
        let p = frame.position();
        let q = frame.facing();

        // Note: this is the slice of vertices specific to this shape.
        let mut intersections = vec![];
        let verts = shape.get_vertices(shape_id);
        let mut visited = HashSet::with_capacity(verts.len());
        for face_info in shape.get_faces(shape_id).values() {
            let m = matrix_for_face(face_info, entity_id, shape_id, shape, heap.as_mut())?;
            for index in face_info.face_loop() {
                if !visited.insert(*index) {
                    continue;
                }
                let v = &verts[*index];
                let pt = p + q * v.position().transform_by(&m);
                if planes.iter().all(|plane| plane.point_is_in_front(&pt)) {
                    intersections.push(v.position());
                }
            }
        }
        let uniq_intersections = intersections
            .iter()
            .unique_by(|v| OrderedFloat((v.at(0) + v.at(1) + v.at(2)).f64()))
            .collect::<Vec<_>>();
        let mut co_verts = vec![];
        for pos in &uniq_intersections {
            let Some(co_vert) = Self::position_to_co_vert(pos, shape_id, shape)? else {
                bail!("expected to find a co-vertex for an already found position");
            };
            co_verts.push(co_vert);
        }

        // FIXME: we should be able to check if the mouse is in a UX element. If we just
        //        naively deselect on click, trying to edit will unselect the thing we are
        //        trying to edit.
        if !shift_pressed && !co_verts.is_empty() {
            self.selected_vertices = co_verts;
        } else {
            co_verts.retain(|cv| {
                !self
                    .selected_vertices
                    .iter()
                    .any(|sv| sv.position == cv.position)
            });
            self.selected_vertices.extend(co_verts);
        }

        Ok(())
    }

    fn show_vertices(
        &self,
        entity_id: Entity,
        frame: &Frame,
        shape_id: ShapeId,
        shape: &Shape,
        markers: &mut Markers,
        mut heap: HeapMut,
    ) -> Result<()> {
        // by index
        let verts = shape.get_vertices(shape_id);
        let mut visited = HashSet::with_capacity(verts.len());
        for face_info in shape.get_faces(shape_id).values() {
            let m = matrix_for_face(face_info, entity_id, shape_id, shape, heap.as_mut())?;
            for index in face_info.face_loop() {
                if !visited.insert(*index) {
                    continue;
                }
                let v = &verts[*index];
                let p = frame.position() + frame.facing() * v.position().transform_by(&m);
                let contains_vertex = self
                    .selected_vertices
                    .iter()
                    .any(|co_vert| co_vert.position == v.position());
                let (radius, color) = if contains_vertex {
                    if v.is_vertex_normal() {
                        let dir = frame.facing() * v.normal().as_dvec3();
                        markers.draw_arrow(
                            &Color::from_rgba8(255, 128, 0, 255),
                            p,
                            dir,
                            meters!(1),
                            meters!(0.1),
                        );
                    }
                    (meters!(0.25), Color::from_rgba8(255, 0, 255, 255))
                } else {
                    (meters!(0.15), Color::from_rgba8(0, 255, 255, 255))
                };
                markers.draw_point(p, radius, &color);
            }
        }
        Ok(())
    }

    fn edit_vertex_group(&self, fields: &mut Vec<FormField>) -> Result<()> {
        fields.push(
            FormField::new_missing("vert_rel_move_group")?
                .with_label("Move Selected Vertices")
                .with_hover_text("A relative move input, not actual vertex data")
                .with_display(FieldDisplay::I16x3(self.vertex_group_move)),
        );
        Ok(())
    }

    fn edit_co_vertex(
        &self,
        i: usize,
        co_vertex: &CoVertex,
        shape_id: ShapeId,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        let code = shape.get_code(shape_id);
        let mut vxbufs = BTreeMap::new();
        for vert_ref in &co_vertex.vertices {
            let (vxbuf_uuid, vxbuf_offset) = vert_ref.vxbuf;
            vxbufs
                .entry(vxbuf_uuid)
                .or_insert_with(Vec::new)
                .push(vxbuf_offset);
        }
        for (vxbuf_uuid, offsets) in &vxbufs {
            let instr = code.instruction(vxbuf_uuid)?;
            fields.push(
                FormField::new_missing(format!(
                    "VxBuf Code Offset 0x{:04X}",
                    instr.instr_offset()
                ))?
                .with_hover_text("Informationa about all the overlapping vertices at this position")
                .inactive()
                .with_display(FieldDisplay::Label(format!("0x{:?}", offsets))),
            );
        }
        fields.push(
            FormField::new_missing(format!("vert_position_{i}"))?
                .with_hover_text("Change the position of this vertex")
                .with_display(FieldDisplay::I16x3(
                    co_vertex.get_fa_vertex_position(shape_id, shape)?,
                )),
        );
        Ok(())
    }

    pub fn get_edit_fields(
        &self,
        (shape_id, shape): (ShapeId, &Shape),
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        if !self.selected_vertices.is_empty() {
            self.edit_vertex_group(fields)?;
        }
        for (i, co_vert) in self.selected_vertices.iter().enumerate() {
            fields.push(FormField::new_h2(format!(
                "{} Co-Vertices",
                co_vert.vertices.len()
            ))?);
            self.edit_co_vertex(i, co_vert, shape_id, shape, fields)?;
        }
        Ok(())
    }

    pub fn apply_event(
        &mut self,
        event: &FormEvent,
        (shape_id, shape): (ShapeId, &mut Shape),
    ) -> Result<()> {
        match event.name() {
            "vert_rel_move_group" => {
                // Add the change to our current position.
                let next = event.next().i16x3()?;
                for co_vertex in &mut self.selected_vertices {
                    let mut orig = co_vertex.get_fa_vertex_position(shape_id, shape)?;
                    for i in 0..3 {
                        orig[i] = orig[i].saturating_add(self.vertex_group_move[i] - next[i]);
                    }
                    co_vertex.update_position(orig, shape_id, shape)?;
                }
                self.vertex_group_move = next;
            }
            name => {
                if let Some(index_str) = name.strip_prefix("vert_position_") {
                    let i = index_str.parse::<usize>()?;
                    let next = event.next().i16x3()?;
                    if let Some(co_vertex) = self.selected_vertices.get_mut(i) {
                        co_vertex.update_position(next, shape_id, shape)?;
                    }
                }
                bail!("unrecognized event name in vertex mode: {name}")
            }
        }
        Ok(())
    }

    pub fn show_vertex_edit_ux(
        &mut self,
        (entity_id, frame): (Entity, &Frame),
        (shape_id, shape): (ShapeId, &Shape),
        markers: &mut Markers,
        heap: HeapMut,
    ) -> Result<()> {
        self.show_vertices(entity_id, frame, shape_id, shape, markers, heap)?;
        Ok(())
    }
}

// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
mod face_mode;
mod vertex_mode;

use crate::{
    edit_sh::{face_mode::FaceMode, vertex_mode::VertexMode},
    widget::{FieldRef, Form, FormEvent, FormField, TabSet, Widget},
    DocumentEditProvider, DocumentImportProvider, EditState, EditableDocument, PanelKind,
};
use absolute_unit::{approx::relative_eq, prelude::*};
use animate::Timeline;
use anyhow::{bail, Result};
use arcball::ArcBallController;
use asset_loader::AssetLoader;
use bevy_ecs::prelude::*;
use camera::ScreenCamera;
use catalog::{AssetCollection, FileInfo, FileSystem, Search};
use csscolorparser::Color;
use egui::Ui;
use event_mapper::{EventMapper, InputFocus};
use game_ux::UxState;
use geodesy::{Bearing, Geodetic, PitchCline};
use geometry::Plane;
use glam::DVec3;
use installations::GameInstallation;
use mantle::Gpu;
use marker::{rgb, Markers};
use nitrous::{HeapMut, Value};
use ordered_float::OrderedFloat;
use orrery::Orrery;
use pal::Palette;
use phase::{Frame, MapFrame};
use runtime::PlayerMarker;
use sh::{DrawSelection, GearFootprintKind, ShCode, ShExtent};
use shape::{Shape, ShapeIds};
use smallvec::{smallvec, SmallVec};
use std::{collections::HashMap, sync::Arc, time::Duration};
use vehicle::{
    AirbrakeControl, AirbrakeEffector, Airframe, BayControl, BayEffector, FlapsControl,
    FlapsEffector, FuelSystem, FuelTank, FuelTankKind, GearControl, GearEffector, GliderEngine,
    HookControl, HookEffector, PitchInceptor, PowerSystem, RollInceptor, ThrottleInceptor,
    ThrustVectorPitchControl, ThrustVectorPitchEffector, ThrustVectorYawControl,
    ThrustVectorYawEffector, VtolAngleControl, VtolAngleEffector, YawInceptor,
};

const LATITUDE: f64 = 58.287_f64;
const LONGITUDE: f64 = 25.641_f64;
const ALTITUDE: f64 = 20_000.0_f64;

#[derive(Debug)]
pub enum ShapeEditMode {
    Face(FaceMode),
    Vertex(VertexMode),
}

impl ShapeEditMode {
    pub fn label(&self) -> &'static str {
        match self {
            Self::Face(_) => "Face",
            Self::Vertex(_) => "Vertex",
        }
    }
}

// impl Display for ShapeEditMode {
//
// }

#[derive(Clone, Debug, Default)]
pub struct ShapeEdit;

impl EditableDocument for ShapeEdit {
    fn make_importer(&self) -> Box<dyn DocumentImportProvider> {
        Box::<ShapeImporter>::default() as Box<dyn DocumentImportProvider>
    }
    fn object_menu(&self, _ui: &mut Ui) -> Option<EditState> {
        None
    }
    fn graphics_menu(&self, ui: &mut Ui) -> Option<EditState> {
        self.waiting_menu(ui)
    }
    fn waiting_menu(&self, ui: &mut Ui) -> Option<EditState> {
        if ui.button(".SH [Shape]").clicked() {
            return Some(EditState::ImportDocument(self.make_importer()));
        }
        None
    }
    fn document_actions(
        &self,
        ui: &mut Ui,
        info: FileInfo,
        install: &GameInstallation,
        collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<Option<EditState>> {
        if info.name().ends_with(".SH") && ui.button("Edit").clicked() {
            return Ok(Some(
                ShapeImporter::default().import(info, install, collection, heap)?,
            ));
        }
        Ok(None)
    }
}

#[derive(Debug, Default)]
pub struct ShapeImporter {
    cache: HashMap<String, ShCode>,
}

impl DocumentImportProvider for ShapeImporter {
    fn all_documents<'a>(&self, collection: &'a AssetCollection) -> Result<Vec<FileInfo<'a>>> {
        collection.search(Search::for_extension("SH"))
    }

    fn column_headers(&self) -> SmallVec<[&'static str; 8]> {
        smallvec![
            "Name", "# Instr", "Damage", "Anim", "AB", "Bay", "Gear", "Wing", "Flaps", "TV", "VT"
        ]
    }

    fn column_values(
        &mut self,
        info: FileInfo,
        _install: &GameInstallation,
        _collection: &AssetCollection,
        _heap: HeapMut,
    ) -> Result<SmallVec<[String; 8]>> {
        let code = self
            .cache
            .entry(info.to_string())
            .or_insert_with(|| ShCode::from_bytes(&info.data().unwrap()).unwrap());
        let caps = code.analysis(DrawSelection::NormalModel)?.capabilities();
        fn two_opt(b: bool) -> String {
            match b {
                false => "",
                true => "✔",
            }
            .to_owned()
        }
        fn three_opt(toggle: bool, anim: bool) -> String {
            match toggle {
                false => "",
                true => match anim {
                    false => "✔",
                    true => "▶",
                },
            }
            .to_owned()
        }
        Ok(smallvec![
            info.name().to_owned(),
            code.code().len().to_string(),
            two_opt(caps.has_damage_model()),
            two_opt(caps.has_animation()),
            two_opt(caps.has_afterburner()),
            three_opt(caps.has_bay(), caps.has_dynamic_bay()),
            three_opt(caps.has_gear(), caps.has_dynamic_gear()),
            two_opt(caps.has_dynamic_wing()),
            two_opt(caps.has_flaps()),
            two_opt(caps.has_dynamic_tv()),
            two_opt(caps.has_dynamic_vt()),
        ])
    }

    fn import(
        &mut self,
        info: FileInfo,
        install: &GameInstallation,
        collection: &AssetCollection,
        mut heap: HeapMut,
    ) -> Result<EditState> {
        heap.resource_scope(|heap: HeapMut, mut shape: Mut<Shape>| {
            Ok(EditState::EditDocument(Box::new(ShapeEditState::new(
                info,
                install,
                collection,
                heap.resource::<Gpu>(),
                &mut shape,
            )?)
                as Box<dyn DocumentEditProvider>))
        })
    }
}

// Don't mind the dust!
#[allow(unused)]
#[derive(Debug)]
pub(crate) struct ShapeEditState {
    // Core data
    name: String,
    shape_ids: ShapeIds,

    // For unwinding
    entity: Entity,
    init_position: MapFrame,
    init_distance: Length<Meters>,

    // UX state
    edit_mode: ShapeEditMode,

    // Draw state
    cursor: (f64, f64),
    selection_press_start: Option<(f64, f64)>,
    selection_rectangle: Option<[Pt3<Meters>; 4]>,
    palette: Palette,
    frame: Frame,
    extent: ShExtent,
    show_extents: bool,
}

impl DocumentEditProvider for ShapeEditState {
    fn plugin(&self) -> &'static str {
        "SH"
    }

    fn panel_requirements(&self) -> PanelKind {
        PanelKind::Side
    }

    fn show_custom_document_edit_ux(&mut self, mut heap: HeapMut) -> Result<()> {
        heap.resource_scope(|mut heap: HeapMut, shape: Mut<Shape>| {
            heap.resource_scope(|heap: HeapMut, mut markers: Mut<Markers>| {
                self.show_edit_ui(&shape, &mut markers, heap)
            })
        })
    }

    fn get_edit_widgets(&mut self, mut heap: HeapMut) -> Result<Vec<Widget>> {
        let tabs = TabSet::from_iter(
            "tabs",
            self.edit_mode.label(),
            ["Face", "Vertex"].into_iter(),
        );
        let mut fields = vec![
            FormField::new("display_name", self)?.with_label("Name"),
            FormField::new("display_extents", self)?.with_label("Show Extents"),
        ];
        heap.resource_scope(
            |_heap: HeapMut, mut shape: Mut<Shape>| match &self.edit_mode {
                ShapeEditMode::Face(mode) => {
                    mode.get_edit_fields((self.shape_ids.normal(), &mut shape), &mut fields)
                }
                ShapeEditMode::Vertex(mode) => {
                    mode.get_edit_fields((self.shape_ids.normal(), &mut shape), &mut fields)
                }
            },
        )?;
        Ok(vec![
            Widget::H1(self.name.clone()),
            Widget::Tabs(tabs),
            Widget::Form(Form::from_table("_display_form", fields)),
        ])
    }

    fn get_field_ref(&mut self, name: &str) -> Result<FieldRef> {
        Ok(match name {
            "tabs" => FieldRef::Tab(self.edit_mode.label().to_owned()),
            // display area
            "display_name" => FieldRef::Line(&mut self.name),
            "display_extents" => FieldRef::Checkbox(&mut self.show_extents),
            name => bail!("unknown field name {name}"),
        })
    }

    fn apply_event(&mut self, event: &FormEvent, mut heap: HeapMut) -> Result<()> {
        match event.name() {
            "tabs" => match event.next().string()? {
                "Face" => self.edit_mode = ShapeEditMode::Face(FaceMode::default()),
                "Vertex" => self.edit_mode = ShapeEditMode::Vertex(VertexMode::default()),
                name => bail!("unknown tab mode {name} in shape edit apply_event"),
            },
            _ => {
                heap.resource_scope(|_heap: HeapMut, mut shape: Mut<Shape>| {
                    match &mut self.edit_mode {
                        ShapeEditMode::Face(mode) => {
                            mode.apply_event(event, (self.shape_ids.normal(), &mut shape))
                        }
                        ShapeEditMode::Vertex(mode) => {
                            mode.apply_event(event, (self.shape_ids.normal(), &mut shape))
                        }
                    }
                })?;
            }
        }
        Ok(())
    }

    // FIXME: we also need to serialize the images
    fn serialize(&self, heap: HeapMut) -> Result<(String, Vec<u8>)> {
        let sh_code = heap.resource::<Shape>().get_code(self.shape_ids.normal());
        Ok((self.name.clone(), sh_code.compile_pe()?))
    }

    fn enter_document_edit(&mut self, camera_ent: Entity, mut heap: HeapMut) -> Result<()> {
        // Just jump to the target to avoid flickering like crazy if it's too far off.
        // TODO: figure out local times and animate within a day cycle
        heap.resource_mut::<Orrery>().set_unix_ms(13459754321.0);

        let arcball = heap.get_by_id_mut::<ArcBallController>(camera_ent)?;
        let init_position = arcball.map_frame().to_owned();
        let init_distance = arcball.distance();
        let tgt_position = MapFrame::new(
            Geodetic::new(degrees!(LATITUDE), degrees!(LONGITUDE), feet!(ALTITUDE)),
            Bearing::new(degrees!(-113)),
            PitchCline::new(degrees!(28.)),
        );
        let tgt_distance = meters!(40);
        self.init_distance = init_distance;
        self.init_position = init_position.clone();
        heap.resource_mut::<Timeline>().ease_in_out_to(
            Value::RustMethod(Arc::new(move |v: &[Value], mut heap| -> Result<Value> {
                let f = v[0].to_float()?;
                let mut arcball = heap.get_by_id_mut::<ArcBallController>(camera_ent)?;
                arcball.set_map_frame(init_position.interpolate(&tgt_position, f));
                arcball
                    .set_distance(init_distance + scalar!(f) * (tgt_distance - init_distance))?;
                Ok(Value::True())
            })),
            0.0.into(),
            1.0.into(),
            seconds!(1.5).f64(),
        )?;

        // Make sure the Baltic map is loaded.
        heap.resource_scope(|heap, game: Mut<AssetLoader>| game.load_map("BAL.MM", heap))?;

        // Create the shape's proxy entity
        let (shape_bundle, flag_bundle, xform_bundle) = heap.resource::<Shape>().shape_bundle(
            &self.name,
            self.shape_ids.clone(),
            Some(GearFootprintKind::Nose),
        )?;
        self.extent = shape_bundle.5.clone();

        let fuel = FuelSystem::default()
            .with_internal_tank(FuelTank::new(FuelTankKind::Center, kilograms!(0_f64)))?;
        let power = PowerSystem::default().with_engine(GliderEngine::default());
        self.entity = {
            let mut ent = heap.spawn("Player")?;
            ent.inject((
                PlayerMarker,
                self.frame.clone(),
                Airframe::new(kilograms!(10_f64)),
                fuel,
                power,
                (
                    PitchInceptor::default(),
                    RollInceptor::default(),
                    YawInceptor::default(),
                    ThrottleInceptor::new_min_power(),
                    ThrustVectorPitchControl::new(degrees!(-30), degrees!(30), degrees!(1)),
                    ThrustVectorYawControl::new(degrees!(-30), degrees!(30), degrees!(1)),
                    VtolAngleControl::new(degrees!(-100), degrees!(10), degrees!(10)),
                    AirbrakeControl::default(),
                    BayControl::default(),
                    GearControl::default(),
                    FlapsControl::default(),
                    HookControl::default(),
                ),
                (
                    ThrustVectorPitchEffector::new(0., Duration::from_millis(1)),
                    ThrustVectorYawEffector::new(0., Duration::from_millis(1)),
                    VtolAngleEffector::new(0., Duration::from_secs(1)),
                    AirbrakeEffector::new(0., Duration::from_millis(1)),
                    BayEffector::new(0., Duration::from_secs(2)),
                    FlapsEffector::new(0., Duration::from_millis(1)),
                    GearEffector::new(0., Duration::from_secs(4)),
                    HookEffector::new(0., Duration::from_millis(1)),
                ),
                shape_bundle,
            ))?;
            if let Some(flag_bundle) = flag_bundle {
                ent.inject(flag_bundle)?;
            }
            if let Some(xform_bundle) = xform_bundle {
                ent.inject(xform_bundle)?;
            }
            ent.id()
        };

        heap.resource_mut::<EventMapper>()
            .set_input_focus(InputFocus::new(
                UxState::EditLib.as_str().to_owned() + ":shape",
            ));

        Ok(())
    }

    fn leave_document_edit(&self, camera_ent: Entity, mut heap: HeapMut) -> Result<()> {
        assert_ne!(self.entity, Entity::PLACEHOLDER);

        let arcball = heap.get_by_id_mut::<ArcBallController>(camera_ent)?;
        let init_position = arcball.map_frame().to_owned();
        let init_distance = arcball.distance();
        let tgt_position = self.init_position.clone();
        let tgt_distance = self.init_distance;
        heap.resource_mut::<Timeline>().ease_in_out_to(
            Value::RustMethod(Arc::new(move |v: &[Value], mut heap| -> Result<Value> {
                let f = v[0].to_float()?;
                let mut arcball = heap.get_by_id_mut::<ArcBallController>(camera_ent)?;
                arcball.set_map_frame(init_position.interpolate(&tgt_position, f));
                arcball
                    .set_distance(init_distance + scalar!(f) * (tgt_distance - init_distance))?;
                Ok(Value::True())
            })),
            0.0.into(),
            1.0.into(),
            seconds!(1.5).f64(),
        )?;

        heap.resource_scope(|heap, game: Mut<AssetLoader>| game.unload_mission(heap))?;
        heap.despawn_by_id(self.entity);

        heap.resource_mut::<EventMapper>()
            .set_input_focus(UxState::EditLib.focus());

        Ok(())
    }

    fn mouse_motion(
        &mut self,
        cursor_x: f64,
        cursor_y: f64,
        shift_pressed: bool,
        mut heap: HeapMut,
    ) -> Result<()> {
        // Origin is top-left corner
        self.cursor = (cursor_x, cursor_y);

        // Region select
        if let Some(origin) = self.selection_press_start {
            let cursor = self.cursor;

            // Early exit if the selection is empty
            if relative_eq!(origin.0, cursor.0) || relative_eq!(origin.1, cursor.1) {
                self.selection_rectangle = None;
                return Ok(());
            }

            // Get the 4 points of the selection, sorted clockwise.
            let mut points = [
                (OrderedFloat(origin.0), OrderedFloat(origin.1)),
                (OrderedFloat(cursor.0), OrderedFloat(origin.1)),
                (OrderedFloat(origin.0), OrderedFloat(cursor.1)),
                (OrderedFloat(cursor.0), OrderedFloat(cursor.1)),
            ]
            .to_vec();
            points.sort();
            points.swap(2, 3);

            // Move all points into world space.
            let points = points
                .into_iter()
                .map(|p| {
                    heap.resource::<ScreenCamera>()
                        .mouse_to_world((p.0.into(), p.1.into()))
                })
                .collect::<Vec<_>>();
            let region = [points[0], points[1], points[2], points[3]];

            // Build planes from our camera origin and pairs of points.
            let position = heap.resource::<ScreenCamera>().position::<Meters>();
            let planes = [
                Plane::from_triangle(&position, &region[0], &region[1]),
                Plane::from_triangle(&position, &region[1], &region[2]),
                Plane::from_triangle(&position, &region[2], &region[3]),
                Plane::from_triangle(&position, &region[3], &region[0]),
            ];

            heap.resource_scope(|heap: HeapMut, mut shape: Mut<Shape>| {
                match &mut self.edit_mode {
                    ShapeEditMode::Face(mode) => mode.region_select(
                        &planes,
                        shift_pressed,
                        (self.entity, &self.frame),
                        (self.shape_ids.normal(), &mut shape),
                        heap,
                    ),
                    ShapeEditMode::Vertex(mode) => mode.region_select(
                        &planes,
                        shift_pressed,
                        (self.entity, &self.frame),
                        (self.shape_ids.normal(), &mut shape),
                        heap,
                    ),
                }
            })?;
            self.selection_rectangle = Some(region);
        }
        Ok(())
    }

    fn mouse_press(&mut self, pressed: bool, shift_pressed: bool, mut heap: HeapMut) -> Result<()> {
        if !pressed {
            self.selection_press_start = None;
            self.selection_rectangle = None;
            return Ok(());
        }

        if self.selection_press_start.is_none() && pressed {
            self.selection_press_start = Some(self.cursor);
            heap.resource_scope(|heap: HeapMut, mut shape: Mut<Shape>| {
                match &mut self.edit_mode {
                    ShapeEditMode::Face(mode) => mode.ray_select(
                        self.cursor,
                        shift_pressed,
                        (self.entity, &self.frame),
                        (self.shape_ids.normal(), &mut shape),
                        heap,
                    ),
                    ShapeEditMode::Vertex(mode) => mode.ray_select(
                        self.cursor,
                        shift_pressed,
                        (self.entity, &self.frame),
                        (self.shape_ids.normal(), &mut shape),
                        heap,
                    ),
                }
            })?;
        }

        Ok(())
    }
}

impl ShapeEditState {
    pub(crate) fn new(
        info: FileInfo,
        install: &GameInstallation,
        collection: &AssetCollection,
        gpu: &Gpu,
        shape: &mut Shape,
    ) -> Result<Self> {
        // Note: we upload here to pin the ShCode and other resources and store pointers here.
        // We can't create the entities at this point, however, so wait for later.
        // It's also worth noting that we do not use the cached shape in any way, so any
        // uuid or other reference that we pull from there is not going to interact.
        let shape_ids = shape.upload_shape(install.palette(), info, collection, gpu)?;

        Ok(Self {
            name: info.name().to_owned(),
            shape_ids,
            entity: Entity::PLACEHOLDER,
            init_distance: meters!(0),
            init_position: MapFrame::default(),
            edit_mode: ShapeEditMode::Face(FaceMode::default()),
            cursor: (0., 0.),
            selection_press_start: None,
            selection_rectangle: None,
            palette: install.palette().to_owned(),
            frame: Frame::from_geodetic_bearing_and_pitch(
                Geodetic::new(degrees!(LATITUDE), degrees!(LONGITUDE), feet!(ALTITUDE)),
                Bearing::new(degrees!(0_f64)),
                PitchCline::new(degrees!(0_f64)),
            ),
            extent: ShExtent::default(),
            show_extents: false,
        })
    }

    fn draw_extents(extent: &ShExtent, frame: &Frame, markers: &mut Markers) {
        for pos in extent.gear_touchpoints() {
            let pos = frame.position() + frame.facing() * *pos;
            markers.draw_arrow(
                &rgb(0xFFFFFF),
                pos,
                frame.facing() * DVec3::Y,
                *extent.gear_max_deflection(),
                meters!(0.3),
            );
        }

        markers.draw_bounding_box(
            &rgb(0xFF00FF),
            &rgb(0x00FF00),
            extent.aabb_body(),
            meters!(0.1),
            frame,
        );
        markers.draw_bounding_box(
            &rgb(0xFF7777),
            &rgb(0xFF0000),
            extent.aabb_afterburner(),
            meters!(0.05),
            frame,
        );
        markers.draw_bounding_box(
            &rgb(0x00FF00),
            &rgb(0x77FF77),
            extent.aabb_airbrake(),
            meters!(0.05),
            frame,
        );
        markers.draw_bounding_box(
            &rgb(0x0000FF),
            &rgb(0x7777FF),
            extent.aabb_bay(),
            meters!(0.05),
            frame,
        );
        markers.draw_bounding_box(
            &rgb(0xFF0000),
            &rgb(0xFF7777),
            extent.aabb_gear(),
            meters!(0.05),
            frame,
        );
        markers.draw_bounding_box(
            &rgb(0x220000),
            &rgb(0x440000),
            extent.aabb_gear_footprint(),
            meters!(0.05),
            frame,
        );
        markers.draw_bounding_box(
            &rgb(0x7777FF),
            &rgb(0xFF7777),
            extent.aabb_hook(),
            meters!(0.05),
            frame,
        );
    }

    pub(crate) fn show_edit_ui(
        &mut self,
        shape: &Shape,
        markers: &mut Markers,
        heap: HeapMut,
    ) -> Result<()> {
        if self.show_extents {
            Self::draw_extents(&self.extent, &self.frame, markers);
        }

        // Draw the selection rectangle
        if let Some(sel) = &self.selection_rectangle {
            let blue = &Color::new(0., 0., 1., 1.);
            let radius = meters!(0.002);
            for i in 0..4 {
                let j = (i + 1) % 4;
                markers.draw_point(sel[i], radius, blue);
                markers.draw_cylinder(blue, sel[i], sel[j], radius);
            }
        }

        match &mut self.edit_mode {
            ShapeEditMode::Face(mode) => mode.show_face_edit_ux(
                (self.entity, &self.frame),
                (self.shape_ids.normal(), shape),
                markers,
                heap,
            )?,
            ShapeEditMode::Vertex(mode) => mode.show_vertex_edit_ux(
                (self.entity, &self.frame),
                (self.shape_ids.normal(), shape),
                markers,
                heap,
            )?,
        };

        Ok(())
    }
}

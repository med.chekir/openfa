// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    widget::{FieldRef, Form, FormEvent, FormField, Widget},
    DocumentEditProvider, DocumentImportProvider, EditState, EditableDocument,
};
use anyhow::{bail, Result};
use catalog::{AssetCollection, FileInfo, FileSystem, Search};
use egui::Ui;
use enum_iterator::all;
use hud::{BombSightKind, Hud};
use installations::GameInstallation;
use nitrous::HeapMut;
use smallvec::{smallvec, SmallVec};

#[derive(Clone, Debug, Default)]
pub struct HudEdit;

impl EditableDocument for HudEdit {
    fn make_importer(&self) -> Box<dyn DocumentImportProvider> {
        Box::<HudImporter>::default() as Box<dyn DocumentImportProvider>
    }
    fn object_menu(&self, _ui: &mut Ui) -> Option<EditState> {
        None
    }
    fn graphics_menu(&self, ui: &mut Ui) -> Option<EditState> {
        self.waiting_menu(ui)
    }
    fn waiting_menu(&self, ui: &mut Ui) -> Option<EditState> {
        if ui.button(".HUD [Heads Up Display]").clicked() {
            return Some(EditState::ImportDocument(self.make_importer()));
        }
        None
    }
    fn document_actions(
        &self,
        ui: &mut Ui,
        info: FileInfo,
        install: &GameInstallation,
        collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<Option<EditState>> {
        if info.name().ends_with(".HUD") && ui.button("Edit").clicked() {
            return Ok(Some(HudImporter.import(info, install, collection, heap)?));
        }
        Ok(None)
    }
}

#[derive(Debug, Default)]
pub struct HudImporter;

impl DocumentImportProvider for HudImporter {
    fn all_documents<'a>(&self, collection: &'a AssetCollection) -> Result<Vec<FileInfo<'a>>> {
        collection.search(Search::for_extension("HUD"))
    }

    fn column_headers(&self) -> SmallVec<[&'static str; 8]> {
        smallvec!["Name"]
    }

    fn column_values(
        &mut self,
        info: FileInfo,
        _install: &GameInstallation,
        _collection: &AssetCollection,
        _heap: HeapMut,
    ) -> Result<SmallVec<[String; 8]>> {
        Ok(smallvec![info.name().to_owned(),])
    }

    fn import(
        &mut self,
        info: FileInfo,
        _install: &GameInstallation,
        _collection: &AssetCollection,
        _heap: HeapMut,
    ) -> Result<EditState> {
        let hud = Hud::from_bytes(&info.data()?, info.name())?;
        return Ok(EditState::EditDocument(
            Box::new(HudEditState::new(info.name(), hud)) as Box<dyn DocumentEditProvider>,
        ));
    }
}

#[derive(Clone, Debug)]
pub(crate) struct HudEditState {
    name: String,
    hud: Hud,
}

impl DocumentEditProvider for HudEditState {
    fn plugin(&self) -> &'static str {
        "HUD"
    }

    fn get_edit_widgets(&mut self, _heap: HeapMut) -> Result<Vec<Widget>> {
        use FormField as FF;
        let form = Form::from_table(
            "hud_form",
            vec![
                FF::new("hud_name", self)?.with_label("File Name"),
                ////
                FF::new_missing("_hud_resolution")?.with_label("HUD Pictures"),
                FF::new("hud_standard_resolution", self)?.with_label("Standard Resolution"),
                FF::new("hud_high_resolution", self)?.with_label("High Resolution"),
                FF::new("hud_super_resolution", self)?.with_label("Super Resolution"),
                ////
                FF::new_missing("_hud_known_fields")?.with_label("Known Fields"),
                FF::new("hud_bomb_sight", self)?.with_label("Bomb Sight"),
                ////
                FF::new_missing("_hud_block_of_positive_coords")?
                    .with_label("A Block of 6 Positive Coords"),
                FF::new("hud_unk_abs_0", self)?.with_label("Unknown Absolute Coord 0"),
                FF::new("hud_unk_abs_1", self)?.with_label("Unknown Absolute Coord 1"),
                FF::new("hud_unk_abs_2", self)?.with_label("Unknown Absolute Coord 2"),
                FF::new("hud_unk_abs_3", self)?.with_label("Unknown Absolute Coord 3"),
                FF::new("hud_unk_abs_4", self)?.with_label("Unknown Absolute Coord 4"),
                FF::new("hud_unk_abs_5", self)?.with_label("Unknown Absolute Coord 5"),
                ////
                FF::new_missing("_hud_block_of_signed_coords")?
                    .with_label("A Block of 8 Signed Coords"),
                FF::new("hud_unk_rel_0", self)?.with_label("Unknown Relative Coord 0"),
                FF::new("hud_unk_rel_1", self)?.with_label("Unknown Relative Coord 1"),
                FF::new("hud_unk_rel_2", self)?.with_label("Unknown Relative Coord 2"),
                FF::new("hud_unk_rel_3", self)?.with_label("Unknown Relative Coord 3"),
                FF::new("hud_unk_rel_4", self)?.with_label("Unknown Relative Coord 4"),
                FF::new("hud_unk_rel_5", self)?.with_label("Unknown Relative Coord 5"),
                FF::new("hud_unk_rel_6", self)?.with_label("Unknown Relative Coord 6"),
                FF::new("hud_unk_rel_7", self)?.with_label("Unknown Relative Coord 7"),
                ////
                FF::new_missing("_hud_unknown_fields")?.with_label("Some Other Stuff"),
                FF::new("hud_unk_0", self)?.with_label("A largeish number after the above"),
                FF::new("hud_unk_1", self)?.with_label("A u32 before blocks of 12 bytes"),
            ],
        );
        Ok(vec![Widget::H1(self.name.clone()), Widget::Form(form)])
    }

    fn get_field_ref(&mut self, name: &str) -> Result<FieldRef> {
        use FieldRef as FR;
        let hud = &mut self.hud;
        Ok(match name {
            "hud_name" => FR::Line(&mut self.name),
            ////
            "hud_standard_resolution" => FR::Line(hud.pic_standard_mut()),
            "hud_high_resolution" => FR::Line(hud.pic_high_mut()),
            "hud_super_resolution" => FR::Line(hud.pic_super_mut()),
            ////
            "hud_bomb_sight" => FR::DropdownEnumU16 {
                value: hud.bomb_sight_kind() as u16,
                options: all::<BombSightKind>().map(|v| v as u16).collect(),
                labels: all::<BombSightKind>().map(|v| v.label()).collect(),
            },
            ////
            "hud_unk_abs_0" => FR::U16(&mut hud.abs_coords_mut()[0]),
            "hud_unk_abs_1" => FR::U16(&mut hud.abs_coords_mut()[1]),
            "hud_unk_abs_2" => FR::U16(&mut hud.abs_coords_mut()[2]),
            "hud_unk_abs_3" => FR::U16(&mut hud.abs_coords_mut()[3]),
            "hud_unk_abs_4" => FR::U16(&mut hud.abs_coords_mut()[4]),
            "hud_unk_abs_5" => FR::U16(&mut hud.abs_coords_mut()[5]),
            // ////
            "hud_unk_rel_0" => FR::I16(&mut hud.rel_coords_mut()[0]),
            "hud_unk_rel_1" => FR::I16(&mut hud.rel_coords_mut()[1]),
            "hud_unk_rel_2" => FR::I16(&mut hud.rel_coords_mut()[2]),
            "hud_unk_rel_3" => FR::I16(&mut hud.rel_coords_mut()[3]),
            "hud_unk_rel_4" => FR::I16(&mut hud.rel_coords_mut()[4]),
            "hud_unk_rel_5" => FR::I16(&mut hud.rel_coords_mut()[5]),
            "hud_unk_rel_6" => FR::I16(&mut hud.rel_coords_mut()[6]),
            "hud_unk_rel_7" => FR::I16(&mut hud.rel_coords_mut()[7]),
            // ////
            "hud_unk_0" => FR::U16(hud.unk0_mut()),
            "hud_unk_1" => FR::U32(hud.unk1_mut()),
            name => {
                bail!("hud asked for missing field {name}")
            }
        })
    }

    fn apply_event(&mut self, event: &FormEvent, _heap: HeapMut) -> Result<()> {
        match event.name() {
            "hud_bomb_sight" => {
                let raw = event.next().u16()?;
                self.hud.set_bomb_sight_kind(BombSightKind::try_from(raw)?);
            }
            _ => bail!("unexpected manual apply event: {}", event.name()),
        }
        Ok(())
    }

    fn serialize(&self, _heap: HeapMut) -> Result<(String, Vec<u8>)> {
        Ok((self.name.clone(), self.hud.to_fa_hud_pe_bytes()?))
    }
}

impl HudEditState {
    pub(crate) fn new(name: &str, hud: Hud) -> Self {
        Self {
            name: name.to_owned(),
            hud,
        }
    }
}

// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    //DocumentEditProvider,
    DocumentImportProvider,
    EditState,
    EditableDocument,
};
use anyhow::Result;
use catalog::{AssetCollection, FileInfo, FileSystem, Search};
use egui::Ui;
use installations::{from_dos_string, GameInstallation};
use mmm::MissionMap;
use nitrous::HeapMut;
use smallvec::{smallvec, SmallVec};
use std::collections::HashMap;
use xt::TypeManager;

#[derive(Clone, Debug, Default)]
pub struct MmEdit;

impl EditableDocument for MmEdit {
    fn make_importer(&self) -> Box<dyn DocumentImportProvider> {
        Box::<MmImporter>::default() as Box<dyn DocumentImportProvider>
    }
    fn object_menu(&self, _ui: &mut Ui) -> Option<EditState> {
        None
    }
    fn graphics_menu(&self, ui: &mut Ui) -> Option<EditState> {
        self.waiting_menu(ui)
    }
    fn waiting_menu(&self, ui: &mut Ui) -> Option<EditState> {
        if ui.button(".MM [Mission Map]")
            .on_hover_text("Mission Maps contain a base set of objects that can be easily shared between Missions").clicked() {
            return Some(EditState::ImportDocument(self.make_importer()));
        }
        None
    }
    fn document_actions(
        &self,
        ui: &mut Ui,
        info: FileInfo,
        install: &GameInstallation,
        collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<Option<EditState>> {
        if info.name().ends_with(".MM") && ui.button("Edit").clicked() {
            return Ok(Some(
                MmImporter::default().import(info, install, collection, heap)?,
            ));
        }
        Ok(None)
    }
}

#[derive(Debug, Default)]
pub struct MmImporter {
    cache: HashMap<String, MissionMap>,
}

impl DocumentImportProvider for MmImporter {
    fn all_documents<'a>(&self, collection: &'a AssetCollection) -> Result<Vec<FileInfo<'a>>> {
        collection.search(Search::for_extension("MM"))
    }

    fn column_headers(&self) -> SmallVec<[&'static str; 8]> {
        smallvec!["Name", "Map", "Layer", "LayerId", "Object Count"]
    }

    fn column_values(
        &mut self,
        info: FileInfo,
        _install: &GameInstallation,
        collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<SmallVec<[String; 8]>> {
        let typeman = heap.resource::<TypeManager>();
        let map = self.cache.entry(info.to_string()).or_insert_with(|| {
            let contents = from_dos_string(info.data().unwrap());
            MissionMap::from_str(&contents, typeman, collection).unwrap()
        });

        Ok(smallvec![
            info.name().to_owned(),
            map.map_name().t2_name(),
            map.layer_name().to_owned(),
            map.layer_index().to_string(),
            map.objects().count().to_string(),
        ])
    }

    fn import(
        &mut self,
        _info: FileInfo,
        _install: &GameInstallation,
        _collection: &AssetCollection,
        _heap: HeapMut,
    ) -> Result<EditState> {
        todo!()
        // let hud = Hud::from_bytes(&info.data()?, info.name())?;
        // return Ok(EditState::EditDocument(
        //     Box::new(MmEditState::new(info.name(), hud)) as Box<dyn DocumentEditProvider>,
        // ));
    }
}

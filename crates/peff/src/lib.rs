// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
mod builder;
mod file_layout;

pub use builder::PeBuilder;

pub use crate::file_layout::{
    BaseRelocation, COFFHeader, DataDirectory, ImportDirectoryEntry, ImportEntry, OptionalHeader,
    SectionFlags, SectionHeader, TrampolineEntry, WindowsHeader, DOSX_SECTION_DATA,
};

use ansi::ansi;
use anyhow::{anyhow, bail, ensure, Result};
use log::trace;
use std::{collections::HashMap, mem, str};
use thiserror::Error;
use zerocopy::Ref;

#[derive(Debug, Error)]
enum PortableExecutableError {
    #[error("name ran off end of file")]
    NameUnending {},
}

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct Trampoline {
    // Offset is from the start of the code section.
    pub offset: usize,

    // The name attached to the thunk that would populate this trampoline.
    pub name: String,

    // Where this trampoline would indirect to, if jumped to.
    pub target: u32,

    // Shape files call into engine functions by setting up a stack frame
    // and then returning. The target of this is always one of these trampolines
    // stored at the tail of the PE. Store the in-memory location of the
    // thunk for fast comparison with relocated addresses.
    pub mem_location: u32,

    // Whatever tool was used to link .SH's bakes in a direct pointer to the GOT
    // PLT base (e.g. target) as the data location. Presumably when doing
    // runtime linking, it uses the IAT's name as a tag and rewrites the direct
    // load to the real address of the symbol (and not a split read of the code
    // and reloc in the GOT). These appear to be both global and per-object data
    // depending on the data -- e.g. brentObjectId is probably per-object and
    // _currentTicks is probably global?
    //
    // A concrete example; if the inline assembly does:
    //    `mov %ebp, [<addr of GOT of data>]`
    //
    // The runtime would presumably use the relocation of the above addr as an
    // opportunity to rewrite the load as a reference to the real memory. We
    // need to take all of this into account when interpreting the referencing
    // code.
    pub is_data: bool,
}

impl Trampoline {
    pub const SIZE: usize = 6;

    pub fn has_trampoline(offset: usize, code: &[u8]) -> bool {
        // pe.section_info.contains_key(".idata") &&
        code.len() >= offset + Self::SIZE && code[offset] == 0xFF && code[offset + 1] == 0x25
    }

    fn from_code(
        imports: &[ImportEntry],
        image_vbase: u32,
        offset: usize,
        code: &[u8],
    ) -> Result<Self> {
        ensure!(Self::has_trampoline(offset, code), "not a trampoline");
        let target = TrampolineEntry::overlay(&code[offset..offset + Self::SIZE])?.target();
        let import = Self::find_matching_thunk(image_vbase, target, imports)?;
        // FIXME: update is_data in caller?
        // let is_data = DATA_RELOCATIONS.contains(&thunk.name);
        Ok(Trampoline {
            offset,
            name: import.name().to_owned(),
            target,
            mem_location: offset as u32,
            is_data: false,
        })
    }

    fn find_matching_thunk(
        image_vbase: u32,
        addr: u32,
        imports: &[ImportEntry],
    ) -> Result<&ImportEntry> {
        // The reason we have to process trampolines during load and not, perhaps,
        // after relocation is that not all SH files contain relocs for the thunk
        // targets(!). This is yet more evidence that they're not actually using
        // LoadLibrary to put shapes in memory. They're probably only using the
        // relocation list to rewrite data access with the thunks as tags. We're
        // using relocation, however, to help decode. So if the thunks are not
        // relocated automatically we have to check the relocated value
        // manually.

        // Since we're checking before relocation, this should "just work", even
        // though not all of the pointers here have relocation entries. e.g. the
        // raw values should be the same.
        trace!(
            "looking for target 0x{:X} in {} import table",
            addr,
            imports.len()
        );
        for entry in imports.iter() {
            if addr == entry.iat_rva() {
                return Ok(entry);
            }
        }

        // Also, in USNF, some of the thunks contain the base address already,
        // so treat them like a normal code pointer.
        let thunk_target = addr - image_vbase;
        trace!(
            "looking for target 0x{:X} in {} import table",
            thunk_target,
            imports.len()
        );
        for entry in imports.iter() {
            if thunk_target == entry.iat_rva() {
                return Ok(entry);
            }
        }

        bail!("did not find thunk with a target of {:08X}", thunk_target)
    }

    pub fn size(&self) -> usize {
        6
    }

    pub fn magic(&self) -> &'static str {
        "Tramp"
    }

    pub fn at_offset(&self) -> usize {
        self.offset
    }

    pub fn show(&self) -> String {
        format!(
            "@{:04X} {}Tramp{}: {}{}{} = {:04X}",
            self.offset,
            ansi().yellow().bold(),
            ansi(),
            ansi().yellow(),
            self.name,
            ansi(),
            self.target
        )
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Relocation {
    // Index into the reloc_base_blocks table this relocation belongs to.
    reloc_base_block_offset: usize,

    // Offset of this relocation within the block
    reloc_offset: usize,

    // Raw value of the reloc
    raw: u16,

    // RVA: value with applied page_rva
    rva: u32,

    // RVA isn't useful to us as we're not dynamic loading anything: instead we
    // want the offset into the code section so that we can do easily do
    // relocations as indices into the code section data.
    code_offset: usize,
}

impl Relocation {
    pub fn reloc_base_block_offset(&self) -> usize {
        self.reloc_base_block_offset
    }

    pub fn reloc_offset(&self) -> usize {
        self.reloc_offset
    }

    pub fn raw(&self) -> u16 {
        self.raw
    }

    pub fn rva(&self) -> u32 {
        self.rva
    }

    pub fn code_offset(&self) -> usize {
        self.code_offset
    }
}

#[derive(Debug)]
pub struct PortableExecutable {
    // Raw versions of decoded header data
    coff_header: COFFHeader,
    opt_header: OptionalHeader,
    win_header: WindowsHeader,
    data_dirs: Vec<DataDirectory>,
    section_headers: Vec<SectionHeader>,
    sections_by_name: HashMap<String, (SectionHeader, Vec<u8>)>,

    // Indirect jump block at end of code, these point to IAT RVAs. We look up the name in the
    // RVA and attach them to a trampoline for static lookup for mapping to functionality in
    // the SH x86 code.
    trampolines: Vec<Trampoline>,

    // Decoded .idata: import tables with both LUT and IAT RVAs
    import_dir: ImportDirectoryEntry,
    imports: Vec<ImportEntry>,

    // Decoded .reloc section.
    reloc_base_table: Vec<BaseRelocation>,
    relocs: Vec<Relocation>,

    // Whatever value relocate was called with.
    relocation_target: u32,
}

impl PortableExecutable {
    pub fn from_bytes(data: &[u8]) -> Result<PortableExecutable> {
        assert_eq!(mem::size_of::<COFFHeader>(), 20);
        assert_eq!(mem::size_of::<OptionalHeader>(), 28);
        assert_eq!(mem::size_of::<WindowsHeader>(), 68);

        ensure!(data.len() > 0x3c + 4, "pe file too short for dos header");
        ensure!(&data[0..2] == b"MZ", "not a dos program file header");
        let pe_offset = u32::from_le_bytes((&data[0x3c..0x40]).try_into()?) as usize;
        assert_eq!(pe_offset, 128);

        ensure!(
            data.len() > pe_offset + 4 + 20 + 28,
            "pe file to short for coff headers"
        );
        ensure!(
            &data[pe_offset..pe_offset + 2] == b"PL",
            "did not find pe header"
        );
        let coff_offset = pe_offset + 4;
        let coff = COFFHeader::overlay_prefix(&data[coff_offset..])?;
        coff.validate()?;

        let opt_offset = pe_offset + 4 + mem::size_of::<COFFHeader>();
        let opt = OptionalHeader::overlay_prefix(&data[opt_offset..])?;
        opt.validate()?;

        let win_offset =
            pe_offset + 4 + mem::size_of::<COFFHeader>() + mem::size_of::<OptionalHeader>();
        let win = WindowsHeader::overlay_prefix(&data[win_offset..])?;
        win.validate()?;

        // Load directory data so we can cross reference with the section labels.
        let dir_offset = win_offset + mem::size_of::<WindowsHeader>();
        let dir_end = dir_offset + 16 * mem::size_of::<DataDirectory>();
        let data_dirs = DataDirectory::overlay_slice(&data[dir_offset..dir_end])?;

        // Load all section headers and read section data into the sections table
        let section_table_offset =
            pe_offset + 4 + mem::size_of::<COFFHeader>() + coff.size_of_optional_header() as usize;
        ensure!(section_table_offset == dir_end);
        let mut sections_by_name = HashMap::new();
        let mut section_headers = Vec::new();
        let mut _size_of_initialized_data = 0;
        for i in 0..coff.number_of_sections() as usize {
            let section_offset = section_table_offset + i * mem::size_of::<SectionHeader>();
            let section = SectionHeader::overlay_prefix(&data[section_offset..])?;
            let name = section.name_str()?;
            section.validate(&name, i, &sections_by_name, data_dirs)?;

            trace!(
                "Section {} starting at offset {:X} loaded at vaddr {:X} base {:X}",
                name,
                section.pointer_to_raw_data(),
                section.virtual_address(),
                win.image_base(),
            );
            if SectionFlags::from_bits(section.characteristics())
                .expect("section characteristics")
                .contains(SectionFlags::IMAGE_SCN_CNT_INITIALIZED_DATA)
            {
                _size_of_initialized_data += section.virtual_size().max(section.size_of_raw_data());
            }
            let start = section.pointer_to_raw_data() as usize;
            let end = start + section.virtual_size() as usize;
            let section_data = data[start..end].to_owned();
            if &name == "$$DOSX" {
                ensure!(
                    section_data == DOSX_SECTION_DATA,
                    "expected a fixed-content DOSX header"
                );
                // Useless, but keep it anyway for debugging.
            }

            section_headers.push(*section);
            if &name == ".text" {
                ensure!(win.image_base() == 0x10000, "USNF image base");
                sections_by_name.insert("CODE".to_owned(), (*section, section_data));
            } else {
                ensure!(
                    &name != "CODE" || win.image_base() == 0,
                    "non-USNF image base"
                );
                sections_by_name.insert(name, (*section, section_data));
            }
        }
        // ensure!(opt.size_of_initialized_data() == size_of_initialized_data);
        // ensure!(opt.size_of_initialized_data() == size_of_initialized_data);

        // Parse the import data (.idata) if present.
        let (import_dir, imports) =
            if let Some((idata_section, idata)) = sections_by_name.get(".idata") {
                PortableExecutable::parse_idata(idata_section, idata)?
            } else {
                (ImportDirectoryEntry::terminator(), vec![])
            };

        // Parse the relocations.
        // There are some files that have a .reloc, but no CODE to reloc; just skip in that case.
        let (reloc_base_table, relocs) =
            if sections_by_name.contains_key(".reloc") && sections_by_name.contains_key("CODE") {
                let (reloc_section, reloc_data) = sections_by_name.get(".reloc").unwrap();
                ensure!(
                    reloc_section.virtual_address() == data_dirs[5].virtual_address(),
                    "mismatched .relocs position",
                );
                // The .reloc section virtual_size is the size with pad; meanwhile size_of_raw_data
                // is zeroed for the .reloc sections, for some reason. Instead we look in the data
                // directory for the real, unpadded size of the relocs data.
                let reloc_data = &reloc_data[0..data_dirs[5].size() as usize];
                PortableExecutable::parse_relocs(reloc_data, &sections_by_name["CODE"].0)?
            } else {
                (vec![], vec![])
            };

        // Extract any trampolines at the end of code, if they exist.
        let trampolines = if let Some((_, code)) = sections_by_name.get("CODE") {
            PortableExecutable::parse_trampolines(&imports, win.image_base(), code)?
        } else {
            vec![]
        };

        Ok(PortableExecutable {
            coff_header: coff.to_owned(),
            opt_header: opt.to_owned(),
            win_header: win.to_owned(),
            data_dirs: data_dirs.to_owned(),
            section_headers,
            sections_by_name,
            import_dir,
            imports,
            reloc_base_table,
            relocs,
            trampolines,
            relocation_target: 0,
        })
    }

    fn parse_idata(
        section: &SectionHeader,
        idata: &[u8],
    ) -> Result<(ImportDirectoryEntry, Vec<ImportEntry>)> {
        ensure!(
            idata.len() > mem::size_of::<ImportDirectoryEntry>() * 2,
            "section data too short for directory"
        );

        // Assert that there is exactly one entry by loading the second section and checking
        // that it is null.
        let dirs_end = 2 * mem::size_of::<ImportDirectoryEntry>();
        let import_dirs = ImportDirectoryEntry::overlay_slice(&idata[..dirs_end])?;
        ensure!(
            import_dirs[1] == ImportDirectoryEntry::terminator(),
            "expected only one import dirctory entry"
        );
        let import_dir = &import_dirs[0];
        import_dir.validate(section)?;

        // Check that the name is main.dll.
        let dll_name_offset = import_dir.name_rva() as usize - section.virtual_address() as usize;
        let dll_name = Self::read_name(&idata[dll_name_offset..])?;
        ensure!(
            dll_name == "main.dll",
            "expected the directory entry to be for main.dll"
        );

        let import_entries = import_dir.read_directory(section, idata)?;
        Ok((*import_dir, import_entries))
    }

    fn read_name(n: &[u8]) -> Result<String> {
        let end_offset: usize = n
            .iter()
            .position(|&c| c == 0)
            .ok_or::<PortableExecutableError>(PortableExecutableError::NameUnending {})?;
        Ok(str::from_utf8(&n[..end_offset])?.to_owned())
    }

    fn parse_relocs(
        reloc_data: &[u8],
        code_section: &SectionHeader,
    ) -> Result<(Vec<BaseRelocation>, Vec<Relocation>)> {
        trace!("relocs section is 0x{:04X} bytes", reloc_data.len());
        let mut reloc_base_blocks = Vec::new();
        let mut out = Vec::new();
        let mut offset = 0usize;
        while offset < reloc_data.len() {
            let base_reloc = BaseRelocation::overlay_prefix(&reloc_data[offset..])?;
            trace!("base reloc at {} is {:?}", offset, base_reloc);
            ensure!(base_reloc.block_size() != 0, "nil block");
            if base_reloc.block_size() > 0 {
                let reloc_cnt =
                    (base_reloc.block_size() as usize - mem::size_of::<BaseRelocation>()) / 2;
                let relocs_start = offset + mem::size_of::<BaseRelocation>();
                let relocs_end = relocs_start + 2 * reloc_cnt;
                let relocs = Ref::<&[u8], [u16]>::new_slice(&reloc_data[relocs_start..relocs_end])
                    .ok_or_else(|| anyhow!("failed to transmute relocations"))?;
                for (i, &reloc) in relocs.iter().enumerate() {
                    let flags = (reloc & 0xF000) >> 12;
                    let reloc_offset = reloc & 0x0FFF;
                    if flags == 0 {
                        continue;
                    }
                    ensure!(flags == 3, "only 32bit relocations are supported");
                    let rva = base_reloc.page_rva() + u32::from(reloc_offset);
                    ensure!(
                        rva >= code_section.virtual_address()
                            && rva < code_section.virtual_address() + code_section.virtual_size(),
                        "relocation rva outside of CODE"
                    );
                    let relocation = Relocation {
                        reloc_base_block_offset: reloc_base_blocks.len(),
                        reloc_offset: i,
                        raw: reloc,
                        rva,
                        code_offset: (rva - code_section.virtual_address()) as usize,
                    };
                    trace!("reloc @{:08X}b is {:0X?}", offset, relocation);
                    out.push(relocation);
                }
                reloc_base_blocks.push(*base_reloc);
            }
            offset += base_reloc.block_size() as usize;
        }
        Ok((reloc_base_blocks, out))
    }

    fn parse_trampolines(
        imports: &[ImportEntry],
        image_vbase: u32,
        code: &[u8],
    ) -> Result<Vec<Trampoline>> {
        let mut trampolines = Vec::new();
        if code.len() < Trampoline::SIZE {
            return Ok(trampolines);
        }
        let mut offset = code.len() - Trampoline::SIZE;
        while offset > 0 {
            if Trampoline::has_trampoline(offset, code) {
                let tramp = Trampoline::from_code(imports, image_vbase, offset, code)?;
                trace!("found trampoline: {}", tramp.show());
                trampolines.push(tramp);
            } else {
                break;
            }
            offset -= Trampoline::SIZE;
        }
        trampolines.reverse();
        Ok(trampolines)
    }

    pub fn relocation_target(&self) -> u32 {
        self.relocation_target
    }

    pub fn relocate_code_offset(&self, code_offset: u32) -> Result<u32> {
        let reloc_delta =
            RelocationDelta::new(self.relocation_target, self.win_header.image_base());
        Ok(reloc_delta.apply(code_offset))
    }

    // Mutate the code using the relocs to be at a new virutal address `target`.
    pub fn relocate(&mut self, target: u32) -> Result<()> {
        self.relocation_target = target;
        let (code_vaddr, code) =
            if let Some((code_section, code)) = self.sections_by_name.get_mut("CODE") {
                (code_section.virtual_address(), code)
            } else {
                return Ok(());
            };

        // let code_vaddr = self.code_vaddr;
        let reloc_delta = RelocationDelta::new(target, self.win_header.image_base() + code_vaddr);
        for reloc in self.relocs.iter() {
            let reloc = reloc.code_offset;
            let pcode = u32::from_le_bytes((&code[reloc..reloc + 4]).try_into()?);
            trace!(
                "Relocating word at 0x{:04X} from 0x{:08X} to 0x{:08X}",
                reloc,
                pcode,
                reloc_delta.apply(pcode)
            );
            // The safe version is less clear, but supports non little-endian architectures.
            // { *pcode = reloc_delta.apply(*pcode); }
            code[reloc..reloc + 4].copy_from_slice(&reloc_delta.apply(pcode).to_le_bytes());
        }

        // FIXME: why the code vaddr? should this come from the section header?
        let idata_delta = RelocationDelta::new(target, code_vaddr);
        for import_entry in self.imports.iter_mut() {
            trace!(
                "Relocating import iat_rva: 0x{:08X} + 0x{:08X} = 0x{:08X}",
                import_entry.iat_rva(),
                idata_delta.delta(),
                idata_delta.apply(import_entry.iat_rva())
            );
            import_entry.relocate(&idata_delta);
        }

        // Note: trampolines are part of the code, so they already have vcode from reloc
        //       ...except for USNF, which doesn't include the trampoline table in relocs
        let tramp_delta = RelocationDelta::new(target, self.win_header.image_base());
        for trampoline in self.trampolines.iter_mut() {
            trace!(
                "Relocating trampoline location: 0x{:08X} + 0x{:08X} = 0x{:08X}",
                trampoline.mem_location,
                tramp_delta.delta(),
                tramp_delta.apply(trampoline.mem_location)
            );
            let mut mem_location = tramp_delta.apply(trampoline.mem_location);
            if mem_location & target != target {
                let usnf_delta = RelocationDelta::new(target, code_vaddr);
                trace!(
                    "Adjust failed trampoline with vaddr: {:08X} + {:08X} => {:08X}",
                    trampoline.mem_location,
                    usnf_delta.delta(),
                    usnf_delta.apply(trampoline.mem_location)
                );
                mem_location = usnf_delta.apply(trampoline.mem_location) + code_vaddr;
            }
            trampoline.mem_location = mem_location;
            assert_eq!(
                trampoline.mem_location & target,
                target,
                "failed relocation"
            );
        }

        Ok(())
    }

    pub fn coff_header(&self) -> &COFFHeader {
        &self.coff_header
    }

    pub fn opt_header(&self) -> &OptionalHeader {
        &self.opt_header
    }

    pub fn win_header(&self) -> &WindowsHeader {
        &self.win_header
    }

    pub fn data_dir(&self, offset: usize) -> &DataDirectory {
        &self.data_dirs[offset]
    }

    pub fn section_headers(&self) -> &[SectionHeader] {
        &self.section_headers
    }

    pub fn section(&self, name: &str) -> Option<SectionHeader> {
        self.sections_by_name.get(name).map(|v| v.0)
    }

    pub fn import_dir(&self) -> &ImportDirectoryEntry {
        &self.import_dir
    }

    pub fn imports(&self) -> &[ImportEntry] {
        &self.imports
    }

    pub fn reloc_base_table(&self) -> &[BaseRelocation] {
        &self.reloc_base_table
    }

    pub fn relocs(&self) -> &[Relocation] {
        &self.relocs
    }

    pub fn has_section(&self, name: &str) -> bool {
        self.sections_by_name.contains_key(name)
    }

    pub fn code_section_header(&self) -> &SectionHeader {
        assert!(self.has_section("CODE"));
        &self.section_headers[0]
    }

    // Get a section's data, return an empty array if no such section
    pub fn section_data(&self, name: &str) -> &[u8] {
        if let Some((_, data)) = self.sections_by_name.get(name) {
            data
        } else {
            &[]
        }
    }

    pub fn code(&self) -> &[u8] {
        self.section_data("CODE")
    }

    pub fn trampolines_start(&self) -> usize {
        if self.trampolines.is_empty() {
            self.code().len()
        } else {
            self.trampolines[0].offset
        }
    }

    pub fn trampolines(&self) -> &[Trampoline] {
        &self.trampolines
    }

    pub fn trampolines_mut(&mut self) -> &mut [Trampoline] {
        &mut self.trampolines
    }

    pub fn lookup_trampoline_for_memory_op(&self, displacement: i32) -> Option<&Trampoline> {
        // Trampoline target data is _also_ displaced, so just use that directly
        self.trampolines
            .iter()
            .find(|&tramp| displacement as u32 == tramp.mem_location)
    }

    pub fn find_trampoline_by_name(&self, name: &str) -> Option<&Trampoline> {
        self.trampolines.iter().find(|&tramp| name == tramp.name)
    }
}

pub(crate) enum RelocationDelta {
    Up(u32),
    Down(u32),
}

impl RelocationDelta {
    pub fn new(target: u32, base: u32) -> Self {
        if target >= base {
            RelocationDelta::Up(target - base)
        } else {
            RelocationDelta::Down(base - target)
        }
    }

    pub fn apply(&self, vaddr: u32) -> u32 {
        match self {
            RelocationDelta::Up(delta) => vaddr.wrapping_add(*delta),
            RelocationDelta::Down(delta) => vaddr.wrapping_sub(*delta),
        }
    }

    pub fn delta(&self) -> u32 {
        match self {
            RelocationDelta::Up(delta) => *delta,
            RelocationDelta::Down(delta) => *delta,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use catalog::{FileSystem, Search};
    use installations::Installations;

    #[test]
    fn it_works() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs
            .search(Search::for_extension("SH"))?
            .iter()
            .chain(libs.search(Search::for_extension("LAY"))?.iter())
            .chain(libs.search(Search::for_extension("DLG"))?.iter())
            .chain(libs.search(Search::for_extension("MNU"))?.iter())
            .chain(libs.search(Search::for_extension("MC"))?.iter())
        {
            println!("At: {info}");
            let _pe = PortableExecutable::from_bytes(info.data()?.as_ref())?;
        }

        Ok(())
    }
}

// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::{PortableExecutable, RelocationDelta};
use anyhow::{anyhow, bail, ensure, Result};
use bitflags::bitflags;
use memoffset::offset_of;
use packed_struct::packed_struct;
use std::{collections::HashMap, mem, str};
use zerocopy::Ref;

#[packed_struct]
#[derive(Clone, Copy)]
pub struct COFFHeader {
    machine: u16,
    number_of_sections: u16,
    time_date_stamp: u32,
    pointer_to_symbol_table: u32,
    number_of_symbols: u32,
    size_of_optional_header: u16,
    characteristics: u16,
}

impl COFFHeader {
    pub fn new(number_of_sections: u16, time_date_stamp: u32) -> Self {
        Self {
            machine: 0x14C,
            number_of_sections,
            time_date_stamp,
            pointer_to_symbol_table: 0,
            number_of_symbols: 0,
            size_of_optional_header: 224,
            characteristics: 0xA18E,
        }
    }

    pub fn validate(&self) -> Result<()> {
        ensure!(self.machine() == 0x14C, "expected i386 machine type field");
        ensure!(
            self.characteristics() == 0xA18E,
            "expected a specific set of coff flags"
        );
        ensure!(
            self.pointer_to_symbol_table() == 0,
            "expected nil symbol table"
        );
        ensure!(self.number_of_symbols() == 0, "expected no symbols");
        ensure!(self.size_of_optional_header() == 224, "a normal PE file");
        Ok(())
    }

    pub fn offset_of_number_of_sections() -> usize {
        offset_of!(Self, number_of_sections)
    }

    pub fn offset_of_time_date_stamp() -> usize {
        offset_of!(Self, time_date_stamp)
    }
}

#[packed_struct]
#[derive(Clone, Copy)]
pub struct OptionalHeader {
    magic: u16,
    major_linker_version: u8,
    minor_linker_version: u8,
    size_of_code: u32,
    size_of_initialized_data: u32,
    size_of_uninitialized_data: u32,
    address_of_entry_point: u32,
    base_of_code: u32,
    base_of_data: u32,
}

impl OptionalHeader {
    pub fn new(
        size_of_code: u32,
        size_of_initialized_data: u32,
        base_of_code: u32,
        base_of_data: u32,
    ) -> Self {
        OptionalHeader {
            magic: 0x10B,
            major_linker_version: 2,
            minor_linker_version: 23,
            size_of_code,
            size_of_initialized_data,
            size_of_uninitialized_data: 0,
            address_of_entry_point: 0,
            base_of_code,
            base_of_data,
        }
    }

    pub fn validate(&self) -> Result<()> {
        ensure!(self.magic() == 0x10B, "expected a PE optional header magic");
        ensure!(
            self.major_linker_version() == 2,
            "expect major linker version 2"
        );
        ensure!(
            self.minor_linker_version() == 23,
            "expect minor linker version 23"
        );
        ensure!(
            self.size_of_uninitialized_data() == 0,
            "expected no uninitialized data"
        );
        ensure!(
            self.address_of_entry_point() == 0,
            "expected entry to be at zero"
        );
        ensure!(
            self.base_of_code() == 0 || self.base_of_code() == 4096,
            "expected code to live at page 0 or 1"
        );
        Ok(())
    }

    pub fn offset_of_size_of_code() -> usize {
        offset_of!(Self, size_of_code)
    }
    pub fn offset_of_size_of_initialized_data() -> usize {
        offset_of!(Self, size_of_initialized_data)
    }
    pub fn offset_of_base_of_code() -> usize {
        offset_of!(Self, base_of_code)
    }
    pub fn offset_of_base_of_data() -> usize {
        offset_of!(Self, base_of_data)
    }
}

#[packed_struct]
#[derive(Clone, Copy)]
pub struct WindowsHeader {
    image_base: u32,
    section_alignment: u32,
    file_alignment: u32,
    major_os_version: u16,
    minor_os_version: u16,
    major_image_version: u16,
    minor_image_version: u16,
    major_subsystem_version: u16,
    minor_subsystem_version: u16,
    win32_version_value: u32,
    size_of_image: u32,
    size_of_headers: u32,
    checksum: u32,
    subsystem: u16,
    dll_characteristics: u16,
    size_of_stack_reserve: u32,
    size_of_stack_commit: u32,
    size_of_heap_reserve: u32,
    size_of_heap_commit: u32,
    loader_flags: u32,
    number_of_rvas_and_sizes: u32,
}

impl WindowsHeader {
    pub fn new(size_of_image: u32, size_of_headers: u32) -> Self {
        Self {
            image_base: 0,
            section_alignment: 4096,
            file_alignment: 512,
            major_os_version: 1,
            minor_os_version: 0,
            major_image_version: 0,
            minor_image_version: 0,
            major_subsystem_version: 3,
            minor_subsystem_version: 10,
            win32_version_value: 0,
            size_of_image,
            size_of_headers,
            checksum: 0,
            subsystem: 66,
            dll_characteristics: 0,
            size_of_stack_reserve: 0,
            size_of_stack_commit: 0,
            size_of_heap_reserve: 1024 * 1024,
            size_of_heap_commit: 4096,
            loader_flags: 0,
            number_of_rvas_and_sizes: 16,
        }
    }

    pub fn validate(&self) -> Result<()> {
        ensure!(
            self.section_alignment() == 4096,
            "expected page aligned memory sections"
        );
        ensure!(
            self.file_alignment() == 512,
            "expected block aligned file sections"
        );
        ensure!(self.major_os_version() == 1, "major os version should be 1");
        ensure!(
            self.minor_os_version() == 0,
            "minor os version should be unset"
        );
        ensure!(
            self.major_image_version() == 0,
            "major image version should be unset"
        );
        ensure!(
            self.minor_image_version() == 0,
            "minor image version should be unset"
        );
        ensure!(
            self.major_subsystem_version() == 3,
            "major_subsystem_version == 3"
        );
        ensure!(
            self.minor_subsystem_version() == 10,
            "minor_subsystem_version == 10"
        );
        ensure!(
            self.win32_version_value() == 0,
            "win32 version version should be unset"
        );
        ensure!(
            self.size_of_headers() == 1024 || self.size_of_headers() == 512,
            "expected exactly 1K of headers"
        );
        ensure!(self.checksum() == 0, "checksum should be unset");
        ensure!(self.subsystem() == 66, "subsystem must be exactly 66");
        ensure!(
            self.dll_characteristics() == 0,
            "dll_characteristics should be unset"
        );
        ensure!(self.size_of_stack_reserve() == 0, "stack not supported");
        ensure!(self.size_of_stack_commit() == 0, "stack not supported");
        ensure!(self.loader_flags() == 0, "loader flags should not be set");
        ensure!(
            self.size_of_heap_reserve() == 1_048_576,
            "heap reserve size"
        );
        ensure!(self.size_of_heap_commit() == 4096, "heap reserve commit");
        ensure!(
            self.number_of_rvas_and_sizes() == 9 // all USNF models
         || self.number_of_rvas_and_sizes() == 16, // everything else
            "number of rvas and sizes"
        );
        Ok(())
    }

    pub fn offset_of_size_of_image() -> usize {
        offset_of!(Self, size_of_image)
    }
    pub fn offset_of_size_of_headers() -> usize {
        offset_of!(Self, size_of_headers)
    }
}

#[packed_struct]
#[derive(Clone, Copy)]
pub struct DataDirectory {
    virtual_address: u32,
    size: u32,
}

impl DataDirectory {
    pub fn empty() -> Self {
        Self {
            virtual_address: 0,
            size: 0,
        }
    }

    pub fn set(&mut self, virtual_address: u32, size: u32) {
        self.virtual_address = virtual_address;
        self.size = size;
    }
}

#[packed_struct]
#[derive(Clone, Copy)]
pub struct SectionHeader {
    name: [u8; 8],
    virtual_size: u32,
    virtual_address: u32,
    size_of_raw_data: u32,
    pointer_to_raw_data: u32,
    pointer_to_relocations: u32,
    pointer_to_line_numbers: u32,
    number_of_relocations: u16,
    number_of_line_numbers: u16,
    characteristics: u32,
}

impl SectionHeader {
    pub fn new(
        name: [u8; 8],
        virtual_size: u32,
        virtual_address: u32,
        size_of_raw_data: u32,
        pointer_to_raw_data: u32,
        characteristics: SectionFlags,
    ) -> Self {
        Self {
            name,
            virtual_size,
            virtual_address,
            size_of_raw_data,
            pointer_to_raw_data,
            pointer_to_relocations: 0,
            pointer_to_line_numbers: 0,
            number_of_relocations: 0,
            number_of_line_numbers: 0,
            characteristics: characteristics.bits(),
        }
    }

    pub fn offset_of_name() -> usize {
        offset_of!(Self, name)
    }
    pub fn offset_of_virtual_size() -> usize {
        offset_of!(Self, virtual_size)
    }
    pub fn offset_of_virtual_address() -> usize {
        offset_of!(Self, virtual_address)
    }
    pub fn offset_of_size_of_raw_data() -> usize {
        offset_of!(Self, size_of_raw_data)
    }
    pub fn offset_of_pointer_to_raw_data() -> usize {
        offset_of!(Self, pointer_to_raw_data)
    }
    pub fn offset_of_characteristics() -> usize {
        offset_of!(Self, characteristics)
    }

    pub fn name_str(&self) -> Result<String> {
        let name_raw = match self.name().iter().position(|&n| n == 0u8) {
            Some(last) => self.name()[..last].to_owned(),
            None => self.name()[0..8].to_owned(),
        };
        Ok(str::from_utf8(&name_raw)?.to_owned())
    }

    pub fn validate(
        &self,
        name: &str,
        offset: usize,
        seen_sections: &HashMap<String, (SectionHeader, Vec<u8>)>,
        data_dirs: &[DataDirectory],
    ) -> Result<()> {
        ensure!(
            self.number_of_relocations() == 0,
            "relocations are not supported"
        );
        ensure!(
            self.pointer_to_relocations() == 0,
            "relocations are not supported"
        );
        ensure!(
            self.number_of_line_numbers() == 0,
            "line numbers are not supported"
        );
        ensure!(
            self.pointer_to_line_numbers() == 0,
            "line numbers are not supported"
        );
        match name {
            "CODE" | ".text" => {
                ensure!(self.virtual_address() == 4096, "load a 4k");
                ensure!(self.pointer_to_raw_data() == 0x200 || self.pointer_to_raw_data() == 0x400);
                ensure!(offset == 0, "expected code section header first in pe");
            }
            ".bss" => {}
            ".idata" => {
                let mut expect_offset = 1;
                if seen_sections.contains_key(".bss") {
                    expect_offset += 1;
                }
                ensure!(offset == expect_offset, ".idata after code and bss");
                ensure!(
                    data_dirs[1].virtual_address() == self.virtual_address(),
                    "mismatched virtual address on .idata section"
                );
            }
            ".reloc" => {
                let mut expect_offset = 0;
                if seen_sections.contains_key("CODE") || seen_sections.contains_key(".text") {
                    expect_offset += 1;
                }
                if seen_sections.contains_key(".bss") {
                    expect_offset += 1;
                }
                if seen_sections.contains_key(".idata") {
                    expect_offset += 1;
                }
                ensure!(
                    offset == expect_offset,
                    "expected reloc after code if present"
                );
                ensure!(
                    data_dirs[5].virtual_address() == self.virtual_address(),
                    "mismatched virtual address on .reloc section"
                );
            }
            "$$DOSX" => {}
            s => bail!("unexpected section name: {}", s),
        };
        ensure!(
            SectionFlags::from_bits(self.characteristics())
                .ok_or_else(|| anyhow!("invalid section flags in characteristics"))?
                == Self::flags_for_section_name(name)?,
            "unexpected section flags"
        );
        Ok(())
    }

    pub fn flags_for_section_name(name: &str) -> Result<SectionFlags> {
        Ok(match name {
            "CODE" | ".text" => {
                SectionFlags::IMAGE_SCN_CNT_CODE
                    | SectionFlags::IMAGE_SCN_MEM_EXECUTE
                    | SectionFlags::IMAGE_SCN_MEM_READ
                    | SectionFlags::IMAGE_SCN_MEM_WRITE
            }
            ".bss" => {
                SectionFlags::IMAGE_SCN_CNT_UNINITIALIZED_DATA
                    | SectionFlags::IMAGE_SCN_MEM_READ
                    | SectionFlags::IMAGE_SCN_MEM_WRITE
            }
            ".idata" => {
                SectionFlags::IMAGE_SCN_CNT_INITIALIZED_DATA
                    | SectionFlags::IMAGE_SCN_MEM_READ
                    | SectionFlags::IMAGE_SCN_MEM_WRITE
            }
            ".reloc" => {
                SectionFlags::IMAGE_SCN_CNT_INITIALIZED_DATA
                    | SectionFlags::IMAGE_SCN_MEM_DISCARDABLE
                    | SectionFlags::IMAGE_SCN_MEM_READ
            }
            "$$DOSX" => {
                SectionFlags::IMAGE_SCN_CNT_INITIALIZED_DATA
                    | SectionFlags::IMAGE_SCN_MEM_DISCARDABLE
                    | SectionFlags::IMAGE_SCN_MEM_READ
            }
            s => bail!("unexpected section name: {}", s),
        })
    }
}

#[packed_struct]
#[derive(Clone, Copy)]
pub struct TrampolineEntry {
    pub jmp: u16,
    pub target: u32,
}

bitflags! {
    #[derive(Clone, Copy, Debug, Eq, PartialEq)]
    pub struct SectionFlags : u32 {
        const _1 = 0x0000_0001;  // Reserved for future use.
        const _2 = 0x0000_0002;  // Reserved for future use.
        const _3 = 0x0000_0004;  // Reserved for future use.
        const IMAGE_SCN_TYPE_NO_PAD = 0x0000_0008;  // The section should not be padded to the next boundary. This flag is obsolete and is replaced by IMAGE_SCN_ALIGN_1BYTES. This is valid only for object files.
        const _5 = 0x0000_0010;  // Reserved for future use.
        const IMAGE_SCN_CNT_CODE = 0x0000_0020;  // The section contains executable code.
        const IMAGE_SCN_CNT_INITIALIZED_DATA = 0x0000_0040;  // The section contains initialized data.
        const IMAGE_SCN_CNT_UNINITIALIZED_DATA = 0x0000_0080;  // The section contains uninitialized data.
        const IMAGE_SCN_LNK_OTHER = 0x0000_0100;  // Reserved for future use.
        const IMAGE_SCN_LNK_INFO = 0x0000_0200; // The section contains comments or other information. The .drectve section has this type. This is valid for object files only.
        const _B = 0x0000_0400;  // Reserved for future use.
        const IMAGE_SCN_LNK_REMOVE = 0x0000_0800;  // The section will not become part of the image. This is valid only for object files.
        const IMAGE_SCN_LNK_COMDAT = 0x0000_1000;  // The section contains COMDAT data. For more information, see COMDAT Sections (Object Only). This is valid only for object files.
        const IMAGE_SCN_GPREL = 0x0000_8000;  // The section contains data referenced through the global pointer (GP).
        const IMAGE_SCN_MEM_PURGEABLE = 0x0002_0000;  // Reserved for future use.
        const IMAGE_SCN_MEM_16BIT = 0x0002_0000;  // Reserved for future use.
        const IMAGE_SCN_MEM_LOCKED = 0x0004_0000;  // Reserved for future use.
        const IMAGE_SCN_MEM_PRELOAD = 0x0008_0000;  // Reserved for future use.
        const IMAGE_SCN_ALIGN_1BYTES = 0x0010_0000;  // Align data on a 1-byte boundary. Valid only for object files.
        const IMAGE_SCN_ALIGN_2BYTES = 0x0020_0000;  // Align data on a 2-byte boundary. Valid only for object files.
        const IMAGE_SCN_ALIGN_4BYTES = 0x0030_0000;  // Align data on a 4-byte boundary. Valid only for object files.
        const IMAGE_SCN_ALIGN_8BYTES = 0x0040_0000;  // Align data on an 8-byte boundary. Valid only for object files.
        const IMAGE_SCN_ALIGN_16BYTES = 0x0050_0000;  // Align data on a 16-byte boundary. Valid only for object files.
        const IMAGE_SCN_ALIGN_32BYTES = 0x0060_0000;  // Align data on a 32-byte boundary. Valid only for object files.
        const IMAGE_SCN_ALIGN_64BYTES = 0x0070_0000;  // Align data on a 64-byte boundary. Valid only for object files.
        const IMAGE_SCN_ALIGN_128BYTES = 0x0080_0000;  // Align data on a 128-byte boundary. Valid only for object files.
        const IMAGE_SCN_ALIGN_256BYTES = 0x0090_0000;  // Align data on a 256-byte boundary. Valid only for object files.
        const IMAGE_SCN_ALIGN_512BYTES = 0x00A0_0000;  // Align data on a 512-byte boundary. Valid only for object files.
        const IMAGE_SCN_ALIGN_1024BYTES = 0x00B0_0000;  // Align data on a 1024-byte boundary. Valid only for object files.
        const IMAGE_SCN_ALIGN_2048BYTES = 0x00C0_0000;  // Align data on a 2048-byte boundary. Valid only for object files.
        const IMAGE_SCN_ALIGN_4096BYTES = 0x00D0_0000;  // Align data on a 4096-byte boundary. Valid only for object files.
        const IMAGE_SCN_ALIGN_8192BYTES = 0x00E0_0000;  // Align data on an 8192-byte boundary. Valid only for object files.
        const IMAGE_SCN_LNK_NRELOC_OVFL = 0x0100_0000;  // The section contains extended relocations.
        const IMAGE_SCN_MEM_DISCARDABLE = 0x0200_0000;  // The section can be discarded as needed.
        const IMAGE_SCN_MEM_NOT_CACHED = 0x0400_0000;  // The section cannot be cached.
        const IMAGE_SCN_MEM_NOT_PAGED = 0x0800_0000;  // The section is not pageable.
        const IMAGE_SCN_MEM_SHARED = 0x1000_0000;  // The section can be shared in memory.
        const IMAGE_SCN_MEM_EXECUTE = 0x2000_0000;  // The section can be executed as code.
        const IMAGE_SCN_MEM_READ = 0x4000_0000;  // The section can be read.
        const IMAGE_SCN_MEM_WRITE = 0x8000_0000;  // The section can be written to.
    }
}

#[packed_struct]
#[derive(Clone, Copy, Eq, PartialEq)]
pub struct ImportDirectoryEntry {
    import_lut_rva: u32,
    timestamp: u32,
    forwarder_chain: u32,
    name_rva: u32,
    import_address_table_rva: u32,
}

impl ImportDirectoryEntry {
    pub fn new(import_lut_rva: u32, name_rva: u32, import_address_table_rva: u32) -> Self {
        Self {
            import_lut_rva,
            timestamp: 0,
            forwarder_chain: 0,
            name_rva,
            import_address_table_rva,
        }
    }

    pub fn terminator() -> Self {
        Self {
            import_lut_rva: 0,
            timestamp: 0,
            forwarder_chain: 0,
            name_rva: 0,
            import_address_table_rva: 0,
        }
    }

    pub fn validate(&self, section: &SectionHeader) -> Result<()> {
        ensure!(self.timestamp() == 0, "expected zero timestamp");
        ensure!(self.forwarder_chain() == 0, "did not expect forwarding");
        ensure!(
            self.name_rva() > section.virtual_address(),
            "dll name not in section"
        );
        ensure!(
            self.name_rva() < section.virtual_address() + section.virtual_size(),
            "dll name not in section"
        );
        ensure!(self.name_rva() > self.import_lut_rva(), "names after luts");
        ensure!(
            self.name_rva() > self.import_address_table_rva(),
            "names after iats"
        );
        ensure!(
            self.import_address_table_rva() > self.import_lut_rva(),
            "iats after luts"
        );
        Ok(())
    }

    pub fn offset_of_import_lut_rva() -> usize {
        offset_of!(Self, import_lut_rva)
    }
    pub fn offset_of_name_rva() -> usize {
        offset_of!(Self, name_rva)
    }
    pub fn offset_of_import_address_table_rva() -> usize {
        offset_of!(Self, import_address_table_rva)
    }

    fn read_raw_import_entries(&self, data: &[u8]) -> Result<Vec<u32>> {
        // Just map whatever we can so we can find the null-terminator.
        let max_luts = data.len() / mem::size_of::<u32>();
        let (dwords, _) = Ref::<&[u8], [u32]>::new_slice_from_prefix(data, max_luts)
            .ok_or_else(|| anyhow!("failed to transmute lut_table"))?;

        let mut entries = Vec::new();
        let mut ordinal = 0;
        while dwords[ordinal] != 0 {
            entries.push(dwords[ordinal]);
            ordinal += 1;
            ensure!(
                ordinal < dwords.len(),
                "read past end of import entries (missing terminator)"
            );
        }

        Ok(entries)
    }

    fn decode_import_entry(
        &self,
        ordinal: u32,
        entry: u32,
        section: &SectionHeader,
        idata: &[u8],
    ) -> Result<ImportEntry> {
        ensure!(entry >> 31 == 0, "only rva luts are supported");
        let import_name_rva = entry & 0x7FFF_FFFF;
        ensure!(
            import_name_rva > section.virtual_address(),
            "import name addr before idata"
        );
        ensure!(
            import_name_rva < section.virtual_address() + section.virtual_size(),
            "import name rva beyond idata"
        );
        ensure!(
            import_name_rva > self.name_rva(),
            "expected import names after dir name"
        );
        // 2 byte "hint" followed by the null-terminated string.
        let import_name_offset = (import_name_rva - section.virtual_address()) as usize;
        let hint =
            u16::from_le_bytes((&idata[import_name_offset..import_name_offset + 2]).try_into()?);
        ensure!(hint == 0, "hint table entries are not supported");
        let name = PortableExecutable::read_name(&idata[import_name_offset + 2..])?;

        // We have the table base for lut and iat, so we can compute the rva for each entry
        // from the ordinal.
        let lut_rva = self.import_lut_rva() + 4 * ordinal;
        let iat_rva = self.import_address_table_rva() + 4 * ordinal;

        Ok(ImportEntry::new(lut_rva, iat_rva, import_name_rva, name))
    }

    pub fn read_directory(
        &self,
        section: &SectionHeader,
        idata: &[u8],
    ) -> Result<Vec<ImportEntry>> {
        let import_luts = {
            let import_lut_offset = (self.import_lut_rva() - section.virtual_address()) as usize;
            ensure!(
                import_lut_offset == mem::size_of::<ImportDirectoryEntry>() * 2,
                "Import LUT located behind directory def"
            );
            let import_lut_data = &idata[import_lut_offset..];
            self.read_raw_import_entries(import_lut_data)?
        };

        let import_ats = {
            let import_at_offset =
                (self.import_address_table_rva() - section.virtual_address()) as usize;
            let import_at_data = &idata[import_at_offset..];
            self.read_raw_import_entries(import_at_data)?
        };
        ensure!(import_luts == import_ats, "luts and iat must be identical");

        let mut imports = vec![];
        for (i, &raw_iat) in import_ats.iter().enumerate() {
            let entry = self.decode_import_entry(i as u32, raw_iat, section, idata)?;
            imports.push(entry);
        }
        Ok(imports)
    }
}

#[derive(Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
pub struct ImportEntry {
    lut_rva: u32,
    iat_rva: u32,
    name_rva: u32,
    name: String,
}

impl ImportEntry {
    pub fn new(lut_rva: u32, iat_rva: u32, name_rva: u32, name: String) -> Self {
        Self {
            lut_rva,
            iat_rva,
            name_rva,
            name,
        }
    }

    pub fn lut_rva(&self) -> u32 {
        self.lut_rva
    }

    pub fn iat_rva(&self) -> u32 {
        self.iat_rva
    }

    pub fn name_rva(&self) -> u32 {
        self.name_rva
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub(crate) fn relocate(&mut self, delta: &RelocationDelta) {
        self.iat_rva = delta.apply(self.iat_rva);
    }
}

#[packed_struct]
#[derive(Copy, Clone)]
pub struct BaseRelocation {
    page_rva: u32,
    block_size: u32,
}

impl BaseRelocation {
    pub fn new(page_rva: u32, block_size: u32) -> Self {
        Self {
            page_rva,
            block_size,
        }
    }
}

pub const DOS_PREFIX: &[u8] = &[
    0o115, 0o132, 0o200, 0o000, 0o001, 0o000, 0o000, 0o000, 0o004, 0o000, 0o000, 0o000, 0o377,
    0o377, 0o000, 0o000, 0o270, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o100, 0o000,
    0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000,
    0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000,
    0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o200, 0o000, 0o000, 0o000, 0o016,
    0o037, 0o272, 0o016, 0o000, 0o264, 0o011, 0o315, 0o041, 0o270, 0o001, 0o114, 0o315, 0o041,
    0o124, 0o150, 0o151, 0o163, 0o040, 0o160, 0o162, 0o157, 0o147, 0o162, 0o141, 0o155, 0o040,
    0o143, 0o141, 0o156, 0o156, 0o157, 0o164, 0o040, 0o142, 0o145, 0o040, 0o162, 0o165, 0o156,
    0o040, 0o151, 0o156, 0o040, 0o104, 0o117, 0o123, 0o040, 0o155, 0o157, 0o144, 0o145, 0o056,
    0o015, 0o015, 0o012, 0o044, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000, 0o000,
];

pub const DOSX_SECTION_DATA: &[u8] = &[
    68, 88, 0, 0, 0, 0, 1, 0, 16, 0, 6, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0,
];

// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.

use crate::{
    file_layout::{
        COFFHeader, DataDirectory, ImportDirectoryEntry, OptionalHeader, SectionFlags,
        SectionHeader, WindowsHeader, DOS_PREFIX,
    },
    {BaseRelocation, DOSX_SECTION_DATA},
};
use std::{collections::HashMap, mem};
use zerocopy::AsBytes;

fn align_to(raw: u32, align: u32) -> u32 {
    (raw + align - 1) & !(align - 1)
}

fn align_to_next(raw: u32, align: u32) -> u32 {
    (raw + align) & !(align - 1)
}

#[derive(Clone, Default)]
pub struct PeBuilder {
    // Direct metadata inputs
    time_date_stamp: u32,
    imports: Vec<String>,
    relocs: Vec<u32>,
}

impl PeBuilder {
    pub fn with_time_date_stamp(mut self, stamp: u32) -> Self {
        self.time_date_stamp = stamp;
        self
    }

    pub fn add_import(&mut self, import_info: &str) {
        self.imports.push(import_info.to_owned());
    }

    pub fn add_reloc(&mut self, code_offset: u32) {
        self.relocs.push(code_offset);
    }

    pub fn build(&mut self, code: Vec<u8>) -> Vec<u8> {
        let size_of_code = code.len() as u32;
        let base_of_code = 4096;
        let base_of_data = align_to(base_of_code + code.len() as u32, 4096);

        let mut number_of_sections = 3; // CODE, [.idata], .relocs, $$DOSX$$
        if !self.imports.is_empty() {
            number_of_sections += 1; // .idata
        }

        // An idata section affects header size. Builtin models never overflow 1 extra block.
        let size_of_headers = if self.imports.is_empty() && self.relocs.is_empty() {
            0x200
        } else {
            0x400
        };

        // Push the CODE section and section header.
        let code_hdr = SectionHeader::new(
            *b"CODE\0\0\0\0",
            size_of_code,
            base_of_code,
            align_to_next(size_of_code, 512),
            size_of_headers, // 0x200 or 0x400; first after headers
            SectionHeader::flags_for_section_name("CODE").expect("section naming"),
        );
        let mut section_headers = vec![code_hdr];
        let mut sections_by_name = HashMap::new();
        sections_by_name.insert("CODE", code_hdr);
        let mut section_data = vec![code];

        // if there are imports, create the idata and header for it.
        if !self.imports.is_empty() {
            let prior = section_headers.last().unwrap();
            let vaddr = prior.virtual_address() + align_to(prior.virtual_size(), 4096);
            let paddr = align_to_next(prior.pointer_to_raw_data() + prior.virtual_size(), 0x200);

            // Immediately after the pair of directory entries
            // is the list of LUT u32's, followed by a 0, followed by the same number
            // of IAT u32's in the same format (are literally identical), followed
            // by the name table, which includes all of the symbol names as well as
            // the directory name, then padding to section end (%512).
            let mut idata_lut = Vec::new();
            let mut idata_iat = Vec::new();
            for _ in &self.imports {
                idata_lut.extend_from_slice(&[0u8; 4]);
                idata_iat.extend_from_slice(&[0u8; 4]);
            }
            // Not sure why it's _2_ u32 instead of just the one marker.
            idata_lut.extend_from_slice(&[0u8; 8]);
            idata_iat.extend_from_slice(&[0u8; 8]);

            // Construct the string table and record offsets from the str table start.
            let mut name_offsets = HashMap::new();
            let mut idata_str = Vec::new();
            let last_name = self.imports.last().unwrap();
            for name in ["main.dll".to_owned()].iter().chain(self.imports.iter()) {
                name_offsets.insert(name.to_owned(), idata_str.len());
                idata_str.extend_from_slice(name.as_bytes());
                idata_str.push(0); // null terminator and/or hint
                if name != last_name {
                    if idata_str.len() % 2 == 0 {
                        idata_str.extend_from_slice(&[0; 2]);
                    } else {
                        idata_str.extend_from_slice(&[0; 3]);
                    }
                } else if idata_str.len() % 2 != 0 {
                    idata_str.extend_from_slice(&[0; 1]);
                }
            }

            // Now that we have our tables, compute the rva offsets.
            let import_lut_rva = vaddr + 2 * mem::size_of::<ImportDirectoryEntry>() as u32;
            let import_address_table_rva = import_lut_rva + idata_lut.len() as u32;
            let name_rva = import_address_table_rva + idata_iat.len() as u32;

            // Update the lut and iat with the discovered offsets
            for (i, name) in self.imports.iter().enumerate() {
                let name_offset = name_rva + (name_offsets.get(name).unwrap() - 2) as u32;
                idata_lut[i * 4..i * 4 + 4].copy_from_slice(&name_offset.to_le_bytes());
                idata_iat[i * 4..i * 4 + 4].copy_from_slice(&name_offset.to_le_bytes());
            }

            // Build the idata from our independent bits
            let mut idata = Vec::new();
            let dir_entry =
                ImportDirectoryEntry::new(import_lut_rva, name_rva, import_address_table_rva);
            idata.extend_from_slice(dir_entry.as_bytes());
            let term_entry = ImportDirectoryEntry::terminator();
            idata.extend_from_slice(term_entry.as_bytes());
            idata.extend_from_slice(&idata_lut);
            idata.extend_from_slice(&idata_iat);
            idata.extend_from_slice(&idata_str);

            let section_hdr = SectionHeader::new(
                *b".idata\0\0",
                idata.len() as u32,
                vaddr,
                align_to_next(idata.len() as u32, 512),
                paddr,
                SectionHeader::flags_for_section_name(".idata").expect("section naming"),
            );
            section_headers.push(section_hdr);
            sections_by_name.insert(".idata", section_hdr);
            section_data.push(idata);
        }

        // Relocs
        let reloc_raw_data_size;
        {
            let prior = section_headers.last().unwrap();
            let vaddr = prior.virtual_address() + align_to(prior.virtual_size(), 4096);
            let paddr = align_to_next(prior.pointer_to_raw_data() + prior.virtual_size(), 0x200);

            let mut reloc_data = Vec::new();
            let code_vaddr = sections_by_name.get("CODE").unwrap().virtual_address();
            let reloc_rvas = self
                .relocs
                .iter()
                .map(|v| *v + code_vaddr)
                .collect::<Vec<u32>>();
            let mut rva_by_page: HashMap<u32, Vec<u32>> = HashMap::new();
            for rva in reloc_rvas {
                let page = rva >> 12 << 12;
                let raw = rva & 0xFFF;
                rva_by_page.entry(page).or_default().push(raw);
            }
            let mut sorted_pages = rva_by_page.keys().cloned().collect::<Vec<u32>>();
            sorted_pages.sort();
            for &page_rva in &sorted_pages {
                let raw_relocs = rva_by_page.get(&page_rva).unwrap();
                let block_size = align_to(8 + 2 * raw_relocs.len() as u32, 4);
                let base_block = BaseRelocation::new(page_rva, block_size);
                reloc_data.extend_from_slice(base_block.as_bytes());
                for reloc in raw_relocs {
                    let raw = 0x3000u16 | *reloc as u16;
                    reloc_data.extend_from_slice(&raw.to_le_bytes());
                }
                // Pad the section with empty rvas until it is 32bit aligned
                if raw_relocs.len() % 2 != 0 {
                    reloc_data.extend_from_slice(&[0; 2]);
                }
            }

            let padded_size = align_to_next(paddr + reloc_data.len() as u32, 0x1000) - paddr;
            reloc_raw_data_size = reloc_data.len() as u32;

            let section_hdr = SectionHeader::new(
                *b".reloc\0\0",
                padded_size,
                vaddr,
                padded_size,
                paddr,
                SectionHeader::flags_for_section_name(".reloc").expect("section naming"),
            );
            section_headers.push(section_hdr);
            sections_by_name.insert(".reloc", section_hdr);
            section_data.push(reloc_data);
        }

        {
            let prior = section_headers.last().unwrap();
            let vaddr = prior.virtual_address() + align_to(prior.virtual_size(), 4096);
            let paddr = prior.pointer_to_raw_data() + align_to(prior.virtual_size(), 512);
            let section_hdr = SectionHeader::new(
                *b"$$DOSX\0\0",
                DOSX_SECTION_DATA.len() as u32,
                vaddr,
                align_to(DOSX_SECTION_DATA.len() as u32, 512),
                paddr,
                SectionHeader::flags_for_section_name("$$DOSX").expect("section naming"),
            );
            section_headers.push(section_hdr);
            sections_by_name.insert("$$DOSX", section_hdr);
            section_data.push(DOSX_SECTION_DATA.to_owned());
        }

        // Compute header sizes that require section info
        let size_of_heap_commit = 4096;
        let mut size_of_initialized_data = 0u32;
        let mut size_of_image = size_of_heap_commit;
        for section in &section_headers {
            size_of_image += align_to(section.virtual_size(), 4096);
            if SectionFlags::from_bits(section.characteristics())
                .unwrap()
                .contains(SectionFlags::IMAGE_SCN_CNT_INITIALIZED_DATA)
            {
                size_of_initialized_data += match section.name_str().unwrap().as_ref() {
                    "$$DOSX" => 0,                      // ignored, apparently
                    ".reloc" => section.virtual_size(), // already aligned, but to 0x200
                    ".idata" => align_to(section.virtual_size(), 0x1000),
                    _ => panic!("unexpected section"),
                };
            }
        }

        let pe_magic = &[0x50, 0x4c, 0, 0];
        let coff_header = COFFHeader::new(number_of_sections, self.time_date_stamp);
        let opt_header = OptionalHeader::new(
            size_of_code,
            size_of_initialized_data,
            base_of_code,
            base_of_data,
        );
        let win_header = WindowsHeader::new(
            size_of_image,
            // size_of_headers: Yes, it's hard coded to 0x400, even when code starts at 0x200.
            0x400,
        );
        let mut data_dirs = Vec::with_capacity(16);
        data_dirs.resize_with(16, DataDirectory::empty);
        if let Some(idata) = sections_by_name.get(".idata") {
            data_dirs[1].set(
                idata.virtual_address(),
                // Lists in idata are null-terminated. Not sure what this does, but it's always 40,
                // at least for SH files in FA.
                40,
            );
        }
        if let Some(reloc) = sections_by_name.get(".reloc") {
            data_dirs[5].set(reloc.virtual_address(), reloc_raw_data_size);
        }

        let mut out = Vec::new();
        // Fixed headers
        out.extend_from_slice(DOS_PREFIX);
        out.extend_from_slice(pe_magic);
        out.extend_from_slice(coff_header.as_bytes());
        out.extend_from_slice(opt_header.as_bytes());
        out.extend_from_slice(win_header.as_bytes());
        out.extend_from_slice(data_dirs.as_bytes());
        for section in &section_headers {
            out.extend_from_slice(section.as_bytes());
        }
        for (header, data) in section_headers.iter().zip(section_data.iter()) {
            let pad = header.pointer_to_raw_data() as usize - out.len();
            out.extend_from_slice(&vec![0; pad]);
            out.extend_from_slice(data);
        }

        out
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::PortableExecutable;
    use anyhow::Result;
    use catalog::{FileSystem, Order, Search};
    use installations::Installations;

    #[test]
    fn test_round_trip() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(
            Search::for_extension("SH")
                .in_collection("FA")
                .sort(Order::Asc)
                .must_match(),
        )? {
            println!("At: {info}");
            let data_0 = info.data()?;
            let pe_0 = PortableExecutable::from_bytes(&data_0)?;

            let mut builder =
                PeBuilder::default().with_time_date_stamp(pe_0.coff_header().time_date_stamp());
            for import in pe_0.imports() {
                builder.add_import(import.name());
            }
            for reloc in pe_0.relocs() {
                builder.add_reloc(reloc.code_offset() as u32);
            }
            let data_1 = builder.build(pe_0.code().to_owned());

            if let Some(idata_hdr) = pe_0.section(".idata") {
                let idata_rng = idata_hdr.pointer_to_raw_data() as usize
                    ..(idata_hdr.pointer_to_raw_data() + idata_hdr.virtual_size()) as usize;
                let idata_0 = data_0[idata_rng.clone()].to_vec();
                let idata_1 = data_1[idata_rng].to_vec();
                assert_eq!(idata_0, idata_1);
            }

            if let Some(reloc_hdr) = pe_0.section(".reloc") {
                let reloc_rng = reloc_hdr.pointer_to_raw_data() as usize
                    ..(reloc_hdr.pointer_to_raw_data() + reloc_hdr.virtual_size()) as usize;
                let reloc_0 = data_0[reloc_rng.clone()].to_vec();
                let reloc_1 = data_1[reloc_rng].to_vec();
                assert_eq!(reloc_0, reloc_1);
            }

            assert_eq!(data_0, data_1);
        }
        Ok(())
    }
}

// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::{degrees, meters, radians, scalar, Degrees, Length, Meters, Pt3, Radians};
use geodesy::{Bearing, Geodetic, PitchCline};
use glam::{DQuat, EulerRot};
use phase::Frame;

#[derive(Clone, Debug)]
struct InputState {
    in_rotate: bool,
}

/// The external camera is a fixed-upwards-orientation arc-ball around.
/// It tries to duplicate FA's kludgy keyboard controls while also offering
/// nice modern mouse control for those who can bother to reach for one.
#[derive(Clone, Debug)]
pub struct ExternalCameraController {
    input: InputState,

    bearing: Bearing,
    pitch: PitchCline,
    eye_distance: Length<Meters>,
}

impl Default for ExternalCameraController {
    fn default() -> Self {
        Self {
            input: InputState { in_rotate: false },
            bearing: Bearing::north(),
            pitch: PitchCline::new(degrees!(-15.)),
            eye_distance: meters!(40.),
        }
    }
}

impl ExternalCameraController {
    // Note: cribbing from arcball currently; figure out what DCS or MSFS does for this.
    pub fn get_frame(&self, player_frame: &Frame) -> Frame {
        let eye = self.eye_pt3(player_frame.geodetic());
        Frame::from_geodetic_bearing_and_pitch(Geodetic::from(eye), self.bearing, -self.pitch)
    }

    fn map_space_forward(&self) -> Pt3<Meters> {
        // Bearing is a map coords; e.g. 2-d with y as north, x as right.

        // Note: pitch is inverted because we are eye rel not target rel
        let mut bearing = self.bearing.dvec3(-self.pitch);
        // Invert x because map coordinates are inverted from our left-handed setup.
        bearing.x = -bearing.x;
        bearing * self.eye_distance
    }

    fn world_space_forward(&self, target: Geodetic) -> Pt3<Meters> {
        let orient = DQuat::from_euler(
            EulerRot::YXZ,
            -target.lon::<Radians>().f64(),
            -target.lat::<Radians>().f64(),
            0.,
        );
        orient * self.map_space_forward()
    }

    fn eye_pt3(&self, target: Geodetic) -> Pt3<Meters> {
        target.pt3() - Pt3::<Meters>::from(&self.world_space_forward(target))
    }

    pub fn set_pan_view(&mut self, pressed: bool) {
        self.input.in_rotate = pressed;
    }

    pub fn handle_mousemotion(&mut self, x: f64, y: f64) {
        if self.input.in_rotate {
            *self.bearing.angle_mut() += radians!(degrees!(x * 0.5));
            *self.bearing.angle_mut() =
                radians!(degrees!(self.bearing.angle::<Degrees>().f64() % 360.));
            *self.pitch.angle_mut() = radians!((self.pitch.angle::<Degrees>() + degrees!(y * 0.5))
                .clamp(degrees!(-80), degrees!(80)));
        }
    }

    pub fn handle_mousewheel(&mut self, delta: f64) {
        self.eye_distance *= scalar!(if delta > 0f64 { 1.1f64 } else { 0.9f64 });
        self.eye_distance = self.eye_distance.max(meters!(0.01));
    }
}

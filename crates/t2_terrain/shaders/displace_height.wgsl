// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <nitrogen/shader_shared/include/buffer_helpers.wgsl>
//!include <nitrogen/terrain/include/terrain_vertex.wgsl>
//!include <t2_terrain/include/t2_tile_set.wgsl>

@group(0) @binding(0) var<storage, read_write> vertices: array<TerrainVertex>;

@group(1) @binding(0) var<uniform> t2_info: T2Info;
@group(1) @binding(1) var height_texture: texture_2d<f32>;
@group(1) @binding(2) var height_sampler: sampler;

var<private> WORKGROUP_WIDTH: u32 = 65536u;

@compute @workgroup_size(64, 2, 1)
fn displace_height(@builtin(global_invocation_id) global_idx: vec3<u32>) {
    // One invocation per vertex.
    let i = global_idx.x + global_idx.y * WORKGROUP_WIDTH;
    let uv = maybe_tile_uv(arr_to_vec2_(vertices[i].graticule), t2_info);

    // FIXME: we need to sample this
    let height_dims = textureDimensions(height_texture);
    let raw_height = textureLoad(height_texture, vec2<i32>(uv.tile_uv * vec2<f32>(height_dims)), 0);
    let new_height = raw_height.r * 255. * t2_info.height_scale;

    let v_normal = arr_to_vec3_(vertices[i].normal);
    let old_position = arr_to_vec3_(vertices[i].position);
    let new_position = arr_to_vec3_(vertices[i].surface_position) + (f32(new_height) * v_normal);
    vertices[i].position = vec3_to_arr(mix(old_position, new_position, vec3<f32>(vec3(uv.inside))));
}

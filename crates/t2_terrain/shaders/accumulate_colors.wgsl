// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <nitrogen/camera/include/camera.wgsl>
//!include <nitrogen/shader_shared/include/fullscreen.wgsl>
//!include <t2_terrain/include/t2_tile_set.wgsl>

//// 0: our t2 input
@group(0) @binding(0) var<uniform> t2_info: T2Info;
@group(0) @binding(1) var height_texture: texture_2d<f32>;
@group(0) @binding(2) var height_sampler: sampler;
@group(0) @binding(3) var atlas_texture: texture_2d<f32>;
@group(0) @binding(4) var atlas_sampler: sampler;
@group(0) @binding(5) var base_color_texture: texture_2d<f32>;
@group(0) @binding(6) var base_color_sampler: sampler;
@group(0) @binding(7) var index_texture: texture_2d<u32>;
@group(0) @binding(8) var index_sampler: sampler;
@group(0) @binding(9) var<storage> t2_frames: array<T2Frame>;

//// 1: prior color accumulator from nitrogen
@group(1) @binding(0) var terrain_color_acc: texture_2d<f32>;

//!bindings [2] <nitrogen/terrain/bindings/offscreen_deferred.wgsl>
//!bindings [3] <nitrogen/camera/bindings/camera.wgsl>

fn get_color(coord: vec2<i32>) -> vec4<f32> {
    // Do a depth check to see if we're even looking at terrain.
    let depth = textureLoad(terrain_deferred_depth, coord, 0);
    if (depth > 0.) {
        // Project the graticule into uv for the given t2 tile.
        let grat_ll = textureLoad(terrain_deferred_texture, coord, 0).xy;
        let t2_base_ll = t2_base_graticule(t2_info);
        let t2_span_ll = t2_span_graticule(t2_info);
        let tile_uv = vec2(
            ((grat_ll.y - t2_base_ll.y) / t2_span_ll.y) * cos(grat_ll.x),
            1. - ((t2_base_ll.x - grat_ll.x) / t2_span_ll.x)
        );

        // TODO: blend at edges to avoid jarring transitions?
        // let inside = all(vec4<bool>(greaterThanEqual(tile_uv, vec2(0.)), lessThanEqual(tile_uv, vec2(1.))));
        var inside = false;
        if (
            tile_uv.x >= 0. &&
            tile_uv.y >= 0. &&
            tile_uv.x <= 1. &&
            tile_uv.y <= 1.
        ) {
            inside = true;
        }

        // Lookup our tile u/v in the index to get the Frame and orientation.
        let tile_uv_index = vec2<i32>(
            i32(tile_uv.x * t2_info.index_width),
            i32(tile_uv.y * t2_info.index_height)
        );
        let index = textureLoad(index_texture, tile_uv_index, 0);
        // uvec4 index = texelFetch(usampler2D(index_texture, index_sampler), tile_uv_index, 0);
        let frame = t2_frames[index.r];
        let orientation = index.g;

        // Map our u/v over the whole image to our u/v within this pixel of the index.
        let tmap_uv = vec2(
            fract(tile_uv.x * t2_info.index_width),
            fract(tile_uv.y * t2_info.index_height)
        );

        // Map from our index u/v into the atlas s/t
        var atlas_uv = vec2(0.);
        if (orientation == 0u) {
            atlas_uv = vec2(
                mix(frame.s0, frame.s1, tmap_uv.x),
                mix(frame.t0, frame.t1, tmap_uv.y)
            );
        } else if (orientation == 1u) {
            atlas_uv = vec2(
                mix(frame.s1, frame.s0, tmap_uv.y),
                mix(frame.t0, frame.t1, tmap_uv.x)
            );
        } else if (orientation == 2u) {
            atlas_uv = vec2(
                mix(frame.s1, frame.s0, tmap_uv.x),
                mix(frame.t1, frame.t0, tmap_uv.y)
            );
        } else {
            atlas_uv = vec2(
                mix(frame.s0, frame.s1, tmap_uv.y),
                mix(frame.t1, frame.t0, tmap_uv.x)
            );
        }

        // Lookup atlas s/t in the atlas
        // let tmap_color = textureSample(atlas_texture, atlas_sampler, atlas_uv);
        let atlas_dims = textureDimensions(atlas_texture);
        let tmap_color = textureLoad(atlas_texture, vec2<i32>(atlas_uv * vec2<f32>(atlas_dims)), 0);

        // Get the base color using the tile u/v. This will get mixed in if we do not have a TMap overlaid
        // at this position in the tile.
        let base_dims = textureDimensions(atlas_texture);
        let base_color = textureLoad(base_color_texture, vec2<i32>(tile_uv * vec2<f32>(base_dims)), 0);

        // Pick which color to use.
        // Fixme: not sure why this isn't working the same as the if below that does work.
        var new_color = mix(base_color, tmap_color, vec4(max(1., f32(index.r))));
        if (index.r == 0u) {
            new_color = base_color;
        } else {
            new_color = tmap_color;
        }

        // Blend based on whether we are inside.
        let old_color = textureLoad(terrain_color_acc, coord, 0);
        let result = mix(old_color, new_color, vec4(min(min(f32(inside), new_color.a), t2_info.blend_factor)));
        return result;
    }
    return vec4(0.);
}

struct VertexOutput {
    @location(0) texcoord: vec2<f32>,
    @builtin(position) position: vec4<f32>,
}

@vertex
fn main_vert(@builtin(vertex_index) i: u32) -> VertexOutput {
    let input = fullscreen_input(i);
    return VertexOutput(input.frame, input.clip);
}

struct FragOut {
    @location(0) color: vec4<f32>,
}

@fragment
fn main_frag(@location(0) uv: vec2<f32>) -> FragOut {
    let coord = vec2<i32>(uv * vec2(f32(camera.render_width), f32(camera.render_height)));
    return FragOut(get_color(coord));
}

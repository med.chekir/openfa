// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

struct T2Frame {
    s0: f32,
    s1: f32,
    t0: f32,
    t1: f32,
}

struct T2Info {
    base_graticule_lat: f32,
    base_graticule_lon: f32,
    span_graticule_lat: f32,
    span_graticule_lon: f32,
    index_width: f32,
    index_height: f32,
    height_scale: f32,
    blend_factor: f32,
}

fn t2_base_graticule(t2_info: T2Info) -> vec2<f32> {
    return vec2(
        t2_info.base_graticule_lat,
        t2_info.base_graticule_lon
    );
}

fn t2_span_graticule(t2_info: T2Info) -> vec2<f32> {
    return vec2(
        t2_info.span_graticule_lat,
        t2_info.span_graticule_lon
    );
}

struct MaybeTileUv {
    tile_uv: vec2<f32>,
    inside: bool,
}

fn maybe_tile_uv(grat_ll: vec2<f32>, t2_info: T2Info) -> MaybeTileUv {
    // Project the graticule into uv for the given t2 tile.
    let t2_base_ll = t2_base_graticule(t2_info);
    let t2_span_ll = t2_span_graticule(t2_info);
    let tile_uv = vec2(
        ((grat_ll.y - t2_base_ll.y) / t2_span_ll.y) * cos(grat_ll.x),
        1. - ((t2_base_ll.x - grat_ll.x) / t2_span_ll.x)
    );

    // TODO: blend at edges to avoid jarring transitions?
    // let inside = all(vec4<bool>(greaterThanEqual(tile_uv, vec2(0.)), lessThanEqual(tile_uv, vec2(1.))));
    var inside = false;
    if (
        tile_uv.x >= 0. &&
        tile_uv.y >= 0. &&
        tile_uv.x <= 1. &&
        tile_uv.y <= 1.
    ) {
        inside = true;
    }

    return MaybeTileUv(tile_uv, inside);
}
// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.

// Load LIB files; find files in them; hand out immutable pointers on request.

mod writer;

pub use crate::writer::LibWriter;

use anyhow::{anyhow, ensure, Result};
use byteorder::{ByteOrder, LittleEndian};
use catalog::{AssetProvider, FileInfo, FileStat, Search};
use log::trace;
use memmap2::{Mmap, MmapOptions};
use once_cell::sync::Lazy;
use packed_struct::packed_struct;
use regex::Regex;
use std::{
    borrow::Cow,
    collections::{BTreeMap, HashMap},
    fs, mem,
    path::{Path, PathBuf},
    str,
};

#[derive(Clone, Copy, Debug)]
pub enum CompressionType {
    None = 0,
    Lzss = 1,   // Compressed with LZSS
    PxPk = 3,   // No compression, but includes 4 byte inline header 'PXPK'
    PkWare = 4, // Compressed with PKWare zip algorithm
}

impl CompressionType {
    fn from_byte(b: u8) -> Result<Self> {
        ensure!(
            b <= 4,
            "invalid compression type byte '{}'; expected 0-4",
            b
        );
        Ok(match b {
            0 => CompressionType::None,
            1 => CompressionType::Lzss,
            3 => CompressionType::PxPk,
            4 => CompressionType::PkWare,
            _ => unreachable!(),
        })
    }

    pub fn to_byte(&self) -> u8 {
        match self {
            CompressionType::None => 0,
            CompressionType::Lzss => 1,
            CompressionType::PxPk => 3,
            CompressionType::PkWare => 4,
        }
    }

    pub fn name(&self) -> Option<&'static str> {
        match self {
            Self::None => None,
            Self::Lzss => Some("lzss"),
            Self::PxPk => Some("pxpk"),
            Self::PkWare => Some("pkware"),
        }
    }
}

#[derive(Clone, Debug, Hash, Ord, PartialOrd, Eq, PartialEq)]
pub struct Priority {
    priority: usize,
    version: usize,
    adjust: i64,
}

impl Priority {
    pub fn from_path(path: &Path, adjust: i64) -> Result<Self> {
        static PRIO_RE: Lazy<Regex> =
            Lazy::new(|| Regex::new(r"(\d+)([a-zA-Z]?)").expect("failed to create regex"));
        let filename = path
            .file_stem()
            .ok_or_else(|| anyhow!("priority: name must not start with a '.'"))?
            .to_str()
            .ok_or_else(|| anyhow!("priority: name not utf8"))?
            .to_owned();
        if let Some(caps) = PRIO_RE.captures(&filename) {
            let priority = caps
                .get(1)
                .ok_or_else(|| anyhow!("priority: expected number match"))?
                .as_str()
                .parse::<usize>()?;
            let version = Self::version_from_char(caps.get(2));
            Ok(Self {
                priority,
                version,
                adjust,
            })
        } else {
            Ok(Self {
                priority: 0,
                version: 0,
                adjust,
            })
        }
    }

    fn version_from_char(opt: Option<regex::Match>) -> usize {
        if opt.is_none() {
            return 0;
        }
        let c = opt.unwrap().as_str().to_uppercase().chars().next();
        if c.is_none() {
            return 0;
        }
        (1u8 + c.unwrap() as u8 - b'A') as usize
    }

    pub fn as_provider_priority(&self) -> i64 {
        let offset = (self.priority * 26 + self.version) as i64;
        -offset - self.adjust
    }
}

pub struct StatInfo {
    pub name: String,
    pub compression: CompressionType,
    pub packed_size: u64,
    pub unpacked_size: u64,
    pub path: Option<PathBuf>,
}

#[derive(Clone, Debug)]
pub struct PackedFileInfo {
    start_offset: usize,
    end_offset: usize,
    compression: CompressionType,
}

impl PackedFileInfo {
    pub fn new(start_offset: usize, end_offset: usize, compression: u8) -> Result<Self> {
        Ok(Self {
            start_offset,
            end_offset,
            compression: CompressionType::from_byte(compression)?,
        })
    }
}

#[packed_struct]
struct LibHeader {
    magic: [u8; 5], // EALIB
    count: u16,
}

impl LibHeader {
    pub fn new(count: u16) -> Self {
        Self {
            magic: [b'E', b'A', b'L', b'I', b'B'],
            count,
        }
    }
}

#[packed_struct]
struct LibEntry {
    name: [u8; 13],
    flags: u8,
    offset: u32,
}

impl LibEntry {
    pub fn new(filename: &[u8], offset: u32, ctype: CompressionType) -> Result<Self> {
        ensure!(filename.len() < 13);
        let mut name: [u8; 13] = [0u8; 13];
        name[0..filename.len()].copy_from_slice(filename);
        Ok(Self {
            name,
            flags: ctype.to_byte(),
            offset,
        })
    }
}

/*
impl DrawerInterface for LibDrawer {
    fn stat(&self, id: DrawerFileId) -> Result<DrawerFileMetadata> {
        ensure!(self.index.contains_key(&id));
        let info = &self.index[&id];
        Ok(match info.compression {
            CompressionType::None => DrawerFileMetadata::new(
                id,
                self.drawer_index[&id].to_owned(),
                info.compression.name(),
                (info.end_offset - info.start_offset) as u64,
                (info.end_offset - info.start_offset) as u64,
                format!("{}[uncompressed]", self.name),
            ),
            CompressionType::PkWare => {
                let unpacked_size = u32::from_le_bytes(
                    (&self.data[info.start_offset..info.start_offset + 4]).try_into()?,
                ) as u64;
                DrawerFileMetadata::new(
                    id,
                    self.drawer_index[&id].to_owned(),
                    info.compression.name(),
                    (info.end_offset - info.start_offset) as u64,
                    unpacked_size,
                    format!("{}[pk]", self.name),
                )
            }
            CompressionType::Lzss => {
                let unpacked_size = u32::from_le_bytes(
                    (&self.data[info.start_offset..info.start_offset + 4]).try_into()?,
                ) as u64;
                DrawerFileMetadata::new(
                    id,
                    self.drawer_index[&id].to_owned(),
                    info.compression.name(),
                    (info.end_offset - info.start_offset) as u64,
                    unpacked_size,
                    format!("{}[lz]", self.name),
                )
            }
            CompressionType::PxPk => unimplemented!(),
        })
    }
}
 */

#[derive(Debug)]
pub struct LibProvider {
    collection_context: (i64, String),
    provider_context: (i64, String),

    path: PathBuf,
    data: Mmap,
    index: HashMap<String, PackedFileInfo>,
}

impl LibProvider {
    pub fn new(path: &Path) -> Result<Self> {
        trace!("opening lib file {:?}", path);
        let fp = fs::File::open(path)?;
        let map = unsafe { MmapOptions::new().populate().map(&fp)? };

        // Header
        ensure!(map.len() >= mem::size_of::<LibHeader>(), "lib too short");
        let hdr = LibHeader::overlay_prefix(&map)?;
        ensure!(&hdr.magic == b"EALIB", "lib missing magic");

        // Entries
        let mut index: HashMap<String, PackedFileInfo> = HashMap::new();
        let entries_start = mem::size_of::<LibHeader>();
        let entries_end = entries_start + hdr.count() as usize * mem::size_of::<LibEntry>();
        ensure!(map.len() >= entries_end, "lib too short for entries");
        let entries = LibEntry::overlay_slice(&map[entries_start..entries_end])?;
        for i in 0..hdr.count() as usize {
            let entry = &entries[i];
            let name = String::from_utf8(entry.name().to_vec())?
                .trim_matches('\0')
                .to_uppercase();
            let end_offset = if i + 1 < hdr.count() as usize {
                entries[i + 1].offset() as usize
            } else {
                map.len()
            };
            // Note: there is at least one duplicate in ATF Gold's 2.LIB.
            let info = PackedFileInfo::new(entry.offset() as usize, end_offset, entry.flags())?;
            index.insert(name, info);
        }

        Ok(Self {
            collection_context: (0, "".to_owned()),
            provider_context: (0, "".to_owned()),
            path: path.to_owned(),
            data: map,
            index,
        })
    }

    // Read the entire lib into memory, sorted by name.
    fn all_content(&self) -> Result<BTreeMap<String, Vec<u8>>> {
        let mut out = BTreeMap::new();
        for name in self.index.keys() {
            let content = self.read(name)?;
            out.insert(name.to_owned(), content.to_vec());
        }
        Ok(out)
    }

    fn rewrite_content(&mut self, content: &BTreeMap<String, Vec<u8>>) -> Result<()> {
        // Close the mapping so that we can write to the backing path.
        let mut map = MmapOptions::new().len(1).map_anon()?.make_read_only()?;
        mem::swap(&mut map, &mut self.data);
        drop(map);

        // Rewrite the underlying file
        let mut writer = LibWriter::new(&self.path, content.len().try_into()?)?;
        for (name, data) in content {
            writer.add_bytes(name, data)?;
        }
        writer.finish()?;

        // Remap the file and recreate the index
        let reloaded = Self::new(&self.path)?;
        *self = reloaded;

        Ok(())
    }
}

impl AssetProvider for LibProvider {
    fn set_context(&mut self, collection: (i64, String), provider: (i64, String)) {
        self.collection_context = collection;
        self.provider_context = provider;
    }

    fn search(&self, search: &Search) -> Result<Vec<FileInfo>> {
        Ok(search
            .filter_iterator(Box::new(self.index.keys().map(|a| a.as_ref())))?
            .map(|name| FileInfo::new(self, name, &self.collection_context, &self.provider_context))
            .collect::<Vec<_>>())
    }

    fn lookup(&self, name: &'_ str) -> Option<FileInfo> {
        if let Some((filename, _)) = self.index.get_key_value(name) {
            Some(FileInfo::new(
                self,
                filename,
                &self.collection_context,
                &self.provider_context,
            ))
        } else {
            None
        }
    }

    fn stat(&self, name: &str) -> Result<FileStat> {
        let info = &self.index[name];
        let (raw_adjust, actual_size) = match info.compression {
            CompressionType::None => (
                0u64,
                info.end_offset.saturating_sub(info.start_offset) as u64,
            ),
            CompressionType::PkWare | CompressionType::Lzss => (
                4u64,
                u32::from_le_bytes(
                    (&self.data[info.start_offset..info.start_offset + 4]).try_into()?,
                ) as u64,
            ),
            CompressionType::PxPk => unimplemented!(),
        };
        Ok(FileStat::new(
            info.compression.name().unwrap_or("none"),
            (info.end_offset.saturating_sub(info.start_offset) as u64).saturating_sub(raw_adjust),
            actual_size,
        ))
    }

    fn read(&self, name: &str) -> Result<Cow<[u8]>> {
        ensure!(self.index.contains_key(name), "no such file: {name}");
        let info = &self.index[name];
        Ok(match info.compression {
            CompressionType::None => Cow::from(&self.data[info.start_offset..info.end_offset]),
            CompressionType::PkWare => {
                assert!(info.start_offset + 4 <= info.end_offset);
                let expect_output_size = LittleEndian::read_u32(&self.data) as usize;
                Cow::from(pksploder::explode(
                    &self.data[info.start_offset + 4..info.end_offset],
                    Some(expect_output_size),
                )?)
            }
            CompressionType::Lzss => {
                assert!(info.start_offset + 4 <= info.end_offset);
                let expect_output_size = LittleEndian::read_u32(&self.data) as usize;
                Cow::from(lzss::explode(
                    &self.data[info.start_offset + 4..info.end_offset],
                    Some(expect_output_size),
                )?)
            }
            CompressionType::PxPk => unimplemented!("pxpk compression"),
        })
    }

    fn write(&mut self, name: &str, data: &[u8]) -> Result<()> {
        // The directory is tightly packed with the entries, so adding an entry inherently
        // requires a re-write of the full content.
        let mut content = self.all_content()?;
        content.insert(name.to_owned(), data.to_owned());
        self.rewrite_content(&content)?;
        Ok(())
    }

    fn remove(&mut self, name: &str) -> Result<()> {
        // The directory is tightly packed with the entries, so removing an entry inherently
        // requires a re-write of the full content.
        let mut content = self.all_content()?;
        content.remove(name);
        self.rewrite_content(&content)?;
        Ok(())
    }
}

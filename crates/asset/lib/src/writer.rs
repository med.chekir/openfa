// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::{CompressionType, LibEntry, LibHeader};
use anyhow::{bail, ensure, Result};
use std::{
    borrow::Cow,
    fs::File,
    io::{BufReader, Read, Seek, SeekFrom, Write},
    mem,
    path::Path,
};
use zerocopy::AsBytes;

pub struct LibWriter {
    fp: File,

    // Write offset from start of next data write.
    cursor: u64,

    // File offset for computing write offset of LibEntry
    file_offset: usize,
    file_count: usize,
}

impl LibWriter {
    pub fn new(path: &Path, file_count: u16) -> Result<Self> {
        let mut fp = File::create(path)?;
        fp.write_all(LibHeader::new(file_count).as_bytes())?;
        let cursor =
            (mem::size_of::<LibHeader>() + mem::size_of::<LibEntry>() * file_count as usize) as u64;
        Ok(Self {
            fp,
            cursor,
            file_count: file_count as usize,
            file_offset: 0,
        })
    }

    pub fn add_file(&mut self, input: &Path) -> Result<()> {
        ensure!(self.file_offset < self.file_count);

        let mut f = File::open(input)?;
        let mut content = Vec::new();
        f.read_to_end(&mut content)?;

        let filename = input.file_name();
        ensure!(filename.is_some(), "directory passed to add_file");
        let mut filename = filename.unwrap().to_owned();
        ensure!(filename.is_ascii());
        filename.make_ascii_uppercase();
        let filename = filename.into_string().unwrap();

        self.add_bytes(&filename, &content)
    }

    pub fn should_compress(name: &str) -> bool {
        const PATTERNS: &[&str] = &["FBC", "SEQ", "VDO"];
        for &pat in PATTERNS {
            if name.ends_with(pat) {
                return false;
            }
        }
        true
    }

    pub fn add_bytes(&mut self, name: &str, data: &[u8]) -> Result<()> {
        let unpacked_size = data.len();
        let (data, ctype) = if Self::should_compress(name) {
            let mut out = Vec::new();
            pksploder::implode(
                &mut BufReader::new(data),
                &mut out,
                pksploder::CompressionType::Binary,
                pksploder::DictSize::Size3,
            )?;
            (Cow::Owned(out), CompressionType::PkWare)
        } else {
            (Cow::Borrowed(data), CompressionType::None)
        };

        // Compute the entry and write it.
        let entry = LibEntry::new(
            name.to_ascii_uppercase().as_bytes(),
            u32::try_from(self.cursor)?,
            ctype,
        )?;
        self.fp.seek(SeekFrom::Start(
            (mem::size_of::<LibHeader>() + mem::size_of::<LibEntry>() * self.file_offset) as u64,
        ))?;
        self.fp.write_all(entry.as_bytes())?;
        self.file_offset += 1;

        // Seek to the cursor and write... Note that this may be past the current end
        // rather than to the end for the first file.
        self.fp.seek(SeekFrom::Start(self.cursor))?;
        match ctype {
            CompressionType::None => {
                self.fp.write_all(&data)?;
                self.cursor += data.len() as u64;
            }
            CompressionType::PkWare => {
                self.fp.write_all(&(unpacked_size as u32).to_le_bytes())?;
                self.fp.write_all(&data)?;
                self.cursor += (mem::size_of::<u32>() + data.len()) as u64;
            }
            _ => bail!("unexpected compressed type {ctype:?}"),
        }

        Ok(())
    }

    pub fn finish(mut self) -> Result<()> {
        self.fp.flush()?;
        Ok(())
    }
}

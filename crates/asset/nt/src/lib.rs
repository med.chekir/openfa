// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
mod hardpoint;

pub use crate::hardpoint::HardpointType;

use anyhow::{bail, ensure, Result};
use ot::{ObjectType, ObjectTypeIo};
use std::{collections::HashMap, fmt, slice::Iter};
use xt_parse::{
    find_pointers, find_section, make_xt_struct_family, FromXt, Nothing, OutOfLineTable,
};

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
enum NpcTypeVersion {
    V0, // USNF, MF, ATF, ATFNATO
    V1, // FA, USNF97, ATFGOLD
}

impl NpcTypeVersion {
    fn from_len(cnt: usize) -> Result<Self> {
        Ok(match cnt {
            9 => NpcTypeVersion::V1,
            7 => NpcTypeVersion::V0,
            x => bail!("unknown npc version with {} lines", x),
        })
    }
}

// Wrap Vec<HP> so that we can impl FromField.
#[derive(Clone, Debug, Default)]
pub struct Hardpoints {
    all: Vec<HardpointType>,
}

impl Hardpoints {
    pub fn iter(&self) -> Iter<HardpointType> {
        self.all.iter()
    }

    pub fn serialize_to_lines(&self, ool: &mut OutOfLineTable) {
        ool.push_hardpoint_line(":hards");
        for (i, hp) in self.iter().enumerate() {
            ool.push_hardpoint_line(format!(";-------- hardpoint {}", i));
            hp.serialize_to_lines(ool);
        }
    }
}

impl FromXt for Hardpoints {
    fn from_xt(lines: &[&str], table: &HashMap<&str, Vec<&str>>) -> Result<Self> {
        ensure!(lines.len() % 12 == 0, "expected 12 lines per hardpoint");
        let mut hards = Vec::new();
        let mut off = 0usize;
        while off < lines.len() {
            let lns = lines[off..off + 12]
                .iter()
                .map(std::convert::AsRef::as_ref)
                .collect::<Vec<_>>();
            let ht = HardpointType::from_lines(Nothing, &lns, table)?;
            hards.push(ht);
            off += 12;
        }
        Ok(Self { all: hards })
    }
}

impl fmt::Display for Hardpoints {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "<Hardpoints:{}>", self.all.len())
    }
}

make_xt_struct_family![
NpcType(ot: ObjectType, version: NpcTypeVersion) {
    (V1, U32,   nt_flags,                    u32, Hex, "flags"),
    (V0, Ptr,   ct_name,          Option<String>, Ptr, ""),
    (V0, U8,    search_frequency_t,           u8, Dec, "searchFrequencyT"),
    (V0, U8,    unready_attack_t,             u8, Dec, "unreadyAttackT"),
    (V0, U8,    attack_t,                     u8, Dec, "attackT"),
    (V1, U16,   retarget_t,                  u16, Dec, "retargetT"),
    (V0, Num,   zone_dist,                   u16, Dec, "zoneDist"),
    (V0, U8,    num_hards,                    u8, Dec, "numHards"),
    (V0, Tbl,   hards,                Hardpoints, Tbl, "hards")
}];

impl NpcType {
    pub fn from_text(data: &str) -> Result<Self> {
        let lines = data.lines().collect::<Vec<&str>>();
        ensure!(
            lines[0] == "[brent's_relocatable_format]",
            "not a type file"
        );
        let pointers = find_pointers(&lines)?;
        let obj_lines = find_section(&lines, "OBJ_TYPE")?;
        let obj = ObjectType::from_lines(Nothing, &obj_lines, &pointers)?;
        let npc_lines = find_section(&lines, "NPC_TYPE")?;
        Self::from_lines(obj, &npc_lines, &pointers)
    }
}

impl NpcTypeIo {
    pub fn to_lines(&self, tbl: &mut OutOfLineTable) -> Vec<String> {
        let mut out = self.ot().to_lines(tbl);
        let raw = self.serialize_to_lines(tbl);

        out.extend(
            [
                "",
                ";---------------- START OF NPC_TYPE ----------------",
                "",
            ]
            .iter()
            .map(|v| v.to_string())
            .collect::<Vec<String>>(),
        );
        out.extend_from_slice(&raw);
        out.extend(
            ["", ";---------------- END OF NPC_TYPE ----------------", ""]
                .iter()
                .map(|v| v.to_string())
                .collect::<Vec<String>>(),
        );

        out
    }
}

impl ToString for NpcTypeIo {
    fn to_string(&self) -> String {
        let mut tbl = OutOfLineTable::default();
        let mut rows = self.to_lines(&mut tbl);
        rows.extend(tbl.take_hards());
        rows.extend(tbl.take_strings());
        rows.push("\tend".to_owned());
        rows.push(String::new());
        rows.join("\r\n")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use catalog::{FileSystem, Search};
    use installations::{from_dos_string, Installations};

    #[test]
    fn can_parse_all_npc_types() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(Search::for_extension("NT").must_match())? {
            println!("At: {info}");
            let contents = from_dos_string(info.data()?);
            let nt = NpcType::from_text(&contents)?;
            assert_eq!(nt.ot().ot_names().file_name(), info.name());
        }
        Ok(())
    }

    #[test]
    fn can_roundtrip() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(Search::for_extension("NT").in_collection("FA").must_match())? {
            println!("At: {info}");
            let contents = from_dos_string(info.data()?);

            let nt = NpcType::from_text(&contents)?;
            let nt_io = NpcTypeIo::from(&nt);
            let contents2 = nt_io.to_string();

            for (a, b) in contents.lines().zip(contents2.lines()) {
                assert_eq!(a, b);
            }
            assert_eq!(contents.lines().count(), contents2.lines().count());
        }
        Ok(())
    }
}

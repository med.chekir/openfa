// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use anyhow::Result;
use std::collections::HashMap;
use xt_parse::{make_xt_struct_family, Nothing, NothingIo, OutOfLineTable};

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
enum HardpointTypeVersion {
    V0,
}

impl HardpointTypeVersion {
    #[allow(clippy::unnecessary_wraps)] // actually necessary
    fn from_len(_: usize) -> Result<Self> {
        Ok(HardpointTypeVersion::V0)
    }
}

make_xt_struct_family![
HardpointType(parent: Nothing, version: HardpointTypeVersion) {
    (V0, U16, flags,                        u16, Hex,                 "flags"),
    (V0, I16, pos_x,               Length<Feet>, Dec,                 "pos.x"),
    (V0, I16, pos_y,               Length<Feet>, Dec,                 "pos.y"),
    (V0, I16, pos_z,               Length<Feet>, Dec,                 "pos.z"),
    (V0, I16, slew_h,                       i16, Dec,                 "slewH"),
    (V0, I16, slew_p,                       i16, Dec,                 "slewP"),
    (V0, U16, slew_limit_h,                 u16, Dec,            "slewLimitH"),
    (V0, U16, slew_limit_p,                 u16, Dec,            "slewLimitP"),
    (V0, Ptr, default_type_name, Option<String>, PtrN,     "defaultTypeName0"),
    (V0, U8,  max_weight,                    u8, Dec,             "maxWeight"),
    (V0, U16, max_items,                    u16, Dec,              "maxItems"),
    (V0, U8,  name,                          u8, Dec,                  "name")
}];

impl HardpointType {
    // Note: HardpointType is not a parent, it is a Tbl, so survives the transition of
    //       the NpcType to NpcTypeIo. As such, we deal without Io here, rather than
    //       in the output type. This is fine as the Hardpoint needs to be put in the
    //       out-of-line table anyway, so couldn't be generalized regardless.
    pub fn serialize_to_lines(&self, ool: &mut OutOfLineTable) {
        let io = HardpointTypeIo::from(self);
        let hard = io.serialize_to_lines(ool);
        for line in hard {
            ool.push_hardpoint_line(line);
        }
    }
}

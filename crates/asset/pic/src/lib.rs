// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::{anyhow, bail, ensure, Result};
use image::{DynamicImage, Pixel, RgbaImage};
use packed_struct::packed_struct;
use pal::{Palette, PaletteFragment};
use std::{borrow::Cow, mem};
use zerocopy::AsBytes;

#[packed_struct]
struct PicHeader {
    format: u16,
    width: u32,
    height: u32,
    pixels_offset: u32,
    pixels_size: u32,
    palette_offset: u32,
    palette_size: u32,
    spans_offset: u32,
    spans_size: u32,
    rowheads_offset: u32,
    rowheads_size: u32,
    font_data_offset: u32,

    // pad to 64 bytes
    padding: [u8; 18],
}

#[packed_struct]
struct Span {
    row: u16,
    start: u16,
    end: u16,
    index: u32,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum PicFormat {
    Texture,
    Image,
    Jpeg,
}

impl PicFormat {
    pub fn from_word(format: u16) -> Result<Self> {
        Ok(match format {
            0 => PicFormat::Texture,
            1 => PicFormat::Image,
            // Note that this is a totally normal jpeg start-of-stream marker and we should pass
            // the full data block to the jpeg decoder rather than trying to slice of the "header".
            0xD8FF => PicFormat::Jpeg,
            _ => bail!("unknown pic format: 0x{:04X}", format),
        })
    }
}

/// Pass to make_texture to control palette
pub enum PicPalette<'a> {
    Inline(Palette),
    System(&'a Palette),
}

impl<'a> AsRef<Palette> for PicPalette<'a> {
    fn as_ref(&self) -> &Palette {
        match self {
            Self::Inline(owned) => owned,
            Self::System(borrowed) => borrowed,
        }
    }
}

pub struct TextureFmt<'a> {
    header: &'a PicHeader,
    palette_frag: Option<PaletteFragment<'a>>,
    _row_heads: &'a [u32],
    pixels_data: &'a [u8],
}

pub struct ImageFmt<'a> {
    header: &'a PicHeader,
    palette_frag: Option<PaletteFragment<'a>>,
    spans: &'a [Span],
    pixels_data: &'a [u8],
}

pub struct JpegFmt<'a> {
    data: &'a [u8],
}

pub enum Pic<'a> {
    Texture(TextureFmt<'a>),
    Image(ImageFmt<'a>),
    Jpeg(JpegFmt<'a>),
}

impl<'a> Pic<'a> {
    /// Parses the header to get as much data as makes sense. Decoding PIC images on the CPU
    /// is generally much slower than on the GPU because of the depalettization that is required.
    /// Some PICs are jpegs and as such can only be done on the CPU. Care should be taken to
    /// check the format and make sure only relevant kinds of images flow through each path to
    /// avoid performance cliffs.
    pub fn from_bytes(data: &'a [u8]) -> Result<Pic> {
        let mut fmt_arr = [0u8; 2];
        fmt_arr.copy_from_slice(&data[0..2]);
        let format = u16::from_le_bytes(fmt_arr);
        let pic_format = PicFormat::from_word(format)?;

        Ok(match pic_format {
            PicFormat::Jpeg => Self::Jpeg(JpegFmt { data }),
            _ => {
                let header = PicHeader::overlay(&data[..mem::size_of::<PicHeader>()])?;
                assert_eq!(header.format(), format);
                ensure!(header.padding() == [0u8; 18]);
                ensure!(header.font_data_offset() == 0 || header.format() == 0);

                let palette_start = header.palette_offset() as usize;
                let palette_end = palette_start + header.palette_size() as usize;
                let palette_frag = if header.palette_size() > 0 {
                    Some(PaletteFragment::from_bytes(
                        &data[palette_start..palette_end],
                    )?)
                } else {
                    None
                };

                let pixels_start = header.pixels_offset() as usize;
                let pixels_end = pixels_start + header.pixels_size() as usize;

                match pic_format {
                    PicFormat::Texture => {
                        ensure!(header.rowheads_offset() != 0);
                        ensure!(header.rowheads_size() != 0);
                        ensure!(header.rowheads_size() == 4 * header.height());
                        let rh0 = header.rowheads_offset() as usize;
                        let rh_sz = header.rowheads_size() as usize;
                        let rh_data = &data[rh0..rh0 + rh_sz];
                        let row_heads = zerocopy::Ref::<&[u8], [u32]>::new_slice(rh_data)
                            .map(|v| v.into_slice())
                            .ok_or_else(|| anyhow!("cannot overlay row heads"))?;
                        #[cfg(debug_assertions)]
                        {
                            let mut offset = header.pixels_offset();
                            for head in row_heads {
                                ensure!(*head == offset);
                                offset += header.width();
                            }
                        }
                        Self::Texture(TextureFmt {
                            header,
                            palette_frag,
                            _row_heads: row_heads,
                            pixels_data: &data[pixels_start..pixels_end],
                        })
                    }
                    PicFormat::Image => {
                        ensure!(header.rowheads_size() == 0);
                        let spans_start = header.spans_offset() as usize;
                        let spans_end =
                            spans_start + header.spans_size() as usize /*- mem::size_of::<Span>()*/;
                        let spans = Span::overlay_slice(&data[spans_start..spans_end])?;
                        ensure!(spans[spans.len() - 1].row() == 0xFFFF);
                        Self::Image(ImageFmt {
                            header,
                            palette_frag,
                            spans: &spans[0..spans.len() - 1],
                            pixels_data: &data[pixels_start..pixels_end],
                        })
                    }
                    PicFormat::Jpeg => {
                        panic!("JPEG PICs handled at a higher level")
                    }
                }
            }
        })
    }

    // Return the pic as an image. This does the palette decoding on CPU, so is quite slow. Users
    // should prefer to use PicUploader, which does depalettization in batches, if possible. The
    // pic_data method can get the palettized image data, in some cases without copying, so can
    // also be used if you are moving to another palettized format.
    pub fn image<'b>(&self, base_palette: &'b Palette) -> Result<DynamicImage>
    where
        'b: 'a,
    {
        Ok(match self {
            Self::Jpeg(JpegFmt { data }) => image::load_from_memory(data)?,
            Self::Texture(fmt0) => {
                let palette = Self::palette_defrag(base_palette, &fmt0.palette_frag)?;
                DynamicImage::ImageRgba8(Self::depalettize_format0(
                    fmt0.header.width(),
                    fmt0.header.height(),
                    &palette,
                    fmt0.pixels_data,
                ))
            }
            Self::Image(fmt1) => {
                let palette = Self::palette_defrag(base_palette, &fmt1.palette_frag)?;
                DynamicImage::ImageRgba8(Self::depalettize_format1_span(
                    fmt1.header.width(),
                    fmt1.header.height(),
                    &palette,
                    fmt1.spans,
                    fmt1.pixels_data,
                )?)
            }
        })
    }

    pub fn width(&self) -> u32 {
        match self {
            Self::Texture(fmt) => fmt.header.width(),
            Self::Image(fmt) => fmt.header.width(),
            Self::Jpeg(_) => unimplemented!("width request for jpeg PIC"),
        }
    }

    pub fn height(&self) -> u32 {
        match self {
            Self::Texture(fmt) => fmt.header.height(),
            Self::Image(fmt) => fmt.header.height(),
            Self::Jpeg(_) => unimplemented!("height request for jpeg PIC"),
        }
    }

    pub fn palette_fragment(&self) -> Option<PaletteFragment> {
        match self {
            Self::Texture(fmt) => fmt.palette_frag,
            Self::Image(fmt) => fmt.palette_frag,
            Self::Jpeg(_) => unimplemented!("palette request for jpeg PIC"),
        }
    }

    pub fn is_texture(&self) -> bool {
        matches!(self, Self::Texture(_))
    }

    pub fn is_compressed(&self) -> bool {
        matches!(self, Self::Image(_))
    }

    pub fn is_jpeg(&self) -> bool {
        matches!(self, Self::Jpeg(_))
    }

    /// Return the raw, palettized pixels if stored directly, or build them from the spans data.
    pub fn pixel_data(&self) -> Result<Cow<'a, [u8]>> {
        Ok(match self {
            Self::Texture(fmt0) => Cow::Borrowed(fmt0.pixels_data),
            Self::Image(fmt1) => {
                let width = fmt1.header.width();
                let height = fmt1.header.height();
                let mut out = vec![0u8; (width * height) as usize];
                for span in fmt1.spans {
                    ensure!(u32::from(span.row()) < height);
                    ensure!((span.index() as usize) < fmt1.pixels_data.len());
                    ensure!(u32::from(span.start()) < width);
                    ensure!(u32::from(span.end()) < width);
                    ensure!(span.start() <= span.end());
                    ensure!(
                        span.index() as usize + ((span.end() - span.start()) as usize)
                            < fmt1.pixels_data.len()
                    );

                    let start_off = (span.row() as u32 * width + span.start() as u32) as usize;
                    let end_off = (span.row() as u32 * width + span.end() as u32) as usize;
                    let cnt = (span.end() - span.start() + 1) as usize;
                    out[start_off..=end_off].copy_from_slice(
                        &fmt1.pixels_data[span.index() as usize..span.index() as usize + cnt],
                    )
                }
                Cow::Owned(out)
            }
            Self::Jpeg(_) => panic!("pixel_data invalid when called on jpeg fmt images"),
        })
    }

    fn palette_defrag<'b>(
        base_palette: &'b Palette,
        fragment: &Option<PaletteFragment<'a>>,
    ) -> Result<Cow<'a, Palette>>
    where
        'b: 'a,
    {
        Ok(if let Some(frag) = fragment {
            let mut full = base_palette.clone();
            full.overlay_fragment(0, frag)?;
            Cow::Owned(full)
        } else {
            Cow::Borrowed(base_palette)
        })
    }

    fn depalettize_format0(width: u32, height: u32, palette: &Palette, pixels: &[u8]) -> RgbaImage {
        let mut imgbuf = RgbaImage::new(width, height);
        for (i, p) in imgbuf.pixels_mut().enumerate() {
            let pix = pixels[i] as usize;
            let mut clr = palette.rgba(pix);
            if pix == 0xFF {
                clr[3] = 0x00;
            }
            *p = clr;
        }
        imgbuf
    }

    fn depalettize_format1_span(
        width: u32,
        height: u32,
        palette: &Palette,
        spans: &[Span],
        pixels: &[u8],
    ) -> Result<RgbaImage> {
        let mut imgbuf = RgbaImage::new(width, height);
        for span in spans {
            ensure!(u32::from(span.row()) < height);
            ensure!((span.index() as usize) < pixels.len());
            ensure!(u32::from(span.start()) < width);
            ensure!(u32::from(span.end()) < width);
            ensure!(span.start() <= span.end());
            ensure!(span.index() as usize + ((span.end() - span.start()) as usize) < pixels.len());

            for (j, column) in (span.start()..=span.end()).enumerate() {
                let offset = span.index() as usize + j;
                let pix = pixels[offset] as usize;
                let clr = palette.rgba(pix);
                imgbuf.put_pixel(column as u32, span.row() as u32, clr);
            }
        }
        Ok(imgbuf)
    }

    fn palettize_format0(buffer: RgbaImage, pal: &Palette, quality: u8) -> Vec<u8> {
        fn find_closest_in_palette(color: image::Rgba<u8>, pal: &Palette, quality: u8) -> u8 {
            // c is target color, p is current palette color
            fn distance_squared(c: image::Rgba<u8>, p: image::Rgb<u8>) -> usize {
                assert_eq!(c[3], 255);
                let dr = p[0] as isize - c[0] as isize;
                let dg = p[1] as isize - c[1] as isize;
                let db = p[2] as isize - c[2] as isize;
                (dr * dr + dg * dg + db * db) as usize
            }

            // Given a small set of close colors (distance in .0), return the index of a close
            // color (index in .1), by weighted random walk over those colors.
            fn find_closest_dithered(top: &[(usize, usize)]) -> usize {
                let sum = top
                    .iter()
                    .fold(0usize, |acc, (x, _)| acc.saturating_add(*x));
                let inverted = top
                    .iter()
                    .map(|(x, i)| (sum.checked_div(*x).unwrap_or(sum), i))
                    .collect::<Vec<_>>();
                let sum = inverted.iter().fold(0, |acc, (x, _)| acc + x);
                let f: usize = fastrand::usize(0..sum.max(1));
                let mut acc = 0;
                for (x, i) in inverted {
                    acc += x;
                    if f < acc {
                        return *i;
                    }
                }
                top[0].1
            }

            // Transparent colors are always the last palette entry.
            if color[3] < 255 {
                return 255;
            }

            // Get the distance from our color to each palette entry and sort by distance, ascending.
            let mut dists = pal
                .iter()
                .enumerate()
                .map(|(i, pal_color)| (distance_squared(color, pal_color.to_rgb()), i))
                .collect::<Vec<(usize, usize)>>();
            dists.sort_by_key(|&(d, _)| d);

            // Always take an exact match, if present.
            if dists[0].0 == 0 {
                return dists[0].1 as u8;
            }

            // Otherwise, select up to `quality` palette entries to walk between, but set a limit
            // on distance: better to be uniformly slightly wrong than speckle a mostly right surface
            // with obviously wrong garbage.
            let mut q = 1;
            for i in 1..quality {
                if dists[i as usize].0 < 200 {
                    q = i;
                } else {
                    break;
                }
            }
            let plausible = &dists[0..q as usize];

            // Pick one from our plausible set by guided random walk
            let index = find_closest_dithered(plausible);
            index as u8
        }

        let dim = buffer.dimensions();
        let mut pix = Vec::with_capacity((dim.0 * dim.1) as usize);
        for c in buffer.pixels() {
            let index = find_closest_in_palette(*c, pal, quality);
            pix.push(index);
        }
        pix
    }

    /// Encode a new texture from the given image and palette.
    pub fn make_texture(image: RgbaImage, palette: PicPalette, dither_quality: u8) -> Vec<u8> {
        let dim = image.dimensions();

        let pix_offset = mem::size_of::<crate::PicHeader>() as u32;
        let pix_data = Self::palettize_format0(image, palette.as_ref(), dither_quality);
        assert_eq!(dim.0 * dim.1, pix_data.len() as u32);

        let pal_offset = pix_offset + pix_data.len() as u32;
        let pal_data = match palette {
            PicPalette::Inline(palette) => palette.as_bytes(),
            PicPalette::System(_) => Vec::new(),
        };

        let rh_offset = pal_offset + pal_data.len() as u32;
        let mut rh_data = Vec::with_capacity(dim.1 as usize);
        for y in 0..dim.1 {
            let offset = pix_offset + y * dim.0;
            rh_data.extend_from_slice(&offset.to_le_bytes());
        }

        let pic_header = PicHeader {
            format: 0,                           // format: u16,
            width: dim.0,                        // width: u32,
            height: dim.1,                       // height: u32,
            pixels_offset: pix_offset,           // pixels_offset: u32 as usize,
            pixels_size: pix_data.len() as u32,  // pixels_size: u32 as usize,
            palette_offset: pal_offset,          // palette_offset: u32 as usize,
            palette_size: pal_data.len() as u32, // palette_size: u32 as usize,
            spans_offset: 0,                     // spans_offset: u32 as usize,
            spans_size: 0,                       // spans_size: u32 as usize,
            rowheads_offset: rh_offset,          // rowheads_offset: u32 as usize,
            rowheads_size: rh_data.len() as u32, // rowheads_size: u32 as usize
            font_data_offset: 0,
            padding: [0u8; 18],
        };

        let mut out = Vec::new();
        out.extend_from_slice(pic_header.as_bytes());
        out.extend_from_slice(&pix_data);
        out.extend_from_slice(&pal_data);
        out.extend_from_slice(&rh_data);
        out
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use catalog::{FileSystem, Search};
    use installations::Installations;
    use std::{fs, path::Path};

    fn can_new_all_pics(section: usize, total_sections: usize) -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        let all = libs.search(Search::for_extension("PIC").must_match())?;
        let files = nitrous::segment_tests(section, total_sections, &all);
        for info in files {
            println!("At: {info}");
            let _img = Pic::from_bytes(&info.data()?)?;
        }
        Ok(())
    }
    nitrous::make_partial_test!(can_new_all_pics, 10);

    fn can_decode_all_pics(section: usize, total_sections: usize) -> Result<()> {
        let (libs, installs) = Installations::for_testing()?;
        let all = libs.search(Search::for_extension("PIC").must_match())?;
        let files = nitrous::segment_tests(section, total_sections, &all);
        for info in files {
            println!("At: {info}");
            let data = info.data()?;
            let pic = Pic::from_bytes(&data)?;
            let palette = installs.palette(info.collection_name())?;
            let img = pic.image(palette)?;

            if false {
                let name = format!(
                    "dump/{}/{}.png",
                    info.collection_name(),
                    info.name().split('.').collect::<Vec<_>>().first().unwrap()
                );
                let path = Path::new(&name);
                println!("Write: {}", path.display());
                let _ = fs::create_dir("dump");
                let _ = fs::create_dir(path.parent().unwrap());
                img.save(path)?;
            }
        }
        Ok(())
    }
    nitrous::make_partial_test!(can_decode_all_pics, 10);
}

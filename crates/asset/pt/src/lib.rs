// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
mod envelope;

pub use crate::envelope::{Envelope, EnvelopeIntersection, EnvelopeTbl};

use absolute_unit::{
    Angle, Degrees, Feet, Force, Length, Mass, MassRate, Meters, PoundsForce, PoundsMass, Seconds,
    Velocity,
};
use anyhow::{bail, ensure, Result};
use bitflags::bitflags;
use nt::{NpcType, NpcTypeIo};
use ot::ObjectType;
use std::{collections::HashMap, fmt, slice::Iter};
use xt_parse::{
    find_pointers, find_section, make_xt_struct_family, FromXt, Nothing, OutOfLineTable,
};

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
enum PlaneTypeVersion {
    // USNF
    V0,
    // ATFGOLD (and all others?)
    V1,
}

impl PlaneTypeVersion {
    fn from_len(cnt: usize) -> Result<Self> {
        Ok(match cnt {
            146 => PlaneTypeVersion::V1,
            130 => PlaneTypeVersion::V0,
            x => bail!("unknown pt version with {} lines", x),
        })
    }
}

#[derive(Copy, Clone, Debug)]
pub enum GloadExtrema {
    Inside(f64),
    Stall(f64),
    OverSpeed(f64),
    LiftFail(f64),
}

impl fmt::Display for GloadExtrema {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // For display in the envelope window
        match self {
            Self::Inside(v) => {
                fmt::Display::fmt(v, f)?;
                write!(f, "G")
            }
            Self::Stall(_) => write!(f, "stall"),
            Self::OverSpeed(_) => write!(f, "max-q"),
            Self::LiftFail(_) => write!(f, "lift-fail"),
        }
    }
}

impl GloadExtrema {
    pub fn max_g_load(&self) -> f64 {
        *match self {
            Self::Inside(f) => f,
            Self::Stall(f) => f,
            Self::OverSpeed(f) => f,
            Self::LiftFail(f) => f,
        }
    }

    pub fn min_g_load(&self) -> f64 {
        *match self {
            Self::Inside(f) => f,
            Self::Stall(f) => f,
            Self::OverSpeed(f) => f,
            Self::LiftFail(f) => f,
        }
    }
}

#[derive(Clone, Debug, Default)]
pub struct Envelopes {
    all: Vec<Envelope>,
    min_g: i16,
    max_g: i16,
}

impl FromXt for Envelopes {
    fn from_xt(lines: &[&str], table: &HashMap<&str, Vec<&str>>) -> Result<Self> {
        let mut off = 0usize;
        let mut envs = Vec::new();

        ensure!(lines.len() % 44 == 0, "expected 44 lines per envelope");
        let mut min_g = 1_000;
        let mut max_g = -1_000;
        while off < lines.len() {
            let lns = lines[off..off + 44]
                .iter()
                .map(std::convert::AsRef::as_ref)
                .collect::<Vec<_>>();
            let env: Envelope = EnvelopeTbl::from_lines(Nothing, &lns, table)?.into();
            if env.gload() > max_g {
                max_g = env.gload();
            }
            if env.gload() < min_g {
                min_g = env.gload();
            }
            envs.push(env);
            off += 44;
        }
        envs.sort_by_cached_key(|envelope| envelope.gload());
        for (i, env) in envs.iter().enumerate() {
            ensure!(
                env.gload() == i as i16 + min_g,
                "envelopes are not contiguous"
            );
        }
        Ok(Envelopes {
            all: envs,
            min_g,
            max_g,
        })
    }
}

impl Envelopes {
    pub fn serialize_to_lines(&self, ool: &mut OutOfLineTable) {
        ool.push_envelope_line(":env");
        ool.push_envelope_line("");
        for env in self.iter() {
            env.serialize_to_lines(ool);
        }
    }

    pub fn serialize_trailer(&self, name: &str, rows: &mut Vec<String>) -> Result<()> {
        rows.pop();
        rows.push(format!(";-------------------------------- {}", name));
        for g in 4..=self.max_g {
            rows.push(format!(
                "; {g}G area = {:0.6}",
                self.envelope(g).unwrap().area_imperial()?
                    / (self.envelope(g - 4).unwrap().max_speed().f64()
                        * self.envelope(g - 4).unwrap().max_altitude().f64()),
            ));
        }
        rows.push(format!("; ??? = rating for {}", name));
        Ok(())
    }

    pub fn iter(&self) -> Iter<Envelope> {
        self.all.iter()
    }

    pub fn min_g_load(&self) -> i16 {
        self.min_g
    }

    pub fn max_g_load(&self) -> i16 {
        self.max_g
    }

    pub fn envelope(&self, gload: i16) -> Option<&Envelope> {
        if gload < self.min_g || gload > self.max_g {
            return None;
        }
        let out = &self.all[gload as usize + (-self.min_g) as usize];
        assert_eq!(out.gload(), gload);
        Some(out)
    }

    /// Speed at which we cross 1g of lift
    pub fn find_min_lift_speed_at(
        &self,
        altitude: Length<Meters>,
    ) -> Option<Velocity<Meters, Seconds>> {
        self.envelope(1).unwrap().find_min_lift_speed_at(altitude)
    }

    pub fn find_max_lift_altitude_at(
        &self,
        speed: Velocity<Meters, Seconds>,
    ) -> Option<Length<Meters>> {
        self.envelope(1).unwrap().find_max_lift_altitude_at(speed)
    }

    pub fn find_g_load_maxima(
        &self,
        speed: Velocity<Meters, Seconds>,
        altitude: Length<Meters>,
    ) -> GloadExtrema {
        // From inside (tightest envelope) outwards.
        let mut prior = None;
        for envelope in self.all.iter().rev() {
            // Check if we are fully in this envelope.
            let intersect = envelope.find_g_load_extrema(speed, altitude);
            if let EnvelopeIntersection::Inside {
                to_stall,
                to_over_speed,
                to_lift_fail,
            } = intersect
            {
                return GloadExtrema::Inside(match prior {
                    // If we are in the highest g-load envelope, that is our max.
                    None => envelope.gload_f64(),
                    Some(EnvelopeIntersection::Stall(v)) => {
                        envelope.gload_f64() + (to_stall / (to_stall + v))
                    }
                    Some(EnvelopeIntersection::OverSpeed(v)) => {
                        envelope.gload_f64() + (to_over_speed / (to_over_speed + v))
                    }
                    Some(EnvelopeIntersection::LiftFail(v)) => {
                        envelope.gload_f64() + (to_lift_fail / (to_lift_fail + v))
                    }
                    Some(EnvelopeIntersection::Inside { .. }) => {
                        panic!("found non-returned intersection?")
                    }
                });
            } else {
                prior = Some(intersect);
            }

            // Our negative extrema is a different loop.
            if envelope.gload() == 0 {
                break;
            }
        }

        // Inside no envelopes... map from the last failed envelope, which should be 0.
        match prior {
            None => panic!("empty envelope!"),
            // Intersection here is distance in m/s from the nearest wall speed at this altitude.
            // In stall or lift fail we have no control authority, in overspeed we can't pull g's
            // without over-stressing the airframe.
            Some(EnvelopeIntersection::Stall(_)) => GloadExtrema::Stall(0.),
            Some(EnvelopeIntersection::OverSpeed(_)) => GloadExtrema::OverSpeed(0.),
            Some(EnvelopeIntersection::LiftFail(_)) => GloadExtrema::LiftFail(0.),
            // Broke after first envelope, therefore must be 0
            Some(EnvelopeIntersection::Inside { .. }) => GloadExtrema::Inside(0.),
        }
    }

    pub fn find_g_load_minima(
        &self,
        speed: Velocity<Meters, Seconds>,
        altitude: Length<Meters>,
    ) -> GloadExtrema {
        // From inside (tightest envelope) outwards.
        let mut prior = None;
        for envelope in self.all.iter() {
            // Check if we are fully in this envelope.
            let intersect = envelope.find_g_load_extrema(speed, altitude);
            if let EnvelopeIntersection::Inside {
                to_stall,
                to_over_speed,
                to_lift_fail,
            } = intersect
            {
                return GloadExtrema::Inside(match prior {
                    // If we are in the highest g-load envelope, that is our max.
                    None => envelope.gload_f64(),
                    Some(EnvelopeIntersection::Stall(v)) => {
                        envelope.gload_f64() - (to_stall / (to_stall + v))
                    }
                    Some(EnvelopeIntersection::OverSpeed(v)) => {
                        envelope.gload_f64() - (to_over_speed / (to_over_speed + v))
                    }
                    Some(EnvelopeIntersection::LiftFail(v)) => {
                        envelope.gload_f64() - (to_lift_fail / (to_lift_fail + v))
                    }
                    Some(EnvelopeIntersection::Inside { .. }) => {
                        panic!("found non-returned intersection?")
                    }
                });
            } else {
                prior = Some(intersect);
            }

            // Our negative extrema is a different loop.
            if envelope.gload() == 0 {
                break;
            }
        }

        // Inside no envelopes... map from the last failed envelope, which should be 0.
        match prior {
            None => panic!("empty envelope!"),
            // Intersection here is distance in m/s from the nearest wall speed at this altitude.
            // In stall or lift fail we have no control authority, in overspeed we can't pull g's
            // without over-stressing the airframe.
            Some(EnvelopeIntersection::Stall(_)) => GloadExtrema::Stall(0.),
            Some(EnvelopeIntersection::OverSpeed(_)) => GloadExtrema::OverSpeed(0.),
            Some(EnvelopeIntersection::LiftFail(_)) => GloadExtrema::LiftFail(0.),
            // Broke after first envelope, therefore must be 0
            Some(EnvelopeIntersection::Inside { .. }) => GloadExtrema::Inside(0.),
        }
    }
}

impl fmt::Display for Envelopes {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "<Envelopes:{},{}>", self.min_g, self.max_g)
    }
}

bitflags! {
    #[derive(Clone, Copy, Debug, Eq, PartialEq)]
    pub struct PlaneFlags : u32 {
        const HAS_JET_ENGINE     = 0b0_0000_0000_0000_0001;
        const HAS_HOOK           = 0b0_0000_0000_0000_0010;
        const TWO_SEAT_COCKPIT   = 0b0_0000_0000_0000_0100;
        const IS_HELICOPTER      = 0b0_0000_0000_0000_1000;
        const HAS_EJECTION_SEATS = 0b0_0000_0000_0001_0000;
        const VTOL_CAPABLE       = 0b0_0000_0000_0010_0000;
        const CARRIER_CAPABLE    = 0b0_0000_0000_0100_0000;
        const HAS_BAY            = 0b0_0000_0000_1000_0000;
        const UNK_BIT9           = 0b0_0000_0001_0000_0000; // F31{EFV} - horizontal vectoring?
        const UNK_BIT11          = 0b0_0000_0100_0000_0000; // ASTOV{LVF}, F31{EFV}, F29 - vertical vectoring, except where is F22?
        const IS_UNMANNED_DECOY  = 0b0_0001_0000_0000_0000;
        const UNK_BIT14          = 0b0_0010_0000_0000_0000; // ATL, E2, IL76, E3, P3
        const UNK_BIT15          = 0b0_0100_0000_0000_0000; // Recon drones + E8 JSTARS
        const IS_UNMANNED_ATTACK = 0b0_1000_0000_0000_0000;
        const IS_UNMANNED_RECON  = 0b1_0000_0000_0000_0000;
    }
}

impl PlaneFlags {
    pub fn off(&self) -> u32 {
        self.bits().trailing_zeros()
    }
}

impl Default for PlaneFlags {
    fn default() -> Self {
        0u32.into()
    }
}

impl fmt::Display for PlaneFlags {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:032b}", self.bits())
    }
}

impl From<u32> for PlaneFlags {
    fn from(value: u32) -> Self {
        Self::from_bits(value)
            .unwrap_or_else(|| panic!("not a valid plane flags pattern {value:032b}"))
    }
}

impl From<PlaneFlags> for u32 {
    fn from(value: PlaneFlags) -> Self {
        value.bits()
    }
}

impl PlaneFlags {
    pub fn label(&self) -> &'static str {
        match *self {
            Self::HAS_JET_ENGINE => "Aircraft has jet engines(s)",
            Self::HAS_HOOK => "Arrestor hook equipped",
            Self::TWO_SEAT_COCKPIT => "Two-seat cockpit",
            Self::IS_HELICOPTER => "Aircraft is helicopter",
            Self::HAS_EJECTION_SEATS => "Crew can bail out",
            Self::VTOL_CAPABLE => "Aircraft has VTOL capability",
            Self::CARRIER_CAPABLE => "Aircraft can takeoff/land from ships",
            Self::HAS_BAY => "Aircraft has a weapons bay",
            Self::UNK_BIT9 => "Aircraft can vector thrust horizontally?",
            Self::UNK_BIT11 => "Aircraft can vector thrust vertically?",
            Self::IS_UNMANNED_DECOY => "Aircraft is an unmanned decoy",
            Self::UNK_BIT14 => "Unknown (bit-14) ATL, E2, IL76, E3, P3",
            Self::UNK_BIT15 => "Unknown (bit-15) DRNR* + E8",
            Self::IS_UNMANNED_ATTACK => "Aircraft is an unmanned attack drone",
            Self::IS_UNMANNED_RECON => "Aircraft is an unmanned recon drone",
            _ => panic!("cannot label a combined bit pattern"),
        }
    }

    pub fn is_helicopter(&self) -> bool {
        self.contains(Self::IS_HELICOPTER)
    }
}

make_xt_struct_family![
PlaneType(nt: NpcType, version: PlaneTypeVersion) { // CMCHE.PT
(V0, U32,   pt_flags,                              PlaneFlags, Hex,             "flags"),
(V0, Tbl,   env,                                    Envelopes, Tbl,               "env"), // Pitch response appears to be largely based on envelope.
(V0, I16,   env_min,                                      i16, Dec,            "envMin"), // num negative g envelopes
(V0, I16,   env_max,                                      i16, Dec,            "envMax"), // num positive g envelopes
(V0, U16,   max_speed_sea_level,      Velocity<Feet, Seconds>, Dec,     "structure [0]"), // Max Speed @ Sea-Level (Mph)
(V0, U16,   max_speed_36a,            Velocity<Feet, Seconds>, Dec,     "structure [1]"), // Max Speed @ 36K Feet (Mph)
(V0, I16,   bv_x_min,                                     i16, Dec,         "_bv.x.min"), // Only blimp.pt has bv_* different from all others. Probably unused?
(V0, I16,   bv_x_max,                                     i16, Dec,         "_bv.x.max"), // "
(V0, I16,   bv_x_acc,                                     i16, Dec,         "_bv.x.acc"), // "
(V0, I16,   bv_x_dacc,                                    i16, Dec,        "_bv.x.dacc"), // "
(V0, I16,   bv_y_min,                                     i16, Dec,         "_bv.y.min"),
(V0, I16,   bv_y_max,                                     i16, Dec,         "_bv.y.max"),
(V0, I16,   bv_y_acc,                                     i16, Dec,         "_bv.y.acc"),
(V0, I16,   bv_y_dacc,                                    i16, Dec,        "_bv.y.dacc"),
(V0, I16,   bv_z_min,                                     i16, Dec,         "_bv.z.min"),
(V0, I16,   bv_z_max,                                     i16, Dec,         "_bv.z.max"),
(V0, I16,   bv_z_acc,                                     i16, Dec,         "_bv.z.acc"),
(V0, I16,   bv_z_dacc,                                    i16, Dec,        "_bv.z.dacc"),
(V0, I16,   brv_x_min,                                    i16, Dec,        "_brv.x.min"), // Max roll rate left, then right; acc is rate towards inceptor away from neutral, dacc is rate towards roll inceptor towards neutral
(V0, I16,   brv_x_max,                                    i16, Dec,        "_brv.x.max"),
(V0, I16,   brv_x_acc,                                    i16, Dec,        "_brv.x.acc"),
(V0, I16,   brv_x_dacc,                                   i16, Dec,       "_brv.x.dacc"),
(V0, I16,   brv_y_min,                                    i16, Dec,        "_brv.y.min"), // Acceleration of the elevator response from 0 to max-g; mostly are [-0,0], [-small,small], probably accel in g/s
(V0, I16,   brv_y_max,                                    i16, Dec,        "_brv.y.max"),
(V0, I16,   brv_y_acc,                                    i16, Dec,        "_brv.y.acc"),
(V0, I16,   brv_y_dacc,                                   i16, Dec,       "_brv.y.dacc"),
(V0, I16,   brv_z_min,                                    i16, Dec,        "_brv.z.min"), // No apparent effect; All acc/dacc are 90/90; Some, but not all, jumbos have -30/30 for min/max; all others have -45/45.
(V0, I16,   brv_z_max,                                    i16, Dec,        "_brv.z.max"),
(V0, I16,   brv_z_acc,                                    i16, Dec,        "_brv.z.acc"),
(V0, I16,   brv_z_dacc,                                   i16, Dec,       "_brv.z.dacc"),
(V0, I16,   gpull_aoa,                         Angle<Degrees>, Dec,          "gpullAOA"),
(V0, I16,   low_aoa_speed,                                i16, Dec,       "lowAOASpeed"),
(V0, I16,   low_aoa_pitch,                                i16, Dec,       "lowAOAPitch"),
(V1, I16,   turbulence_percent,                           i16, Dec, "turbulencePercent"),
(V0, I16,   rudder_yaw_min,                               i16, Dec,     "rudderYaw.min"), // Velocity and Acceleration of yaw offsets, like for roll, but within rudderSlip bounds.
(V0, I16,   rudder_yaw_max,                               i16, Dec,     "rudderYaw.max"), // Actual rate of yaw of plane appears independent of anything here.
(V0, I16,   rudder_yaw_acc,                               i16, Dec,     "rudderYaw.acc"),
(V0, I16,   rudder_yaw_dacc,                              i16, Dec,    "rudderYaw.dacc"),
(V0, I16,   rudder_slip,                                  i16, Dec,        "rudderSlip"), // Left and right yaw deflection offset; 90 appears about 45 degrees; 255 point to ~135 degrees; 360 points ~180 or a bit more off center
(V0, I16,   rudder_drag,                                  i16, Dec,        "rudderDrag"),
(V0, I16,   rudder_bank,                                  i16, Dec,        "rudderBank"), // Velocity of roll appears fixed and proportional to yaw, but much lower than degrees per second at 360, takes about 5 seconds to do a full roll
(V1, I16,   puff_rot_x_min,                               i16, Dec,     "puffRot.x.min"), // No apparent effect? Highly specialized acc/dacc. Maybe for vtol? Would make sense, given the name.
(V1, I16,   puff_rot_x_max,                               i16, Dec,     "puffRot.x.max"), // "
(V1, I16,   puff_rot_x_acc,                               i16, Dec,     "puffRot.x.acc"), // "
(V1, I16,   puff_rot_x_dacc,                              i16, Dec,    "puffRot.x.dacc"), // "
(V1, I16,   puff_rot_y_min,                               i16, Dec,     "puffRot.y.min"), // "
(V1, I16,   puff_rot_y_max,                               i16, Dec,     "puffRot.y.max"), // "
(V1, I16,   puff_rot_y_acc,                               i16, Dec,     "puffRot.y.acc"), // "
(V1, I16,   puff_rot_y_dacc,                              i16, Dec,    "puffRot.y.dacc"), // "
(V1, I16,   puff_rot_z_min,                               i16, Dec,     "puffRot.z.min"), // "
(V1, I16,   puff_rot_z_max,                               i16, Dec,     "puffRot.z.max"), // "
(V1, I16,   puff_rot_z_acc,                               i16, Dec,     "puffRot.z.acc"), // "
(V1, I16,   puff_rot_z_dacc,                              i16, Dec,    "puffRot.z.dacc"), // "
(V0, I16,   stall_warning_delay,                          i16, Dec, "stallWarningDelay"),
(V0, I16,   stall_delay,                                  i16, Dec,        "stallDelay"),
(V0, I16,   stall_severity,                               i16, Dec,     "stallSeverity"),
(V0, I16,   stall_pitch_down,                             i16, Dec,    "stallPitchDown"),
(V0, I16,   spin_entry,                                   i16, Dec,         "spinEntry"),
(V0, I16,   spin_exit,                                    i16, Dec,          "spinExit"),
(V0, I16,   spin_yaw_low,                                 i16, Dec,        "spinYawLow"),
(V0, I16,   spin_yaw_high,                                i16, Dec,       "spinYawHigh"),
(V0, I16,   spin_aoa_low,                                 i16, Dec,        "spinAOALow"),
(V0, I16,   spin_aoa_high,                                i16, Dec,       "spinAOAHigh"),
(V0, I16,   spin_bank_low,                                i16, Dec,       "spinBankLow"),
(V0, I16,   spin_bank_high,                               i16, Dec,      "spinBankHigh"),
(V0, I16,   gear_pitch,                                   i16, Dec,         "gearPitch"),
(V0, I16,   crash_speed_forward,                          i16, Dec, "crashSpeedForward"),
(V0, I16,   crash_speed_side,                             i16, Dec,    "crashSpeedSide"),
(V0, I16,   crash_speed_vertical,                         i16, Dec,"crashSpeedVertical"),
(V0, I16,   crash_pitch,                                  i16, Dec,        "crashPitch"),
(V0, I16,   crash_roll,                                   i16, Dec,         "crashRoll"),
(V0,  U8,   engines,                                       u8, Dec,           "engines"),
(V0, I16,   neg_g_limit,                                  i16, Dec,         "negGLimit"),
(V0, U32,   thrust,                        Force<PoundsForce>, Dec,            "thrust"),
(V0, U32,   aft_thrust,                    Force<PoundsForce>, Dec,         "aftThrust"),
(V0, I16,   throttle_acc,                                 i16, Dec,       "throttleAcc"),
(V0, I16,   throttle_dacc,                                i16, Dec,      "throttleDacc"),
(V1, I16,   vt_limit_up,                                  i16, Dec,         "vtLimitUp"),
(V1, I16,   vt_limit_down,                                i16, Dec,       "vtLimitDown"),
(V1, I16,   vt_speed,                                     i16, Dec,           "vtSpeed"),
(V0, I16,   fuel_consumption,    MassRate<PoundsMass,Seconds>, Dec,   "fuelConsumption"),
(V0, I16,   aft_fuel_consumption,MassRate<PoundsMass,Seconds>, Dec,"aftFuelConsumption"),
(V0, U32,   internal_fuel,                   Mass<PoundsMass>, Dec,      "internalFuel"),
(V0, I16,   coef_drag,                                    i16, Dec,          "coefDrag"),
(V0, I16,   _gpull_drag,                                  i16, Dec,        "_gpullDrag"),
(V0, I16,   air_brakes_drag,                              i16, Dec,     "airBrakesDrag"),
(V0, I16,   wheel_brakes_drag,                            i16, Dec,   "wheelBrakesDrag"),
(V0, I16,   flaps_drag,                                   i16, Dec,         "flapsDrag"),
(V0, I16,   gear_drag,                                    i16, Dec,          "gearDrag"),
(V0, I16,   bay_drag,                                     i16, Dec,           "bayDrag"),
(V0, I16,   flaps_lift,                                   i16, Dec,         "flapsLift"),
(V0, I16,   loaded_drag,                                  i16, Dec,        "loadedDrag"),
(V0, I16,   loaded_gpull_drag,                            i16, Dec,   "loadedGpullDrag"),
(V0, I16,   loaded_elevator,                              i16, Dec,    "loadedElevator"),
(V0, I16,   loaded_aileron,                               i16, Dec,     "loadedAileron"),
(V0, I16,   loaded_rudder,                                i16, Dec,      "loadedRudder"),
(V0, I16,   structure_warn_limit,                         i16, Dec,"structureWarnLimit"),
(V0, I16,   structure_limit,                              i16, Dec,    "structureLimit"),
(V0,  U8,   system_damage_0,                               u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_1,                               u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_2,                               u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_3,                               u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_4,                               u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_5,                               u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_6,                               u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_7,                               u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_8,                               u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_9,                               u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_10,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_11,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_12,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_13,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_14,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_15,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_16,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_17,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_18,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_19,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_20,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_21,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_22,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_23,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_24,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_25,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_26,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_27,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_28,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_29,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_30,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_31,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_32,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_33,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_34,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_35,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_36,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_37,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_38,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_39,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_40,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_41,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_42,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_43,                              u8, Dec,  "systemDamage [i]"),
(V0,  U8,   system_damage_44,                              u8, Dec,  "systemDamage [i]"),
(V0, I16,   misc_per_flight,                              i16, Dec,     "miscPerFlight"),
(V0, I16,   repair_multiplier,                            i16, Dec,  "repairMultiplier"),
(V0, U32,   max_takeoff_weight,              Mass<PoundsMass>, Dec,  "maxTakeoffWeight")
}];

impl PlaneType {
    pub fn from_text(data: &str) -> Result<Self> {
        let lines = data.lines().collect::<Vec<&str>>();
        ensure!(
            lines[0] == "[brent's_relocatable_format]",
            "not a type file"
        );
        let pointers = find_pointers(&lines)?;
        let obj_lines = find_section(&lines, "OBJ_TYPE")?;
        let obj = ObjectType::from_lines(Nothing, &obj_lines, &pointers)?;
        let npc_lines = find_section(&lines, "NPC_TYPE")?;
        let npc = NpcType::from_lines(obj, &npc_lines, &pointers)?;

        // The :hards and :env pointer sections are inside of the PLANE_TYPE section
        // for some reason, so filter those out by finding the first :foo.
        let plane_lines = find_section(&lines, "PLANE_TYPE")?
            .iter()
            .take_while(|&l| !l.starts_with(':'))
            .cloned()
            .collect::<Vec<_>>();
        Self::from_lines(npc, &plane_lines, &pointers)
    }
}

impl PlaneTypeIo {
    fn to_lines(&self, tbl: &mut OutOfLineTable) -> Vec<String> {
        let mut out = self.nt().to_lines(tbl);
        let raw = self.serialize_to_lines(tbl);

        out.extend(
            [
                "",
                ";---------------- START OF PLANE_TYPE ----------------",
                "",
            ]
            .iter()
            .map(|v| v.to_string())
            .collect::<Vec<String>>(),
        );
        out.extend_from_slice(&raw);

        out
    }

    pub fn to_string(&self) -> Result<String> {
        let mut tbl = OutOfLineTable::default();
        let mut rows = self.to_lines(&mut tbl);
        rows.extend(tbl.take_hards());
        rows.extend(tbl.take_envs());
        self.env
            .serialize_trailer(self.nt.ot().ot_names().short_name(), &mut rows)?;
        rows.extend(
            [
                "",
                ";---------------- END OF PLANE_TYPE ----------------",
                "",
            ]
            .iter()
            .map(|v| v.to_string())
            .collect::<Vec<String>>(),
        );
        rows.extend(tbl.take_strings());
        rows.push("\tend".to_owned());
        rows.push(String::new()); // expected trailing newline
        Ok(rows.join("\r\n"))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use catalog::{FileSystem, Search};
    use installations::{from_dos_string, Installations};
    use std::collections::HashSet;

    #[test]
    fn it_can_parse_all_plane_files() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        let mut check_count = HashSet::new();
        for info in libs.search(Search::for_extension("PT").must_match())? {
            println!("At: {info}");
            let contents = from_dos_string(info.data()?);
            let pt = PlaneType::from_text(contents.as_ref())?;
            if pt.puff_rot_x_acc != pt.brv_x_acc || pt.puff_rot_x_dacc != pt.brv_x_dacc {
                // Past USNF, these are VTOL airplanes only: AV8, SEAHAR, YAK, ASTOVL
                // VTOL was implemented in Marine Fighters, so it makes sense that puff_rot
                // is used exclusively in vtol flight mode.
                check_count.insert(info.name().to_owned());
            }
            assert_eq!(-pt.brv_x_min, pt.brv_x_max);
            assert_eq!(pt.brv_y_acc, pt.brv_y_dacc);
            assert_eq!(pt.brv_z_acc, 90);
            assert_eq!(pt.brv_z_dacc, 90);
            assert_eq!(pt.nt().ot().ot_names().file_name(), info.name());
        }
        // TODO: figure out why puff_rot != brv in only a handful of models
        assert!(check_count.len() <= 86);
        Ok(())
    }

    #[test]
    fn can_roundtrip() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(Search::for_extension("PT").in_collection("FA").must_match())? {
            println!("At: {info}");
            let contents = from_dos_string(info.data()?);

            let pt = PlaneType::from_text(&contents)?;
            let pt_io = PlaneTypeIo::from(&pt);
            let contents2 = pt_io.to_string()?;

            for (a, b) in contents.lines().zip(contents2.lines()) {
                // println!("{a:<50} | {b}");
                if b.contains("G area = ") || b.contains(" = rating for ") {
                    continue;
                }
                assert_eq!(a, b);
            }
            assert_eq!(contents.lines().count(), contents2.lines().count());
        }
        Ok(())
    }

    #[ignore]
    #[test]
    fn show_all_with() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(Search::for_extension("PT").in_collection("FA").must_match())? {
            let contents = from_dos_string(info.data()?);

            let pt = PlaneType::from_text(&contents)?;
            if !pt.pt_flags().contains(PlaneFlags::HAS_JET_ENGINE) {
                println!("X: {info}");
            }
        }
        Ok(())
    }
}

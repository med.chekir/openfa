// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use anyhow::Result;
use glam::DVec2;
use std::collections::HashMap;
use xt_parse::{make_xt_struct_family, Nothing, NothingIo, OutOfLineTable};

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
enum EnvelopeVersion {
    V0,
}

impl EnvelopeVersion {
    #[allow(clippy::unnecessary_wraps)] // actually necessary
    fn from_len(_: usize) -> Result<Self> {
        Ok(EnvelopeVersion::V0)
    }
}

#[derive(Clone, Debug)]
pub struct EnvelopeCoord {
    speed: Velocity<Feet, Seconds>,
    altitude: Length<Feet>,
}

impl EnvelopeCoord {
    fn new(speed: Velocity<Feet, Seconds>, altitude: Length<Feet>) -> Self {
        Self { speed, altitude }
    }

    pub fn speed(&self) -> Velocity<Meters, Seconds> {
        meters_per_second!(self.speed)
    }

    pub fn altitude(&self) -> Length<Meters> {
        meters!(self.altitude)
    }
}

// Result of testing a position with an envelope. Tracks where inside we are, or how far out in
// what direction. This lets us interpolate to get sub-G results from a nested envelope set.
#[derive(Debug)]
pub enum EnvelopeIntersection {
    Inside {
        to_stall: f64,
        to_over_speed: f64,
        to_lift_fail: f64,
    },
    Stall(f64),
    LiftFail(f64),
    OverSpeed(f64),
}

make_xt_struct_family![
EnvelopeTbl(parent: Nothing, version: EnvelopeVersion) {
(V0, I16, gload,                            i16, Dec, "env [ii].gload"),
(V0, U16, count,                            u16, Dec, "env [ii].count"),
(V0, U16, stall_lift,                       u16, Dec, "env [ii].stallLift"),
(V0, U16, max_speed_index,                  u16, Dec, "env [ii].maxSpeed"),
(V0, I16, data00_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data00_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt"),
(V0, I16, data01_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data01_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt"),
(V0, I16, data02_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data02_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt"),
(V0, I16, data03_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data03_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt"),
(V0, I16, data04_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data04_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt"),
(V0, I16, data05_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data05_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt"),
(V0, I16, data06_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data06_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt"),
(V0, I16, data07_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data07_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt"),
(V0, I16, data08_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data08_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt"),
(V0, I16, data09_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data09_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt"),
(V0, I16, data10_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data10_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt"),
(V0, I16, data11_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data11_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt"),
(V0, I16, data12_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data12_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt"),
(V0, I16, data13_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data13_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt"),
(V0, I16, data14_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data14_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt"),
(V0, I16, data15_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data15_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt"),
(V0, I16, data16_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data16_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt"),
(V0, I16, data17_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data17_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt"),
(V0, I16, data18_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data18_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt"),
(V0, I16, data19_speed, Velocity<Feet, Seconds>, Dec, "env [ii].data [j].speed"),
(V0, U32, data19_alt,    Length<Feet>,           Dec, "env [ii].data [j].alt")
}];

impl EnvelopeTbl {
    // Note: EnvelopeTbl is not a parent, it is a Tbl, so survives the transition of
    //       the NpcType to NpcTypeIo. As such, we deal without Io here, rather than
    //       in the output type. This is fine as the Hardpoint needs to be put in the
    //       out-of-line table anyway, so couldn't be generalized regardless.
    pub fn serialize_to_lines(&self, ool: &mut OutOfLineTable) {
        ool.push_envelope_line(format!(";--- envelope for G = {}", self.gload));
        let io = EnvelopeTblIo::from(self);
        let env = io.serialize_to_lines(ool);
        for line in env {
            ool.push_envelope_line(line);
        }
        ool.push_envelope_line("");
    }
}

impl EnvelopeTbl {
    pub fn all_data(&self) -> [(Velocity<Feet, Seconds>, Length<Feet>); 20] {
        [
            (self.data00_speed, self.data00_alt),
            (self.data01_speed, self.data01_alt),
            (self.data02_speed, self.data02_alt),
            (self.data03_speed, self.data03_alt),
            (self.data04_speed, self.data04_alt),
            (self.data05_speed, self.data05_alt),
            (self.data06_speed, self.data06_alt),
            (self.data07_speed, self.data07_alt),
            (self.data08_speed, self.data08_alt),
            (self.data09_speed, self.data09_alt),
            (self.data10_speed, self.data10_alt),
            (self.data11_speed, self.data11_alt),
            (self.data12_speed, self.data12_alt),
            (self.data13_speed, self.data13_alt),
            (self.data14_speed, self.data14_alt),
            (self.data15_speed, self.data15_alt),
            (self.data16_speed, self.data16_alt),
            (self.data17_speed, self.data17_alt),
            (self.data18_speed, self.data18_alt),
            (self.data19_speed, self.data19_alt),
        ]
    }
}

#[derive(Clone, Debug)]
pub struct Envelope {
    gload: i16,
    stall_lift: usize,
    max_speed_index: usize,
    shape: Vec<EnvelopeCoord>,
}

impl From<EnvelopeTbl> for Envelope {
    fn from(tbl: EnvelopeTbl) -> Self {
        Self {
            gload: tbl.gload,
            stall_lift: tbl.stall_lift as usize,
            max_speed_index: tbl.max_speed_index as usize,
            shape: tbl.all_data()[0..*tbl.count() as usize]
                .iter()
                .map(|(v, h)| EnvelopeCoord::new(*v, *h))
                .collect::<Vec<EnvelopeCoord>>(),
        }
    }
}

impl From<&Envelope> for EnvelopeTbl {
    fn from(env: &Envelope) -> Self {
        Self {
            parent: Nothing,
            gload: env.gload,
            count: env.shape.len() as u16,
            stall_lift: env.stall_lift as u16,
            max_speed_index: env.max_speed_index as u16,
            data00_speed: env.velocity_at(0).unwrap_or_else(|| feet_per_second!(0)),
            data00_alt: env.height_at(0).unwrap_or_else(|| feet!(0)),
            data01_speed: env.velocity_at(1).unwrap_or_else(|| feet_per_second!(0)),
            data01_alt: env.height_at(1).unwrap_or_else(|| feet!(0)),
            data02_speed: env.velocity_at(2).unwrap_or_else(|| feet_per_second!(0)),
            data02_alt: env.height_at(2).unwrap_or_else(|| feet!(0)),
            data03_speed: env.velocity_at(3).unwrap_or_else(|| feet_per_second!(0)),
            data03_alt: env.height_at(3).unwrap_or_else(|| feet!(0)),
            data04_speed: env.velocity_at(4).unwrap_or_else(|| feet_per_second!(0)),
            data04_alt: env.height_at(4).unwrap_or_else(|| feet!(0)),
            data05_speed: env.velocity_at(5).unwrap_or_else(|| feet_per_second!(0)),
            data05_alt: env.height_at(5).unwrap_or_else(|| feet!(0)),
            data06_speed: env.velocity_at(6).unwrap_or_else(|| feet_per_second!(0)),
            data06_alt: env.height_at(6).unwrap_or_else(|| feet!(0)),
            data07_speed: env.velocity_at(7).unwrap_or_else(|| feet_per_second!(0)),
            data07_alt: env.height_at(7).unwrap_or_else(|| feet!(0)),
            data08_speed: env.velocity_at(8).unwrap_or_else(|| feet_per_second!(0)),
            data08_alt: env.height_at(8).unwrap_or_else(|| feet!(0)),
            data09_speed: env.velocity_at(9).unwrap_or_else(|| feet_per_second!(0)),
            data09_alt: env.height_at(9).unwrap_or_else(|| feet!(0)),
            data10_speed: env.velocity_at(10).unwrap_or_else(|| feet_per_second!(0)),
            data10_alt: env.height_at(10).unwrap_or_else(|| feet!(0)),
            data11_speed: env.velocity_at(11).unwrap_or_else(|| feet_per_second!(0)),
            data11_alt: env.height_at(11).unwrap_or_else(|| feet!(0)),
            data12_speed: env.velocity_at(12).unwrap_or_else(|| feet_per_second!(0)),
            data12_alt: env.height_at(12).unwrap_or_else(|| feet!(0)),
            data13_speed: env.velocity_at(13).unwrap_or_else(|| feet_per_second!(0)),
            data13_alt: env.height_at(13).unwrap_or_else(|| feet!(0)),
            data14_speed: env.velocity_at(14).unwrap_or_else(|| feet_per_second!(0)),
            data14_alt: env.height_at(14).unwrap_or_else(|| feet!(0)),
            data15_speed: env.velocity_at(15).unwrap_or_else(|| feet_per_second!(0)),
            data15_alt: env.height_at(15).unwrap_or_else(|| feet!(0)),
            data16_speed: env.velocity_at(16).unwrap_or_else(|| feet_per_second!(0)),
            data16_alt: env.height_at(16).unwrap_or_else(|| feet!(0)),
            data17_speed: env.velocity_at(17).unwrap_or_else(|| feet_per_second!(0)),
            data17_alt: env.height_at(17).unwrap_or_else(|| feet!(0)),
            data18_speed: env.velocity_at(18).unwrap_or_else(|| feet_per_second!(0)),
            data18_alt: env.height_at(18).unwrap_or_else(|| feet!(0)),
            data19_speed: env.velocity_at(19).unwrap_or_else(|| feet_per_second!(0)),
            data19_alt: env.height_at(19).unwrap_or_else(|| feet!(0)),
        }
    }
}

impl Envelope {
    pub fn serialize_to_lines(&self, ool: &mut OutOfLineTable) {
        EnvelopeTbl::from(self).serialize_to_lines(ool);
    }

    pub fn gload(&self) -> i16 {
        self.gload
    }

    pub fn gload_f64(&self) -> f64 {
        self.gload as f64
    }

    pub fn shape(&self) -> &[EnvelopeCoord] {
        &self.shape
    }

    fn velocity_at(&self, i: usize) -> Option<Velocity<Feet, Seconds>> {
        self.shape.get(i).map(|c| c.speed)
    }

    fn height_at(&self, i: usize) -> Option<Length<Feet>> {
        self.shape.get(i).map(|c| c.altitude)
    }

    pub fn max_speed(&self) -> Velocity<Feet, Seconds> {
        feet_per_second!(self.shape[self.max_speed_index].speed())
    }

    pub fn max_altitude(&self) -> Length<Feet> {
        feet!(self.shape[self.max_speed_index].altitude())
    }

    pub fn area_imperial(&self) -> Result<f64> {
        let mut coords = Vec::new();
        for coord in &self.shape {
            coords.push(feet_per_second!(coord.speed()).f64());
            coords.push(feet!(coord.altitude()).f64());
        }
        let idx = earcutr::earcut(&coords, &[], 2)?;
        let mut total = 0.0;
        for i in 0..idx.len() / 3 {
            // each of the indices is an index into a pair
            let a = idx[i * 3];
            let b = idx[i * 3 + 1];
            let c = idx[i * 3 + 2];

            let ax = coords[a * 2];
            let ay = coords[a * 2 + 1];
            let bx = coords[b * 2];
            let by = coords[b * 2 + 1];
            let cx = coords[c * 2];
            let cy = coords[c * 2 + 1];
            let area = ((bx * ay - ax * by) + (cx * by - bx * cy) + (ax * cy - cx * ay)).abs() / 2.;
            total += area;
        }
        Ok(total)
    }

    // https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
    #[inline]
    fn segment_intersection(
        (p0_x, p0_y): (f64, f64),
        (p1_x, p1_y): (f64, f64),
        (p2_x, p2_y): (f64, f64),
        (p3_x, p3_y): (f64, f64),
    ) -> Option<(f64, f64)> {
        let s1_x = p1_x - p0_x;
        let s1_y = p1_y - p0_y;
        let s2_x = p3_x - p2_x;
        let s2_y = p3_y - p2_y;

        let s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
        let t = (s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

        if (0. ..=1.).contains(&s) && (0. ..=1.).contains(&t) {
            let i_x = p0_x + (t * s1_x);
            let i_y = p0_y + (t * s1_y);
            return Some((i_x, i_y));
        }
        None
    }

    pub fn find_g_load_extrema(
        &self,
        speed: Velocity<Meters, Seconds>,
        altitude: Length<Meters>,
    ) -> EnvelopeIntersection {
        self.is_in_envelope(speed, altitude)
    }

    pub fn is_in_envelope(
        &self,
        speed: Velocity<Meters, Seconds>,
        altitude: Length<Meters>,
    ) -> EnvelopeIntersection {
        let origin = DVec2::new(speed.f64(), altitude.f64());

        let ends = [
            // Left
            DVec2::new(-1_000f64, altitude.f64()),
            // Right
            DVec2::new(7_000f64, altitude.f64()),
            // Down
            DVec2::new(speed.f64(), -1_000f64),
            // Up
            DVec2::new(speed.f64(), 120_000f64),
        ];
        let mut counts = [0, 0, 0, 0];
        let mut minimums2 = [f64::INFINITY, f64::INFINITY, f64::INFINITY, f64::INFINITY];

        for (i, coord0) in self.shape.iter().enumerate() {
            let j = (i + 1) % self.shape.len();
            let coord1 = &self.shape[j];

            for dir in 0..4 {
                if let Some((intersect_x, intersect_y)) = Self::segment_intersection(
                    (origin.x, origin.y),
                    (ends[dir].x, ends[dir].y),
                    (coord0.speed().f64(), coord0.altitude().f64()),
                    (coord1.speed().f64(), coord1.altitude().f64()),
                ) {
                    counts[dir] += 1;
                    let dx = intersect_x - origin.x;
                    let dy = intersect_y - origin.y;
                    let d2 = dx * dx + dy * dy;
                    if d2 < minimums2[dir] {
                        minimums2[dir] = d2;
                    }
                }
            }
        }

        // If one intersects then, in a perfect universe, all others would intersect.
        let intersect = counts.map(|c| c > 0 && c % 2 == 1);
        if intersect[0] || intersect[1] || intersect[2] || intersect[3] {
            return EnvelopeIntersection::Inside {
                to_stall: minimums2[0].sqrt(),
                to_over_speed: minimums2[1].sqrt(),
                to_lift_fail: minimums2[3].sqrt(),
            };
        }

        let visible = counts.map(|c| c > 0);
        match visible {
            [true, _, _, _] => EnvelopeIntersection::OverSpeed(minimums2[0].sqrt()),
            [_, true, _, _] => EnvelopeIntersection::Stall(minimums2[1].sqrt()),
            [_, _, true, _] => EnvelopeIntersection::LiftFail(minimums2[2].sqrt()),
            _ => {
                // Upper left or upper right, or below.
                if altitude < meters!(0f64) {
                    EnvelopeIntersection::Inside {
                        to_stall: minimums2[0].sqrt(),
                        to_over_speed: minimums2[1].sqrt(),
                        to_lift_fail: minimums2[3].sqrt(),
                    }
                } else {
                    EnvelopeIntersection::Stall(minimums2[1].sqrt())
                }
            }
        }
    }

    #[inline]
    pub fn find_x_extrema(
        &self,
        altitude: Length<Meters>,
    ) -> (
        Option<Velocity<Meters, Seconds>>,
        Option<Velocity<Meters, Seconds>>,
    ) {
        let origin = DVec2::new(0_f64, altitude.f64());
        let end = DVec2::new(10_000_f64, altitude.f64());

        let mut minima = None;
        let mut maxima = None;

        for (i, coord0) in self.shape.iter().enumerate() {
            let j = (i + 1) % self.shape.len();
            let coord1 = &self.shape[j];

            if let Some((intersect_x, intersect_y)) = Self::segment_intersection(
                (origin.x, origin.y),
                (end.x, end.y),
                (coord0.speed().f64(), coord0.altitude().f64()),
                (coord1.speed().f64(), coord1.altitude().f64()),
            ) {
                let dx = intersect_x - origin.x;
                let dy = intersect_y - origin.y;
                let d = meters_per_second!((dx * dx + dy * dy).sqrt());
                if let Some(m) = minima {
                    if d < m {
                        minima = Some(d);
                    }
                } else {
                    minima = Some(d);
                }
                if let Some(m) = maxima {
                    if d > m {
                        maxima = Some(d);
                    }
                } else {
                    maxima = Some(d);
                }
            }
        }

        assert!(minima.map(|v| maxima.unwrap() >= v).unwrap_or(true));

        (minima, maxima)
    }

    #[inline]
    pub fn find_y_extrema(&self, speed: Velocity<Meters, Seconds>) -> Option<Length<Meters>> {
        let origin = DVec2::new(speed.f64(), 0f64);
        let end = DVec2::new(speed.f64(), 1_000_000f64);

        let mut maxima = None;

        for (i, coord0) in self.shape.iter().enumerate() {
            let j = (i + 1) % self.shape.len();
            let coord1 = &self.shape[j];

            if let Some((intersect_x, intersect_y)) = Self::segment_intersection(
                (origin.x, origin.y),
                (end.x, end.y),
                (coord0.speed().f64(), coord0.altitude().f64()),
                (coord1.speed().f64(), coord1.altitude().f64()),
            ) {
                let dx = intersect_x - origin.x;
                let dy = intersect_y - origin.y;
                let d = meters!((dx * dx + dy * dy).sqrt());
                if let Some(m) = maxima {
                    if d > m {
                        maxima = Some(d);
                    }
                } else {
                    maxima = Some(d);
                }
            }
        }

        maxima
    }

    pub fn find_min_lift_speed_at(
        &self,
        altitude: Length<Meters>,
    ) -> Option<Velocity<Meters, Seconds>> {
        self.find_x_extrema(altitude).0
    }

    pub fn find_max_velocity_at(
        &self,
        altitude: Length<Meters>,
    ) -> Option<Velocity<Meters, Seconds>> {
        self.find_x_extrema(altitude).1
    }

    pub fn find_max_lift_altitude_at(
        &self,
        speed: Velocity<Meters, Seconds>,
    ) -> Option<Length<Meters>> {
        self.find_y_extrema(speed)
    }
}

// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::{Feet, Length, Mass, PoundsMass};
use anyhow::{bail, ensure, Result};
use bitflags::bitflags;
use enum_iterator::Sequence;
use std::{
    collections::HashMap,
    fmt,
    fmt::{Debug, Display, Formatter},
    str::FromStr,
};
use xt_parse::{
    extract_table_data, find_section, make_xt_struct_family, mxsf_parse_string, FromXt, Nothing,
    NothingIo, OutOfLineTable,
};

// FIXME: this should be a bitflag
#[derive(Clone, Debug, Default, Eq, PartialEq)]
#[repr(u8)]
pub enum TypeTag {
    #[default]
    Object = 1,
    Npc = 3,
    Plane = 5,
    Projectile = 7,
}

impl TryFrom<u8> for TypeTag {
    type Error = anyhow::Error;
    fn try_from(value: u8) -> Result<Self> {
        Ok(match value {
            1 => Self::Object,
            3 => Self::Npc,
            5 => Self::Plane,
            7 => Self::Projectile,
            _ => bail!("unknown TypeTag {}", value),
        })
    }
}

impl From<TypeTag> for u8 {
    fn from(value: TypeTag) -> Self {
        value as u8
    }
}

impl fmt::Display for TypeTag {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{self:?}")
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq, Sequence)]
#[repr(u16)]
pub enum ObjectKind {
    Fighter = 0b1000_0000_0000_0000,
    Bomber = 0b0100_0000_0000_0000,
    Ship = 0b0010_0000_0000_0000,
    Sam = 0b0001_0000_0000_0000,
    Aaa = 0b0000_1000_0000_0000,
    Vehicle = 0b0000_0010_0000_0000,
    Tank = 0b0000_0100_0000_0000,
    #[default]
    Structure1 = 0b0000_0001_0000_0000,
    Projectile = 0b0000_0000_1000_0000,
    Structure2 = 0b0000_0000_0100_0000,
}

impl TryFrom<u16> for ObjectKind {
    type Error = anyhow::Error;
    fn try_from(value: u16) -> Result<Self> {
        match value {
            0b1000_0000_0000_0000 => Ok(ObjectKind::Fighter),
            0b0100_0000_0000_0000 => Ok(ObjectKind::Bomber),
            0b0010_0000_0000_0000 => Ok(ObjectKind::Ship),
            0b0001_0000_0000_0000 => Ok(ObjectKind::Sam),
            0b0000_1000_0000_0000 => Ok(ObjectKind::Aaa),
            0b0000_0100_0000_0000 => Ok(ObjectKind::Tank),
            0b0000_0010_0000_0000 => Ok(ObjectKind::Vehicle),
            0b0000_0001_0000_0000 => Ok(ObjectKind::Structure1),
            0b0000_0000_1000_0000 => Ok(ObjectKind::Projectile),
            0b0000_0000_0100_0000 => Ok(ObjectKind::Structure2),
            // There is a mistaken 0 entry in $BLDR.JT when it was first introduced in ATF Nato Fighters.
            0b0000_0000_0000_0000 => Ok(ObjectKind::Projectile),
            _ => bail!("unknown ObjectKind {value}"),
        }
    }
}

impl From<ObjectKind> for u16 {
    fn from(value: ObjectKind) -> Self {
        value as u16
    }
}

impl Display for ObjectKind {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{self:?}")
    }
}

impl ObjectKind {
    pub fn label(&self) -> &'static str {
        match self {
            Self::Fighter => "Fighter",
            Self::Bomber => "Bomber / other aircraft",
            Self::Ship => "Ship",
            Self::Sam => "Surface-to-Air Missile (SAM)",
            Self::Aaa => "Anti-aircraft Artillery (AAA)",
            Self::Vehicle => "Unarmored Vehicle",
            Self::Tank => "Armored Vehicle",
            Self::Structure1 => "Structure",
            Self::Projectile => "Missile",
            Self::Structure2 => "Other",
        }
    }
}

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub enum ProcKind {
    #[default]
    OBJ,
    PLANE,
    CARRIER,
    GV,
    PROJ,
    EJECT,
    STRIP,
    CATGUY,
}

impl FromStr for ProcKind {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> Result<Self> {
        Ok(match s {
            "_OBJProc" => ProcKind::OBJ,
            "_PLANEProc" => ProcKind::PLANE,
            "_CARRIERProc" => ProcKind::CARRIER,
            "_GVProc" => ProcKind::GV,
            "_PROJProc" => ProcKind::PROJ,
            "_EJECTProc" => ProcKind::EJECT,
            "_STRIPProc" => ProcKind::STRIP,
            "_CATGUYProc" => ProcKind::CATGUY,
            _ => bail!("Unexpected proc kind: {}", s),
        })
    }
}

impl TryFrom<String> for ProcKind {
    type Error = anyhow::Error;
    fn try_from(value: String) -> Result<Self> {
        Self::from_str(&value)
    }
}

impl ToString for ProcKind {
    fn to_string(&self) -> String {
        match self {
            Self::OBJ => "_OBJProc",
            Self::PLANE => "_PLANEProc",
            Self::CARRIER => "_CARRIERProc",
            Self::GV => "_GVProc",
            Self::PROJ => "_PROJProc",
            Self::EJECT => "_EJECTProc",
            Self::STRIP => "_STRIPProc",
            Self::CATGUY => "_CATGUYProc",
        }
        .to_owned()
    }
}

impl From<ProcKind> for String {
    fn from(value: ProcKind) -> Self {
        value.to_string()
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq, Sequence)]
#[repr(u8)]
pub enum ExplosionType {
    #[default]
    Undefined = 00,
    SmallGround = 15,
    Unk16 = 16,
    Unk17 = 17,
    SmallAir = 18,
    Unk20 = 20,
    MediumGround = 21,
    Unk24 = 24,
    LargeGround26 = 26,
    FlakBurst = 27,
    LargeAir = 30,
    LargeGround32 = 32,
    LargeGround = 35,
}

impl ExplosionType {
    pub fn label(&self) -> &'static str {
        match self {
            Self::Undefined => "Undefined",
            Self::SmallGround => "Small Ground Explosion",
            Self::Unk16 => "Unk16",
            Self::Unk17 => "Unk17",
            Self::SmallAir => "Small Air Explosion",
            Self::Unk20 => "Unk20",
            Self::MediumGround => "Medium Ground Explosion",
            Self::Unk24 => "Unk24",
            Self::LargeGround26 => "Large Ground Explosion [26]",
            Self::FlakBurst => "Flak Burst",
            Self::LargeAir => "Large Air Explosion",
            Self::LargeGround32 => "Large Ground Explosion [32]",
            Self::LargeGround => "Large Ground Explosion",
        }
    }
}

impl Display for ExplosionType {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.label())
    }
}

impl FromStr for ExplosionType {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> Result<Self> {
        Ok(match s {
            "Undefined" => Self::Undefined,
            "Small Ground Explosion" => Self::SmallGround,
            "Small Air Explosion" => Self::SmallAir,
            "Medium Ground Explosion" => Self::MediumGround,
            "Large Ground Explosion [26]" => Self::LargeGround26,
            "Flak Burst" => Self::FlakBurst,
            "Large Air Explosion" => Self::LargeAir,
            "Large Ground Explosion [32]" => Self::LargeGround32,
            "Large Ground Explosion" => Self::LargeGround,
            _ => bail!("unknown explosion type"),
        })
    }
}

impl From<u8> for ExplosionType {
    fn from(value: u8) -> Self {
        match value {
            00 => Self::Undefined,
            15 => Self::SmallGround,
            16 => Self::Unk16,
            17 => Self::Unk17,
            18 => Self::SmallAir,
            20 => Self::Unk20,
            21 => Self::MediumGround,
            24 => Self::Unk24,
            26 => Self::LargeGround26,
            27 => Self::FlakBurst,
            30 => Self::LargeAir,
            32 => Self::LargeGround32,
            35 => Self::LargeGround,
            _ => Self::Undefined,
        }
    }
}

impl From<ExplosionType> for u8 {
    fn from(value: ExplosionType) -> Self {
        value as u8
    }
}

// ($6bf3 = flyable, $2bf3 not flyable, if prefixed by 800, doesn't show up in in-game reference)
bitflags! {
    #[derive(Clone, Copy, Debug)]
    pub struct ObjectFlags : u32 {
        const UNK_BIT28   = 0b0000_1000_0000_0000_0000_0000_0000_0000; // does not show up in reference
        const UNK_BIT27   = 0b0000_0100_0000_0000_0000_0000_0000_0000;
        const UNK_BIT26   = 0b0000_0010_0000_0000_0000_0000_0000_0000;
        const UNK_BIT25   = 0b0000_0001_0000_0000_0000_0000_0000_0000;
        const UNK_BIT24   = 0b0000_0000_1000_0000_0000_0000_0000_0000;
        const UNK_BIT23   = 0b0000_0000_0100_0000_0000_0000_0000_0000;
        const RUNWAY      = 0b0000_0000_0010_0000_0000_0000_0000_0000;
        const UNK_BIT21   = 0b0000_0000_0001_0000_0000_0000_0000_0000;
        const UNK_BIT20   = 0b0000_0000_0000_1000_0000_0000_0000_0000;
        const UNK_BIT19   = 0b0000_0000_0000_0100_0000_0000_0000_0000;
        const UNK_BIT18   = 0b0000_0000_0000_0010_0000_0000_0000_0000;
        const UNK_BIT17   = 0b0000_0000_0000_0001_0000_0000_0000_0000;
        const UNK_BIT16   = 0b0000_0000_0000_0000_1000_0000_0000_0000;
        const FLYABLE     = 0b0000_0000_0000_0000_0100_0000_0000_0000;
        const UNK_BIT14   = 0b0000_0000_0000_0000_0010_0000_0000_0000;
        const UNK_BIT13   = 0b0000_0000_0000_0000_0001_0000_0000_0000;
        const UNK_BIT12   = 0b0000_0000_0000_0000_0000_1000_0000_0000;
        const UNK_BIT11   = 0b0000_0000_0000_0000_0000_0100_0000_0000;
        const UNK_BIT10   = 0b0000_0000_0000_0000_0000_0010_0000_0000;
        const UNK_BIT9    = 0b0000_0000_0000_0000_0000_0001_0000_0000;
        const UNK_BIT8    = 0b0000_0000_0000_0000_0000_0000_1000_0000;
        const UNK_BIT7    = 0b0000_0000_0000_0000_0000_0000_0100_0000;
        const UNK_BIT6    = 0b0000_0000_0000_0000_0000_0000_0010_0000;
        const UNK_BIT5    = 0b0000_0000_0000_0000_0000_0000_0001_0000;
        const UNK_BIT3    = 0b0000_0000_0000_0000_0000_0000_0000_0100;
        const UNK_BIT2    = 0b0000_0000_0000_0000_0000_0000_0000_0010;
        const UNK_BIT1    = 0b0000_0000_0000_0000_0000_0000_0000_0001;
    }
}

impl Default for ObjectFlags {
    fn default() -> Self {
        0u32.into()
    }
}

impl Display for ObjectFlags {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{:032b}", self.bits())
    }
}

impl From<u32> for ObjectFlags {
    fn from(value: u32) -> Self {
        Self::from_bits(value).expect("not a valid object flags pattern")
    }
}

impl From<ObjectFlags> for u32 {
    fn from(value: ObjectFlags) -> Self {
        value.bits()
    }
}

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct ObjectNames {
    short_name: String,
    long_name: String,
    file_name: String,
}

impl FromXt for ObjectNames {
    fn from_xt(lines: &[&str], _table: &HashMap<&str, Vec<&str>>) -> Result<Self> {
        ensure!(lines.len() == 3);
        Ok(Self {
            short_name: mxsf_parse_string(lines[0])?.to_owned(),
            long_name: mxsf_parse_string(lines[1])?.to_owned(),
            file_name: mxsf_parse_string(lines[2])?.to_owned(),
        })
    }
}

impl ObjectNames {
    // Note: we can only get the mut variants out of an Io type, so this safe in both variants.

    pub fn short_name(&self) -> &str {
        &self.short_name
    }

    pub fn short_name_mut(&mut self) -> &mut String {
        &mut self.short_name
    }

    pub fn long_name(&self) -> &str {
        &self.long_name
    }

    pub fn long_name_mut(&mut self) -> &mut String {
        &mut self.long_name
    }

    pub fn file_name(&self) -> &str {
        &self.file_name
    }

    pub fn file_name_mut(&mut self) -> &mut String {
        &mut self.file_name
    }

    pub fn serialize_to_lines(&self, table: &mut OutOfLineTable) {
        table.push_string_table(
            "ot_names",
            &[&self.short_name, &self.long_name, &self.file_name],
        );
    }
}

impl fmt::Display for ObjectNames {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{} ({})[{}]",
            self.short_name, self.long_name, self.file_name
        )
    }
}

// We can detect the version by the number of lines.
#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
enum ObjectTypeVersion {
    // USNF only
    V0 = 49,
    // MF only
    V1 = 51,
    // ATF & USNF97
    V2 = 63,
    // Nato, Gold, & FA
    V3 = 64,
}

impl ObjectTypeVersion {
    fn from_len(n: usize) -> Result<Self> {
        Ok(match n {
            49 => ObjectTypeVersion::V0,
            51 => ObjectTypeVersion::V1,
            63 => ObjectTypeVersion::V2,
            64 => ObjectTypeVersion::V3,
            _ => bail!("unknown object type version for length: {}", n),
        })
    }
}

make_xt_struct_family![
ObjectType(parent: Nothing, version: ObjectTypeVersion) {
    (V0, U8,    struct_type,             TypeTag, Dec, "structType"),
    (V0, U16,   type_size,                   u16, Dec, "typeSize"),
    (V0, U16,   instance_size,               u16, Dec, "instanceSize"),
    (V0, Tbl,   ot_names,            ObjectNames, Tbl, "ot_names"),
    (V0, U32,   ot_flags,            ObjectFlags, Hex, "flags"),
    (V0, U16,   obj_class,            ObjectKind, HexS,"obj_class"),
    (V0, Ptr,   shape,            Option<String>, Ptr, "shape"),
    (V0, Ptr,   shadow_shape,     Option<String>, Ptr, "shadowShape"),
    (V2, U32,   unk8,                        u32, Dec, ""),
    (V2, U32,   unk9,                        u32, Dec, ""),
    (V2, I16,   dmg_debris_pos_x,   Length<Feet>, Dec, "dmgDebrisPos.x"),
    (V2, I16,   dmg_debris_pos_y,   Length<Feet>, Dec, "dmgDebrisPos.y"),
    (V2, I16,   dmg_debris_pos_z,   Length<Feet>, Dec, "dmgDebrisPos.z"),
    (V2, U32,   unk13,                       u32, Dec, ""),
    (V2, U32,   unk14,                       u32, Dec, ""),
    (V2, I16,   dst_debris_pos_x,   Length<Feet>, Dec, "dstDebrisPos.x"),
    (V2, I16,   dst_debris_pos_y,   Length<Feet>, Dec, "dstDebrisPos.y"),
    (V2, I16,   dst_debris_pos_z,   Length<Feet>, Dec, "dstDebrisPos.z"),
    (V2, U32,   dmg_type,                    u32, Dec, "dmgType"),
    (V3, U32,   year_available,              u32, Dec, "year"),
    (V0, I16,   max_vis_dist,       Length<Feet>, Dec, "maxVisDist"),
    (V0, I16,   camera_dist,        Length<Feet>, Dec, "cameraDist"),
    (V0, U16,   unk_sig_22,                  u16, Dec, "sigs [i]"),
    (V0, U16,   unk_sig_laser,               u16, Dec, "sigs [i]"),
    (V0, U16,   unk_sig_ir,                  u16, Dec, "sigs [i]"),
    (V0, U16,   unk_sig_radar,               u16, Dec, "sigs [i]"),
    (V0, U16,   unk_sig_26,                  u16, Dec, "sigs [i]"),
    (V0, U16,   hit_points,                  u16, Dec, "hitPoints"),
    (V0, U16,   damage_on_planes,            u16, Dec, "damage [i]"),
    (V0, U16,   damage_on_ships,             u16, Dec, "damage [i]"),
    (V0, U16,   damage_on_structures,        u16, Dec, "damage [i]"),
    (V0, U16,   damage_on_armor,             u16, Dec, "damage [i]"),
    (V0, U16,   damage_on_other,             u16, Dec, "damage [i]"),
    (V0, U8,    explosion_type,    ExplosionType, Dec, "expType"),
    (V0, U8,    crater_size,        Length<Feet>, Dec, "craterSize"),
    (V0, U32,   empty_weight,   Mass<PoundsMass>, Dec, "weight"),
    (V0, U16,   cmd_buf_size,                u16, Dec, "cmdBufSize"),
    // Movement Info
    (V0, U16,   turn_rate,                   u16, Dec, "_turnRate"),
    (V0, U16,   bank_rate,                   u16, Dec, "_bankRate"),
    (V0, I16,   max_climb,                   i16, Dec, "maxClimb"),
    (V0, I16,   max_dive,                    i16, Dec, "maxDive"),
    (V0, I16,   max_bank,                    i16, Dec, "maxBank"),
    (V0, U16,   min_speed,                   u16, Dec, "_minSpeed"),
    (V0, U16,   corner_speed,                u16, Dec, "_cornerSpeed"),
    (V0, U16,   max_speed,                   u16, Dec, "_maxSpeed"),
    (V0, U32,   acceleration,                u32, Car, "_acc"),
    (V0, U32,   deceleration,                u32, Car, "_dacc"),
    (V0, I32,   min_altitude,                i32, Car, "minAlt"),
    (V0, I32,   max_altitude,                i32, Car, "maxAlt"),
    (V0, Sym,   util_proc,              ProcKind, Sym, "utilProc"),
    // Sound Info
    (V0, Ptr,   loop_sound,       Option<String>, Ptr, "loopSound"),
    (V0, Ptr,   second_sound,     Option<String>, Ptr, "secondSound"),
    (V1, Ptr,   engine_on_sound,  Option<String>, Ptr, "engineOnSound"),
    (V1, Ptr,   engine_off_sound, Option<String>, Ptr, "engineOffSound"),
    (V0, U8,    do_doppler,                 bool, Dec, "doDoppler"),
    (V0, U16,   max_snd_dist,                u16, Dec, "maxSndDist"),
    (V0, I16,   max_plus_doppler_pitch,      i16, Dec, "maxPlusDopplerPitch"),
    (V0, I16,   max_minus_doppler_pitch,     i16, Dec, "maxMinusDopplerPitch"),
    (V0, I16,   min_doppler_speed,           i16, Dec, "minDopplerSpeed"),
    (V0, I16,   max_doppler_speed,           i16, Dec, "maxDopplerSpeed"),
    (V0, I16,   view_offset_x,      Length<Feet>, Dec, "viewOffset.x"),
    (V0, I16,   view_offset_y,      Length<Feet>, Dec, "viewOffset.y"),
    (V0, I16,   view_offset_z,      Length<Feet>, Dec, "viewOffset.z"),
    (V2, Ptr,   hud_name,         Option<String>, Ptr, "hudName")
}];

impl ObjectType {
    pub fn from_text(data: &str) -> Result<Self> {
        let lines = data.lines().collect::<Vec<&str>>();
        ensure!(
            lines[0] == "[brent's_relocatable_format]",
            "not a type file"
        );
        let pointers = extract_table_data(&lines)?;
        let obj_lines = find_section(&lines, "OBJ_TYPE")?;
        Self::from_lines(Nothing, &obj_lines, &pointers)
    }

    // Specialized accessors
    pub fn shape_file(&self) -> Option<String> {
        self.shape.as_deref().map(|s| s.to_uppercase())
    }
}

impl ObjectTypeIo {
    pub fn to_lines(&self, tbl: &mut OutOfLineTable) -> Vec<String> {
        let raw = self.serialize_to_lines(tbl);
        let general = &raw[0..37];
        let movement = &raw[37..50];
        let sound = &raw[50..64];

        let mut out = [
            "[brent's_relocatable_format]",
            "",
            ";---------------- START OF OBJ_TYPE ----------------",
            "",
            "",
            ";---------------- general info ----------------",
            "",
        ]
        .iter()
        .map(|v| v.to_string())
        .collect::<Vec<String>>();
        out.extend_from_slice(general);
        out.extend(
            ["", ";---------------- movement info ----------------", ""]
                .iter()
                .map(|v| v.to_string())
                .collect::<Vec<String>>(),
        );
        out.extend_from_slice(movement);
        out.extend(
            ["", ";---------------- sound info ----------------", ""]
                .iter()
                .map(|v| v.to_string())
                .collect::<Vec<String>>(),
        );
        out.extend_from_slice(sound);
        out.extend(
            [
                "",
                "",
                "",
                ";---------------- END OF OBJ_TYPE ----------------",
                "",
            ]
            .iter()
            .map(|v| v.to_string())
            .collect::<Vec<String>>(),
        );

        out
    }
}

impl ToString for ObjectTypeIo {
    fn to_string(&self) -> String {
        let mut tbl = OutOfLineTable::default();
        let mut rows = self.to_lines(&mut tbl);
        rows.extend(tbl.take_strings());
        rows.push("\tend".to_owned());
        rows.push(String::new());
        rows.join("\r\n")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use catalog::{FileSystem, Order, Search};
    use installations::{from_dos_string, Installations};

    #[test]
    fn can_parse_all_entity_types() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(Search::for_glob("*.[OJNP]T").must_match())? {
            println!("At: {info}");
            let contents = from_dos_string(info.data()?);

            let ot = ObjectType::from_text(&contents)?;
            // Only one misspelling in 2500 files.
            assert!(ot.ot_names().file_name == info.name() || info.name() == "SMALLARM.JT");
        }
        Ok(())
    }

    #[test]
    fn can_roundtrip() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(
            Search::for_extension("OT")
                .in_collection("FA")
                .sort(Order::Asc)
                .must_match(),
        )? {
            println!("At: {info}");
            let contents = from_dos_string(info.data()?);

            let ot = ObjectType::from_text(&contents)?;
            let ot_io = ObjectTypeIo::from(&ot);
            let contents2 = ot_io.to_string();

            for (a, b) in contents.lines().zip(contents2.lines()) {
                assert_eq!(a, b);
            }
            assert_eq!(contents.lines().count(), contents2.lines().count());
        }
        Ok(())
    }

    #[ignore]
    #[test]
    fn test_histograph_flags() -> Result<()> {
        let mut hist = [0usize; 32];

        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(Search::for_glob("*.[OJNP]T"))? {
            let contents = from_dos_string(info.data()?);
            let ot = ObjectType::from_text(&contents)?;
            for (i, slot) in hist.iter_mut().enumerate() {
                let bit_is_set = ot.ot_flags.bits() >> i & 1 > 0;
                if bit_is_set {
                    *slot += 1;
                }
            }

            // println!("At: {info}");
        }

        for (i, slot) in hist.iter().enumerate() {
            println!("Bit {}: {}", i + 1, slot);
        }

        Ok(())
    }
}

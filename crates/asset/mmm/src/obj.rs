// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    canonicalize,
    formation::WingFormation,
    util::{maybe_hex, parse_header_delimited},
    waypoint::Waypoints,
};
use absolute_unit::{
    approx, degrees, feet, pounds_mass, Angle, Degrees, Feet, Mass, PoundsMass, Pt3,
};
use anyhow::{anyhow, bail, ensure, Result};
use approx::assert_relative_eq;
use catalog::FileSystem;
use std::{collections::HashMap, str::SplitAsciiWhitespace};
use xt::{TypeManager, TypeRef};

#[repr(u8)]
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Aggression {
    Friendly,
    Enemy,
}

// From: http://myplace.frontier.com/~usnraptor/Fighters%20Anthology/Misc/Country%20Index.txt
// The top bit flips the friendliness, which is stored elsewhere here.
#[repr(u8)]
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Nationality {
    UnitedStates = 0,
    UnitedKingdom = 1,
    China = 2,
    France = 3,
    Germany = 4,
    Belgium = 5,
    Jordan = 6,
    Israel = 7,
    Japan = 8,
    NorthKorea = 9,
    Russia = 10,
    SouthKorea = 11,
    Syria = 12,
    ArabEgypt = 13,
    IslamEgypt = 14,
    Estonia = 15,
    Latvia = 16,
    Lithuania = 17,
    Poland = 18,
    Belarus = 19,
    NorthVietnam = 20,
    SouthVietnam = 21,
    Ukraine = 22,
    Iraq = 23,
    Iran = 24,
    Kuwait = 25,
    SaudiArabia = 26,
    Oman = 27,
    UnitedArabEmirates = 28,
    Qatar = 29,
    Bahrain = 30,
    Libya = 31,
    Sudan = 32,
    Cuba = 33,
    Panama = 34,
    Colombia = 35,
    Pakistan = 36,
    India = 37,
    Afghanistan = 38,
    Taiwan = 39,
    Greece = 40,
    Turkey = 41,
    Italy = 42,
    Sweden = 43,
    Finland = 44,
    Norway = 45,
    Spain = 46,
    Portugal = 47,
    Austria = 48,
    Denmark = 49,
    Netherlands = 50,
    Canada = 51,
    Bulgaria = 52,
    Hungary = 53,
    Romania = 54,
    Australia = 55,
    Philippines = 56,
    Argentina = 57,
    Bosnia = 58,
    Serbia = 59,
}

impl Nationality {
    fn from_ordinal(n: usize) -> Result<(Self, Aggression)> {
        let nationality = match n {
            128 | 0 => Self::UnitedStates,
            129 | 1 => Self::UnitedKingdom,
            130 | 2 => Self::China,
            131 | 3 => Self::France,
            132 | 4 => Self::Germany,
            133 | 5 => Self::Belgium,
            134 | 6 => Self::Jordan,
            135 | 7 => Self::Israel,
            136 | 8 => Self::Japan,
            137 | 9 => Self::NorthKorea,
            138 | 10 => Self::Russia,
            139 | 11 => Self::SouthKorea,
            140 | 12 => Self::Syria,
            141 | 13 => Self::ArabEgypt,
            142 | 14 => Self::IslamEgypt,
            143 | 15 => Self::Estonia,
            144 | 16 => Self::Latvia,
            145 | 17 => Self::Lithuania,
            146 | 18 => Self::Poland,
            147 | 19 => Self::Belarus,
            148 | 20 => Self::NorthVietnam,
            149 | 21 => Self::SouthVietnam,
            150 | 22 => Self::Ukraine,
            151 | 23 => Self::Iraq,
            152 | 24 => Self::Iran,
            153 | 25 => Self::Kuwait,
            154 | 26 => Self::SaudiArabia,
            155 | 27 => Self::Oman,
            156 | 28 => Self::UnitedArabEmirates,
            157 | 29 => Self::Qatar,
            158 | 30 => Self::Bahrain,
            159 | 31 => Self::Libya,
            160 | 32 => Self::Sudan,
            161 | 33 => Self::Cuba,
            162 | 34 => Self::Panama,
            163 | 35 => Self::Colombia,
            164 | 36 => Self::Pakistan,
            165 | 37 => Self::India,
            166 | 38 => Self::Afghanistan,
            167 | 39 => Self::Taiwan,
            168 | 40 => Self::Greece,
            169 | 41 => Self::Turkey,
            170 | 42 => Self::Italy,
            171 | 43 => Self::Sweden,
            172 | 44 => Self::Finland,
            173 | 45 => Self::Norway,
            174 | 46 => Self::Spain,
            175 | 47 => Self::Portugal,
            176 | 48 => Self::Austria,
            177 | 49 => Self::Denmark,
            178 | 50 => Self::Netherlands,
            179 | 51 => Self::Canada,
            180 | 52 => Self::Bulgaria,
            181 | 53 => Self::Hungary,
            182 | 54 => Self::Romania,
            183 | 55 => Self::Australia,
            184 | 56 => Self::Philippines,
            185 | 57 => Self::Argentina,
            186 | 58 => Self::Bosnia,
            187 | 59 => Self::Serbia,
            _ => bail!("invalid nationality: {n}"),
        };
        let aggression = if n >= 128 {
            Aggression::Enemy
        } else {
            Aggression::Friendly
        };
        Ok((nationality, aggression))
    }
}

#[derive(Debug, Default)]
pub struct EulerAngles {
    yaw: Angle<Degrees>,
    pitch: Angle<Degrees>,
    roll: Angle<Degrees>,
}

impl EulerAngles {
    pub fn yaw(&self) -> Angle<Degrees> {
        self.yaw
    }

    pub fn pitch(&self) -> Angle<Degrees> {
        self.pitch
    }

    pub fn roll(&self) -> Angle<Degrees> {
        self.roll
    }
}

#[derive(Debug)]
#[allow(dead_code)]
pub struct ObjectInfo {
    xt: TypeRef,
    name: Option<String>,
    pos: Pt3<Feet>,
    angle: EulerAngles,
    nationality: Nationality,
    aggression: Aggression,
    flags: u16,
    speed: f32,
    alias: Option<i32>,
    // NT only.
    skill: Option<u8>,
    react: Option<(u16, u16, u16)>,
    search_dist: Option<u32>,
    // like wing but for ground convoys; baltics only
    group: Option<(u8, u8)>,
    // PT only.
    waypoints: Option<Waypoints>,
    wing: Option<(u8, u8)>,
    wng_formation: Option<WingFormation>,
    start_time: u32,
    controller: u8,
    preferred_target_id: Option<u32>,
    npc_flags: Option<u8>,
    hardpoint_overrides: Option<HashMap<usize, (u8, Option<TypeRef>)>>,
    fuel_override: Option<Mass<PoundsMass>>, // Only in VIET03.M
}

impl ObjectInfo {
    pub fn from_xt(xt: TypeRef, name: &str) -> Self {
        Self {
            xt,
            name: Some(name.to_owned()),
            pos: Pt3::new_unit(1., 1., 1.), // avoid origin for special discover of e.g. gear
            angle: EulerAngles::default(),
            nationality: Nationality::UnitedStates,
            aggression: Aggression::Friendly,
            flags: 0,
            speed: 200.,
            alias: None,
            // NT only.
            skill: None,
            react: None,
            search_dist: None,
            group: None,
            // PT only.
            waypoints: None,
            wing: None,
            wng_formation: None,
            start_time: 0,
            controller: 0,
            preferred_target_id: None,
            npc_flags: None,
            hardpoint_overrides: None,
            fuel_override: None,
        }
    }

    pub(crate) fn from_tokens<FS: FileSystem>(
        tokens: &mut SplitAsciiWhitespace,
        type_manager: &TypeManager,
        catalog: &FS,
    ) -> Result<Self> {
        let mut xt = None;
        let mut name = None;
        let mut pos = None;
        let mut angle = EulerAngles::default();
        let mut nationality = None;
        let mut aggression = None;
        let mut flags = 0u16;
        let mut speed = 0f32;
        let mut alias = None;
        // NT only.
        let mut skill = None;
        let mut react = None;
        let mut search_dist = None;
        let mut group = None;
        // PT only.
        let mut wing = None;
        let mut wng_formation = None;
        let mut start_time = 0;
        let mut controller = 0;
        let mut preferred_target_id = None;
        let mut npc_flags = None;
        let mut hardpoint_overrides = None;
        let mut fuel_override = None;

        while let Some(token) = tokens.next() {
            match token {
                "type" => {
                    // TODO: pass raw in so we can dedup on the &str.
                    let name = tokens.next().expect("type").to_uppercase();

                    let info = catalog
                        .lookup(&name)
                        .or_else(|_| catalog.lookup(&canonicalize(&name)))?;
                    xt = Some(type_manager.load(info)?);
                }
                "name" => {
                    name = parse_header_delimited(tokens);
                }
                "pos" => {
                    let x = feet!(tokens.next().expect("pos x").parse::<i32>()?);
                    let y = feet!(tokens.next().expect("pos y").parse::<i32>()?);
                    let z = feet!(tokens.next().expect("pos z").parse::<i32>()?);
                    pos = Some(Pt3::new(x, y, z));
                    // All non-plane entities are at height 0 and need to be moved
                    // to the right elevation at startup.
                    if !xt.as_ref().expect("xt").is_pt()
                        && xt.as_ref().expect("xt").ot().ot_names().file_name() != "MICROM.OT"
                    {
                        // In UKR17, there is a fight over a radio antenna on a roof.
                        // Otherwise all objects are on the ground.
                        assert_relative_eq!(y, feet!(0.));
                    }
                }
                "angle" => {
                    let yaw = tokens.next().expect("ang yaw").parse::<i32>()?;
                    let pitch = tokens.next().expect("ang pitch").parse::<i32>()?;
                    let roll = tokens.next().expect("ang roll").parse::<i32>()?;
                    angle = EulerAngles {
                        yaw: -degrees!(yaw),
                        pitch: -degrees!(pitch),
                        roll: -degrees!(roll),
                    };
                }
                "nationality" => {
                    let (nat, iff) = Nationality::from_ordinal(
                        tokens.next().expect("nationality").parse::<usize>()?,
                    )?;
                    nationality = Some(nat);
                    aggression = Some(iff);
                }
                "nationality2" => {
                    let (nat, iff) = Nationality::from_ordinal(maybe_hex(
                        tokens.next().expect("nationality2"),
                    )?)?;
                    nationality = Some(nat);
                    aggression = Some(iff);
                }
                "nationality3" => {
                    let (nat, iff) = Nationality::from_ordinal(
                        tokens.next().expect("nationality3").parse::<usize>()?,
                    )?;
                    nationality = Some(nat);
                    aggression = Some(iff);
                }
                "flags" => flags = maybe_hex::<u16>(tokens.next().expect("flags"))?,
                "speed" => speed = tokens.next().expect("speed").parse::<i32>()? as f32,
                "alias" => alias = Some(tokens.next().expect("alias").parse::<i32>()?),
                "skill" => skill = Some(tokens.next().expect("skill").parse::<u8>()?),
                "react" => {
                    react = Some((
                        maybe_hex::<u16>(tokens.next().expect("react[0]"))?,
                        maybe_hex::<u16>(tokens.next().expect("react[1]"))?,
                        maybe_hex::<u16>(tokens.next().expect("react[2]"))?,
                    ));
                }
                "searchDist" => {
                    search_dist = Some(tokens.next().expect("search dist").parse::<u32>()?)
                }
                "group" => {
                    let squad = str::parse::<u8>(tokens.next().expect("group squad"))?;
                    let offset = str::parse::<u8>(tokens.next().expect("group offset"))?;
                    group = Some((squad, offset));
                }
                "wing" => {
                    let squad = str::parse::<u8>(tokens.next().expect("wing squad"))?;
                    let offset = str::parse::<u8>(tokens.next().expect("wing offset"))?;
                    wing = Some((squad, offset));
                }
                "wng" => {
                    wng_formation = Some(WingFormation::from_tokens(tokens)?);
                }
                "startTime" => {
                    let t = str::parse::<u32>(tokens.next().expect("startTime t"))?;
                    start_time = t; // unknown units
                }
                "controller" => {
                    let v = maybe_hex::<u8>(tokens.next().expect("controller v"))?;
                    ensure!(v == 128);
                    controller = v;
                }
                "preferredTargetId" => {
                    let v = str::parse::<u32>(tokens.next().expect("preferredTargetId v"))?;
                    preferred_target_id = Some(v);
                }
                "preferredTargetId2" => {
                    let v = maybe_hex::<u32>(tokens.next().expect("preferredTargetId2 $v"))?;
                    preferred_target_id = Some(v);
                }
                "npcFlags" => {
                    let flags = str::parse::<u8>(tokens.next().expect("npcFlags v"))?;
                    ensure!(flags == 2);
                    npc_flags = Some(flags);
                }
                "hardpoint" => {
                    if hardpoint_overrides.is_none() {
                        hardpoint_overrides = Some(HashMap::new());
                    }
                    let idx = str::parse::<usize>(tokens.next().expect("hardpoint a"))?;
                    let cnt = str::parse::<u8>(tokens.next().expect("hardpoint b"))?;
                    let hp_xt = if cnt > 0 {
                        let ty_name = tokens.next().expect("hardpoint c").to_uppercase();
                        let info = catalog.lookup(&ty_name)?;
                        Some(type_manager.load(info)?)
                    } else {
                        None
                    };
                    hardpoint_overrides
                        .as_mut()
                        .map(|hps| hps.insert(idx, (cnt, hp_xt)));
                }
                "fuel" => {
                    let unk = str::parse::<u8>(tokens.next().expect("fuel unk"))?;
                    fuel_override = Some(pounds_mass!(unk));
                }
                "." => break,
                _ => {
                    bail!("unknown obj key: {}", token);
                }
            }
        }

        Ok(ObjectInfo {
            xt: xt.ok_or_else(|| anyhow!("mm:obj: type not set in obj"))?,
            name,
            pos: pos.ok_or_else(|| anyhow!("mm:obj: pos not set in obj"))?,
            angle,
            nationality: nationality
                .ok_or_else(|| anyhow!("mm:obj: nationality not set in obj",))?,
            aggression: aggression.ok_or_else(|| anyhow!("mm:obj: aggression not set in obj",))?,
            flags,
            speed,
            alias,
            skill,
            react,
            search_dist,
            group,
            wing,
            wng_formation,
            start_time,
            controller,
            preferred_target_id,
            npc_flags,
            hardpoint_overrides,
            fuel_override,
            waypoints: None,
        })
    }

    pub fn set_waypoints(&mut self, waypoints: Waypoints) {
        self.waypoints = Some(waypoints);
    }

    pub fn waypoints(&self) -> Option<&Waypoints> {
        self.waypoints.as_ref()
    }

    pub fn alias(&self) -> Option<i32> {
        self.alias
    }

    pub fn name(&self) -> Option<String> {
        self.name.clone()
    }

    pub fn xt(&self) -> TypeRef {
        self.xt.clone()
    }

    pub fn position(&self) -> &Pt3<Feet> {
        &self.pos
    }

    pub fn angle(&self) -> &EulerAngles {
        &self.angle
    }

    pub fn speed(&self) -> f32 {
        self.speed
    }

    pub fn fuel_override(&self) -> Option<Mass<PoundsMass>> {
        self.fuel_override
    }
}

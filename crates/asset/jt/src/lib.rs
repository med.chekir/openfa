// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::{bail, ensure, Result};
use ot::{ObjectType, ObjectTypeIo};
use std::{collections::HashMap, fmt};
use xt_parse::{
    find_pointers, find_section, make_xt_struct_family, mxsf_parse_string, FromXt, Nothing,
    OutOfLineTable,
};

#[derive(Clone, Debug, Default)]
#[allow(dead_code)]
pub struct ProjectileNames {
    pub short_name: String,
    pub long_name: String,
    pub file_name: Option<String>,
}

impl FromXt for ProjectileNames {
    fn from_xt(lines: &[&str], _table: &HashMap<&str, Vec<&str>>) -> Result<Self> {
        Ok(Self {
            short_name: mxsf_parse_string(lines[0])?.to_owned(),
            long_name: mxsf_parse_string(lines[1])?.to_owned(),
            file_name: if lines.len() > 2 {
                Some(mxsf_parse_string(lines[2])?.to_owned())
            } else {
                None
            },
        })
    }
}

impl fmt::Display for ProjectileNames {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{} ({} @ {:?})",
            self.short_name, self.long_name, self.file_name
        )
    }
}

impl ProjectileNames {
    pub fn short_name(&self) -> &str {
        &self.short_name
    }

    pub fn short_name_mut(&mut self) -> &mut String {
        &mut self.short_name
    }

    pub fn long_name(&self) -> &str {
        &self.long_name
    }

    pub fn long_name_mut(&mut self) -> &mut String {
        &mut self.long_name
    }

    pub fn file_name(&self) -> Option<&str> {
        self.file_name.as_deref()
    }

    pub fn file_name_mut(&mut self) -> &mut Option<String> {
        &mut self.file_name
    }

    pub fn serialize_to_lines(&self, table: &mut OutOfLineTable) {
        if let Some(filename) = &self.file_name {
            table.push_string_table(
                "si_names",
                &[&self.short_name, &self.long_name, filename.as_str()],
            );
        } else {
            table.push_string_table("si_names", &[&self.short_name, &self.long_name]);
        }
    }
}

// We can detect the version by the number of lines.
#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
enum ProjectileTypeVersion {
    // USNF only
    V0 = 81,
    // MF, ATF, Nato
    V1 = 84,
    // USNF97, AtfGold, and FA
    V2 = 90,
}

impl ProjectileTypeVersion {
    fn from_len(n: usize) -> Result<Self> {
        Ok(match n {
            81 => ProjectileTypeVersion::V0,
            84 => ProjectileTypeVersion::V1,
            90 => ProjectileTypeVersion::V2,
            _ => bail!("unknown projectile type version for length: {}", n),
        })
    }
}

make_xt_struct_family![
ProjectileType(ot: ObjectType, version: ProjectileTypeVersion) {
    (V0, U32,   flags0,                      u32, Hex, "flags"),
    (V0, Num,   projs_in_pod,                u16, Dec, "projsInPod"),
    (V0, U8,    struct_type,                  u8, Dec, "structType"),
    (V0, Tbl,   si_names,        ProjectileNames, Tbl, "si_names"),
    (V0, U16,   weight,                      u16, Dec, "weight"),
    (V0, U8,    flags1,                       u8, Hex, "flags"),
    (V0, U8,    sig,                          u8, Dec, "sig"),
    (V0, U8,    flags2,                       u8, Hex, "flags"),
    (V0, U8,    look_down,                    u8, Dec, "lookDown"),
    (V0, U8,    doppler_speed_above,          u8, Dec, "dopplerSpeedAbove"),
    (V0, U8,    doppler_speed_below,          u8, Dec, "dopplerSpeedBelow"),
    (V0, U8,    doppler_min_range,            u8, Dec, "dopplerMinRange"),
    (V0, U8,    all_aspect,                   u8, Dec, "allAspect"),
    (V0, U16,   h0,                          u16, Dec, "h"),
    (V0, U16,   p0,                          u16, Dec, "p"),
    (V0, U32,   min_range0,                  u32, Car, "minRange"),
    (V0, U32,   max_range0,                  u32, Car, "maxRange"),
    (V0, I32,   min_alt0,                    i32, Car, "minAlt"),
    (V0, I32,   max_alt0,                    i32, Car, "maxAlt"),
    (V0, U16,   h1,                          u16, Dec, "h"),
    (V0, U16,   p1,                          u16, Dec, "p"),
    (V0, U32,   min_range1,                  u32, Car, "minRange"),
    (V0, U32,   max_range1,                  u32, Car, "maxRange"),
    (V0, I32,   min_alt1,                    i32, Car, "minAlt"),
    (V0, I32,   max_alt1,                    i32, Car, "maxAlt"),
    (V0, U8,    chaff_flare_chance,           u8, Dec, "chaffFlareChance"),
    (V0, U8,    deception_chance,             u8, Dec, "deceptionChance"),
    (V0, U8,    track_t,                      u8, Dec, "trackT"),
    (V0, U8,    track_max_g,                  u8, Dec, "trackMaxG"),
    (V0, U8,    target_sun_chance,            u8, Dec, "targetSunChance"),
    (V2, U16,   random_fire_percent,         u16, Dec, "randomFirePercent"),
    (V2, U16,   offset_fire_percent,         u16, Dec, "offsetFirePercent"),
    (V2, U16,   offset_fire_h,               u16, Dec, "offsetFireH"),
    (V2, U16,   offset_fire_p,               u16, Dec, "offsetFireP"),
    (V2, U8,    actual_rounds_per_game,       u8, Dec, "actualRoundsPerGame"),
    (V0, U8,    game_rounds_in_burst,         u8, Dec, "gameRoundsInBurst"),
    (V0, U8,    game_rounds_in_carpet_burst,  u8, Dec, "gameRoundsInCarpetBurst"),
    (V0, U8,    game_burst_t,                 u8, Dec, "gameBurstT"),
    (V0, U8,    reload_t,                     u8, Dec, "reloadT"),
    (V0, U8,    startup_shots,                u8, Dec, "startupShots"),
    (V0, U8,    h_sines,                      u8, Dec, "hSines"),
    (V0, U8,    h_sine_degrees,               u8, Dec, "hSineDegrees"),
    (V0, U8,    v_sines,                      u8, Dec, "vSines"),
    (V2, U8,    v_sine_degrees,               u8, Dec, "vSineDegrees"),
    (V0, U8,    max_aon,                      u8, Dec, "maxAON"),
    (V0, U16,   initial_speed,               u16, Dec, "initialSpeed"),
    (V0, U16,   final_speed,                 u16, Dec, "finalSpeed"),
    (V0, U16,   ignite_t,                    u16, Dec, "igniteT"),
    (V0, U16,   fuel_t,                      u16, Dec, "fuelT"),
    (V0, U16,   remove_t,                    u16, Dec, "removeT"),
    (V0, U16,   powered_turn_rate,           u16, Dec, "poweredTurnRate"),
    (V0, U16,   unpowered_turn_rate,         u16, Dec, "unpoweredTurnRate"),
    (V0, U8,    performance_at_0,             u8, Dec, "performanceAt0"),
    (V0, U8,    performance_at_20,            u8, Dec, "performanceAt20"),
    (V0, U8,    cruise1_dist,                 u8, Dec, "cruise1Dist"),
    (V0, U8,    cruise1_alt,                  u8, Dec, "cruise1Alt"),
    (V0, U8,    cruise2_dist,                 u8, Dec, "cruise2Dist"),
    (V0, U8,    cruise2_alt,                  u8, Dec, "cruise2Alt"),
    (V0, U16,   jink_size,                   u16, Dec, "jinkSize"),
    (V0, U16,   jink_t,                      u16, Dec, "jinkT"),
    (V0, U16,   total_jink_t,                u16, Dec, "totalJinkT"),
    (V0, U8,    launch_retard,                u8, Dec, "launchRetard"),
    (V0, U8,    smoke_type,                   u8, Dec, "smokeType"),
    (V0, U8,    smoke_freq,                   u8, Dec, "smokeFreq"),
        // Set in 1/4 second intervals:
        //  1 = 1/4
        //  2 = 1/2
        //  3 = 3/4
        //  4 = 1
    (V0, U8,    smoke_exist_time,             u8, Dec, "smokeExistTime"),
    (V0, U8,    smoke_start_size,             u8, Dec, "smokeStartSize"),
    (V0, U8,    smoke_end_size,               u8, Dec, "smokeEndSize"),
    (V0, U8,    chances_i_0,                  u8, Dec, "chances [i]"),
    (V0, U8,    chances_i_1,                  u8, Dec, "chances [i]"),
    (V0, U8,    chances_i_2,                  u8, Dec, "chances [i]"),
    (V0, U8,    chances_i_3,                  u8, Dec, "chances [i]"),
    (V0, U8,    taa_hit_change,               u8, Dec, "taaHitChange"),
    (V0, U8,    climb_hit_change,             u8, Dec, "climbHitChange"),
    (V0, U8,    g_hit_change,                 u8, Dec, "gHitChange"),
    (V0, U8,    air_hit_change,               u8, Dec, "airHitChange"),
    (V0, U8,    speed_hit_change,             u8, Dec, "speedHitChange"),
    (V0, U8,    speed_hit_min,                u8, Dec, "speedHitMin"),
    (V0, U8,    predictable_hit_change,       u8, Dec, "predictableHitChange"),
    (V1, U8,    big_plane_change,             u8, Dec, "bigPlaneChange"),
    (V0, U8,    g_miss,                       u8, Dec, "gMiss"),
    (V0, U16,   fuze_arm_t,                  u16, Dec, "fuzeArmT"),
    (V0, U16,   fuze_radius,                 u16, Dec, "fuzeRadius"),
    (V0, U8,    side_hit_fuze_failure,        u8, Dec, "sideHitFuzeFailure"),
    (V0, U8,    exp_type_for_land,            u8, Dec, "expTypeForLand"),
    (V0, U8,    exp_type_for_water,           u8, Dec, "expTypeForWater"),
    (V0, Ptr,   fire_sound,       Option<String>, Ptr, "fireSound"),
    (V0, U16,   max_snd_dist,                u16, Dec, "maxSndDist"),
    (V0, U16,   freq_adj,                    u16, Dec, "freqAdj"),
    (V1, U16,   collateral_damage_radius,    u16, Dec, "collateralDamageRadius"),
    (V1, U16,   collateral_damage_percent,   u16, Dec, "collateralDamagePercent")
}];

impl ProjectileType {
    pub fn from_text(data: &str) -> Result<Self> {
        let lines = data.lines().collect::<Vec<&str>>();
        ensure!(
            lines[0] == "[brent's_relocatable_format]",
            "not a type file"
        );
        let pointers = find_pointers(&lines)?;
        let obj_lines = find_section(&lines, "OBJ_TYPE")?;
        let obj = ObjectType::from_lines(Nothing, &obj_lines, &pointers)?;
        let proj_lines = find_section(&lines, "PROJ_TYPE")?;
        Self::from_lines(obj, &proj_lines, &pointers)
    }
}

impl ProjectileTypeIo {
    fn to_lines(&self, tbl: &mut OutOfLineTable) -> Vec<String> {
        let mut out = self.ot().to_lines(tbl);
        let raw = self.serialize_to_lines(tbl);

        out.extend(
            [
                "",
                ";---------------- START OF PROJ_TYPE ----------------",
                "",
            ]
            .iter()
            .map(|v| v.to_string())
            .collect::<Vec<String>>(),
        );
        out.extend_from_slice(&raw);
        out.extend(
            [
                "",
                ";---------------- END OF PROJ_TYPE ----------------",
                "",
            ]
            .iter()
            .map(|v| v.to_string())
            .collect::<Vec<String>>(),
        );

        out
    }
}

impl ToString for ProjectileTypeIo {
    fn to_string(&self) -> String {
        let mut tbl = OutOfLineTable::default();
        let mut rows = self.to_lines(&mut tbl);
        rows.extend(tbl.take_strings());
        rows.push("\tend".to_owned());
        rows.push(String::new());
        rows.join("\r\n")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use catalog::{FileSystem, Search};
    use installations::{from_dos_string, Installations};

    #[test]
    fn it_can_parse_all_projectile_files() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(Search::for_extension("JT").must_match())? {
            println!("At: {info}");
            let contents = from_dos_string(info.data()?);
            let jt = ProjectileType::from_text(contents.as_ref())?;
            // Only one misspelling in 2500 files.
            assert!(jt.ot.ot_names().file_name() == info.name() || info.name() == "SMALLARM.JT");
        }

        Ok(())
    }

    #[test]
    fn can_roundtrip() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(Search::for_extension("JT").in_collection("FA").must_match())? {
            println!("At: {info}");
            let contents = from_dos_string(info.data()?);

            let jt = ProjectileType::from_text(&contents)?;
            let jt_io = ProjectileTypeIo::from(&jt);
            let contents2 = jt_io.to_string();

            for (a, b) in contents.lines().zip(contents2.lines()) {
                assert_eq!(a, b);
            }
            assert_eq!(contents.lines().count(), contents2.lines().count());
        }
        Ok(())
    }
}

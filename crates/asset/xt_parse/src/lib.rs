// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
pub use anyhow::{anyhow, bail, ensure, Result};
pub use convert_case;
pub use paste::paste;

use std::collections::{HashMap, HashSet};

pub trait FromXt: Sized {
    fn from_xt(line: &[&str], table: &HashMap<&str, Vec<&str>>) -> anyhow::Result<Self>;
}

// An I/O-able parent for bottoming out the OT stack.
#[derive(Clone, Copy, Debug, Default)]
pub struct Nothing;

#[derive(Clone, Copy, Debug, Default)]
pub struct NothingIo;

impl From<&Nothing> for NothingIo {
    fn from(_: &Nothing) -> Self {
        NothingIo
    }
}

#[macro_export]
macro_rules! mxsf_row_storage {
    (Num, $_ty:path) => {
        i64
    };
    (U8, $_ty:path) => {
        u8
    };
    (U16, $_ty:path) => {
        u16
    };
    (I16, $_ty:path) => {
        i16
    };
    (U32, $_ty:path) => {
        u32
    };
    (I32, $_ty:path) => {
        i32
    };
    (Ptr, $_ty:path) => {
        Option<String>
    };
    (Sym, $_ty:path) => {
        String
    };
    (Tbl, $ty:path) => {
        $ty
    };
}

#[macro_export]
macro_rules! mxsf_check_kind {
    (U8, $kind:ident, $_vs:ident) => {
        $crate::ensure!("byte" == $kind);
    };
    (Num, $kind:ident, $_vs:ident) => {
        $crate::ensure!("byte" == $kind || "word" == $kind || "dword" == $kind);
    };
    (U16, $kind:ident, $_vs:ident) => {
        $crate::ensure!("word" == $kind);
    };
    (I16, $kind:ident, $_vs:ident) => {
        $crate::ensure!("word" == $kind);
    };
    (U32, $kind:ident, $_vs:ident) => {
        // Note: byte is for USNF flags that are u32, like PlaneFlags
        $crate::ensure!("dword" == $kind || "byte" == $kind);
    };
    (I32, $kind:ident, $_vs:ident) => {
        $crate::ensure!("dword" == $kind);
    };
    (Ptr, $kind:ident, $vs:ident) => {
        $crate::ensure!("ptr" == $kind || ("dword" == $kind && "0" == $vs) || "byte" == $kind);
    };
    (Sym, $kind:ident, $_vs:ident) => {
        $crate::ensure!("symbol" == $kind);
    };
    (Tbl, $kind:ident, $_vs:ident) => {
        $crate::ensure!("ptr" == $kind);
    };
}

#[macro_export]
macro_rules! mxsf_parse_to_storage {
    (U8, $_kind:ident, $vs:ident, $_ty:path, $table:ident) => {
        $crate::mxsf_parse_number($vs)? as u8
    };
    (Num, $_kind:ident, $vs:ident, $_ty:path, $table:ident) => {
        $crate::mxsf_parse_number($vs)?
    };
    (U16, $_kind:ident, $vs:ident, $_ty:path, $table:ident) => {
        $crate::mxsf_parse_number($vs)? as u16
    };
    (I16, $_kind:ident, $vs:ident, $_ty:path, $table:ident) => {
        $crate::mxsf_parse_number($vs)? as i16
    };
    (U32, $_kind:ident, $vs:ident, $_ty:path, $table:ident) => {
        $crate::mxsf_parse_number($vs)? as u32
    };
    (I32, $_kind:ident, $vs:ident, $_ty:path, $table:ident) => {
        $crate::mxsf_parse_number($vs)? as i32
    };
    (Ptr, $kind:ident, $vs:ident, $_ty:path, $table:ident) => {
        if $kind == "dword" {
            $crate::ensure!($vs == "0");
            None
        } else if $kind == "byte" {
            // In USNF, ptr names are represented inline as `byte NN NN NN NN NNN NN NN NNN`.
            // We want to upgrade these automatically when we see them.
            let mut ptr = String::with_capacity(12);
            for s in $vs.split(' ') {
                let n = s.parse::<u8>()?;
                if n == 0 {
                    break;
                }
                ptr.push(n as char);
            }
            if ptr.is_empty() {
                None
            } else {
                Some(ptr)
            }
        } else {
            Some($crate::mxsf_lookup_string($vs, $table)?.to_string())
        }
    };
    (Sym, $_kind:ident, $vs:ident, $_ty:path, $table:ident) => {
        $vs.to_owned()
    };
    (Tbl, $_kind:ident, $vs:ident, $ty:path, $table:ident) => {{
        let names = $table
            .get($vs)
            .ok_or_else(|| $crate::anyhow!("no such pointer {} in string table", $vs))?;
        <$ty as $crate::FromXt>::from_xt(names, $table)?
    }};
}

#[macro_export]
macro_rules! mxsf_storage_to_actual {
    (U8, $vs:ident, bool) => {
        $vs > 0
    };
    (U8, $vs:ident, $field_ty:path) => {
        <$field_ty as TryFrom<u8>>::try_from($vs)?
    };
    (Num, $vs:ident, $field_ty:path) => {
        <$field_ty as TryFrom<i64>>::try_from($vs)?
    };
    (U16, $vs:ident, $field_ty:path) => {
        <$field_ty as TryFrom<u16>>::try_from($vs)?
    };
    (I16, $vs:ident, $field_ty:path) => {
        <$field_ty as TryFrom<i16>>::try_from($vs)?
    };
    (U32, $vs:ident, $field_ty:path) => {
        <$field_ty as TryFrom<u32>>::try_from($vs)?
    };
    (I32, $vs:ident, $field_ty:path) => {
        <$field_ty as TryFrom<i32>>::try_from($vs)?
    };
    (Ptr, $vs:ident, $field_ty:path) => {
        $vs
    };
    (Sym, $vs:ident, $field_ty:path) => {
        <$field_ty as TryFrom<String>>::try_from($vs)?
    };
    (Tbl, $vs:ident, $field_ty:path) => {
        $vs
    };
}

#[macro_export]
macro_rules! mxsf_to_string {
    (Ptr, $vs:expr) => {
        match &$vs {
            Some(v) => v,
            None => "",
        }
        .to_string()
    };
    ($_kind:ident, $vs:expr) => {
        $vs.to_string()
    };
}

#[macro_export]
macro_rules! mxsf_row_string {
    (Num, $value:expr, $field_ty_str:expr) => {
        if $field_ty_str == "u16" {
            "    word"
        } else {
            "    dword"
        }
    };
    (U8, $value:expr, $field_ty_str:expr) => {
        "    byte"
    };
    (U16, $value:expr, $field_ty_str:expr) => {
        "    word"
    };
    (I16, $value:expr, $field_ty_str:expr) => {
        "    word"
    };
    (U32, $value:expr, $field_ty_str:expr) => {
        "    dword"
    };
    (I32, $value:expr, $field_ty_str:expr) => {
        "    dword"
    };
    (Ptr, $value:expr, $field_ty_str:expr) => {
        if let Some(v) = &$value {
            "\tptr"
        } else {
            "    dword"
        }
    };
    (Sym, $value:expr, $field_ty_str:expr) => {
        "\tsymbol"
    };
    (Tbl, $value:expr, $field_ty_str:expr) => {
        "\tptr"
    };
}

#[macro_export]
macro_rules! mxsf_row_stringify {
    (Dec, $field_name:ident, $value:expr, $ool:expr) => {
        $value.to_string()
    };
    (Hex, $field_name:ident, $value:expr, $ool:expr) => {
        format!("${:x}", $value)
    };
    (HexS, $field_name:ident, $value:expr, $ool:expr) => {
        if $value & 0x8000 > 0 {
            format!("$ffff{:x}", $value)
        } else {
            format!("${:x}", $value)
        }
    };
    (Car, $field_name:ident, $value:expr, $ool:expr) => {
        if ($value as i64).abs() > 0x1000_0000 {
            format!("${:x}", $value)
        } else {
            format!("^{}", $value / 256)
        }
    };
    (Sym, $field_name:ident, $value:expr, $ool:expr) => {
        format!("{}\t; utilProc", $value.to_string())
    };
    (Tbl, $field_name:ident, $value:expr, $ool:expr) => {{
        $value.serialize_to_lines($ool);
        stringify!($field_name)
    }};
    (Ptr, $field_name:ident, $value:expr, $ool:expr) => {
        if let Some(v) = &$value {
            let field = stringify!($field_name).to_case(Case::Camel);
            $ool.push_string(&field, v);
            field
        } else {
            "0".to_owned()
        }
    };
    (PtrN, $field_name:ident, $value:expr, $ool:expr) => {{
        let base = stringify!($field_name).to_case(Case::Camel);
        let mut offset = 0;
        let mut field = format!("{base}{offset}");
        while $ool.has_string(&field) {
            offset += 1;
            field = format!("{base}{offset}");
        }
        if let Some(v) = &$value {
            $ool.push_string(&field, v);
            field
        } else {
            $ool.use_string_name(&field);
            "0".to_owned()
        }
    }};
}

pub fn find_pointers<'a>(lines: &[&'a str]) -> Result<HashMap<&'a str, Vec<&'a str>>> {
    let mut pointers = HashMap::new();
    let pointer_names = lines
        .iter()
        .filter(|&l| l.starts_with(':'))
        .cloned()
        .collect::<Vec<&str>>();
    for pointer_name in pointer_names {
        let pointer_data = lines
            .iter()
            .cloned()
            .skip_while(|&l| l != pointer_name)
            .skip(1)
            .take_while(|&l| !l.starts_with(':') && !l.ends_with("end"))
            .map(str::trim)
            .filter(|l| !l.is_empty())
            .filter(|l| !l.starts_with(';'))
            .collect::<Vec<&str>>();
        pointers.insert(&pointer_name[1..], pointer_data);
    }
    pointers.insert("__empty__", Vec::new());
    Ok(pointers)
}

pub fn find_section<'a>(lines: &[&'a str], section_tag: &str) -> Result<Vec<&'a str>> {
    let start_pattern = format!("START OF {section_tag}");
    let end_pattern = format!("END OF {section_tag}");
    let out = lines
        .iter()
        .skip_while(|&l| !l.contains(&start_pattern))
        .take_while(|&l| !l.contains(&end_pattern))
        .map(|&l| l.trim())
        .filter(|&l| !l.is_empty() && !l.starts_with(';'))
        .collect::<Vec<&str>>();
    Ok(out)
}

pub fn mxsf_split_line(line: &str) -> Result<(&str, &str, Option<&str>)> {
    let mut parts = line.splitn(2, ';');
    let mut words = parts
        .next()
        .ok_or_else(|| anyhow!("empty line"))?
        .splitn(2, ' ')
        .map(|v| v.trim())
        .filter(|s| !s.is_empty());
    let comment = parts.next().map(|s| s.trim());

    let kind = words.next().ok_or_else(|| anyhow!("missing kind"))?;
    let value = words.next().ok_or_else(|| anyhow!("missing value"))?; //collect::<Vec<&str>>().join(" ");

    Ok((kind, value, comment))
}

pub fn mxsf_lookup_string<'a>(
    key: &str,
    table: &HashMap<&'a str, Vec<&'a str>>,
) -> Result<&'a str> {
    mxsf_parse_string(
        table
            .get(key)
            .ok_or_else(|| anyhow!("no such pointer {} in string table", key))?
            .first()
            .ok_or_else(|| anyhow!("not enough values under {} in string table", key))?,
    )
}

pub fn mxsf_parse_number(vs: &str) -> Result<i64> {
    Ok(if let Some(hex) = vs.strip_prefix('$') {
        i64::from(u32::from_str_radix(hex, 16)?)
    } else if let Some(short) = vs.strip_prefix('^') {
        i64::from(short.parse::<u32>()? * 256)
    } else {
        vs.parse::<i64>()?
    })
}

pub fn mxsf_parse_string(line: &str) -> Result<&str> {
    let (kind, value, _comment) = mxsf_split_line(line)?;
    ensure!(kind == "string");
    mxsf_parse_quoted(value)
}

pub fn mxsf_parse_quoted(vs: &str) -> Result<&str> {
    ensure!(vs.starts_with('"'), "expected string to be quoted");
    ensure!(vs.ends_with('"'), "expected string to be quoted");
    Ok(&vs[1..vs.len() - 1])
}

pub fn extract_table_data<'a>(lines: &[&'a str]) -> Result<HashMap<&'a str, Vec<&'a str>>> {
    let mut table = HashMap::new();

    let table_lines = lines
        .iter()
        .cloned()
        .skip_while(|vs| !vs.starts_with(':'))
        .take_while(|vs| *vs != "end")
        .collect::<Vec<&str>>();
    let table_lines = table_lines
        .iter()
        .cloned()
        .map(|vs| vs.trim())
        .filter(|vs| !vs.starts_with(';'));

    let mut current = "";
    for line in table_lines {
        if line == "end" {
            return Ok(table);
        }
        if let Some(name) = line.strip_prefix(':') {
            current = name;
        } else {
            table
                .entry(current)
                .and_modify(|lns: &mut Vec<&str>| lns.push(line))
                .or_insert_with(|| vec![line]);
        }
    }
    bail!("no end detected on table data")
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
pub enum CarOrHexConfig {
    #[default]
    Car,
    Hex,
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
pub enum HexOptConfig {
    #[default]
    Normal,
    Fat,
}

#[derive(Clone, Debug, Default)]
pub struct OutOfLineTable {
    hards: Vec<String>,
    envelopes: Vec<String>,
    string_names: HashSet<String>,
    strings: Vec<String>,
}

impl OutOfLineTable {
    pub fn push_string(&mut self, name: &str, value: &str) {
        self.string_names.insert(name.to_owned());
        self.strings.push(format!(":{}", name));
        self.strings.push(format!("\tstring \"{}\"", value));
    }

    pub fn use_string_name(&mut self, name: &str) {
        self.string_names.insert(name.to_owned());
    }

    pub fn push_string_table(&mut self, name: &str, values: &[&str]) {
        self.string_names.insert(name.to_owned());
        self.strings.push(format!(":{}", name));
        for value in values {
            self.strings.push(format!("\tstring \"{}\"", value));
        }
    }

    pub fn push_string_line<S: ToString>(&mut self, line: S) {
        debug_assert!(!line.to_string().starts_with(':'));
        self.strings.push(line.to_string());
    }

    pub fn has_string(&self, s: &str) -> bool {
        self.string_names.contains(s)
    }

    pub fn push_hardpoint_line<S: ToString>(&mut self, line: S) {
        self.hards.push(line.to_string());
    }

    pub fn push_envelope_line<S: ToString>(&mut self, line: S) {
        self.envelopes.push(line.to_string());
    }

    pub fn take_envs(&mut self) -> Vec<String> {
        let mut out = Vec::new();
        std::mem::swap(&mut out, &mut self.envelopes);
        out
    }

    pub fn take_hards(&mut self) -> Vec<String> {
        let mut out = Vec::new();
        std::mem::swap(&mut out, &mut self.hards);
        out
    }

    pub fn iter_strings_mut(&mut self) -> impl Iterator<Item = &mut String> {
        self.strings.iter_mut()
    }

    pub fn take_strings(&mut self) -> Vec<String> {
        let mut out = Vec::new();
        std::mem::swap(&mut out, &mut self.strings);
        out
    }

    pub fn merge_into(&mut self, other: Self) {
        self.hards.extend(other.hards);
        self.envelopes.extend(other.envelopes);
        self.strings.extend(other.strings);
        self.string_names.extend(other.string_names);
    }

    pub fn merge(mut self, other: Self) -> Self {
        self.merge_into(other);
        self
    }
}

#[macro_export]
macro_rules! make_xt_struct_family {
    ($structname:ident($parent:ident: $parent_ty:ty, version: $version_ty:ident) {
        $( ($version_supported:ident, $row_type:ident, $field_name:ident, $field_ty:path, $row_format:ident, $comment:expr) ),*
    }) => {
        $crate::paste! {
            // The core type is read-only and stores data typed with units, where possible.
            #[derive(Clone, Debug)]
            pub struct $structname {
                $parent: $parent_ty,

                $($field_name: $field_ty),*
            }

            impl $structname {
                pub fn $parent(&self) -> &$parent_ty {
                    &self.$parent
                }

                $(
                pub fn $field_name(&self) -> &$field_ty {
                    &self.$field_name
                }
                )*

                pub fn from_lines(
                    $parent: $parent_ty,
                    lines: &[&str],
                    pointers: &HashMap<&str, Vec<&str>>
                ) -> Result<Self> {
                    let file_version = $version_ty::from_len(lines.len())?;

                    let mut offset = 0;
                    $(
                    // Take a field if it exists in this version of the format.
                    let field_version = $version_ty::$version_supported;
                    let $field_name = if field_version <= file_version {
                        let (kind, value, comment) = $crate::mxsf_split_line(&lines[offset])?;
                        offset += 1;
                        $crate::mxsf_check_kind!($row_type, kind, value);
                        if let Some(comment) = comment {
                            $crate::ensure!($comment == comment);
                        }
                        let value = $crate::mxsf_parse_to_storage!($row_type, kind, value, $field_ty, pointers);
                        $crate::mxsf_storage_to_actual!($row_type, value, $field_ty)
                    } else {
                        Default::default()
                    };
                    )*

                    Ok(Self {
                        $parent,
                        $($field_name),*
                    })
                }

                pub fn fields() -> &'static [&'static str] {
                    &[$(stringify!($field_name)),*]
                }

                pub fn get_field(&self, field: &'static str) -> Result<String> {
                    Ok(match field {
                        $(
                            stringify!($field_name) => $crate::mxsf_to_string!($row_type, self.$field_name)
                        ),*,
                        _ => $crate::bail!("no such field {field}")
                    })
                }
            }

            // The I/O type is read/write and stores data in native formats.
            #[derive(Clone, Debug)]
            pub struct [<$structname Io>] {
                $parent: [<$parent_ty Io>],

                $(
                $field_name: $crate::mxsf_row_storage!($row_type, $field_ty)
                ),*
            }

            impl [<$structname Io>] {
                pub fn $parent(&self) -> &[<$parent_ty Io>] {
                    &self.$parent
                }

                pub fn [<$parent _mut>](&mut self) -> &mut [<$parent_ty Io>] {
                    &mut self.$parent
                }

                $(
                pub fn $field_name(&self) -> &$crate::mxsf_row_storage!($row_type, $field_ty) {
                    &self.$field_name
                }

                pub fn [<$field_name _mut>](&mut self) -> &mut $crate::mxsf_row_storage!($row_type, $field_ty) {
                    &mut self.$field_name
                }
                )*

                pub fn serialize_to_lines(&self, table: &mut $crate::OutOfLineTable) -> Vec<String> {
                    use $crate::convert_case::{Case, Casing};
                    let raw = vec![
                    $(
                        format!(
                            "{} {}",
                            $crate::mxsf_row_string!($row_type, self.$field_name, stringify!($field_ty)),
                            $crate::mxsf_row_stringify!($row_format, $field_name, self.$field_name, table)
                        )
                    ),*
                    ];
                    raw
                }
            }

            impl From<&$structname> for [<$structname Io>] {
                fn from(v: &$structname) -> Self {
                    Self {
                        $parent: [<$parent_ty Io>]::from(&v.$parent),
                        $(
                        $field_name: v.$field_name.clone().into()
                        ),*
                    }
                }
            }
        }
    };
}

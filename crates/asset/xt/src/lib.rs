// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::{anyhow, bail, Result};
use bevy_ecs::prelude::*;
use catalog::FileInfo;
use installations::from_dos_string;
use jt::ProjectileType;
use log::trace;
use nitrous::{
    inject_nitrous_component, inject_nitrous_resource, NitrousComponent, NitrousResource,
};
use nt::NpcType;
use ot::ObjectType;
use parking_lot::Mutex;
use pt::PlaneType;
use runtime::{Extension, Runtime};
use std::{collections::HashMap, sync::Arc};

// A generic type.
#[derive(Debug)]
pub enum Type {
    JT(Box<ProjectileType>),
    NT(Box<NpcType>),
    OT(Box<ObjectType>),
    PT(Box<PlaneType>),
}

impl Type {
    pub fn ot(&self) -> &ObjectType {
        match self {
            Type::OT(ref ot) => ot,
            Type::JT(ref jt) => jt.ot(),
            Type::NT(ref nt) => nt.ot(),
            Type::PT(ref pt) => pt.nt().ot(),
        }
    }

    pub fn jt(&self) -> Option<&ProjectileType> {
        match self {
            Type::JT(ref jt) => Some(jt),
            _ => None,
        }
    }

    pub fn nt(&self) -> Option<&NpcType> {
        match self {
            Type::NT(ref nt) => Some(nt),
            Type::PT(ref pt) => Some(pt.nt()),
            _ => None,
        }
    }

    pub fn pt(&self) -> Option<&PlaneType> {
        match self {
            Type::PT(ref pt) => Some(pt),
            _ => None,
        }
    }
}

// Any single type is likely used by multiple game objects at once so we cache
// type loads aggressively and hand out a Ref to an immutable, shared global
// copy of the Type.
#[derive(NitrousComponent, Clone, Debug)]
pub struct TypeRef(Arc<Type>);

#[inject_nitrous_component]
impl TypeRef {
    fn new(item: Type) -> Self {
        TypeRef(Arc::new(item))
    }

    pub fn ot(&self) -> &ObjectType {
        self.0.ot()
    }

    pub fn jt(&self) -> Option<&ProjectileType> {
        self.0.jt()
    }

    pub fn nt(&self) -> Option<&NpcType> {
        self.0.nt()
    }

    pub fn pt(&self) -> Option<&PlaneType> {
        self.0.pt()
    }

    pub fn xjt(&self) -> &ProjectileType {
        self.0.jt().unwrap()
    }

    pub fn xnt(&self) -> &NpcType {
        self.0.nt().unwrap()
    }

    pub fn xpt(&self) -> &PlaneType {
        self.0.pt().unwrap()
    }

    pub fn is_pt(&self) -> bool {
        self.pt().is_some()
    }

    pub fn is_nt(&self) -> bool {
        self.nt().is_some()
    }

    pub fn is_jt(&self) -> bool {
        self.jt().is_some()
    }
}

// Knows how to load a type from a game library. Keeps a cached copy and hands
// out a pointer to the type, since we frequently need to load the same item
// repeatedly.
#[derive(Debug, NitrousResource)]
pub struct TypeManager {
    // Cache immutable resources. Use interior mutability for ease of use.
    cache: Mutex<HashMap<String, TypeRef>>,
}

impl Extension for TypeManager {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        runtime.inject_resource(TypeManager::empty())?;
        Ok(())
    }
}

#[inject_nitrous_resource]
impl TypeManager {
    pub fn empty() -> TypeManager {
        trace!("TypeManager::new");
        TypeManager {
            cache: Mutex::new(HashMap::new()),
        }
    }

    pub fn unload(&self, info: FileInfo<'_>) -> Result<TypeRef> {
        let cache_key = info.to_string();
        self.cache
            .lock()
            .remove(&cache_key)
            .ok_or_else(|| anyhow!("the file {info} was not cached!"))
    }

    pub fn load(&self, info: FileInfo<'_>) -> Result<TypeRef> {
        let cache_key = info.to_string();
        if let Some(item) = self.cache.lock().get(&cache_key) {
            trace!("TypeManager::load({}) -- cached", info);
            return Ok(item.clone());
        };

        trace!("TypeManager::load({})", cache_key);
        let content = from_dos_string(info.data()?);

        let ext = info.name()[info.name().len() - 3..].to_ascii_uppercase();

        // let ext = name.rsplitn(2, '.').collect::<Vec<&str>>();
        let item = match ext.as_ref() {
            ".OT" => {
                let ot = ObjectType::from_text(&content)?;
                Type::OT(Box::new(ot))
            }
            ".JT" => {
                let jt = ProjectileType::from_text(&content)?;
                Type::JT(Box::new(jt))
            }
            ".NT" => {
                let nt = NpcType::from_text(&content)?;
                Type::NT(Box::new(nt))
            }
            ".PT" => {
                let pt = PlaneType::from_text(&content)?;
                Type::PT(Box::new(pt))
            }
            _ => bail!("resource: unknown type {info}"),
        };
        let xt = TypeRef::new(item);
        self.cache.lock().insert(cache_key, xt.clone());
        Ok(xt)
    }

    // // FIXME: what does this interface actually need to look like?
    // pub fn load(&self, name: &str, catalog: &Catalog) -> Result<TypeRef> {
    //     let data = catalog.read_name(catalog.lookup(name)?)?;
    //     self.load_raw(catalog.label(), name, data)
    // }
    //
    // pub fn bust_cache(&mut self, name: &str, catalog: &Catalog) {
    //     let cache_key = format!("{}:{}", catalog.label(), name);
    //     trace!("TypeManager::bust_cache({})", cache_key);
    //     self.cache.lock().remove(&cache_key);
    // }
}

#[cfg(test)]
mod tests {
    use super::*;
    use catalog::{FileSystem, Search};
    use installations::Installations;

    #[test]
    fn can_parse_all_entity_types() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(Search::for_glob("*.[OJNP]T").must_match())? {
            println!("At: {info}");
            let types = TypeManager::empty();
            let ty = types.load(info)?;
            // // Only one misspelling in 2500 files.
            assert!(ty.ot().ot_names().file_name() == info.name() || info.name() == "SMALLARM.JT");
        }

        // for (game, _palette, catalog) in catalogs.all() {
        //     for name in catalog.find(Search::for_glob("*.[OJNP]T"))? {
        //         let meta = catalog.stat_name(name)?;
        //         println!("At: {}:{:13} @ {}", game.test_dir, meta.name(), meta.path());
        //         let types = TypeManager::empty();
        //         let ty = types.load(meta.name(), catalog)?;
        //         // Only one misspelling in 2500 files.
        //         assert!(
        //             ty.ot().ot_names().file_name() == meta.name() || meta.name() == "SMALLARM.JT"
        //         );
        //         // println!(
        //         //     "{}:{:13}> {:?} <> {}",
        //         //     game, name, ot.explosion_type, ot.long_name
        //         // );
        //     }
        // }

        Ok(())
    }
}

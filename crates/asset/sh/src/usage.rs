// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::analysis::ShCapabilities;
use anyhow::{bail, ensure, Result};
use nitrous::{Dict, Value};
use vehicle::{
    AirbrakeEffector, BayEffector, FlapsEffector, GearEffector, HookEffector, PowerSystem,
};

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
#[repr(i8)]
pub enum AfterBurnerState {
    #[default]
    Off = 0,
    On = 1,
}
impl TryFrom<i8> for AfterBurnerState {
    type Error = anyhow::Error;
    fn try_from(value: i8) -> Result<Self> {
        Ok(match value {
            0 => Self::Off,
            1 => Self::On,
            _ => bail!("invalid afterburner state {value}"),
        })
    }
}
impl AfterBurnerState {
    pub fn for_power(power: &PowerSystem) -> Self {
        if power.is_afterburner() {
            Self::On
        } else {
            Self::Off
        }
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
#[repr(i8)]
pub enum AileronPosition {
    #[default]
    Center = 0,
    Down = -1,
    Up = 1,
}
impl TryFrom<i8> for AileronPosition {
    type Error = anyhow::Error;
    fn try_from(value: i8) -> Result<Self> {
        Ok(match value {
            0 => Self::Center,
            -1 => Self::Down,
            1 => Self::Up,
            _ => bail!("invalid aileron position {value}"),
        })
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
#[repr(i8)]
pub enum AirbrakeState {
    #[default]
    Retracted = 0,
    Extended = 1,
    Inverted = -1,
}
impl TryFrom<i8> for AirbrakeState {
    type Error = anyhow::Error;
    fn try_from(value: i8) -> Result<Self> {
        Ok(match value {
            0 => Self::Retracted,
            1 => Self::Extended,
            -1 => Self::Inverted,
            _ => bail!("invalid airbrake state {value}"),
        })
    }
}
impl AirbrakeState {
    pub fn for_effector(effect: &AirbrakeEffector) -> Self {
        if effect.position() > 0.1 {
            Self::Extended
        } else {
            Self::Retracted
        }
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
#[repr(i8)]
pub enum BayState {
    #[default]
    Closed = 0,
    Open = 1,
}
impl TryFrom<i8> for BayState {
    type Error = anyhow::Error;
    fn try_from(value: i8) -> Result<Self> {
        Ok(match value {
            0 => Self::Closed,
            1 => Self::Open,
            _ => bail!("invalid bay state {value}"),
        })
    }
}
impl BayState {
    pub fn for_effector(effect: &BayEffector) -> Self {
        if effect.position() > 0. {
            BayState::Open
        } else {
            BayState::Closed
        }
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
#[repr(i8)]
pub enum EjectState {
    #[default]
    Unk11 = 0x11,
    Unk12 = 0x12,
    Unk13 = 0x13,
    Unk14 = 0x14,
    Unk15 = 0x15,

    Unk1A = 0x1A,
    Unk1B = 0x1B,
    Unk1C = 0x1C,
    Unk1D = 0x1D,
    Unk1E = 0x1E,

    Unk22 = 0x22,
    Unk23 = 0x23,
    Unk24 = 0x24,
    Unk25 = 0x25,
    Unk26 = 0x26,
}
impl TryFrom<i8> for EjectState {
    type Error = anyhow::Error;
    fn try_from(value: i8) -> Result<Self> {
        Ok(match value {
            0x11 => Self::Unk11,
            0x12 => Self::Unk12,
            0x13 => Self::Unk13,
            0x14 => Self::Unk14,
            0x15 => Self::Unk15,

            0x1A => Self::Unk1A,
            0x1B => Self::Unk1B,
            0x1C => Self::Unk1C,
            0x1D => Self::Unk1D,
            0x1E => Self::Unk1E,

            0x22 => Self::Unk22,
            0x23 => Self::Unk23,
            0x24 => Self::Unk24,
            0x25 => Self::Unk25,
            0x26 => Self::Unk26,

            _ => bail!("invalid eject state {value}"),
        })
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
#[repr(i8)]
pub enum FlapState {
    #[default]
    Up = 0,
    Down = -1,
    Hover = -2,
    Brake = 1,
}
impl TryFrom<i8> for FlapState {
    type Error = anyhow::Error;
    fn try_from(value: i8) -> Result<Self> {
        Ok(match value {
            0 => Self::Up,
            -1 => Self::Down,
            -2 => Self::Hover,
            1 => Self::Brake,
            _ => bail!("invalid flap position {value}"),
        })
    }
}
impl FlapState {
    pub fn for_effector(effect: &FlapsEffector) -> Self {
        if effect.position() > 0.1 {
            FlapState::Down
        } else {
            FlapState::Up
        }
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
#[repr(i8)]
pub enum GearState {
    #[default]
    Retracted = 0,
    Extended = 1,
    Strut = 4,
}
impl TryFrom<i8> for GearState {
    type Error = anyhow::Error;
    fn try_from(value: i8) -> Result<Self> {
        Ok(match value {
            0 => Self::Retracted,
            1 => Self::Extended,
            4 => Self::Strut,
            _ => bail!("invalid gear state {value}"),
        })
    }
}
impl GearState {
    pub fn for_effector(effect: &GearEffector) -> Self {
        if effect.position() > 0. {
            GearState::Extended
        } else {
            GearState::Retracted
        }
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
#[repr(i8)]
pub enum HookState {
    #[default]
    Retracted = 0,
    Extended = 1,
}
impl TryFrom<i8> for HookState {
    type Error = anyhow::Error;
    fn try_from(value: i8) -> Result<Self> {
        Ok(match value {
            0 => Self::Retracted,
            1 => Self::Extended,
            _ => bail!("invalid hook state {value}"),
        })
    }
}
impl HookState {
    pub fn for_effector(effect: &HookEffector) -> Self {
        if effect.position() > 0.1 {
            Self::Extended
        } else {
            Self::Retracted
        }
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
#[repr(i8)]
pub enum RudderPosition {
    #[default]
    Center = 0,
    Left = -1,
    Right = 1,
}
impl TryFrom<i8> for RudderPosition {
    type Error = anyhow::Error;
    fn try_from(value: i8) -> Result<Self> {
        Ok(match value {
            0 => Self::Center,
            1 => Self::Right,
            -1 => Self::Left,
            _ => bail!("invalid rudder position {value}"),
        })
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
#[repr(i8)]
pub enum PlayerState {
    #[default]
    Alive = 0,
    Dead = 1,
}
impl TryFrom<i8> for PlayerState {
    type Error = anyhow::Error;
    fn try_from(value: i8) -> Result<Self> {
        Ok(match value {
            0 => Self::Alive,
            1 => Self::Dead,
            _ => bail!("invalid player state {value}"),
        })
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct SamCount {
    num: i8,
}
impl SamCount {
    pub fn new(value: i8) -> Self {
        assert!((0..=3).contains(&value), "invalid sam count: {value}");
        Self { num: value }
    }
    pub fn num(&self) -> i8 {
        self.num
    }
}
impl Default for SamCount {
    fn default() -> Self {
        Self { num: 3 }
    }
}
impl TryFrom<i8> for SamCount {
    type Error = anyhow::Error;
    fn try_from(value: i8) -> Result<Self> {
        ensure!((0..=3).contains(&value), "invalid sam count: {value}");
        Ok(Self { num: value })
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
#[repr(i8)]
pub enum SlatsState {
    #[default]
    Retracted = 0,
    Extended = 1,
}
impl TryFrom<i8> for SlatsState {
    type Error = anyhow::Error;
    fn try_from(value: i8) -> Result<Self> {
        Ok(match value {
            0 => Self::Retracted,
            1 => Self::Extended,
            _ => bail!("invalid slats state {value}"),
        })
    }
}
impl SlatsState {
    pub fn for_effector(effect: &FlapsEffector) -> Self {
        if effect.position() > 0.1 {
            Self::Extended
        } else {
            Self::Retracted
        }
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
pub enum VertBufUsage {
    Afterburner(AfterBurnerState),
    AileronLeft(AileronPosition),
    AileronRight(AileronPosition),
    Bay(BayState),
    Brake(AirbrakeState),
    Eject(EjectState),
    FlapLeft(FlapState),
    FlapRight(FlapState),
    Gear(GearState),
    Hook(HookState),
    PlayerDead(PlayerState),
    Rudder(RudderPosition),
    SamCount(SamCount),
    Slats(SlatsState),
    #[default]
    Static,
}

impl VertBufUsage {
    pub fn to_value(&self) -> Value {
        let mut map = Dict::default();
        match self {
            Self::Static => map.insert("static", true.into()),
            Self::Afterburner(v) => map.insert("afterburner", (*v as i8).into()),
            Self::AileronLeft(v) => map.insert("aileron_left", (*v as i8).into()),
            Self::AileronRight(v) => map.insert("aileron_right", (*v as i8).into()),
            Self::Bay(v) => map.insert("bay", (*v as i8).into()),
            Self::Brake(v) => map.insert("brake", (*v as i8).into()),
            Self::Eject(v) => map.insert("eject", (*v as i8).into()),
            Self::FlapLeft(v) => map.insert("flap_left", (*v as i8).into()),
            Self::FlapRight(v) => map.insert("flap_right", (*v as i8).into()),
            Self::Gear(v) => map.insert("gear", (*v as i8).into()),
            Self::Hook(v) => map.insert("hook", (*v as i8).into()),
            Self::Rudder(v) => map.insert("rudder", (*v as i8).into()),
            Self::PlayerDead(v) => map.insert("player_dead", (*v as i8).into()),
            Self::Slats(v) => map.insert("slats", (*v as i8).into()),
            Self::SamCount(v) => map.insert("sam_count", v.num.into()),
        }
        map.into()
    }

    pub fn from_value(v: &Value) -> Result<Self> {
        let map = v.to_dict()?;
        Ok(if let Some(v) = map.get("afterburner") {
            Self::Afterburner(i8::try_from(v.to_int()?)?.try_into()?)
        } else if let Some(v) = map.get("aileron_left") {
            Self::AileronLeft(i8::try_from(v.to_int()?)?.try_into()?)
        } else if let Some(v) = map.get("aileron_right") {
            Self::AileronRight(i8::try_from(v.to_int()?)?.try_into()?)
        } else if let Some(v) = map.get("bay") {
            Self::Bay(i8::try_from(v.to_int()?)?.try_into()?)
        } else if let Some(v) = map.get("brake") {
            Self::Brake(i8::try_from(v.to_int()?)?.try_into()?)
        } else if let Some(v) = map.get("eject") {
            Self::Eject(i8::try_from(v.to_int()?)?.try_into()?)
        } else if let Some(v) = map.get("flap_left") {
            Self::FlapLeft(i8::try_from(v.to_int()?)?.try_into()?)
        } else if let Some(v) = map.get("flap_right") {
            Self::FlapRight(i8::try_from(v.to_int()?)?.try_into()?)
        } else if let Some(v) = map.get("gear") {
            Self::Gear(i8::try_from(v.to_int()?)?.try_into()?)
        } else if let Some(v) = map.get("hook") {
            Self::Hook(i8::try_from(v.to_int()?)?.try_into()?)
        } else if let Some(v) = map.get("rudder") {
            Self::Rudder(i8::try_from(v.to_int()?)?.try_into()?)
        } else if let Some(v) = map.get("player_dead") {
            Self::PlayerDead(i8::try_from(v.to_int()?)?.try_into()?)
        } else if let Some(v) = map.get("sam_count") {
            Self::SamCount(i8::try_from(v.to_int()?)?.try_into()?)
        } else if let Some(v) = map.get("slats") {
            Self::Slats(i8::try_from(v.to_int()?)?.try_into()?)
        } else if let Some(v) = map.get("static") {
            ensure!(v.to_bool()? == true);
            Self::Static
        } else {
            bail!("unable to from_value VertBufUsage: {v:?}");
        })
    }

    // Return the capability that this usage provides.
    pub fn capability(&self) -> ShCapabilities {
        match self {
            Self::Static => ShCapabilities::empty(),
            Self::Afterburner(_) => ShCapabilities::HAS_AFTERBURNER,
            Self::AileronLeft(_) => ShCapabilities::HAS_AILERONS,
            Self::AileronRight(_) => ShCapabilities::HAS_AILERONS,
            Self::Bay(_) => ShCapabilities::HAS_BAY,
            Self::Brake(_) => ShCapabilities::HAS_AIRBRAKE,
            Self::Eject(_) => ShCapabilities::HAS_EJECTION_STATE,
            Self::FlapLeft(FlapState::Brake) => ShCapabilities::HAS_FLAP_AIRBRAKE,
            Self::FlapLeft(FlapState::Hover) => ShCapabilities::HAS_HOVER_FLAPS,
            Self::FlapLeft(_) => ShCapabilities::HAS_FLAPS,
            Self::FlapRight(FlapState::Brake) => ShCapabilities::HAS_FLAP_AIRBRAKE,
            Self::FlapRight(FlapState::Hover) => ShCapabilities::HAS_HOVER_FLAPS,
            Self::FlapRight(_) => ShCapabilities::HAS_FLAPS,
            Self::Gear(GearState::Strut) => ShCapabilities::HAS_GEAR_STRUT,
            Self::Gear(_) => ShCapabilities::HAS_GEAR,
            Self::Hook(_) => ShCapabilities::HAS_HOOK,
            Self::Rudder(_) => ShCapabilities::HAS_RUDDER,
            Self::PlayerDead(_) => ShCapabilities::HAS_PLAYER_STATE,
            Self::Slats(_) => ShCapabilities::HAS_SLATS,
            Self::SamCount(_) => ShCapabilities::HAS_SAM_COUNT,
        }
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
pub enum FaceUsage {
    #[default]
    FromBuffer,
    Animation {
        frame_number: u8,
        num_frames: u8,
    },
}

impl FaceUsage {
    pub fn to_value(&self) -> Value {
        match self {
            Self::FromBuffer => "from_buffer".into(),
            Self::Animation {
                frame_number,
                num_frames,
            } => {
                let mut map = Dict::default();
                map.insert("frame_number", (*frame_number).into());
                map.insert("num_frames", (*num_frames).into());
                map.into()
            }
        }
    }

    pub fn from_value(v: &Value) -> Result<Self> {
        if let Ok(s) = v.to_str() {
            ensure!(s == "from_buffer");
            return Ok(Self::FromBuffer);
        }
        let map = v.to_dict()?;
        let frame_number: u8 = map.try_get("frame_number")?.to_int()?.try_into()?;
        let num_frames: u8 = map.try_get("num_frames")?.to_int()?.try_into()?;
        Ok(Self::Animation {
            frame_number,
            num_frames,
        })
    }
}

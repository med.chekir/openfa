// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
#![allow(non_camel_case_types)]

use crate::{
    sh_code::{CodePtr, InstrDetail, InstrMagic, ShInstrSpec},
    usage::{FaceUsage, VertBufUsage},
    xform::XformId,
};
use ansi::{ansi, Color};
use anyhow::{anyhow, bail, ensure, Result};
use bitflags::bitflags;
use byteorder::LittleEndian;
use nitrous::{Dict, List};
use std::{
    collections::HashMap,
    fmt::{Display, Formatter},
    ops::Range,
    str,
};
use uuid::Uuid;
use zerocopy::{
    byteorder::{I16, LE, U16},
    AsBytes, ByteOrder, FromBytes, FromZeroes, Ref, Unaligned,
};

// Alias to save space.
type LinkMap = HashMap<usize, (Uuid, usize)>;

pub fn read_name(n: &[u8]) -> Result<String> {
    let end_offset: usize = n
        .iter()
        .position(|&c| c == 0)
        .ok_or_else(|| anyhow!("no end to name"))?;
    Ok(str::from_utf8(&n[..end_offset])?.to_owned())
}

macro_rules! make_unk_instr {
    ($name:ident, $short_name:ident, $magic:expr, $size:expr) => {
        // These all use no-detail, so can't store exact bytes here... do we expand our enum?
        // No.
        // Instead, specialize the display method to deal with no-detail.
        // Except, this means we need an allocation per no-detail for the Vec.
        // Solution: use smallvec to avoid the allocation in most cases.
        // Face detail varient is enormous, so we can stuff our bytes here and make
        // use of all the wasted space in non-Face Instrs.
        pub struct $short_name;
        impl ShInstrSpec for $short_name {
            const MAGIC: InstrMagic = InstrMagic::Word($magic);
            const SIZE: Option<usize> = Some($size);
            const NAME: &'static str = stringify!($name);
            const SHORT_NAME: &'static str = concat!("  ", stringify!($short_name));
        }
    };
}

pub struct i00_EndObject;

impl ShInstrSpec for i00_EndObject {
    const MAGIC: InstrMagic = InstrMagic::Byte(0x00);
    const SIZE: Option<usize> = None;
    const NAME: &'static str = "EndObject";
    const SHORT_NAME: &'static str = "EndOb";
    const COLOR: Color = Color::Green;
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        // If we're just out of space... :shrug:
        if idata.len() < 18 {
            return Ok((InstrDetail::NoDetail(idata.into()), idata.len()));
        }

        // There are some cases where EndObj is its short self, even though there's more data.
        // There are actually more than these but the errata is up a level, where we have access
        // to other instructions where we can infer from 2EndO.
        if idata[16] == 0x00 && idata[17] == 0x00 && idata[0..6] != [0u8; 6] {
            return Ok((InstrDetail::NoDetail(idata[..18].into()), 18));
        }

        // In the normal case the EndObj will be the last thing in idata anyway.
        Ok((InstrDetail::NoDetail(idata.into()), idata.len()))
    }
}

pub struct i01_EndShape;

impl ShInstrSpec for i01_EndShape {
    const MAGIC: InstrMagic = InstrMagic::Byte(0x01);
    const SIZE: Option<usize> = None;
    const NAME: &'static str = "EndShape";
    const SHORT_NAME: &'static str = "EndSh";
    const COLOR: Color = Color::Green;
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        Ok((InstrDetail::NoDetail(idata.into()), idata.len()))
    }
}

make_unk_instr!(Unk08, i08, 0x08, 4);

#[derive(Clone, Debug)]
pub struct i06 {
    #[allow(unused)]
    code_ptr0: CodePtr,
    #[allow(unused)]
    code_ptr1: CodePtr,
    #[allow(unused)]
    extra: Option<u8>,
}

impl ShInstrSpec for i06 {
    const MAGIC: InstrMagic = InstrMagic::Word(0x06);
    const SIZE: Option<usize> = None;
    const NAME: &'static str = "Unk06";
    const SHORT_NAME: &'static str = "  i06";
    const NOTES: &'static str = "There is a u16 at offset 14 that tells us how many additional \
        bytes to read.";
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        let count = LittleEndian::read_u16(&idata[14..16]);
        ensure!(count == 5 || count == 8);
        let _extra = if count == 5 {
            ensure!(idata[18] == 0x38);
            None
        } else {
            assert_eq!(count, 8);
            let last = LittleEndian::read_u16(&idata[22..24]);
            ensure!(last == 0);
            ensure!(idata[18] == 0x50);
            Some(idata[21])
        };
        let size = 16 + count as usize;
        Ok((
            // InstrDetail::Unk06(i06 {
            //     code_ptr0: CodePtr::new_rel16_bs(&idata[16..18]),
            //     code_ptr1: CodePtr::new_rel16_bs(&idata[19..21]),
            //     extra,
            // }),
            InstrDetail::NoDetail(idata[..size].into()),
            size,
        ))
    }
    // fn link(&mut self, _offset: usize, _size: usize, _uuid_map: &mut LinkMap) -> Result<()> {
    //     // TODO: figure out where these point. Plurry found that relocating them by however many bytes
    //     //       the instruction shifted in the file causes FA to not crash, so I suspect these are
    //     //       relative, but it's not clear how as using the standard style of relative addressing
    //     //       puts them in the middle of random instructions.
    //     // self.code_ptr0.link(offset + size, uuid_map)?;
    //     // self.code_ptr1.link(offset + size, uuid_map)?;
    //     Ok(())
    // }
    // fn unlink(&mut self, offset: usize, size: usize, map: &HashMap<Uuid, usize>) -> Result<()> {
    //     // self.code_ptr0.unlink_rel16(offset + size, map)?;
    //     //self.code_ptr0.unlink_rel16(offset + size, map)?;
    //     Ok(())
    // }
}

pub struct i0C;

impl ShInstrSpec for i0C {
    const MAGIC: InstrMagic = InstrMagic::Word(0x0C);
    const SIZE: Option<usize> = None;
    const NAME: &'static str = "Unk0C";
    const SHORT_NAME: &'static str = "  i0C";
    const NOTES: &'static str = "There is a u16 at offset 10 that tells us how many additional \
        bytes to read.";
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        let count = LittleEndian::read_u16(&idata[10..12]);
        let size = 12 + count as usize;
        Ok((InstrDetail::NoDetail(idata[..size].into()), size))
    }
}

pub struct i0E;

impl ShInstrSpec for i0E {
    const MAGIC: InstrMagic = InstrMagic::Word(0x0E);
    const SIZE: Option<usize> = None;
    const NAME: &'static str = "Unk0E";
    const SHORT_NAME: &'static str = "  i0E";
    const NOTES: &'static str = "There is a u16 at offset 10 that tells us how many additional \
        bytes to read.";
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        let count = LittleEndian::read_u16(&idata[10..12]);
        let size = 12 + count as usize;
        Ok((InstrDetail::NoDetail(idata[..size].into()), size))
    }
}

pub struct i10;

impl ShInstrSpec for i10 {
    const MAGIC: InstrMagic = InstrMagic::Word(0x10);
    const SIZE: Option<usize> = None;
    const NAME: &'static str = "Unk10";
    const SHORT_NAME: &'static str = "  i10";
    const NOTES: &'static str = "There is a u16 at offset 10 that tells us how many additional \
        bytes to read. Identical to 0C?";
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        let count = LittleEndian::read_u16(&idata[10..12]);
        let size = 12 + count as usize;
        Ok((InstrDetail::NoDetail(idata[..size].into()), size))
    }
}

#[derive(Clone, Debug)]
pub struct i12_Unmask {
    code_ptr: CodePtr,
    usage: VertBufUsage,
}

impl ShInstrSpec for i12_Unmask {
    const MAGIC: InstrMagic = InstrMagic::Word(0x12);
    const SIZE: Option<usize> = Some(4);
    const NAME: &'static str = "Unmask";
    const SHORT_NAME: &'static str = "UMask";
    const COLOR: Color = Color::Blue;
    const NOTES: &'static str = "\
        When points to a VertexBuf, it \"unmasks\" the facets that occur after that vertex buffer up \
        to the next vertex buffer or Header. When it points elsewhere, :shrug:. It usually points \
        to something that jumps, I think to do manual backface culling, maybe? Doesn't seem important. \
    ";
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        Ok((
            InstrDetail::Unmask(Self {
                code_ptr: CodePtr::new_rel16_bs(&idata[2..4]),
                usage: VertBufUsage::default(),
            }),
            Self::SIZE.unwrap(),
        ))
    }
    fn to_bytes(&self, buf: &mut [u8]) -> Result<()> {
        buf[2..4].copy_from_slice(&self.code_ptr.as_rel16()?.to_le_bytes());
        Ok(())
    }
    fn link(&mut self, offset: usize, size: usize, uuid_map: &mut LinkMap) -> Result<()> {
        self.code_ptr.link(offset + size, uuid_map)
    }
    fn unlink(&mut self, offset: usize, size: usize, map: &HashMap<Uuid, usize>) -> Result<()> {
        self.code_ptr.unlink_rel16(offset + size, map)
    }
    fn as_value(&self, map: &mut Dict) -> Result<()> {
        map.insert("code_ptr", self.code_ptr.as_value());
        map.insert("usage", self.usage.to_value());
        Ok(())
    }
    fn display_content(&self, offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        Ok(format!(
            "{}tgt:{}, usage:{:?}{}",
            ansi().fg(Self::COLOR),
            self.code_ptr.display_content(offset_map)?,
            self.usage,
            ansi()
        ))
    }
}

impl i12_Unmask {
    pub fn from_value(map: &Dict) -> Result<(InstrDetail, usize)> {
        Ok((
            InstrDetail::Unmask(Self {
                code_ptr: CodePtr::from_value(map.try_get("code_ptr")?)?,
                usage: VertBufUsage::from_value(map.try_get("usage")?)?,
            }),
            Self::SIZE.unwrap(),
        ))
    }
    pub fn code_ptr(&self) -> &CodePtr {
        &self.code_ptr
    }
    pub fn set_usage(&mut self, usage: VertBufUsage) {
        debug_assert_ne!(usage, VertBufUsage::default(), "setting default usage");
        assert_eq!(self.usage, VertBufUsage::default(), "usage already set");
        self.usage = usage;
    }
}

pub struct i1E_Pad;

impl ShInstrSpec for i1E_Pad {
    const MAGIC: InstrMagic = InstrMagic::Byte(0x1E);
    const SIZE: Option<usize> = None;
    const NAME: &'static str = "Pad";
    const SHORT_NAME: &'static str = "  Pad";
    const COLOR: Color = Color::White;
    const NOTES: &'static str = "Used for re-alignment; just a Nop";
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        let mut size = 1;
        while size < idata.len() && idata[size] == 0x1E {
            size += 1;
        }
        Ok((InstrDetail::NoDetail(idata[..size].into()), size))
    }
}

make_unk_instr!(Unk2E, i2E, 0x2E, 4);

pub struct i38;

impl ShInstrSpec for i38 {
    const MAGIC: InstrMagic = InstrMagic::Byte(0x38);
    const SIZE: Option<usize> = Some(3);
    const NAME: &'static str = "Unk38";
    const SHORT_NAME: &'static str = "  i38";
    const NOTES: &'static str = "\
        I think this probably means something like: fastforward to \"next_offset\" if \
        we are running in low-detail mode. Seems to be before nose art and a couple random \
        polys in F22, a big rock in ROCKB, and the textured polys in BUNK. \
        The number does not appear to be a normal code pointer? \
        This maybe works with UNKC8 somehow?\
    ";
}

make_unk_instr!(Unk3A, i3A, 0x3A, 6);

#[derive(Clone, Debug)]
pub struct i40_JumpToFrame {
    frame_pointers: Vec<CodePtr>,
}

impl ShInstrSpec for i40_JumpToFrame {
    const MAGIC: InstrMagic = InstrMagic::Word(0x40);
    const SIZE: Option<usize> = None;
    const NAME: &'static str = "JumpToFrame";
    const SHORT_NAME: &'static str = "ToFrm";
    const COLOR: Color = Color::BrightBlue;
    const NOTES: &'static str = "A list of frames to jump to on successive animation ticks. \
    This mechanism provides most propeller effects, sparking on damage models, and \
     probably other stuff as well.";
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        ensure!(idata.len() > 4, "code too short for i40 header");
        let n_frames = LittleEndian::read_u16(&idata[2..4]) as usize;
        let buf_end = 4 + n_frames * 2;
        ensure!(idata.len() >= buf_end, "code too short for i40 frames");
        let mut frame_pointers = Vec::new();
        for i in 0..n_frames {
            let off = 4 + 2 * i;
            frame_pointers.push(CodePtr::new_rel16_bs(&idata[off..off + 2]));
        }
        Ok((InstrDetail::JumpToFrame(Self { frame_pointers }), buf_end))
    }
    fn to_bytes(&self, buf: &mut [u8]) -> Result<()> {
        let nframes: u16 = self.frame_pointers.len().try_into()?;
        buf[2..4].copy_from_slice(&nframes.to_le_bytes());
        let mut off = 4;
        for frame_pointer in &self.frame_pointers {
            buf[off..off + 2].copy_from_slice(&frame_pointer.as_rel16()?.to_le_bytes());
            off += 2;
        }
        ensure!(off == buf.len());
        Ok(())
    }
    fn link(&mut self, offset: usize, _size: usize, uuid_map: &mut LinkMap) -> Result<()> {
        for (i, code_ptr) in self.frame_pointers.iter_mut().enumerate() {
            let base = offset + 4 + i * 2;
            code_ptr.link(base, uuid_map)?;
        }
        Ok(())
    }
    fn unlink(&mut self, offset: usize, _size: usize, map: &HashMap<Uuid, usize>) -> Result<()> {
        for (i, code_ptr) in self.frame_pointers.iter_mut().enumerate() {
            let base = offset + 4 + i * 2;
            code_ptr.unlink_rel16(base, map)?;
        }
        Ok(())
    }
    fn as_value(&self, map: &mut Dict) -> Result<()> {
        let mut seq = List::default();
        for code_ptr in &self.frame_pointers {
            seq.push(code_ptr.as_value());
        }
        map.insert("frame_pointers", seq.into());
        Ok(())
    }
    fn display_content(&self, offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        let mut parts = vec![];
        for (i, tgt) in self.frame_pointers.iter().enumerate() {
            parts.push(format!("{}:{}", i, tgt.display_content(offset_map)?));
        }
        Ok(format!(
            "{}{}{}",
            ansi().fg(Self::COLOR),
            parts.join(", "),
            ansi()
        ))
    }
}

impl i40_JumpToFrame {
    pub fn from_value(map: &Dict) -> Result<(InstrDetail, usize)> {
        let mut frame_pointers = Vec::new();
        for fp in map.try_get("frame_pointers")?.to_list()?.iter() {
            frame_pointers.push(CodePtr::from_value(fp)?);
        }
        let size = 4 + frame_pointers.len() * 2;
        Ok((InstrDetail::JumpToFrame(Self { frame_pointers }), size))
    }
    pub fn frame_pointers(&self) -> impl Iterator<Item = &CodePtr> {
        self.frame_pointers.iter()
    }
    pub fn num_frames(&self) -> usize {
        self.frame_pointers.len()
    }
}

#[derive(Clone, Debug)]
pub struct i42_SourceName {
    source: String,
}

impl ShInstrSpec for i42_SourceName {
    const MAGIC: InstrMagic = InstrMagic::Word(0x42);
    const SIZE: Option<usize> = None;
    const NAME: &'static str = "SourceName";
    const SHORT_NAME: &'static str = "SrcNm";
    const COLOR: Color = Color::Yellow;
    const NOTES: &'static str = "The source for SH files was raw .asm files.";
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        let source = read_name(&idata[2..])?;
        let size = 2 + source.len() + 1;
        Ok((InstrDetail::SourceName(Self { source }), size))
    }
    fn to_bytes(&self, buf: &mut [u8]) -> Result<()> {
        buf[2..2 + self.source.len()].copy_from_slice(self.source.as_bytes());
        buf[buf.len() - 1] = 0;
        Ok(())
    }
    fn as_value(&self, map: &mut Dict) -> Result<()> {
        map.insert("source", self.source.clone().into());
        Ok(())
    }
    fn display_content(&self, _offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        let s = &self.source;
        Ok(format!("{}{}{}", ansi().fg(Self::COLOR), s, ansi()))
    }
}

impl i42_SourceName {
    pub fn from_value(map: &Dict) -> Result<(InstrDetail, usize)> {
        let src = Self {
            source: map.try_get("source")?.to_str()?.to_owned(),
        };
        let size = src.source.len() + 3;
        Ok((InstrDetail::SourceName(src), size))
    }
}

make_unk_instr!(Unk44, i44, 0x44, 4);
make_unk_instr!(Unk46, i46, 0x46, 2);
make_unk_instr!(Unk4E, i4E, 0x4E, 2);

#[derive(Clone, Debug)]
pub struct i48_Jump {
    code_ptr: CodePtr,
}

impl ShInstrSpec for i48_Jump {
    const MAGIC: InstrMagic = InstrMagic::Word(0x48);
    const SIZE: Option<usize> = Some(4);
    const NAME: &'static str = "Jump";
    const SHORT_NAME: &'static str = "Jump!";
    const COLOR: Color = Color::BrightBlue;
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        let code_ptr = CodePtr::new_rel16_bs(&idata[2..4]);
        Ok((InstrDetail::Jump(Self { code_ptr }), Self::SIZE.unwrap()))
    }
    fn to_bytes(&self, buf: &mut [u8]) -> Result<()> {
        buf[2..4].copy_from_slice(&self.code_ptr.as_rel16()?.to_le_bytes());
        Ok(())
    }
    fn link(&mut self, offset: usize, size: usize, uuid_map: &mut LinkMap) -> Result<()> {
        self.code_ptr.link(offset + size, uuid_map)
    }
    fn unlink(&mut self, offset: usize, size: usize, map: &HashMap<Uuid, usize>) -> Result<()> {
        self.code_ptr.unlink_rel16(offset + size, map)
    }
    fn as_value(&self, map: &mut Dict) -> Result<()> {
        map.insert("code_ptr", self.code_ptr.as_value());
        Ok(())
    }
    fn display_content(&self, offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        let ptr = self.code_ptr.display_content(offset_map)?;
        Ok(format!("{}tgt:{}{}", ansi().fg(Self::COLOR), ptr, ansi()))
    }
}

impl i48_Jump {
    pub fn from_value(map: &Dict) -> Result<(InstrDetail, usize)> {
        Ok((
            InstrDetail::Jump(Self {
                code_ptr: CodePtr::from_value(map.try_get("code_ptr")?)?,
            }),
            Self::SIZE.unwrap(),
        ))
    }
    pub fn code_ptr(&self) -> &CodePtr {
        &self.code_ptr
    }
}

make_unk_instr!(Unk50, i50, 0x50, 6);
make_unk_instr!(Unk66, i66, 0x66, 10);
make_unk_instr!(Unk68, i68, 0x68, 8);

pub struct i6C;

impl ShInstrSpec for i6C {
    const MAGIC: InstrMagic = InstrMagic::Word(0x6C);
    const SIZE: Option<usize> = None;
    const NAME: &'static str = "Unk6C";
    const SHORT_NAME: &'static str = "  i6C";
    const NOTES: &'static str = "There is probably something going on here; the size seems to \
        depend on a byte in the middle, including a couple of values that are only used in one SH";
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        let flag = idata[10];
        let size = match flag {
            0x38 => 13, // Normal
            0x48 => 14, // F18 -- one of our errata?
            0x50 => 16, // F8
            _ => bail!("unexpected flag byte in 6C instruction: {:02X}", flag),
        };
        Ok((InstrDetail::NoDetail(idata[..size].into()), size))
    }
}

#[derive(Clone, Debug)]
pub struct i6E_UnmaskLong {
    code_ptr: CodePtr,
    usage: VertBufUsage,
}

impl ShInstrSpec for i6E_UnmaskLong {
    const MAGIC: InstrMagic = InstrMagic::Word(0x6E);
    const SIZE: Option<usize> = Some(6);
    const NAME: &'static str = "UnmaskLong";
    const SHORT_NAME: &'static str = "UMsk4";
    const COLOR: Color = Color::Blue;
    const NOTES: &'static str = "\
        When points to a VertexBuf, it \"unmasks\" the facets that occur after that vertex buffer up \
        to the next vertex buffer or Header. When it points elsewhere, :shrug:. It usually points \
        to something that jumps, I think to do manual backface culling, maybe? Doesn't seem important. \
        This version is the 'long' version that encodes a 4 byte relative offset instead of 2 byte.\
    ";
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        Ok((
            InstrDetail::UnmaskLong(Self {
                code_ptr: CodePtr::new_rel32_bs(&idata[2..6]),
                usage: VertBufUsage::default(),
            }),
            Self::SIZE.unwrap(),
        ))
    }
    fn to_bytes(&self, buf: &mut [u8]) -> Result<()> {
        buf[2..6].copy_from_slice(&self.code_ptr.as_rel32()?.to_le_bytes());
        Ok(())
    }
    fn link(&mut self, offset: usize, size: usize, uuid_map: &mut LinkMap) -> Result<()> {
        self.code_ptr.link(offset + size, uuid_map)
    }
    fn unlink(&mut self, offset: usize, size: usize, map: &HashMap<Uuid, usize>) -> Result<()> {
        self.code_ptr.unlink_rel32(offset + size, map)
    }
    fn as_value(&self, map: &mut Dict) -> Result<()> {
        map.insert("code_ptr", self.code_ptr.as_value());
        map.insert("usage", self.usage.to_value());
        Ok(())
    }
    fn display_content(&self, offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        Ok(format!(
            "{}tgt:{}, usage:{:?}{}",
            ansi().fg(Self::COLOR),
            self.code_ptr.display_content(offset_map)?,
            self.usage,
            ansi()
        ))
    }
}

impl i6E_UnmaskLong {
    pub fn from_value(map: &Dict) -> Result<(InstrDetail, usize)> {
        Ok((
            InstrDetail::UnmaskLong(Self {
                code_ptr: CodePtr::from_value(map.try_get("code_ptr")?)?,
                usage: VertBufUsage::from_value(map.try_get("usage")?)?,
            }),
            Self::SIZE.unwrap(),
        ))
    }
    pub fn code_ptr(&self) -> &CodePtr {
        &self.code_ptr
    }
    pub fn set_usage(&mut self, usage: VertBufUsage) {
        debug_assert_ne!(usage, VertBufUsage::default(), "setting default usage");
        assert_eq!(self.usage, VertBufUsage::default(), "usage already set");
        self.usage = usage;
    }
}

make_unk_instr!(Unk72, i72, 0x72, 4);
make_unk_instr!(Unk76, i76, 0x76, 10);
make_unk_instr!(Unk78, i78, 0x78, 12);
make_unk_instr!(Unk7A, i7A, 0x7A, 10);

#[derive(Clone, Debug)]
pub struct i82_VertexBuffer {
    push_at_byte: usize,
    verts: Vec<[i16; 3]>,
    usage: VertBufUsage,
    xform_id: XformId,
}

impl ShInstrSpec for i82_VertexBuffer {
    const MAGIC: InstrMagic = InstrMagic::Word(0x82);
    const SIZE: Option<usize> = None;
    const NAME: &'static str = "VertexBuffer";
    const SHORT_NAME: &'static str = "VxBuf";
    const COLOR: Color = Color::Magenta;
    const NOTES: &'static str = "This is not the full current vertex context, we need to push \
        these vertices on the current vertex buffer, at the given truncation point.";
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        ensure!(idata.len() >= 6, "code too short for buffer header");
        let nverts = LittleEndian::read_u16(&idata[2..4]) as usize;
        let push_at_byte = LittleEndian::read_u16(&idata[4..6]) as usize;
        ensure!(
            push_at_byte % 8 == 0,
            "vert buffer target offset must be a multiple of 8"
        );
        let buf_end = 6 + nverts * 2 * 3;
        ensure!(idata.len() >= buf_end, "code too short for buffer verts");
        let words = Ref::<&[u8], [I16<LE>]>::new_slice_unaligned(&idata[6..buf_end])
            .ok_or_else(|| anyhow!("failed to read vertex buffer"))?;
        let mut verts = Vec::with_capacity(nverts);
        for i in 0..nverts {
            let x = words[i * 3].get();
            let y = words[i * 3 + 1].get();
            let z = words[i * 3 + 2].get();
            verts.push([x, y, z]);
        }
        Ok((
            InstrDetail::VertexBuffer(Self {
                push_at_byte,
                verts,
                usage: VertBufUsage::default(),
                xform_id: XformId::default(),
            }),
            buf_end,
        ))
    }
    fn to_bytes(&self, buf: &mut [u8]) -> Result<()> {
        let nverts: u16 = self.verts.len().try_into()?;
        buf[2..4].copy_from_slice(&nverts.to_le_bytes());
        let push_off: u16 = self.push_at_byte.try_into()?;
        buf[4..6].copy_from_slice(&push_off.to_le_bytes());
        let mut off = 6;
        for vert in &self.verts {
            for &v in vert {
                buf[off..off + 2].copy_from_slice(&v.to_le_bytes());
                off += 2;
            }
        }
        // Note: usage is controlled by x86 fragments elsewhere
        ensure!(off == buf.len());
        Ok(())
    }
    fn as_value(&self, map: &mut Dict) -> Result<()> {
        map.insert("usage", self.usage.to_value());
        map.insert("push_at_byte", self.push_at_byte.into());
        let mut seq = List::default();
        for vert in &self.verts {
            let mut vseq = List::default();
            for &v in vert {
                vseq.push(v.into());
            }
            seq.push(vseq.into());
        }
        map.insert("verts", seq.into());
        Ok(())
    }
    fn display_content(&self, _offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        Ok(format!(
            "{}count:{}, offset:{}b, usage:{:?}, xform:{:?}{}",
            ansi().fg(Self::COLOR),
            self.verts.len(),
            self.push_at_byte,
            self.usage,
            self.xform_id,
            ansi()
        ))
    }
}

impl i82_VertexBuffer {
    pub fn from_value(map: &Dict) -> Result<(InstrDetail, usize)> {
        let usage = VertBufUsage::from_value(map.try_get("usage")?)?;
        let push_at_byte = map.try_get("push_at_byte")?.to_int()?.try_into()?;
        let mut verts = Vec::new();
        for v in map.try_get("verts")?.to_list()?.iter() {
            verts.push(v.to_list()?.to_array()?);
        }
        let size = 6 + verts.len() * 2 * 3;
        Ok((
            InstrDetail::VertexBuffer(Self {
                push_at_byte,
                verts,
                usage,
                xform_id: XformId::default(),
            }),
            size,
        ))
    }
    pub fn vertices(&self) -> impl Iterator<Item = &[i16; 3]> {
        self.verts.iter()
    }
    pub fn vertex(&self, offset: usize) -> &[i16; 3] {
        &self.verts[offset]
    }
    pub fn vertex_mut(&mut self, offset: usize) -> &mut [i16; 3] {
        &mut self.verts[offset]
    }
    pub fn target_offset(&self) -> usize {
        // 3, 2-byte coords per vertex should be 6 bytes, but the
        // vertex slots are word aligned
        self.push_at_byte / 8
    }
    pub fn pool_range(&self) -> Range<usize> {
        self.target_offset()..self.target_offset() + self.verts.len()
    }
    pub fn set_usage(&mut self, usage: VertBufUsage) {
        debug_assert_ne!(usage, VertBufUsage::default(), "setting default usage");
        assert_eq!(self.usage, VertBufUsage::default(), "usage already set");
        self.usage = usage;
    }
    pub fn usage(&self) -> &VertBufUsage {
        &self.usage
    }
    pub fn set_xform(&mut self, xform_id: XformId) {
        self.xform_id = xform_id;
    }
    pub fn xform_id(&self) -> XformId {
        self.xform_id
    }
}

make_unk_instr!(Unk96, i96, 0x96, 6);

#[derive(Clone, Debug)]
pub struct iA6_JumpToDetail {
    code_ptr: CodePtr,
    level: u16,
}

impl ShInstrSpec for iA6_JumpToDetail {
    const MAGIC: InstrMagic = InstrMagic::Word(0xA6);
    const SIZE: Option<usize> = Some(6);
    const NAME: &'static str = "JumpToDetail";
    const SHORT_NAME: &'static str = "ToDtl";
    const COLOR: Color = Color::BrightBlue;
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        Ok((
            InstrDetail::JumpToDetail(Self {
                code_ptr: CodePtr::new_rel16_bs(&idata[2..4]),
                level: LittleEndian::read_u16(&idata[4..6]),
            }),
            Self::SIZE.unwrap(),
        ))
    }
    fn to_bytes(&self, buf: &mut [u8]) -> Result<()> {
        buf[2..4].copy_from_slice(&self.code_ptr.as_rel16()?.to_le_bytes());
        buf[4..6].copy_from_slice(&self.level.to_le_bytes());
        Ok(())
    }
    fn link(&mut self, offset: usize, size: usize, uuid_map: &mut LinkMap) -> Result<()> {
        self.code_ptr.link(offset + size, uuid_map)
    }
    fn unlink(&mut self, offset: usize, size: usize, map: &HashMap<Uuid, usize>) -> Result<()> {
        self.code_ptr.unlink_rel16(offset + size, map)
    }
    fn as_value(&self, map: &mut Dict) -> Result<()> {
        map.insert("code_ptr", self.code_ptr.as_value());
        map.insert("level", self.level.into());
        Ok(())
    }
    fn display_content(&self, offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        Ok(format!(
            "{}tgt:{}, level:{}{}",
            ansi().fg(Self::COLOR),
            self.code_ptr.display_content(offset_map)?,
            self.level,
            ansi()
        ))
    }
}

impl iA6_JumpToDetail {
    pub fn from_value(map: &Dict) -> Result<(InstrDetail, usize)> {
        Ok((
            InstrDetail::JumpToDetail(Self {
                code_ptr: CodePtr::from_value(map.try_get("code_ptr")?)?,
                level: map.try_get("level")?.to_int()?.try_into()?,
            }),
            Self::SIZE.unwrap(),
        ))
    }
    pub fn code_ptr(&self) -> &CodePtr {
        &self.code_ptr
    }
}

#[derive(Clone, Debug)]
pub struct iAC_JumpToDamage {
    code_ptr: CodePtr,
}

impl ShInstrSpec for iAC_JumpToDamage {
    const MAGIC: InstrMagic = InstrMagic::Word(0xAC);
    const SIZE: Option<usize> = Some(4);
    const NAME: &'static str = "JumpToDamage";
    const SHORT_NAME: &'static str = "ToDam";
    const COLOR: Color = Color::BrightBlue;
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        Ok((
            InstrDetail::JumpToDamage(Self {
                code_ptr: CodePtr::new_rel16_bs(&idata[2..4]),
            }),
            Self::SIZE.unwrap(),
        ))
    }
    fn to_bytes(&self, buf: &mut [u8]) -> Result<()> {
        buf[2..4].copy_from_slice(&self.code_ptr.as_rel16()?.to_le_bytes());
        Ok(())
    }
    fn link(&mut self, offset: usize, size: usize, uuid_map: &mut LinkMap) -> Result<()> {
        self.code_ptr.link(offset + size, uuid_map)
    }
    fn unlink(&mut self, offset: usize, size: usize, map: &HashMap<Uuid, usize>) -> Result<()> {
        self.code_ptr.unlink_rel16(offset + size, map)
    }
    fn as_value(&self, map: &mut Dict) -> Result<()> {
        map.insert("code_ptr", self.code_ptr.as_value());
        Ok(())
    }
    fn display_content(&self, offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        let s = self.code_ptr.display_content(offset_map)?;
        Ok(format!("{}tgt:{}{}", ansi().fg(Self::COLOR), s, ansi()))
    }
}

impl iAC_JumpToDamage {
    pub fn from_value(map: &Dict) -> Result<(InstrDetail, usize)> {
        Ok((
            InstrDetail::JumpToDamage(Self {
                code_ptr: CodePtr::from_value(map.try_get("code_ptr")?)?,
            }),
            Self::SIZE.unwrap(),
        ))
    }
    pub fn code_ptr(&self) -> &CodePtr {
        &self.code_ptr
    }
}

make_unk_instr!(UnkB2, iB2, 0xB2, 2);
make_unk_instr!(UnkB8, iB8, 0xB8, 4);

pub struct iBC;

impl ShInstrSpec for iBC {
    const MAGIC: InstrMagic = InstrMagic::Byte(0xBC);
    const SIZE: Option<usize> = Some(2);
    const NAME: &'static str = "UnkBC";
    const SHORT_NAME: &'static str = "  iBC";
}

#[derive(Clone, Debug)]
pub struct iC8_JumpToLOD {
    unk01: u16,
    unk02: u16,
    code_ptr: CodePtr,
}

impl ShInstrSpec for iC8_JumpToLOD {
    const MAGIC: InstrMagic = InstrMagic::Word(0xC8);
    const SIZE: Option<usize> = Some(8);
    const NAME: &'static str = "JumpToLOD";
    const SHORT_NAME: &'static str = "ToLOD";
    const COLOR: Color = Color::BrightBlue;
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        Ok((
            InstrDetail::JumpToLOD(Self {
                unk01: LittleEndian::read_u16(&idata[2..4]),
                unk02: LittleEndian::read_u16(&idata[4..6]),
                code_ptr: CodePtr::new_rel16_bs(&idata[6..8]),
            }),
            Self::SIZE.unwrap(),
        ))
    }
    fn to_bytes(&self, buf: &mut [u8]) -> Result<()> {
        buf[2..4].copy_from_slice(&self.unk01.to_le_bytes());
        buf[4..6].copy_from_slice(&self.unk02.to_le_bytes());
        buf[6..8].copy_from_slice(&self.code_ptr.as_rel16()?.to_le_bytes());
        Ok(())
    }
    fn link(&mut self, offset: usize, size: usize, uuid_map: &mut LinkMap) -> Result<()> {
        self.code_ptr.link(offset + size, uuid_map)
    }
    fn unlink(&mut self, offset: usize, size: usize, map: &HashMap<Uuid, usize>) -> Result<()> {
        self.code_ptr.unlink_rel16(offset + size, map)
    }
    fn as_value(&self, map: &mut Dict) -> Result<()> {
        map.insert("unk01", self.unk01.into());
        map.insert("unk02", self.unk02.into());
        map.insert("code_ptr", self.code_ptr.as_value());
        Ok(())
    }
    fn display_content(&self, offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        Ok(format!(
            "{}tgt:{}, unk0:{}, unk1:{}{}",
            ansi().fg(Self::COLOR),
            self.code_ptr.display_content(offset_map)?,
            self.unk01,
            self.unk02,
            ansi()
        ))
    }
}

impl iC8_JumpToLOD {
    pub fn from_value(map: &Dict) -> Result<(InstrDetail, usize)> {
        Ok((
            InstrDetail::JumpToLOD(Self {
                unk01: map.try_get("unk01")?.to_int()?.try_into()?,
                unk02: map.try_get("unk02")?.to_int()?.try_into()?,
                code_ptr: CodePtr::from_value(map.try_get("code_ptr")?)?,
            }),
            Self::SIZE.unwrap(),
        ))
    }
    pub fn code_ptr(&self) -> &CodePtr {
        &self.code_ptr
    }
}

#[derive(Clone, Debug)]
pub struct iC4_XformUnmask {
    transform: [i16; 3],
    rotation: [i16; 3],
    code_ptr: CodePtr,
    usage: VertBufUsage,
}

#[derive(AsBytes, FromBytes, FromZeroes, Unaligned)]
#[repr(packed)]
struct iC4_XformUnmask_Layout {
    transform: [I16<LE>; 3],
    rotation: [I16<LE>; 3],
    code_ptr: i16,
}

impl ShInstrSpec for iC4_XformUnmask {
    const MAGIC: InstrMagic = InstrMagic::Word(0xC4);
    const SIZE: Option<usize> = Some(16);
    const NAME: &'static str = "XformUnmask";
    const SHORT_NAME: &'static str = "Xform";
    const COLOR: Color = Color::BrightBlue;
    const NOTES: &'static str = "Does the same as Unmask, but also includes a transform to apply.";

    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        let layout = Ref::<&[u8], iC4_XformUnmask_Layout>::new(&idata[2..16])
            .ok_or_else(|| anyhow!("failed to layout 0xC4"))?;
        Ok((
            InstrDetail::XformUnmask(Self {
                transform: layout.transform.map(|v| v.get()),
                rotation: layout.rotation.map(|v| v.get()),
                code_ptr: CodePtr::new_rel16(layout.code_ptr),
                usage: VertBufUsage::default(),
            }),
            Self::SIZE.unwrap(),
        ))
    }
    fn to_bytes(&self, buf: &mut [u8]) -> Result<()> {
        let layout = iC4_XformUnmask_Layout {
            transform: self.transform.map(|v| v.into()),
            rotation: self.rotation.map(|v| v.into()),
            code_ptr: self.code_ptr.as_rel16()?,
        };
        buf[2..].copy_from_slice(layout.as_bytes());
        Ok(())
    }
    fn link(&mut self, offset: usize, size: usize, uuid_map: &mut LinkMap) -> Result<()> {
        self.code_ptr.link(offset + size, uuid_map)
    }
    fn unlink(&mut self, offset: usize, size: usize, map: &HashMap<Uuid, usize>) -> Result<()> {
        self.code_ptr.unlink_rel16(offset + size, map)
    }
    fn as_value(&self, map: &mut Dict) -> Result<()> {
        map.insert("transform", List::from_slice(&self.transform).into());
        map.insert("rotation", List::from_slice(&self.rotation).into());
        map.insert("code_ptr", self.code_ptr.as_value());
        map.insert("usage", self.usage.to_value());
        Ok(())
    }
    fn display_content(&self, offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        Ok(format!(
            "{}tgt:{}, usage:{:?}, xform:{:?}, rot:{:?}{}",
            ansi().fg(Self::COLOR),
            self.code_ptr.display_content(offset_map)?,
            self.usage,
            self.transform,
            self.rotation,
            ansi()
        ))
    }
}

impl iC4_XformUnmask {
    pub fn from_value(map: &Dict) -> Result<(InstrDetail, usize)> {
        Ok((
            InstrDetail::XformUnmask(Self {
                transform: map.try_get("transform")?.to_list()?.to_array()?,
                rotation: map.try_get("rotation")?.to_list()?.to_array()?,
                code_ptr: CodePtr::from_value(map.try_get("code_ptr")?)?,
                usage: VertBufUsage::from_value(map.try_get("usage")?)?,
            }),
            Self::SIZE.unwrap(),
        ))
    }
    pub fn code_ptr(&self) -> &CodePtr {
        &self.code_ptr
    }
    pub fn set_usage(&mut self, usage: VertBufUsage) {
        debug_assert_ne!(usage, VertBufUsage::default(), "setting default usage");
        assert_eq!(self.usage, VertBufUsage::default(), "usage already set");
        self.usage = usage;
    }
    pub fn base_xform(&self) -> [i16; 6] {
        [
            self.transform[0],
            self.transform[1],
            self.transform[2],
            self.rotation[0],
            self.rotation[1],
            self.rotation[2],
        ]
    }
}

#[derive(Clone, Debug)]
pub struct iC6_XformUnmaskLong {
    transform: [i16; 3],
    rotation: [i16; 3],
    code_ptr: CodePtr,
    usage: VertBufUsage,
}

#[derive(AsBytes, FromBytes, FromZeroes, Unaligned)]
#[repr(packed)]
struct iC6_XformUnmaskLong_Layout {
    transform: [I16<LE>; 3],
    rotation: [I16<LE>; 3],
    code_ptr: i32,
}

impl ShInstrSpec for iC6_XformUnmaskLong {
    const MAGIC: InstrMagic = InstrMagic::Word(0xC6);
    const SIZE: Option<usize> = Some(18);
    const NAME: &'static str = "XformUnmaskLong";
    const SHORT_NAME: &'static str = "Xfrm4";
    const COLOR: Color = Color::BrightBlue;
    const NOTES: &'static str = "The same as XformUnmask, but has a 4byte code pointer.";
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        let layout = Ref::<&[u8], iC6_XformUnmaskLong_Layout>::new(&idata[2..18])
            .ok_or_else(|| anyhow!("failed to layout 0xC6"))?;
        Ok((
            InstrDetail::XformUnmaskLong(Self {
                transform: layout.transform.map(|v| v.get()),
                rotation: layout.rotation.map(|v| v.get()),
                code_ptr: CodePtr::new_rel32(layout.code_ptr),
                usage: VertBufUsage::default(),
            }),
            Self::SIZE.unwrap(),
        ))
    }
    fn to_bytes(&self, buf: &mut [u8]) -> Result<()> {
        let layout = iC6_XformUnmaskLong_Layout {
            transform: self.transform.map(|v| v.into()),
            rotation: self.rotation.map(|v| v.into()),
            code_ptr: self.code_ptr.as_rel32()?,
        };
        buf[2..].copy_from_slice(layout.as_bytes());
        Ok(())
    }
    fn link(&mut self, offset: usize, size: usize, uuid_map: &mut LinkMap) -> Result<()> {
        self.code_ptr.link(offset + size, uuid_map)
    }
    fn unlink(&mut self, offset: usize, size: usize, map: &HashMap<Uuid, usize>) -> Result<()> {
        self.code_ptr.unlink_rel32(offset + size, map)
    }
    fn as_value(&self, map: &mut Dict) -> Result<()> {
        map.insert("transform", List::from_slice(&self.transform).into());
        map.insert("rotation", List::from_slice(&self.rotation).into());
        map.insert("code_ptr", self.code_ptr.as_value());
        map.insert("usage", self.usage.to_value());
        Ok(())
    }
    fn display_content(&self, offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        Ok(format!(
            "{}tgt:{}, usage:{:?}, xform:{:?}, rot:{:?}{}",
            ansi().fg(Self::COLOR),
            self.code_ptr.display_content(offset_map)?,
            self.usage,
            self.transform,
            self.rotation,
            ansi()
        ))
    }
}

impl iC6_XformUnmaskLong {
    pub fn from_value(map: &Dict) -> Result<(InstrDetail, usize)> {
        Ok((
            InstrDetail::XformUnmaskLong(Self {
                transform: map.try_get("transform")?.to_list()?.to_array()?,
                rotation: map.try_get("rotation")?.to_list()?.to_array()?,
                code_ptr: CodePtr::from_value(map.try_get("code_ptr")?)?,
                usage: VertBufUsage::from_value(map.try_get("usage")?)?,
            }),
            Self::SIZE.unwrap(),
        ))
    }
    pub fn code_ptr(&self) -> &CodePtr {
        &self.code_ptr
    }
    pub fn set_usage(&mut self, usage: VertBufUsage) {
        debug_assert_ne!(usage, VertBufUsage::default(), "setting default usage");
        assert_eq!(self.usage, VertBufUsage::default(), "usage already set");
        self.usage = usage;
    }
    pub fn base_xform(&self) -> [i16; 6] {
        [
            self.transform[0],
            self.transform[1],
            self.transform[2],
            self.rotation[0],
            self.rotation[1],
            self.rotation[2],
        ]
    }
}

make_unk_instr!(UnkCA, iCA, 0xCA, 4);
make_unk_instr!(UnkCE, iCE, 0xCE, 40);
make_unk_instr!(UnkD0, iD0, 0xD0, 4);
make_unk_instr!(UnkD2, iD2, 0xD2, 8);
make_unk_instr!(UnkDA, iDA, 0xDA, 4);
make_unk_instr!(UnkDC, iDC, 0xDC, 12);

#[derive(Clone, Debug)]
pub struct iE0_TextureIndex {
    index: u16,
}

impl ShInstrSpec for iE0_TextureIndex {
    const MAGIC: InstrMagic = InstrMagic::Word(0xE0);
    const SIZE: Option<usize> = Some(4);
    const NAME: &'static str = "TextureIndex";
    const SHORT_NAME: &'static str = "TxtId";
    const COLOR: Color = Color::Yellow;
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        let index = LittleEndian::read_u16(&idata[2..4]);
        Ok((
            InstrDetail::TextureIndex(Self { index }),
            Self::SIZE.unwrap(),
        ))
    }
    fn to_bytes(&self, buf: &mut [u8]) -> Result<()> {
        buf[2..4].copy_from_slice(&self.index.to_le_bytes());
        Ok(())
    }
    fn as_value(&self, map: &mut Dict) -> Result<()> {
        map.insert("index", self.index.into());
        Ok(())
    }
    fn display_content(&self, _offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        Ok(format!(
            "{}index:{}{}",
            ansi().fg(Self::COLOR),
            self.index,
            ansi()
        ))
    }
}

impl iE0_TextureIndex {
    pub fn from_value(map: &Dict) -> Result<(InstrDetail, usize)> {
        Ok((
            InstrDetail::TextureIndex(Self {
                index: map.try_get("index")?.to_int()?.try_into()?,
            }),
            Self::SIZE.unwrap(),
        ))
    }
}

#[derive(Clone, Debug)]
pub struct iE2_TextureFile {
    filename: String,
}

impl ShInstrSpec for iE2_TextureFile {
    const MAGIC: InstrMagic = InstrMagic::Word(0xE2);
    const SIZE: Option<usize> = Some(16);
    const NAME: &'static str = "TextureFile";
    const SHORT_NAME: &'static str = "Txtre";
    const COLOR: Color = Color::Yellow;
    const NOTES: &'static str =
        "Drawing operations after this instruction use the given texture file";
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        let filename = read_name(&idata[2..])?;
        Ok((
            InstrDetail::TextureFile(Self { filename }),
            Self::SIZE.unwrap(),
        ))
    }
    fn to_bytes(&self, buf: &mut [u8]) -> Result<()> {
        buf[2..2 + self.filename.len()].copy_from_slice(self.filename.as_bytes());
        Ok(())
    }
    fn as_value(&self, map: &mut Dict) -> Result<()> {
        map.insert("filename", self.filename.clone().into());
        Ok(())
    }
    fn display_content(&self, _offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        let s = &self.filename;
        Ok(format!("{}{}{}", ansi().fg(Self::COLOR), s, ansi()))
    }
}

impl iE2_TextureFile {
    pub fn from_value(map: &Dict) -> Result<(InstrDetail, usize)> {
        Ok((
            InstrDetail::TextureFile(Self {
                filename: map.try_get("filename")?.to_str()?.to_owned(),
            }),
            Self::SIZE.unwrap(),
        ))
    }
    pub fn filename(&self) -> &str {
        &self.filename
    }
}

make_unk_instr!(UnkE4, iE4, 0xE4, 20);
make_unk_instr!(UnkE6, iE6, 0xE6, 10);
make_unk_instr!(UnkE8, iE8, 0xE8, 6);
make_unk_instr!(UnkEA, iEA, 0xEA, 8);
make_unk_instr!(UnkEE, iEE, 0xEE, 2);

// pub struct iF0_X86Code { /* F0 split out for clarity: sh_x86.rs for implementation! */ }

#[derive(Clone, Debug)]
pub struct iF2_PtrToObjEnd {
    pub(crate) code_ptr: CodePtr,
}

impl ShInstrSpec for iF2_PtrToObjEnd {
    const MAGIC: InstrMagic = InstrMagic::Word(0xF2);
    const SIZE: Option<usize> = Some(4);
    const NAME: &'static str = "PointerToObjectEnd";
    const SHORT_NAME: &'static str = "2EndO";
    const COLOR: Color = Color::BrightBlue;
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        Ok((
            InstrDetail::PtrToObjEnd(Self {
                code_ptr: CodePtr::new_off16(&idata[2..4]),
            }),
            Self::SIZE.unwrap(),
        ))
    }
    fn to_bytes(&self, buf: &mut [u8]) -> Result<()> {
        buf[2..4].copy_from_slice(&self.code_ptr.as_off16()?.to_le_bytes());
        Ok(())
    }
    fn link(&mut self, offset: usize, size: usize, uuid_map: &mut LinkMap) -> Result<()> {
        self.code_ptr.link(offset + size, uuid_map)
    }
    fn unlink(&mut self, offset: usize, size: usize, map: &HashMap<Uuid, usize>) -> Result<()> {
        self.code_ptr.unlink_off16(offset + size, map)
    }
    fn as_value(&self, map: &mut Dict) -> Result<()> {
        map.insert("code_ptr", self.code_ptr.as_value());
        Ok(())
    }
    fn display_content(&self, offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        let ptr = self.code_ptr.display_content(offset_map)?;
        Ok(format!("{}tgt:{}{}", ansi().fg(Self::COLOR), ptr, ansi()))
    }
}

impl iF2_PtrToObjEnd {
    pub fn from_value(map: &Dict) -> Result<(InstrDetail, usize)> {
        Ok((
            InstrDetail::PtrToObjEnd(Self {
                code_ptr: CodePtr::from_value(map.try_get("code_ptr")?)?,
            }),
            Self::SIZE.unwrap(),
        ))
    }
    pub fn code_ptr(&self) -> &CodePtr {
        &self.code_ptr
    }
}

#[derive(Clone, Copy, Debug, AsBytes, FromBytes, FromZeroes, Unaligned)]
#[repr(packed)]
pub struct iF6_VertexInfo {
    vertex_buffer_index: U16<LE>,
    color: u8,
    normal: [i8; 3],
}

impl ShInstrSpec for iF6_VertexInfo {
    const MAGIC: InstrMagic = InstrMagic::Byte(0xF6);
    const SIZE: Option<usize> = Some(7);
    const NAME: &'static str = "VertexInfo";
    const SHORT_NAME: &'static str = "VxNfo";
    const COLOR: Color = Color::Blue;
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        let raw = Ref::<&[u8], iF6_VertexInfo>::new_unaligned(&idata[1..7])
            .map(|v| v.into_ref())
            .ok_or_else(|| anyhow!("failed to overlay VertexInfo"))?;
        Ok((InstrDetail::VertexInfo(*raw), Self::SIZE.unwrap()))
    }
    fn to_bytes(&self, buf: &mut [u8]) -> Result<()> {
        buf[1..Self::SIZE.unwrap()].copy_from_slice(self.as_bytes());
        Ok(())
    }
    fn as_value(&self, map: &mut Dict) -> Result<()> {
        map.insert("vertex_buffer_index", self.vertex_buffer_index.get().into());
        map.insert("color", self.color.into());
        map.insert("normal", List::from_slice(&self.normal).into());
        Ok(())
    }
    fn display_content(&self, _offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        Ok(format!(
            "{}idx:{}, color:{}, normal:{:?}{}",
            ansi().fg(Self::COLOR),
            self.vertex_buffer_index,
            self.color,
            self.normal,
            ansi()
        ))
    }
}

impl iF6_VertexInfo {
    pub fn from_value(map: &Dict) -> Result<(InstrDetail, usize)> {
        let vertex_buffer_index: u16 = map.try_get("vertex_buffer_index")?.to_int()?.try_into()?;
        Ok((
            InstrDetail::VertexInfo(Self {
                vertex_buffer_index: vertex_buffer_index.into(),
                color: map.try_get("color")?.to_int()?.try_into()?,
                normal: map.try_get("normal")?.to_list()?.to_array()?,
            }),
            Self::SIZE.unwrap(),
        ))
    }
    pub fn index(&self) -> usize {
        self.vertex_buffer_index.get() as usize
    }
    pub fn normal(&self) -> &[i8; 3] {
        &self.normal
    }
}

bitflags! {
    #[derive(Clone, Copy, Debug, Eq, PartialEq)]
    pub struct FaceContentFlags : u8 {
        const UNK1                 = 0b1000_0000;
        const HAVE_FACE_NORMAL     = 0b0100_0000;

        // Turning this on makes the texture appear brighter, but still receives light.
        const UNK2                 = 0b0010_0000; // brighten

        // This flag is _not_ set on the apron of RUNWAY.SH and those polygons have
        // a wonky texture mapping effect. Whereas RNWY4.SH has is flag set and it is
        // projecting the texture normally.
        const UNK3                 = 0b0001_0000; // use_perspective_correct_mapping

        const UNK4                 = 0b0000_1000;

        const HAVE_TEXCOORDS       = 0b0000_0100;
        const FILL_BACKGROUND      = 0b0000_0010;

        const UNK5                 = 0b0000_0001;
    }
}
impl FaceContentFlags {
    pub fn as_value(&self) -> Dict {
        let mut map = Dict::default();
        map.insert("unk1", self.contains(Self::UNK1).into());
        map.insert("unk2", self.contains(Self::UNK2).into());
        map.insert("unk3", self.contains(Self::UNK3).into());
        map.insert("unk4", self.contains(Self::UNK4).into());
        map.insert("unk5", self.contains(Self::UNK5).into());
        map.insert(
            "fill_background",
            self.contains(Self::FILL_BACKGROUND).into(),
        );
        map
    }
    pub fn from_value(
        map: &Dict,
        face_normal: &Option<[i8; 3]>,
        tex_coords: &[(u16, u16)],
    ) -> Result<Self> {
        let mut flags = FaceContentFlags::empty();
        if map.try_get("unk1")?.to_bool()? {
            flags.insert(FaceContentFlags::UNK1);
        }
        if map.try_get("unk2")?.to_bool()? {
            flags.insert(FaceContentFlags::UNK2);
        }
        if map.try_get("unk3")?.to_bool()? {
            flags.insert(FaceContentFlags::UNK3);
        }
        if map.try_get("unk4")?.to_bool()? {
            flags.insert(FaceContentFlags::UNK4);
        }
        if map.try_get("unk5")?.to_bool()? {
            flags.insert(FaceContentFlags::UNK5);
        }
        if map.try_get("fill_background")?.to_bool()? {
            flags.insert(FaceContentFlags::FILL_BACKGROUND);
        }
        if face_normal.is_some() {
            flags.insert(FaceContentFlags::HAVE_FACE_NORMAL);
        }
        if !tex_coords.is_empty() {
            flags.insert(FaceContentFlags::HAVE_TEXCOORDS);
        }
        Ok(flags)
    }
}

impl Display for FaceContentFlags {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

bitflags! {
    #[derive(Clone, Copy, Debug, Eq, PartialEq)]
    pub struct FaceLayoutFlags : u8 {
        const UNK0                 = 0b0000_1000;
        const USE_SHORT_INDICES    = 0b0000_0100;
        const USE_BYTE_FACE_CENTER = 0b0000_0010;
        const USE_BYTE_TEXCOORDS   = 0b0000_0001;
    }
}
impl FaceLayoutFlags {
    pub fn as_value(&self) -> Dict {
        let mut map = Dict::default();
        map.insert("unk0", self.contains(Self::UNK0).into());
        // Note: 0x02 is set to indicate byte "face center", but also in other
        // cases when there is no face center because the content flags doesn't
        // have a face normal. There is something weird here.
        map.insert(
            "unk_use_byte_face_center",
            self.contains(Self::USE_BYTE_FACE_CENTER).into(),
        );
        map
    }
    pub fn from_value(
        map: &Dict,
        indices: &[u16],
        face_center: &Option<[i16; 3]>,
        tex_coords: &[(u16, u16)],
    ) -> Result<Self> {
        let mut flags = FaceLayoutFlags::empty();
        if map.try_get("unk0")?.to_bool()? {
            flags.insert(Self::UNK0);
        }
        if indices.iter().any(|&v| v > u8::MAX.into()) {
            flags.insert(Self::USE_SHORT_INDICES);
        }
        if map.try_get("unk_use_byte_face_center")?.to_bool()? {
            flags.insert(Self::USE_BYTE_FACE_CENTER);
            if let Some(face_center) = face_center {
                ensure!(face_center
                    .iter()
                    .all(|&v| v <= i8::MAX as i16 && v >= i8::MIN as i16));
            }
        }
        if !tex_coords.is_empty()
            && tex_coords
                .iter()
                .all(|&(s, t)| s <= u8::MAX.into() && t <= u8::MAX.into())
        {
            flags.insert(Self::USE_BYTE_TEXCOORDS);
        }
        Ok(flags)
    }
}

#[derive(Clone, Debug)]
pub struct iFC_Face {
    content_flags: FaceContentFlags,
    layout_flags: FaceLayoutFlags,
    color: u8,
    is_shadow: bool,
    face_normal: Option<[i8; 3]>,
    face_unk_residual: Option<[i8; 3]>,
    face_center: Option<[i16; 3]>,
    indices: Vec<u16>,
    tex_coords: Vec<(u16, u16)>,
    usage: FaceUsage,
}

impl ShInstrSpec for iFC_Face {
    const MAGIC: InstrMagic = InstrMagic::Byte(0xFC);
    const SIZE: Option<usize> = None;
    const NAME: &'static str = "Face";
    const SHORT_NAME: &'static str = " Face";
    const COLOR: Color = Color::Cyan;
    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        ensure!(idata.len() > 5, "face too short");
        let content_flags = FaceContentFlags::from_bits(idata[1])
            .ok_or_else(|| anyhow!("invalid face content flags"))?;
        let layout_flags = FaceLayoutFlags::from_bits(idata[2])
            .ok_or_else(|| anyhow!("invalid face layout flags"))?;
        let color = idata[3];
        let is_shadow = idata[4] > 0;

        let mut off = 5;
        let (face_normal, face_unk_residual, face_center) =
            if content_flags.contains(FaceContentFlags::HAVE_FACE_NORMAL) {
                let face_normal = [
                    idata[off + 1] as i8,
                    idata[off + 3] as i8,
                    idata[off + 5] as i8,
                ];
                let face_residual = [idata[off] as i8, idata[off + 2] as i8, idata[off + 4] as i8];
                off += 6;

                let center = if layout_flags.contains(FaceLayoutFlags::USE_BYTE_FACE_CENTER) {
                    let center = [
                        idata[off] as i8 as i16,
                        idata[off + 1] as i8 as i16,
                        idata[off + 2] as i8 as i16,
                    ];
                    off += 3;
                    center
                } else {
                    let cdata = Ref::<&[u8], [I16<LE>; 3]>::new(&idata[off..off + 6])
                        .ok_or_else(|| anyhow!("failed to parse face center"))?;
                    let center = [cdata[0].get(), cdata[1].get(), cdata[2].get()];
                    off += 6;
                    ensure!(center
                        .iter()
                        .any(|&v| v > i8::MAX as i16 || v < i8::MIN as i16));
                    center
                };
                (Some(face_normal), Some(face_residual), Some(center))
            } else {
                (None, None, None)
            };

        // Index count.
        let index_count = idata[off] as usize;
        off += 1;

        // Indexes.
        let indices: Vec<u16> = if layout_flags.contains(FaceLayoutFlags::USE_SHORT_INDICES) {
            let words = Ref::<&[u8], [U16<LE>]>::new_slice(&idata[off..off + index_count * 2])
                .ok_or_else(|| anyhow!("failed to parse indices"))?;
            off += index_count * 2;
            words.iter().map(|v| v.get()).collect()
        } else {
            let index_u8 = &idata[off..off + index_count];
            off += index_count;
            index_u8.iter().map(|&v| u16::from(v)).collect()
        };

        // Tex Coords
        let mut tex_coords = Vec::with_capacity(index_count);
        if content_flags.contains(FaceContentFlags::HAVE_TEXCOORDS) {
            if layout_flags.contains(FaceLayoutFlags::USE_BYTE_TEXCOORDS) {
                let mut words = idata[off..off + index_count * 2]
                    .iter()
                    .map(|&v| u16::from(v));
                for _ in 0..index_count {
                    tex_coords.push((words.next().unwrap(), words.next().unwrap()));
                }
                off += index_count * 2;
            } else {
                let words = Ref::<&[u8], [U16<LE>]>::new_slice(&idata[off..off + index_count * 4])
                    .ok_or_else(|| anyhow!("failed to parse texcoords"))?;
                for i in 0..index_count {
                    tex_coords.push((words[i * 2].get(), words[i * 2 + 1].get()));
                }
                off += index_count * 4;
            }
            assert_eq!(tex_coords.len(), indices.len());
        }
        Ok((
            InstrDetail::Face(Self {
                content_flags,
                layout_flags,
                color,
                is_shadow,
                face_normal,
                face_unk_residual,
                face_center,
                indices,
                tex_coords,
                usage: FaceUsage::default(),
            }),
            off,
        ))
    }
    fn to_bytes(&self, out: &mut [u8]) -> Result<()> {
        out[1..5].copy_from_slice(&[
            self.content_flags.bits(),
            self.layout_flags.bits(),
            self.color,
            if self.is_shadow { 1u8 } else { 0u8 },
        ]);
        let mut off = 5;
        if let Some(norm) = &self.face_normal {
            // FIXME: what are these interleaved bytes?
            let resid = self.face_unk_residual.unwrap();
            out[off] = resid[0] as u8;
            out[off + 1] = norm[0] as u8;
            out[off + 2] = resid[1] as u8;
            out[off + 3] = norm[1] as u8;
            out[off + 4] = resid[2] as u8;
            out[off + 5] = norm[2] as u8;
            off += 6;
        }
        if let Some(center) = &self.face_center {
            if self
                .layout_flags
                .contains(FaceLayoutFlags::USE_BYTE_FACE_CENTER)
            {
                out[off] = center[0] as i8 as u8;
                out[off + 1] = center[1] as i8 as u8;
                out[off + 2] = center[2] as i8 as u8;
                off += 3;
            } else {
                out[off..off + 6].copy_from_slice(center.as_bytes());
                off += 6;
            }
        }
        out[off] = self.indices.len().try_into()?;
        off += 1;
        for &index in &self.indices {
            if self
                .layout_flags
                .contains(FaceLayoutFlags::USE_SHORT_INDICES)
            {
                out[off..off + 2].copy_from_slice(&index.to_le_bytes());
                off += 2;
            } else {
                out[off] = index.try_into()?;
                off += 1;
            }
        }
        for &(s, t) in &self.tex_coords {
            if self
                .layout_flags
                .contains(FaceLayoutFlags::USE_BYTE_TEXCOORDS)
            {
                out[off] = s.try_into()?;
                out[off + 1] = t.try_into()?;
                off += 2;
            } else {
                out[off..off + 2].copy_from_slice(&s.to_le_bytes());
                out[off + 2..off + 4].copy_from_slice(&t.to_le_bytes());
                off += 4;
            }
        }
        ensure!(off == out.len(), "did not push enough face bytes");
        Ok(())
    }
    fn as_value(&self, map: &mut Dict) -> Result<()> {
        map.insert("layout_flags", self.layout_flags.as_value().into());
        map.insert("content_flags", self.content_flags.as_value().into());
        map.insert("color", self.color.into());
        map.insert("is_shadow", self.is_shadow.into());
        if let Some(face_normal) = &self.face_normal {
            map.insert("face_normal", List::from_slice(face_normal).into());
        }
        if let Some(face_unk_residual) = &self.face_unk_residual {
            map.insert("face_residual", List::from_slice(face_unk_residual).into());
        }
        if let Some(face_unk_center) = &self.face_center {
            map.insert("face_center", List::from_slice(face_unk_center).into());
        }
        let mut indices = List::default();
        for &index in &self.indices {
            indices.push(index.into());
        }
        map.insert("indices", indices.into());
        if !self.tex_coords.is_empty() {
            let mut tex_coords = List::default();
            for tex_coord in &self.tex_coords {
                tex_coords.push(List::from_slice(&[tex_coord.0, tex_coord.1]).into());
            }
            map.insert("tex_coords", tex_coords.into());
        }
        map.insert("usage", self.usage.to_value());
        Ok(())
    }
    fn display_content(&self, _offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        let mut parts = vec![];

        let mut cfs = vec![];
        if self.content_flags.contains(FaceContentFlags::UNK1) {
            cfs.push("UNK1");
        }
        if self.content_flags.contains(FaceContentFlags::UNK2) {
            cfs.push("UNK2");
        }
        if self.content_flags.contains(FaceContentFlags::UNK3) {
            cfs.push("UNK3");
        }
        if self.content_flags.contains(FaceContentFlags::UNK4) {
            cfs.push("UNK4");
        }
        if self.content_flags.contains(FaceContentFlags::UNK5) {
            cfs.push("UNK5");
        }
        if self
            .content_flags
            .contains(FaceContentFlags::FILL_BACKGROUND)
        {
            cfs.push("FILL");
        }
        parts.push(format!("flags:{}", cfs.join("|")));
        if self.is_shadow {
            parts.push(format!("shadow:{}", self.color));
        } else {
            parts.push(format!("color:{}", self.color));
        }
        if let Some(center) = self.center() {
            parts.push(format!(
                "center:[{},{},{}]",
                center[0], center[1], center[2]
            ));
        }
        if let Some(normal) = self.face_normal {
            parts.push(format!(
                "normal:[{},{},{}]",
                normal[0], normal[1], normal[2]
            ));
        }

        parts.push("\n                   ".to_owned());

        let mut idxs = Vec::new();
        for idx in &self.indices {
            idxs.push(idx.to_string());
        }
        parts.push(format!("indices:[{}]", idxs.join(",")));
        if self
            .content_flags
            .contains(FaceContentFlags::HAVE_TEXCOORDS)
        {
            let mut tcs = vec![];
            for tc in &self.tex_coords {
                tcs.push(format!("({},{})", tc.0, tc.1));
            }
            parts.push(format!("tc:[{}]", tcs.join(" ")));
        }

        // if let Some(residual) = self.residual() {
        //     parts.push(format!(
        //         "residual:[{},{},{}]",
        //         residual[0], residual[1], residual[2]
        //     ));
        // }

        Ok(format!(
            "{}{}{}",
            ansi().fg(Self::COLOR),
            parts.join(" "),
            ansi()
        ))
    }
}

impl iFC_Face {
    pub fn from_value(map: &Dict) -> Result<(InstrDetail, usize)> {
        let usage = FaceUsage::from_value(map.try_get("usage")?)?;

        let mut tex_coords: Vec<(u16, u16)> = Vec::new();
        if let Some(vtcs) = map.get("tex_coords") {
            for vtc in vtcs.to_list()?.iter() {
                let arr: [u16; 2] = vtc.to_list()?.to_array()?;
                tex_coords.push((arr[0], arr[1]));
            }
        }

        let mut indices: Vec<u16> = Vec::new();
        for vidx in map.try_get("indices")?.to_list()?.iter() {
            indices.push(vidx.to_int()?.try_into()?);
        }

        let face_unk_center = if let Some(vfc) = map.get("face_center") {
            Some(vfc.to_list()?.to_array()?)
        } else {
            None
        };

        let face_unk_residual = if let Some(vfc) = map.get("face_residual") {
            Some(vfc.to_list()?.to_array()?)
        } else {
            None
        };

        let face_normal = if let Some(vfn) = map.get("face_normal") {
            Some(vfn.to_list()?.to_array()?)
        } else {
            None
        };

        let is_shadow = map.try_get("is_shadow")?.to_bool()?;
        let color: u8 = map.try_get("color")?.to_int()?.try_into()?;

        let layout_flags = FaceLayoutFlags::from_value(
            map.try_get("layout_flags")?.to_dict()?,
            &indices,
            &face_unk_center,
            &tex_coords,
        )?;

        let content_flags = FaceContentFlags::from_value(
            map.try_get("content_flags")?.to_dict()?,
            &face_normal,
            &tex_coords,
        )?;

        let face = Self {
            content_flags,
            layout_flags,
            color,
            is_shadow,
            face_normal,
            face_unk_residual,
            face_center: face_unk_center,
            indices,
            tex_coords,
            usage,
        };
        let size = face.compute_size();
        Ok((InstrDetail::Face(face), size))
    }
    pub fn compute_size(&self) -> usize {
        let mut out = 6;
        if self.face_normal.is_some() {
            out += 6;
        }
        if self.face_center.is_some() {
            if self
                .layout_flags
                .contains(FaceLayoutFlags::USE_BYTE_FACE_CENTER)
            {
                out += 3;
            } else {
                out += 6;
            }
        }
        if self
            .layout_flags
            .contains(FaceLayoutFlags::USE_SHORT_INDICES)
        {
            out += self.indices.len() * 2;
        } else {
            out += self.indices.len();
        }
        if self
            .layout_flags
            .contains(FaceLayoutFlags::USE_BYTE_TEXCOORDS)
        {
            out += self.tex_coords.len() * 2;
        } else {
            out += self.tex_coords.len() * 4;
        }
        out
    }
    pub fn residual(&self) -> &Option<[i8; 3]> {
        &self.face_unk_residual
    }
    pub fn set_residual(&mut self, residual: [i8; 3]) {
        self.face_unk_residual = Some(residual);
    }
    pub fn center(&self) -> &Option<[i16; 3]> {
        &self.face_center
    }
    pub fn set_center(&mut self, center: [i16; 3]) {
        self.face_center = Some(center);
    }
    pub fn layout_flags(&self) -> FaceLayoutFlags {
        self.layout_flags
    }
    pub fn content_flags(&self) -> FaceContentFlags {
        self.content_flags
    }
    pub fn content_flags_mut(&mut self) -> &mut FaceContentFlags {
        &mut self.content_flags
    }
    pub fn set_usage(&mut self, usage: FaceUsage) {
        debug_assert_ne!(usage, FaceUsage::FromBuffer);
        assert_eq!(self.usage, FaceUsage::FromBuffer);
        self.usage = usage;
    }
    pub fn usage(&self) -> FaceUsage {
        self.usage
    }
    pub fn indices(&self) -> &[u16] {
        &self.indices
    }
    pub fn index(&self, offset: usize) -> usize {
        self.indices[offset] as usize
    }
    pub fn tex_coords(&self) -> &[(u16, u16)] {
        &self.tex_coords
    }
    pub fn tex_coord(&self, offset: usize) -> [u16; 2] {
        [self.tex_coords[offset].0, self.tex_coords[offset].1]
    }
    pub fn set_tex_coord(&mut self, offset: usize, tex_coord: (u16, u16)) -> Result<()> {
        if self
            .layout_flags
            .contains(FaceLayoutFlags::USE_BYTE_TEXCOORDS)
        {
            ensure!(tex_coord.0 <= 255);
            ensure!(tex_coord.1 <= 255);
        }
        self.tex_coords[offset] = tex_coord;
        Ok(())
    }
    pub fn set_tex_coord_arr(&mut self, offset: usize, tex_coord: [u16; 2]) -> Result<()> {
        self.set_tex_coord(offset, (tex_coord[0], tex_coord[1]))
    }
    pub fn normal(&self) -> &Option<[i8; 3]> {
        &self.face_normal
    }
    pub fn set_normal(&mut self, normal: [i8; 3]) {
        self.face_normal = Some(normal);
    }
    pub fn color(&self) -> u8 {
        self.color
    }
    pub fn color_mut(&mut self) -> &mut u8 {
        &mut self.color
    }
}

pub struct iFF_Header;

impl ShInstrSpec for iFF_Header {
    const MAGIC: InstrMagic = InstrMagic::Byte(0xFF);
    const SIZE: Option<usize> = Some(14);
    const NAME: &'static str = "Header";
    const SHORT_NAME: &'static str = "Headr";
    const COLOR: Color = Color::Green;
    const NOTES: &'static str =
        "Always appears first in SH files. May also appear in other places.\
     Has some numbers that are probably super important, but I don't know what they mean";
}

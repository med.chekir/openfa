// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
mod analysis;
mod attic;
mod coordinate;
mod csv;
mod draw_state;
mod extent;
mod instrs;
mod sh_code;
mod sh_x86;
mod synthetics;
mod usage;
mod xform;

pub use crate::{
    analysis::{ShAnalysis, ShCapabilities, ShXforms},
    attic::{
        instr::{Facet, FacetFlags, VertexBuf, X86Code},
        Instr, RawShape, UnknownData,
    },
    coordinate::{
        fa_norm_to_vec, fa_pos_to_pt3, fa_rot16_to_angle, fa_xform_to_pt3, matrix_for_xform,
    },
    csv::{export_csv, Record},
    draw_state::DrawState,
    extent::{GearFootprintKind, ShExtent},
    instrs::*,
    sh_code::{DrawSelection, InstrDetail, ShCode, ShInstr},
    usage::{
        AfterBurnerState, AileronPosition, AirbrakeState, BayState, EjectState, FaceUsage,
        FlapState, GearState, HookState, PlayerState, RudderPosition, SamCount, SlatsState,
        VertBufUsage,
    },
    xform::{XformId, Xformer},
};

pub const SHAPE_LOAD_BASE: u32 = 0xAA00_0000;

// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    draw_state::DrawState,
    extent::ExtentBuilder,
    instrs::{i1E_Pad, iB8},
    sh_code::{CodePtr, DrawVisitor, InstrDetail, ShInstr, ShInstrSpec},
    sh_x86::iF0_X86Code,
    usage::{
        AfterBurnerState, AileronPosition, AirbrakeState, BayState, EjectState, FaceUsage,
        FlapState, GearState, HookState, PlayerState, RudderPosition, SamCount, SlatsState,
        VertBufUsage,
    },
    xform::{XformCallInput, XformDataInput, XformDataInputKind, XformId, Xformer},
    DrawSelection, ShExtent,
};
use anyhow::{anyhow, bail, ensure, Result};
use bevy_ecs::prelude::*;
use bitflags::bitflags;
use i386::Interpreter;
use itertools::Itertools;
use nitrous::{inject_nitrous_component, NitrousComponent};
use once_cell::sync::Lazy;
use peff::{PortableExecutable, Trampoline};
use std::{collections::HashMap, time::Instant};
use uuid::Uuid;

const MAX_XFORMS: usize = 14;

bitflags! {
    #[derive(NitrousComponent, Clone, Copy, Debug, Default, Eq, PartialEq)]
    pub struct ShCapabilities : u32 {
        // Base Capabilities
        const HAS_FRAME_ANIMATIONS = 0x0000_0001;
        const HAS_DAMAGE_MODEL     = 0x0000_0002;

        // Xformers
        // Note: _PLvtOn is only used by xforms, but is still a bool
        const XFORM_HAS_VT         = 0x8000_0080;
        const XFORM_HARDPOINT_ANGLE= 0x0000_0100;
        const XFORM_INSECT_WING    = 0x0000_0200;
        const XFORM_BAY_DOOR       = 0x0000_0400;
        const XFORM_TV_CANARD      = 0x0000_0800;
        const XFORM_GEAR           = 0x0000_1000;
        const XFORM_SWING_WING     = 0x0000_2000;
        const XFORM_VT_ANGLE       = 0x0000_4000;
        const XFORM_CURRENT_TICKS  = 0x0000_8000;
        const XFORM_MASK           = 0x0000_FF80;

        // Usages
        const HAS_AFTERBURNER      = 0x0001_0000;
        const HAS_AILERONS         = 0x0002_0000;
        const HAS_AIRBRAKE         = 0x0004_0000;
        const HAS_BAY              = 0x0008_0000;
        const HAS_EJECTION_STATE   = 0x0010_0000;
        const HAS_FLAPS            = 0x0020_0000;
        const HAS_FLAP_AIRBRAKE    = 0x0040_0000;
        const HAS_GEAR             = 0x0080_0000;
        const HAS_GEAR_STRUT       = 0x0100_0000;
        const HAS_HOOK             = 0x0200_0000;
        const HAS_HOVER_FLAPS      = 0x0400_0000;
        const HAS_PLAYER_STATE     = 0x0800_0000;
        const HAS_RUDDER           = 0x1000_0000;
        const HAS_SAM_COUNT        = 0x2000_0000;
        const HAS_SLATS            = 0x4000_0000;
        const USAGES_MASK          = 0x7FFF_0000;

        const DRAW_STATE_MASK      = 0x7FFF_FF81;
    }
}

#[inject_nitrous_component]
impl ShCapabilities {
    pub fn has_animation(&self) -> bool {
        self.contains(ShCapabilities::HAS_FRAME_ANIMATIONS)
    }

    pub fn has_damage_model(&self) -> bool {
        self.contains(ShCapabilities::HAS_DAMAGE_MODEL)
    }

    pub fn has_xforms(&self) -> bool {
        self.intersects(ShCapabilities::XFORM_MASK)
    }

    pub fn has_flags(&self) -> bool {
        self.intersects(ShCapabilities::USAGES_MASK)
    }

    pub fn needs_draw_state(&self) -> bool {
        self.intersects(ShCapabilities::DRAW_STATE_MASK)
    }

    pub fn has_dynamic_bay(&self) -> bool {
        self.contains(Self::XFORM_BAY_DOOR)
    }

    pub fn has_dynamic_gear(&self) -> bool {
        self.contains(Self::XFORM_GEAR)
    }

    pub fn has_dynamic_wing(&self) -> bool {
        self.contains(Self::XFORM_SWING_WING)
    }

    pub fn has_dynamic_tv(&self) -> bool {
        self.contains(Self::XFORM_TV_CANARD)
    }

    pub fn has_dynamic_vt(&self) -> bool {
        self.contains(Self::XFORM_HAS_VT)
    }

    pub fn has_afterburner(&self) -> bool {
        self.contains(Self::HAS_AFTERBURNER)
    }

    pub fn has_bay(&self) -> bool {
        self.contains(Self::HAS_BAY)
    }

    pub fn has_flaps(&self) -> bool {
        self.contains(Self::HAS_FLAPS)
    }

    pub fn has_gear(&self) -> bool {
        self.contains(Self::HAS_GEAR)
    }

    pub fn has_hook(&self) -> bool {
        self.contains(Self::HAS_HOOK)
    }

    pub fn has_rudder(&self) -> bool {
        self.contains(Self::HAS_RUDDER)
    }

    pub fn has_sam_count(&self) -> bool {
        self.contains(Self::HAS_SAM_COUNT)
    }

    pub fn has_slats(&self) -> bool {
        self.contains(Self::HAS_SLATS)
    }
}

// We don't know what a block of code does until we run it. Given a variable
// read in the code with the name in the key, collect a list of tuples that map
// from an input to that variable to an expected behavior. If the code falls
// through to the unmask, the target VertBuf is enabled and will be drawn. This
// lets us assign a usage to a vertex buffer and implicitly to faces following.
type V = VertBufUsage;

static TOGGLE_TABLE: Lazy<HashMap<&'static str, Vec<(i32, VertBufUsage)>>> = Lazy::new(|| {
    let mut table = HashMap::new();
    table.insert(
        "_PLrightFlap",
        vec![
            (-2, V::FlapRight(FlapState::Hover)), // hover
            (-1, V::FlapRight(FlapState::Down)),  // down
            (0, V::FlapRight(FlapState::Up)),     // up
            // TODO: check in FA:SU27.SH
            (1, V::FlapRight(FlapState::Brake)), // brake
        ],
    );
    table.insert(
        "_PLleftFlap",
        vec![
            (-2, V::FlapLeft(FlapState::Hover)), // hover
            (-1, V::FlapLeft(FlapState::Down)),  // down
            (0, V::FlapLeft(FlapState::Up)),     // up
            (1, V::FlapLeft(FlapState::Brake)),  // brake
        ],
    );
    table.insert(
        "_PLslats",
        vec![
            (0, V::Slats(SlatsState::Retracted)), // up
            (1, V::Slats(SlatsState::Extended)),  // down
        ],
    );
    table.insert(
        "_PLgearDown",
        vec![
            (0, V::Gear(GearState::Retracted)), // up
            (1, V::Gear(GearState::Extended)),  // down
            (4, V::Gear(GearState::Strut)),     // FA:F117.SH
        ],
    );
    table.insert(
        "_PLbayOpen",
        vec![(0, V::Bay(BayState::Closed)), (1, V::Bay(BayState::Open))],
    );
    table.insert(
        "_PLbrake",
        vec![
            (-1, V::Brake(AirbrakeState::Inverted)),
            (0, V::Brake(AirbrakeState::Retracted)),
            (1, V::Brake(AirbrakeState::Extended)),
        ],
    );
    table.insert(
        "_PLhook",
        vec![
            (0, V::Hook(HookState::Retracted)),
            (1, V::Hook(HookState::Extended)),
        ],
    );
    table.insert(
        "_PLrudder",
        vec![
            // TODO: test with FA:A310
            // FIXME: this doesn't line up with our left/right above?
            (0, V::Rudder(RudderPosition::Center)), // center
            (1, V::Rudder(RudderPosition::Right)),  // right
            (-1, V::Rudder(RudderPosition::Left)),  // left
        ],
    );
    table.insert(
        "_PLrightAln",
        vec![
            // TODO: test with USNF97:A7
            (0, V::AileronRight(AileronPosition::Center)), // center
            (1, V::AileronRight(AileronPosition::Up)),     // up
            (-1, V::AileronRight(AileronPosition::Down)),  // down
        ],
    );
    table.insert(
        "_PLleftAln",
        vec![
            (0, V::AileronLeft(AileronPosition::Center)), // center
            (1, V::AileronLeft(AileronPosition::Up)),     // up
            (-1, V::AileronLeft(AileronPosition::Down)),  // down
        ],
    );
    table.insert(
        "_PLafterBurner",
        vec![
            (0, V::Afterburner(AfterBurnerState::Off)),
            (1, V::Afterburner(AfterBurnerState::On)),
        ],
    );
    table.insert(
        "_SAMcount",
        vec![
            // TODO: test with ATFNATO:CHAP.SH
            (0, V::SamCount(SamCount::new(0))),
            (1, V::SamCount(SamCount::new(1))),
            (2, V::SamCount(SamCount::new(2))),
            (3, V::SamCount(SamCount::new(3))),
        ],
    );
    table.insert(
        "_PLstate",
        vec![
            (0x11, V::Eject(EjectState::Unk11)),
            (0x12, V::Eject(EjectState::Unk12)),
            (0x13, V::Eject(EjectState::Unk13)),
            (0x14, V::Eject(EjectState::Unk14)),
            (0x15, V::Eject(EjectState::Unk15)),
            (0x1A, V::Eject(EjectState::Unk1A)),
            (0x1B, V::Eject(EjectState::Unk1B)),
            (0x1C, V::Eject(EjectState::Unk1C)),
            (0x1D, V::Eject(EjectState::Unk1D)),
            (0x1E, V::Eject(EjectState::Unk1E)),
            (0x22, V::Eject(EjectState::Unk22)),
            (0x23, V::Eject(EjectState::Unk23)),
            (0x24, V::Eject(EjectState::Unk24)),
            (0x25, V::Eject(EjectState::Unk25)),
            (0x26, V::Eject(EjectState::Unk26)),
        ],
    );
    table.insert(
        "_PLdead",
        vec![
            (0, V::PlayerDead(PlayerState::Dead)),
            (1, V::PlayerDead(PlayerState::Alive)),
        ],
    );
    table
});

#[derive(NitrousComponent, Clone, Debug, Default)]
pub struct ShXforms {
    xformers: Vec<Xformer>,
}

#[inject_nitrous_component]
impl ShXforms {
    pub fn run_xforms(
        &mut self,
        state: &DrawState,
        start: &Instant,
        now: &Instant,
        output: &mut [[f32; 6]; MAX_XFORMS],
    ) -> Result<()> {
        assert!(self.xformers.len() <= MAX_XFORMS);
        for (xform_id, xformer) in self.xformers.iter_mut().enumerate() {
            xformer.run(state, start, now, &mut output[xform_id])?;
        }
        Ok(())
    }

    pub fn get_mut(&mut self, xform_id: XformId) -> Option<&mut Xformer> {
        match xform_id {
            XformId::Static => None,
            XformId::Xform(offset) => Some(&mut self.xformers[usize::from(offset)]),
        }
    }
}

#[derive(Clone, Debug, Default)]
pub struct ShAnalysis {
    xformers: Vec<Xformer>,
    capabilities: ShCapabilities,
    extent: ShExtent,
}

impl ShAnalysis {
    pub fn analyze(
        pe: &PortableExecutable,
        instr_map: &HashMap<Uuid, usize>,
        instrs: &mut [ShInstr],
    ) -> Result<(Self, Option<Self>)> {
        let normal_analysis =
            Self::analyze_inner(pe, DrawSelection::NormalModel, instr_map, instrs)?;
        let mut damage_analysis = None;
        if normal_analysis.has_damage_model() {
            damage_analysis = Some(Self::analyze_inner(
                pe,
                DrawSelection::DamageModel,
                instr_map,
                instrs,
            )?);
        }
        Ok((normal_analysis, damage_analysis))
    }

    // Clone the per-shape analysis (with pre-initialized x86 interpreters) into a per-instance
    // xformers bundle that we can bind to the instance and execute each frame.
    pub fn xforms_for_shape_bundle(&self) -> Option<ShXforms> {
        if !self.xformers.is_empty() {
            Some(ShXforms {
                xformers: self.xformers.clone(),
            })
        } else {
            None
        }
    }

    // Panics if has_xforms is not true.
    pub fn xformers(&self) -> &[Xformer] {
        debug_assert!(self.has_xforms());
        &self.xformers
    }

    // Panics if has_xforms is not true.
    pub fn xformers_mut(&mut self) -> &mut [Xformer] {
        debug_assert!(self.has_xforms());
        &mut self.xformers
    }

    fn analyze_inner(
        pe: &PortableExecutable,
        draw: DrawSelection,
        instr_map: &HashMap<Uuid, usize>,
        instrs: &mut [ShInstr],
    ) -> Result<Self> {
        let mut capabilities = ShCapabilities::empty();

        // Find usage of unmasks via code analysis, annotate with results, and return xform funcs.
        let mut xformers =
            Self::annotate_vertex_bufs_operations(pe, draw, instr_map, instrs, &mut capabilities)?;

        // Annotate each frame target face with a usage
        Self::annotate_frame_targets(draw, instr_map, instrs, &mut capabilities)?;

        // Measure extents of all parts of the shape
        let mut extent =
            Self::compute_extents(&mut xformers, draw, instr_map, instrs, capabilities)?;

        // Use hints from our bounding information to infer the gear touch points and strut length
        Self::find_gear_struts(
            &mut extent,
            &mut xformers,
            draw,
            instr_map,
            instrs,
            capabilities,
        )?;

        Ok(Self {
            xformers,
            capabilities,
            extent,
        })
    }

    pub fn has_flags(&self) -> bool {
        self.capabilities.has_flags()
    }

    pub fn has_xforms(&self) -> bool {
        self.capabilities.has_xforms()
    }

    pub fn has_animation(&self) -> bool {
        self.capabilities.has_animation()
    }

    pub fn has_damage_model(&self) -> bool {
        self.capabilities.has_damage_model()
    }

    pub fn capabilities(&self) -> ShCapabilities {
        self.capabilities
    }

    pub fn extent(&self) -> &ShExtent {
        &self.extent
    }

    fn annotate_vertex_bufs_operations(
        pe: &PortableExecutable,
        draw: DrawSelection,
        instr_map: &HashMap<Uuid, usize>,
        instrs: &mut [ShInstr],
        capabilities: &mut ShCapabilities,
    ) -> Result<Vec<Xformer>> {
        let mut usage_map = HashMap::new();
        let mut xform_id_map = HashMap::new();
        let mut xforms = Vec::new();

        let visitor = DrawVisitor::new(instrs, instr_map, draw);
        for instr in visitor {
            let i = instr_map[instr.uuid()];
            //for (i, instr) in instrs.iter().enumerate() {

            // x86 code
            if let InstrDetail::X86Code(x86_header) = instr.detail() {
                // followed by unmask; x86 is never last
                if let Some(code_ptr) = Self::is_unmask_op(&instrs[i + 1]) {
                    // followed by more code; unmask is never last
                    let InstrDetail::X86Code(x86_trailer) = instrs[i + 2].detail() else {
                        bail!("x86 code with an unmask after must have a trailer x86")
                    };
                    // And the code matches a certain pattern
                    let usage = Self::is_toggle_op_for(
                        pe,
                        x86_header,
                        instrs[i + 1].instr_offset(),
                        x86_trailer,
                    )?;
                    ensure!(usage != VertBufUsage::Static, "expected non-static usage");
                    ensure!(!usage_map.contains_key(&code_ptr.as_uuid()?));
                    usage_map.insert(code_ptr.as_uuid()?, usage);
                    usage_map.insert(*instrs[i].uuid(), usage);
                    usage_map.insert(*instrs[i + 1].uuid(), usage);
                    usage_map.insert(*instrs[i + 2].uuid(), usage);
                    capabilities.insert(usage.capability());
                } else if let Some((code_ptr, base_xform)) = Self::is_xform_op(&instrs[i + 1]) {
                    // Unconditionally shown, but xformed, shapes like swing wings never needs
                    // to skip the xform-unmask, so doesn't have a trailer x86.
                    let x86_trailer =
                        if let InstrDetail::X86Code(x86_trailer) = instrs[i + 2].detail() {
                            Some(x86_trailer)
                        } else {
                            None
                        };

                    // Build the xformer and select usage of target
                    let (usage, xformer) = Self::make_xformer(
                        pe,
                        base_xform,
                        x86_header,
                        instrs[i + 1].instr_offset(),
                        x86_trailer,
                    )?;
                    assert!(!usage_map.contains_key(&code_ptr.as_uuid()?));
                    assert!(!xform_id_map.contains_key(&code_ptr.as_uuid()?));
                    xform_id_map.insert(code_ptr.as_uuid()?, xforms.len());
                    capabilities.insert(xformer.capabilities());
                    xforms.push(xformer);
                    if usage != VertBufUsage::Static {
                        usage_map.insert(code_ptr.as_uuid()?, usage);
                        usage_map.insert(*instrs[i].uuid(), usage);
                        usage_map.insert(*instrs[i + 1].uuid(), usage);
                        if x86_trailer.is_some() {
                            usage_map.insert(*instrs[i + 2].uuid(), usage);
                        }
                        capabilities.insert(usage.capability());
                    }
                }
            }
        }

        // Update buffer usages
        for (uuid, &usage) in usage_map.iter() {
            let target_instr_offset = instr_map[uuid];
            let target_instr = &mut instrs[target_instr_offset];
            match target_instr.detail_mut() {
                InstrDetail::VertexBuffer(vxbuf) => vxbuf.set_usage(usage),
                InstrDetail::X86Code(code) => code.set_usage(usage),
                InstrDetail::Unmask(unmask) => unmask.set_usage(usage),
                InstrDetail::UnmaskLong(unmask) => unmask.set_usage(usage),
                InstrDetail::XformUnmask(unmask) => unmask.set_usage(usage),
                InstrDetail::XformUnmaskLong(unmask) => unmask.set_usage(usage),
                InstrDetail::NoDetail(_) => {
                    // In some shapes (M17, others?), there is a landing instr in front
                    // of the vertex buffer with a u16 in it that is 0 or 1, which
                    // presumably controls something.
                    // TODO: analyze M17 to figure out what vxbuf at 3c50 and 3d47 is doing
                    //       to figure out what the B8's control.
                    ensure!(
                        target_instr.magic() == iB8::MAGIC,
                        "unexpected odd toggle target"
                    );
                    let mut i = 1;
                    while instrs[target_instr_offset + i].magic() == i1E_Pad::MAGIC {
                        i += 1;
                    }
                    let target_instr = &mut instrs[target_instr_offset + i];
                    if let InstrDetail::VertexBuffer(vxbuf) = target_instr.detail_mut() {
                        vxbuf.set_usage(usage);
                    } else {
                        bail!("expected a vertex buffer after toggle target B8");
                    }
                }
                _ => bail!("expected an vertex buffer target or source for usage info"),
            }
        }

        // Update faces with xform_ids
        for (uuid, &xform_id) in xform_id_map.iter() {
            let target_instr_offset = instr_map[uuid];
            let target_instr = &mut instrs[target_instr_offset];
            match target_instr.detail_mut() {
                InstrDetail::VertexBuffer(vxbuf) => {
                    vxbuf.set_xform(XformId::new(xform_id)?);
                }
                InstrDetail::NoDetail(_) => {
                    // In some shapes (B1_S, others?), there is a landing instr in front
                    // of the vertex buffer with a u16 in it that is 0 or 1, which
                    // presumably controls something.
                    //
                    // 2x occurrences in each of:
                    //   * B1_S
                    //   * F111_S
                    //   * F14_S
                    //   * MIG23_S
                    //   * SU24_S
                    //   * TU160_S
                    //   * TU26_S
                    //
                    // So clearly just the swing wing aircraft.
                    ensure!(
                        target_instr.magic() == iB8::MAGIC,
                        "unexpected odd toggle target"
                    );
                    let mut i = 1;
                    while instrs[target_instr_offset + i].magic() == i1E_Pad::MAGIC {
                        i += 1;
                    }
                    let target_instr = &mut instrs[target_instr_offset + i];
                    if let InstrDetail::VertexBuffer(vxbuf) = target_instr.detail_mut() {
                        vxbuf.set_xform(XformId::new(xform_id)?);
                    } else {
                        bail!("expected a vertex buffer after toggle target B8");
                    }
                }
                _ => bail!("expected an vertex buffer target for xform id"),
            }
        }

        ensure!(xforms.len() <= MAX_XFORMS);
        Ok(xforms)
    }

    fn is_unmask_op(instr: &ShInstr) -> Option<CodePtr> {
        if let InstrDetail::Unmask(umask) = instr.detail() {
            return Some(umask.code_ptr().to_owned());
        }
        if let InstrDetail::UnmaskLong(umask) = instr.detail() {
            return Some(umask.code_ptr().to_owned());
        }
        None
    }

    fn is_xform_op(instr: &ShInstr) -> Option<(CodePtr, [i16; 6])> {
        if let InstrDetail::XformUnmask(umask) = instr.detail() {
            return Some((umask.code_ptr().to_owned(), umask.base_xform()));
        }
        if let InstrDetail::XformUnmaskLong(umask) = instr.detail() {
            return Some((umask.code_ptr().to_owned(), umask.base_xform()));
        }
        None
    }

    fn is_toggle_op_for(
        pe: &PortableExecutable,
        x86_header: &iF0_X86Code,
        inner_offset: usize,
        x86_trailer: &iF0_X86Code,
    ) -> Result<VertBufUsage> {
        let symrefs = x86_header.find_external_refs(pe);
        ensure!(symrefs.len() == 1, "expect unmask with one parameter");
        let symref = symrefs[0];
        let do_start_interp = pe
            .find_trampoline_by_name("do_start_interp")
            .ok_or_else(|| anyhow!("no do_start_interp"))?;

        if let Some(possible_inputs) = TOGGLE_TABLE.get(symref.name.as_str()) {
            return Self::run_toggle_value_to_see_what_it_does(
                possible_inputs,
                (symref, do_start_interp),
                x86_header,
                inner_offset,
                x86_trailer,
                pe.relocation_target(),
            );
        }

        if symref.name == "brentObjId" {
            let mut callrefs = x86_header.find_external_calls(pe);
            ensure!(callrefs.len() == 2, "expected one call");
            callrefs.sort_by_key(|&v| &v.name);
            ensure!(
                callrefs[0].name == "@HARDNumLoaded@8",
                "expected call to @HARDNumLoaded@8"
            );
            ensure!(
                callrefs[1].name == "do_start_interp",
                "expected call to do_start_interp"
            );
            let brent_obj_id = pe
                .find_trampoline_by_name("brentObjId")
                .ok_or_else(|| anyhow!("no brentObjId"))?;
            return Self::run_toggle_call_to_see_what_it_does(
                &TOGGLE_TABLE["_SAMcount"],
                (brent_obj_id, callrefs[0], do_start_interp),
                x86_header,
                inner_offset,
                x86_trailer,
                pe.relocation_target(),
            );
        }

        bail!("unrecognized symbol: {}", symref.name);
    }

    fn run_toggle_value_to_see_what_it_does(
        possible_inputs: &[(i32, VertBufUsage)],
        (symref, do_start_interp): (&Trampoline, &Trampoline),
        x86_header: &iF0_X86Code,
        inner_offset: usize,
        x86_trailer: &iF0_X86Code,
        load_base: u32,
    ) -> Result<VertBufUsage> {
        // TODO: detect common byte patterns to avoid needing to spin up a full
        //       interpreter in order to judge what this controls.

        let mut interp = i386::Interpreter::new();
        interp.add_code(x86_header.decoded().clone());
        interp.add_code(x86_trailer.decoded().clone());
        interp.add_trampoline(do_start_interp.mem_location, &do_start_interp.name, 1);

        // We need to interpret the code to see if it falls through to the unmask
        // or jumps through, keeping the controlled shape hidden. Try each input
        // one at a time to see what happens.
        for (input, usage) in possible_inputs {
            interp.map_value(symref.mem_location, *input as u32);
            let exec_at = load_base + x86_header.decoded().start_offset() as u32;
            let exit_info = interp.interpret_at(exec_at)?;

            let (name, args) = exit_info.ok_trampoline()?;
            ensure!(name == "do_start_interp", "unexpected trampoline return");
            ensure!(args.len() == 1, "unexpected arg count");
            if inner_offset == args[0].wrapping_sub(load_base) as usize {
                return Ok(*usage);
            }
            interp.unmap_value(symref.mem_location);
        }
        bail!(
            "failed to find the toggle kind in our known inputs for {}",
            symref.name
        );
    }

    // As for run_toggle_value, but with a call to provide the input
    fn run_toggle_call_to_see_what_it_does(
        possible_inputs: &[(i32, VertBufUsage)],
        (brent_obj_id, hard_num_loaded, do_start_interp): (&Trampoline, &Trampoline, &Trampoline),
        x86_header: &iF0_X86Code,
        inner_offset: usize,
        x86_trailer: &iF0_X86Code,
        load_base: u32,
    ) -> Result<VertBufUsage> {
        let mut interp = i386::Interpreter::new();
        interp.add_code(x86_header.decoded().clone());
        interp.add_code(x86_trailer.decoded().clone());
        interp.add_trampoline(hard_num_loaded.mem_location, &hard_num_loaded.name, 1);
        interp.add_trampoline(do_start_interp.mem_location, &do_start_interp.name, 1);
        interp.map_value(brent_obj_id.mem_location, 0x60000);

        for (input, usage) in possible_inputs {
            let exec_at = load_base + x86_header.decoded().start_offset() as u32;
            let exit_info = interp.interpret_at(exec_at)?;
            let (name, args) = exit_info.ok_trampoline()?;
            ensure!(name == "@HARDNumLoaded@8", "expected num_loaded request");
            ensure!(args.len() == 1, "unexpected arg count");
            // Set up the return for our first call
            interp.set_register_value(i386::Reg::EAX, *input as u32);
            // Continue execution with provided sam count
            let exit_info = interp.interpret_at(interp.eip())?;
            let (name, args) = exit_info.ok_trampoline()?;
            ensure!(name == "do_start_interp", "unexpected trampoline return");
            ensure!(args.len() == 1, "unexpected arg count");
            if inner_offset == args[0].wrapping_sub(load_base) as usize {
                return Ok(*usage);
            }
        }
        bail!("failed to find the toggle kind in our known inputs for call toggle");
    }

    fn make_xformer(
        pe: &PortableExecutable,
        base_xform: [i16; 6],
        x86_header: &iF0_X86Code,
        inner_offset: usize,
        x86_trailer: Option<&iF0_X86Code>,
    ) -> Result<(VertBufUsage, Xformer)> {
        // Create inputs for symbols
        let symrefs = x86_header.find_external_refs(pe);
        let data_inputs: Vec<XformDataInput> = symrefs
            .iter()
            .map(|&v| XformDataInput::from_symref(v))
            .try_collect()?;

        let callrefs = x86_header.find_external_calls(pe);
        let call_inputs: Vec<XformCallInput> = callrefs
            .iter()
            .map(|&v| XformCallInput::from_callref(v))
            .try_collect()?;

        // Infer draw condition of referenced buffer from inputs and calls
        let mut usage = VertBufUsage::Static;
        for inp in &data_inputs {
            let next_usage = match inp.kind() {
                XformDataInputKind::AfterBurner => VertBufUsage::Afterburner(AfterBurnerState::On),
                XformDataInputKind::BayDoorPos => VertBufUsage::Bay(BayState::Open),
                XformDataInputKind::BayOpen => VertBufUsage::Bay(BayState::Open),
                XformDataInputKind::GearDown => VertBufUsage::Gear(GearState::Extended),
                XformDataInputKind::GearPos => VertBufUsage::Gear(GearState::Extended),
                _ => VertBufUsage::Static,
            };
            if usage != VertBufUsage::Static && next_usage != VertBufUsage::Static {
                assert_eq!(usage, next_usage);
            }
            if next_usage != VertBufUsage::Static {
                usage = next_usage;
            }
        }

        // Build the base interpreter we will use to transform the points in the
        // target vertex buffer.
        let inner_offset = pe.relocate_code_offset(inner_offset as u32 + 2)?;
        let mut interp = Interpreter::new();
        interp.add_entry_point(x86_header.decoded().clone());
        if let Some(x86) = x86_trailer {
            interp.add_code(x86.decoded().clone());
        }
        interp.map_writable(inner_offset, vec![0u8; 12])?;
        let do_start_interp = pe
            .find_trampoline_by_name("do_start_interp")
            .ok_or_else(|| anyhow!("missing do_start_interp"))?;
        interp.add_trampoline(do_start_interp.mem_location, &do_start_interp.name, 1);

        let xformer = Xformer::new(
            usage,
            base_xform,
            interp,
            data_inputs,
            call_inputs,
            inner_offset,
        );

        Ok((usage, xformer))
    }

    fn annotate_frame_targets(
        draw: DrawSelection,
        instr_map: &HashMap<Uuid, usize>,
        instrs: &mut [ShInstr],
        capabilities: &mut ShCapabilities,
    ) -> Result<()> {
        let mut usage_map = HashMap::new();

        let visitor = DrawVisitor::new(instrs, instr_map, draw);
        for instr in visitor {
            if let InstrDetail::JumpToFrame(frameset) = instr.detail() {
                for (i, frame_ptr) in frameset.frame_pointers().enumerate() {
                    usage_map.insert(
                        frame_ptr.as_uuid()?,
                        FaceUsage::Animation {
                            frame_number: i.try_into()?,
                            num_frames: frameset.num_frames().try_into()?,
                        },
                    );
                }
            }
        }

        for (uuid, usage) in usage_map.iter() {
            let InstrDetail::Face(face) = instrs[instr_map[uuid]].detail_mut() else {
                bail!("animation frames must point to a face");
            };
            face.set_usage(*usage);
        }

        if !usage_map.is_empty() {
            capabilities.insert(ShCapabilities::HAS_FRAME_ANIMATIONS);
        }
        Ok(())
    }

    fn compute_extents(
        xformers: &mut [Xformer],
        draw: DrawSelection,
        instr_map: &HashMap<Uuid, usize>,
        instrs: &mut [ShInstr],
        caps: ShCapabilities,
    ) -> Result<ShExtent> {
        let mut builder = ExtentBuilder::new(caps);

        let visitor = DrawVisitor::new(instrs, instr_map, draw);
        for instr in visitor {
            if let InstrDetail::VertexBuffer(vxbuf) = instr.detail() {
                let usage = vxbuf.usage();
                let xform_id = vxbuf.xform_id();
                let mut xformer = match xform_id {
                    XformId::Static => None,
                    XformId::Xform(offset) => Some(&mut xformers[offset as usize]),
                };
                for pos in vxbuf.vertices() {
                    builder.extend_with(usage, &mut xformer, pos)?;
                }
            }
        }

        Ok(builder.build())
    }

    fn find_gear_struts(
        extent: &mut ShExtent,
        xformers: &mut [Xformer],
        draw: DrawSelection,
        instr_map: &HashMap<Uuid, usize>,
        instrs: &mut [ShInstr],
        caps: ShCapabilities,
    ) -> Result<()> {
        if extent.aabb_gear().is_degenerate() {
            // No gear, no gear touch points.
            return Ok(());
        }

        // Given our existing extents, compute a window where the gear footprint could lie.
        let window = extent.gear_search_window();

        // Revisit gear points in window to infer the actual gear touchpoint box, which should
        // be much smaller and more accurate than the box with all doors and whatnot.
        let mut builder2 = ExtentBuilder::new(caps).set_window(window);
        let visitor = DrawVisitor::new(instrs, instr_map, draw);
        for instr in visitor {
            if let InstrDetail::VertexBuffer(vxbuf) = instr.detail() {
                let usage = vxbuf.usage();
                let xform_id = vxbuf.xform_id();
                let mut xformer = match xform_id {
                    XformId::Static => None,
                    XformId::Xform(offset) => Some(&mut xformers[offset as usize]),
                };
                for pos in vxbuf.vertices() {
                    builder2.extend_with(usage, &mut xformer, pos)?;
                }
            }
        }

        // The gear box of the new analysis is now tightly bound to the bottom (e.g. the footprint)
        let footprint_extents = *builder2.build().aabb_gear();
        extent.set_aabb_gear_footprint(footprint_extents);

        Ok(())
    }
}

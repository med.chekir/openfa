// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
#![allow(non_camel_case_types)]

use crate::{
    instrs::i00_EndObject,
    sh_code::{InstrDetail, InstrMagic, ShCode, ShInstr, ShInstrSpec},
    usage::VertBufUsage,
};
use ansi::{ansi, Color, StyleFlags};
use anyhow::{anyhow, ensure, Result};
use i386::{ByteCode, Disassembler, MemBlock};
use log::{debug, log_enabled, trace, Level};
use nitrous::{Dict, List};
use peff::{PortableExecutable, Trampoline};
use reverse::{bs2s, s2bs};
use std::collections::HashMap;
use uuid::Uuid;

#[derive(Clone, Debug)]
pub enum MaybeByteCode {
    Decoded(ByteCode),
    Raw(Vec<u8>),
}

impl MaybeByteCode {
    pub fn decoded(&self) -> &ByteCode {
        if let Self::Decoded(bc) = self {
            return bc;
        }
        panic!("not a decoded bytecode")
    }

    pub fn decoded_mut(&mut self) -> &mut ByteCode {
        if let Self::Decoded(bc) = self {
            return bc;
        }
        panic!("not a decoded bytecode")
    }
}

pub struct X86Unknown;

impl ShInstrSpec for X86Unknown {
    // const MAGIC: InstrMagic = 0xEC;
    const MAGIC: InstrMagic = InstrMagic::Bare;
    // const MAGIC_KIND: InstrMagicKind = InstrMagicKind::Byte;
    const SIZE: Option<usize> = None;
    const NAME: &'static str = "X86Unknown";
    const SHORT_NAME: &'static str = "!Unk!";
    const COLOR: Color = Color::BrightRed;
    const STYLE: StyleFlags = StyleFlags::BOLD;
}

#[derive(Clone, Debug)]
pub struct X86Message {
    message: String,
}

impl ShInstrSpec for X86Message {
    const MAGIC: InstrMagic = InstrMagic::Bare;
    // const MAGIC: u8 = 0xEF; // I think this is unused? Isn't worth making all Optional.
    // const MAGIC_KIND: InstrMagicKind = InstrMagicKind::Bare;
    const SIZE: Option<usize> = None;
    const NAME: &'static str = "X86Message";
    const SHORT_NAME: &'static str = "X86Ms";
    fn to_bytes(&self, buf: &mut [u8]) -> Result<()> {
        buf[0..self.message.len()].copy_from_slice(self.message.as_bytes());
        Ok(())
    }
    fn as_value(&self, map: &mut Dict) -> Result<()> {
        map.insert("message", self.message.clone().into());
        Ok(())
    }
}

impl X86Message {
    pub fn from_value(map: &Dict) -> Result<(InstrDetail, usize)> {
        let msg = Self {
            message: map
                .get("message")
                .ok_or_else(|| anyhow!("expected filename"))?
                .to_str()?
                .to_owned(),
        };
        let size = msg.message.len();
        Ok((InstrDetail::X86Message(msg), size))
    }
}

#[derive(Clone, Debug)]
pub struct X86Trampoline {
    trampoline: String,
    raw_data: [u8; 6],
}

impl ShInstrSpec for X86Trampoline {
    // Actually, the "magic" for these is just the x86 opcodes 0xFF25, e.g. from the PE reloc.
    // We treat these ase "Bare" because the PE is responsible for dealing with the header.
    const MAGIC: InstrMagic = InstrMagic::Bare;
    const SIZE: Option<usize> = Some(6);
    const NAME: &'static str = "X86Trampoline";
    const SHORT_NAME: &'static str = "Tramp";
    const COLOR: Color = Color::Yellow;
    fn to_bytes(&self, buf: &mut [u8]) -> Result<()> {
        buf.copy_from_slice(&self.raw_data);
        Ok(())
    }
    fn as_value(&self, map: &mut Dict) -> Result<()> {
        map.insert("trampoline", self.trampoline.to_owned().into());
        map.insert("data", bs2s(&self.raw_data).into());
        Ok(())
    }
    fn display_content(&self, _offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        Ok(format!(
            "{}{}{}",
            ansi().fg(Self::COLOR),
            self.trampoline,
            ansi()
        ))
    }
}

impl X86Trampoline {
    pub fn new(tramp: &Trampoline) -> Self {
        let mut raw_data = [0xFF, 0x25, 0, 0, 0, 0];
        raw_data[2..].copy_from_slice(&tramp.target.to_le_bytes());
        Self {
            trampoline: tramp.name.clone(),
            raw_data,
        }
    }

    pub fn from_value(map: &Dict) -> Result<(InstrDetail, usize)> {
        let trampoline = map.try_get("trampoline")?.to_str()?.into();
        let packed_data = map.try_get("data")?.to_str()?;
        let data = s2bs(packed_data);
        let mut raw_data = [0u8; 6];
        raw_data.copy_from_slice(&data);
        Ok((
            InstrDetail::X86Trampoline(Self {
                trampoline,
                raw_data,
            }),
            Self::SIZE.unwrap(),
        ))
    }

    pub fn name(&self) -> &str {
        &self.trampoline
    }
}

#[derive(Clone, Debug)]
pub struct iF0_X86Code {
    have_header: bool,
    code: MaybeByteCode,

    // Relative to the start of this code block rather than the code section.
    x86_relocs: Vec<usize>,

    // Annotations
    usage: VertBufUsage,
}

impl ShInstrSpec for iF0_X86Code {
    const MAGIC: InstrMagic = InstrMagic::Word(0xF0);
    const SIZE: Option<usize> = None;
    const NAME: &'static str = "X86Code";
    const SHORT_NAME: &'static str = "X86Cd";
    fn extract(_idata: &[u8]) -> Result<(InstrDetail, usize)> {
        // X86 decode needs lots more context, so has its own method.
        unimplemented!("this page left intentionally blank")
    }
    fn to_bytes(&self, buf: &mut [u8]) -> Result<()> {
        let mut off = 0;
        if self.have_header {
            // Note: header data is already filled in. Not fast forwarding here will
            // cause us to overwrite the pre-set header if we shouldn't have one.
            off += 2;
        }
        match &self.code {
            MaybeByteCode::Decoded(bytecode) => {
                buf[off..off + bytecode.size()].copy_from_slice(&bytecode.to_bytes());
                off += bytecode.size();
            }
            MaybeByteCode::Raw(raw) => {
                buf[off..off + raw.len()].copy_from_slice(raw);
                off += raw.len();
            }
        }
        ensure!(off == buf.len());
        Ok(())
    }
    fn as_value(&self, map: &mut Dict) -> Result<()> {
        map.insert("usage", self.usage.to_value());
        map.insert("have_header", self.have_header.into());
        map.insert("bytecode", bs2s(&self.code.decoded().to_bytes()).into());
        let mut x86_relocs = List::default();
        for &reloc in &self.x86_relocs {
            x86_relocs.push(reloc.into());
        }
        map.insert("x86_relocs", x86_relocs.into());
        Ok(())
    }
    fn display_content(&self, _offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        Ok(format!(
            "{}\n{}{}",
            ansi().fg(Self::COLOR),
            self.decoded().show_relative(0).trim(),
            ansi()
        ))
    }
}

impl iF0_X86Code {
    fn actual_magic_inner(have_header: bool) -> InstrMagic {
        if have_header {
            Self::MAGIC
        } else {
            InstrMagic::Bare
        }
    }

    pub fn actual_magic(&self) -> InstrMagic {
        Self::actual_magic_inner(self.have_header)
    }

    pub fn from_value(map: &Dict) -> Result<(InstrDetail, usize)> {
        let mut x86_relocs = Vec::new();
        for reloc in map.try_get("x86_relocs")?.to_list()?.iter() {
            x86_relocs.push(reloc.to_int()?.try_into()?);
        }
        let usage = VertBufUsage::from_value(map.try_get("usage")?)?;
        let have_header = map.try_get("have_header")?.to_bool()?;
        let bc_bytes = s2bs(map.try_get("bytecode")?.to_str()?);
        let size = bc_bytes.len() + if have_header { 2 } else { 0 };
        let raw_code = Self {
            have_header,
            code: MaybeByteCode::Raw(bc_bytes),
            x86_relocs,
            usage,
        };
        Ok((InstrDetail::X86Code(raw_code), size))
    }

    fn make_code(bytecode: ByteCode, code: &[u8], pe: &PortableExecutable) -> ShInstr {
        let bc_size = bytecode.size();
        let offset = bytecode.start_offset();
        let have_header = code[offset - 2] == 0xF0;
        let section_offset = if have_header { offset - 2 } else { offset };
        let section_length = bc_size + if have_header { 2 } else { 0 };

        let mut x86_relocs = Vec::new();
        for reloc in pe.relocs() {
            let off = reloc.code_offset();
            if off >= section_offset && off < section_offset + section_length {
                x86_relocs.push(off - section_offset);
            }
        }

        ShInstr::new_magic::<iF0_X86Code>(
            Self::actual_magic_inner(have_header),
            section_offset,
            section_length,
            InstrDetail::X86Code(iF0_X86Code {
                have_header,
                code: MaybeByteCode::Decoded(bytecode),
                x86_relocs,
                usage: VertBufUsage::default(),
            }),
        )
    }

    fn merge_code(prior: &mut Self, bytecode: ByteCode, code: &[u8], pe: &PortableExecutable) {
        let bc_size = bytecode.size();
        let offset = bytecode.start_offset();
        let have_header = code[offset - 2] == 0xF0;
        let section_offset = if have_header { offset - 2 } else { offset };
        let section_length = bc_size + if have_header { 2 } else { 0 };
        let prior_section_offset =
            prior.decoded().start_offset() - if prior.have_header { 2 } else { 0 };

        for reloc in pe.relocs() {
            let off = reloc.code_offset();
            if off >= section_offset && off < section_offset + section_length {
                prior.x86_relocs.push(off - prior_section_offset);
            }
        }
        prior.code.decoded_mut().merge(bytecode);
    }

    // When we find all branch targets in the assembly, that frequently jumps over interleaved
    // Sh code, random data, non-random data strings, etc. We try to understand the full contiguous
    // Sh code however, so we want to create correct (or best reasonable) instructions for data
    // stranded in these regions.
    fn handle_interspersed_data(
        end_offset: usize,
        offset: &mut usize,
        code: &[u8],
        pe: &PortableExecutable,
        vinstrs: &mut Vec<ShInstr>,
    ) -> Result<bool> {
        // Check in order for:
        //   1) Interspersed SH instructions
        //   2) Interspersed random ascii strings
        //   3) Anything we don't recognize is just data
        //      Q?: Up to end, or do we want to pick F0 00 sequences out by hand?
        while *offset < end_offset {
            debug!(
                "handling_interspered_data at: 0x{:04X} to {:04X}, first word: {}",
                *offset, end_offset, code[*offset]
            );

            // Decode interspersed SH instructions
            while *offset < end_offset {
                let magic = code[*offset];

                // Note: look before we leap to avoid accidentally recursing here.
                // In the case where we jumped into the middle of an F0, the F0 will
                // show up in the middle of this memory block, possibly not even at
                // the end, depending on the jump target. In this case, we return false
                // here, discarding the later jump targets so that we will re-parse
                // them in order.
                //
                // If there are interspersed NULL bytes, we know these are not actually trailer
                // magic because we're de-facto not at the end if there is more code ahead.
                if InstrMagic::Byte(magic) == i00_EndObject::MAGIC {
                    break;
                }

                // We could look before we leap, but this doesn't quite work. A jump target will
                // not always have an F0 in front of it, so we might miss subsequent code blocks
                // if we bail and re-enter without the current memory-view. One caveat to this is
                // if the F0 is 2 bytes before the jump target: or in this case, 2 bytes before
                // the end. We will bail out with our end_addr in that case and fail to parse.
                if end_offset - *offset == 2
                    && InstrMagic::Word(code[*offset]) == iF0_X86Code::MAGIC
                {
                    return Ok(true);
                }

                trace!("resuming parse at: {:04X}", *offset);
                if ShCode::disassemble_one(end_offset, pe, (code, offset, vinstrs, None)).is_err() {
                    trace!("failed parse at: {:04X}", *offset);
                    break;
                }
                trace!("parsed sh code to: {:04X}", *offset);
            }

            if *offset > end_offset {
                return Ok(false);
            }

            // Secondly, check if there is ascii content at the start of the block.
            // Try to interpret as utf8 up to the first null, if present.
            // This always falls through so that we consume whatever is left as opaque data.
            if let Some(str_end_offset) = (code[*offset..end_offset]).iter().position(|&v| v == 0u8)
            {
                if str_end_offset > 0 {
                    if let Ok(message) =
                        std::str::from_utf8(&code[*offset..*offset + str_end_offset])
                    {
                        debug!(
                            "Found message at 0x{:04X}: len: {}, {}",
                            *offset,
                            message.len(),
                            message
                        );
                        let msg = ShInstr::new_for::<X86Message>(
                            *offset,
                            str_end_offset,
                            InstrDetail::X86Message(X86Message {
                                message: message.to_owned(),
                            }),
                        );
                        *offset += str_end_offset;
                        vinstrs.push(msg);
                        continue;
                    }
                }
            }

            // Fall through to just taking everything
            if end_offset > *offset {
                let length = end_offset - *offset;
                debug!("using DEBRIS.SH hack; {} pad bytes", length);
                let instr = ShInstr::new_for::<X86Unknown>(
                    *offset,
                    length,
                    InstrDetail::NoDetail(code[*offset..end_offset].into()),
                );
                vinstrs.push(instr);
                *offset += length;
            }
        }

        Ok(true)
    }

    // Given the offset of this instruction, return the relocs relative to 0.
    pub fn relocs_relative_to_base(&self, offset: usize) -> Vec<usize> {
        self.x86_relocs.iter().map(|&v| offset + v).collect()
    }

    pub fn from_bytes(
        end_offset: usize,
        pe: &PortableExecutable,
        (code, offset, vinstrs, _): (&[u8], &mut usize, &mut Vec<ShInstr>, Option<usize>),
    ) -> Result<()> {
        let section = &code[*offset..];
        assert_eq!(InstrMagic::Word(section[0]), Self::MAGIC);
        assert_eq!(section[1], 0);

        let mut disasm = Disassembler::default();
        disasm.add_non_standard_retpoline("do_start_interp");
        disasm.add_non_standard_retpoline("_ErrorExit");
        disasm.disassemble_at(*offset + 2, pe)?;
        let view = disasm.build_memory_view(pe);
        Self::debug_memory_view(&view);

        for block in view {
            match block {
                MemBlock::Code(bytecode) => {
                    // Back jumps occur in a few files, we can ignore them as we've presumably
                    // already parsed this area.
                    if bytecode.start_offset() < *offset {
                        continue;
                    }
                    if bytecode.start_offset() >= end_offset {
                        trace!("ending early because of end_offset: {:04X}", end_offset);
                        return Ok(());
                    }
                    trace!("Code at 0x{:08X}", *offset);
                    let prior_instr = vinstrs.last_mut().unwrap();
                    let merge_size = if let InstrDetail::X86Code(_) = prior_instr.detail() {
                        if code[bytecode.start_offset() - 2] != 0xF0 {
                            // Only merge if the jump target abuts a code section and we don't have a header
                            Some(bytecode.size())
                        } else {
                            None
                        }
                    } else {
                        None
                    };
                    if let Some(size) = merge_size {
                        prior_instr.size += size;
                        *offset += size;
                        if let InstrDetail::X86Code(prior) = prior_instr.detail_mut() {
                            Self::merge_code(prior, bytecode, code, pe);
                        } else {
                            panic!("should merge with no code");
                        }
                    } else {
                        let instr = Self::make_code(bytecode, code, pe);
                        *offset += instr.instr_size();
                        vinstrs.push(instr);
                    }
                }
                MemBlock::Data {
                    start_offset, data, ..
                } => {
                    // Back jumps.
                    if start_offset < *offset {
                        continue;
                    }
                    if start_offset >= end_offset {
                        return Ok(());
                    }
                    assert_eq!(start_offset, *offset);
                    trace!("Data at 0x{:08X}", start_offset);
                    let end_offset = start_offset + data.len();

                    while *offset < end_offset && code[*offset] != 0xF0 {
                        if !Self::handle_interspersed_data(end_offset, offset, code, pe, vinstrs)? {
                            return Ok(());
                        }
                    }

                    #[cfg(debug_assertions)]
                    {
                        let mut expect_offset = 0;
                        for instr in vinstrs.iter() {
                            assert_eq!(
                                expect_offset,
                                instr.instr_offset(),
                                "instr size and offset misaligned at 0x{expect_offset:08X}"
                            );
                            expect_offset += instr.instr_size();
                        }
                    }
                }
            }
        }

        Ok(())
    }

    fn debug_memory_view(view: &[MemBlock]) {
        if log_enabled!(Level::Trace) {
            trace!("MemView:");
            for block in view {
                match block {
                    MemBlock::Code(bytecode) => {
                        trace!(
                            "Code: {:04X} to {:04X}",
                            bytecode.start_offset(),
                            bytecode.start_offset() + bytecode.size()
                        );
                    }
                    MemBlock::Data {
                        start_offset, data, ..
                    } => {
                        trace!(
                            "Mem : {:04X} to {:04X}",
                            start_offset,
                            start_offset + data.len()
                        );
                    }
                }
            }
        }
    }

    pub fn find_external_refs<'b>(&self, pe: &'b PortableExecutable) -> Vec<&'b Trampoline> {
        self.code.decoded().find_external_refs(pe)
    }

    pub fn find_external_calls<'b>(&self, pe: &'b PortableExecutable) -> Vec<&'b Trampoline> {
        self.code.decoded().find_external_calls(pe)
    }

    // Panic if we don't have a decoded bytecode already
    pub fn decoded(&self) -> &ByteCode {
        self.code.decoded()
    }

    pub fn have_header(&self) -> bool {
        self.have_header
    }

    pub fn set_usage(&mut self, usage: VertBufUsage) {
        debug_assert_ne!(usage, VertBufUsage::default());
        assert_eq!(self.usage, VertBufUsage::default());
        self.usage = usage;
    }
}

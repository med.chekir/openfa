// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::{analysis::ShCapabilities, draw_state::DrawState, VertBufUsage};
use absolute_unit::prelude::*;
use anyhow::{bail, ensure, Result};
use i386::Interpreter;
use peff::Trampoline;
use std::time::Instant;
use zerocopy::{AsBytes, Ref};

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
pub enum XformId {
    #[default]
    Static,
    Xform(u8),
}
impl XformId {
    pub fn new(index: usize) -> Result<Self> {
        Ok(Self::Xform(index.try_into()?))
    }

    pub fn as_gpu(&self) -> u32 {
        match self {
            Self::Static => 0xFF,
            Self::Xform(offset) => *offset as u32,
        }
    }
}

// Which input is selected for this script.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum XformDataInputKind {
    AfterBurner,
    BayDoorPos,
    BayOpen,
    CanardPos,
    GearDown,
    GearPos,
    Slats,
    SwingWing,
    VtAngle,
    VtOn,
    CurrentTicks,
}
impl XformDataInputKind {
    pub fn from_symref(sym: &Trampoline) -> Result<Self> {
        Ok(match sym.name.as_ref() {
            "_PLafterBurner" => Self::AfterBurner,
            "_PLbayDoorPos" => Self::BayDoorPos,
            "_PLbayOpen" => Self::BayOpen,
            "_PLcanardPos" => Self::CanardPos,
            "_PLgearDown" => Self::GearDown,
            "_PLgearPos" => Self::GearPos,
            "_PLslats" => Self::Slats,
            "_PLswingWing" => Self::SwingWing,
            "_PLvtAngle" => Self::VtAngle,
            "_PLvtOn" => Self::VtOn,
            "_currentTicks" => Self::CurrentTicks,
            _ => bail!("unknown xform input: {}", sym.name),
        })
    }

    pub fn capability(&self) -> ShCapabilities {
        match self {
            Self::BayDoorPos => ShCapabilities::XFORM_BAY_DOOR,
            Self::CanardPos => ShCapabilities::XFORM_TV_CANARD,
            Self::GearPos => ShCapabilities::XFORM_GEAR,
            Self::SwingWing => ShCapabilities::XFORM_SWING_WING,
            Self::VtAngle => ShCapabilities::XFORM_VT_ANGLE,
            Self::VtOn => ShCapabilities::XFORM_HAS_VT,
            Self::CurrentTicks => ShCapabilities::XFORM_CURRENT_TICKS,
            _ => ShCapabilities::empty(),
        }
    }

    pub fn get_value(&self, state: &DrawState, start: &Instant, now: &Instant) -> u32 {
        match self {
            Self::AfterBurner => state.x86_afterburner_state(),
            Self::BayDoorPos => state.x86_bay_position(),
            Self::BayOpen => state.x86_bay_open(),
            Self::CanardPos => state.x86_canard_position(),
            Self::GearDown => state.x86_gear_down(),
            Self::GearPos => state.x86_gear_position(),
            Self::Slats => state.x86_slats_down(),
            Self::SwingWing => state.x86_swing_wing_angle(),
            Self::VtAngle => state.x86_vtol_angle(),
            Self::VtOn => state.x86_vtol_on(),
            Self::CurrentTicks => (((*now - *start).as_millis() as u32) >> 4) & 0x0FFF,
        }
    }
}

// What input and where should it go in memory.
#[derive(Clone, Debug)]
pub struct XformDataInput {
    mem_location: u32,
    kind: XformDataInputKind,
}
impl XformDataInput {
    pub fn from_symref(sym: &Trampoline) -> Result<Self> {
        Ok(Self {
            mem_location: sym.mem_location,
            kind: XformDataInputKind::from_symref(sym)?,
        })
    }

    pub fn kind(&self) -> XformDataInputKind {
        self.kind
    }

    pub fn capabilities(&self) -> ShCapabilities {
        self.kind.capability()
    }
}

#[derive(Clone, Copy, Debug)]
pub enum XformCallInputKind {
    DoStartInterp,
    HardpointAngle,
    InsectWingAngle,
}
impl XformCallInputKind {
    pub fn from_callref(call: &Trampoline) -> Result<Self> {
        Ok(match call.name.as_ref() {
            "do_start_interp" => Self::DoStartInterp,
            "_InsectWingAngle@0" => Self::InsectWingAngle,
            "@HardpointAngle@4" => Self::HardpointAngle,
            _ => bail!("unknown xform call: {}", call.name),
        })
    }

    pub fn capability(&self) -> ShCapabilities {
        match self {
            Self::DoStartInterp => ShCapabilities::empty(),
            Self::HardpointAngle => ShCapabilities::XFORM_HARDPOINT_ANGLE,
            Self::InsectWingAngle => ShCapabilities::XFORM_INSECT_WING,
        }
    }
}

#[derive(Clone, Debug)]
pub struct XformCallInput {
    // mem_location: u32,
    // name: String,
    // arg_count: usize,
    kind: XformCallInputKind,
}
impl XformCallInput {
    pub fn from_callref(call: &Trampoline) -> Result<Self> {
        Ok(Self {
            // mem_location,
            kind: XformCallInputKind::from_callref(call)?,
        })
    }

    pub fn kind(&self) -> XformCallInputKind {
        self.kind
    }

    pub fn capabilities(&self) -> ShCapabilities {
        self.kind.capability()
    }
}

// O(256 bytes) in size. Small enough to clone to each instance, as most heavily used instances
// will not have significant xformer usage.
#[derive(Clone, Debug)]
pub struct Xformer {
    #[allow(unused)] // for debugging
    usage: VertBufUsage,
    base_xform: [i16; 6],
    interp: Interpreter,
    data_inputs: Vec<XformDataInput>,
    call_inputs: Vec<XformCallInput>,
    unmask_offset: u32,
}
impl Xformer {
    pub fn new(
        usage: VertBufUsage,
        base_xform: [i16; 6],
        interp: Interpreter,
        data_inputs: Vec<XformDataInput>,
        call_inputs: Vec<XformCallInput>,
        unmask_offset: u32,
    ) -> Self {
        Self {
            usage,
            base_xform,
            interp,
            data_inputs,
            call_inputs,
            unmask_offset,
        }
    }

    pub fn capabilities(&self) -> ShCapabilities {
        let a = self
            .data_inputs
            .iter()
            .map(|v| v.capabilities())
            .fold(ShCapabilities::empty(), |a, b| a | b);
        let b = self
            .call_inputs
            .iter()
            .map(|v| v.capabilities())
            .fold(ShCapabilities::empty(), |a, b| a | b);
        a | b
    }

    pub fn run(
        &mut self,
        state: &DrawState,
        start: &Instant,
        now: &Instant,
        out: &mut [f32; 6],
    ) -> Result<()> {
        for data_inp in &self.data_inputs {
            self.interp.map_value(
                data_inp.mem_location,
                data_inp.kind.get_value(state, start, now),
            );
        }
        for _call_inp in &self.call_inputs {
            // TODO: handle call-based inputs for xforms
            // self.interp
            //     .add_trampoline(call_inp.mem_location, &call_inp.name, call_inp.arg_count);
            // call_inp.kind.get_value(state);
            // println!("NOT MAPPING: {call_inp:?}");
        }

        self.interp
            .write_to_map(self.unmask_offset, self.base_xform.as_bytes())?;
        let result = self.interp.interpret()?;
        let (tramp, _) = result.ok_trampoline()?;
        ensure!(tramp == "do_start_interp", "unexpected interp result");

        let mut xformed_bytes = [0u8; 12];
        self.interp
            .read_from_map(self.unmask_offset, &mut xformed_bytes)?;
        let xformed = Ref::<&[u8], [i16]>::new_slice(xformed_bytes.as_slice()).expect("overlay");

        pub(crate) fn fa_rot16_to_angle(d: i16) -> Angle<Radians> {
            radians!(-f64::from(d) * std::f64::consts::PI / 8192_f64)
        }

        out[0] = meters!(feet!(xformed[0])).f32();
        out[1] = meters!(feet!(xformed[1])).f32();
        out[2] = meters!(feet!(xformed[2])).f32();
        out[3] = fa_rot16_to_angle(xformed[4]).f32();
        out[4] = fa_rot16_to_angle(xformed[3]).f32();
        out[5] = fa_rot16_to_angle(xformed[5]).f32();
        Ok(())
    }
}

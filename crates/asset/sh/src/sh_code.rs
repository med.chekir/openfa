// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    analysis::ShAnalysis,
    instrs::*,
    sh_x86::{iF0_X86Code, X86Message, X86Trampoline, X86Unknown},
    SHAPE_LOAD_BASE,
};
use ansi::{ansi, Color, StyleFlags};
use anyhow::{anyhow, bail, ensure, Result};
use nitrous::{Dict, List, Value};
use peff::{PeBuilder, PortableExecutable};
use reverse::{bs2s, s2bs};
use smallvec::SmallVec;
use std::{
    collections::HashMap,
    fmt::{Display, Formatter},
    str::FromStr,
};
use uuid::Uuid;

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub enum DrawSelection {
    NormalModel,
    DamageModel,
}

impl DrawSelection {
    pub fn is_damage(&self) -> bool {
        self == &DrawSelection::DamageModel
    }

    pub fn offset(&self) -> usize {
        match self {
            Self::NormalModel => 0,
            Self::DamageModel => 1,
        }
    }
}

// Encodes business logic for iterating instructions for drawing.
pub struct DrawVisitor<'a> {
    instrs: &'a [ShInstr],
    instr_map: &'a HashMap<Uuid, usize>,
    selection: DrawSelection,

    // Iteration State
    section_close_ptr: Uuid,
    damage_model_ptr: Uuid,
    end_ptr: Uuid,
    offset: usize,
}

impl<'a> DrawVisitor<'a> {
    pub fn new(
        instrs: &'a [ShInstr],
        instr_map: &'a HashMap<Uuid, usize>,
        selection: DrawSelection,
    ) -> Self {
        Self {
            instrs,
            instr_map,
            selection,
            section_close_ptr: Uuid::new_v4(),
            damage_model_ptr: Uuid::new_v4(),
            end_ptr: Uuid::new_v4(),
            offset: 0,
        }
    }

    pub fn ptr_to_offset(&self, code_ptr: &CodePtr) -> Result<usize> {
        Ok(self
            .instr_map
            .get(&code_ptr.as_uuid()?)
            .ok_or_else(|| anyhow!("unknown pointer {code_ptr:?}"))?
            .to_owned())
    }

    pub fn uuid_to_offset(&self, uuid: &Uuid) -> Result<usize> {
        Ok(self
            .instr_map
            .get(uuid)
            .ok_or_else(|| anyhow!("unknown uuid in to_offset {uuid:?}"))?
            .to_owned())
    }
}

impl<'a> Iterator for DrawVisitor<'a> {
    type Item = &'a ShInstr;

    fn next(&mut self) -> Option<Self::Item> {
        if self.offset >= self.instrs.len() {
            // BULLET.SH and others without an ToEndObj need a fallthrough
            return None;
        }
        let instr = &self.instrs[self.offset];

        // Done with Obj, as detected by low detail pointer
        if instr.uuid() == &self.section_close_ptr {
            self.offset = self.uuid_to_offset(&self.end_ptr).ok()?;
            return Some(instr);
        }
        // If we're not drawing damage and we reach the damage section, ff to end
        if instr.uuid() == &self.damage_model_ptr && self.selection != DrawSelection::DamageModel {
            self.offset = self.uuid_to_offset(&self.end_ptr).ok()?;
            return Some(instr);
        }

        //println!("At: {:3} => {}", pc.instr_offset, instr.show());
        match instr.detail() {
            InstrDetail::PtrToObjEnd(end) => {
                self.end_ptr = end.code_ptr().as_uuid().ok()?;
                self.offset += 1;
            }
            InstrDetail::Jump(jump) => self.offset = self.ptr_to_offset(jump.code_ptr()).ok()?,
            InstrDetail::JumpToDamage(jump) => {
                self.damage_model_ptr = jump.code_ptr().as_uuid().ok()?;
                if self.selection == DrawSelection::DamageModel {
                    self.offset = self.ptr_to_offset(jump.code_ptr()).ok()?;
                } else {
                    self.offset += 1;
                }
            }
            InstrDetail::JumpToDetail(detail) => {
                self.section_close_ptr = detail.code_ptr().as_uuid().ok()?;
                self.offset += 1;
            }
            InstrDetail::JumpToLOD(lod) => {
                self.section_close_ptr = lod.code_ptr().as_uuid().ok()?;
                self.offset += 1;
            }
            InstrDetail::JumpToFrame(frameset) => {
                self.offset = self
                    .ptr_to_offset(frameset.frame_pointers().next().unwrap())
                    .ok()?;
            }
            _ => {
                if *instr.uuid() == self.end_ptr {
                    return None;
                }
                self.offset += 1
            }
        }

        Some(instr)
    }
}

#[derive(Clone, Copy, Debug)]
pub enum CodePtr {
    // Computed absolute code offset and uuid of the target instruction.
    Uuid(Uuid),

    // Supports backwards jumps, interpret as i16
    Relative(i16),

    // Supports backwards jumps, interpret as i32
    RelativeLong(i32),

    // Only forwards, interpret as u16
    Offset(u16),
}

impl CodePtr {
    pub fn new_rel16_bs(data: &[u8]) -> Self {
        let relative = i16::from_le_bytes(
            data[0..2]
                .try_into()
                .expect("enough data to get a code pointer (i16)"),
        );
        Self::Relative(relative)
    }

    pub fn new_rel16(raw: i16) -> Self {
        Self::Relative(raw)
    }

    pub fn new_rel32_bs(data: &[u8]) -> Self {
        let relative = i32::from_le_bytes(
            data[0..4]
                .try_into()
                .expect("enough data to get a code pointer (i32)"),
        );
        Self::RelativeLong(relative)
    }

    pub fn new_rel32(raw: i32) -> Self {
        Self::RelativeLong(raw)
    }

    pub fn new_off16(data: &[u8]) -> Self {
        let offset = u16::from_le_bytes(
            data[0..2]
                .try_into()
                .expect("enough data to get a code pointer (u16)"),
        );
        Self::Offset(offset)
    }

    pub(crate) fn compute_absolute(&self, base: usize) -> Result<usize> {
        Ok(match *self {
            Self::Offset(offset) => base + offset as usize,
            Self::Relative(relative) => (base as i64 + relative as i64) as usize,
            Self::RelativeLong(relative) => (base as i64 + relative as i64) as usize,
            _ => bail!("attempting to link an already linked code pointer"),
        })
    }

    pub(crate) fn link(
        &mut self,
        base: usize,
        uuid_map: &mut HashMap<usize, (Uuid, usize)>,
    ) -> Result<()> {
        let absolute = self.compute_absolute(base)?;
        let (uuid, usages) = uuid_map
            .get_mut(&absolute)
            .ok_or_else(|| anyhow!("code pointer does not point to anything"))?;
        *usages += 1;
        *self = Self::Uuid(*uuid);
        Ok(())
    }

    fn unlink_offset(&mut self, offset_map: &HashMap<Uuid, usize>) -> Result<usize> {
        let uuid = self.as_uuid()?;
        Ok(*offset_map
            .get(&uuid)
            .ok_or_else(|| anyhow!("code pointer does not match any instr uuid"))?)
    }

    pub(crate) fn unlink_rel16(
        &mut self,
        base: usize,
        offset_map: &HashMap<Uuid, usize>,
    ) -> Result<()> {
        let offset = self.unlink_offset(offset_map)?;
        *self = Self::Relative((offset as i64 - base as i64).try_into()?);
        Ok(())
    }

    pub(crate) fn unlink_rel32(
        &mut self,
        base: usize,
        offset_map: &HashMap<Uuid, usize>,
    ) -> Result<()> {
        let offset = self.unlink_offset(offset_map)?;
        *self = Self::RelativeLong((offset as i64 - base as i64).try_into()?);
        Ok(())
    }

    pub(crate) fn unlink_off16(
        &mut self,
        base: usize,
        offset_map: &HashMap<Uuid, usize>,
    ) -> Result<()> {
        let offset = self.unlink_offset(offset_map)?;
        ensure!(offset > base, "only forward jumps allowed here");
        *self = Self::Offset((offset - base).try_into()?);
        Ok(())
    }

    pub fn as_uuid(&self) -> Result<Uuid> {
        Ok(match *self {
            Self::Uuid(uuid) => uuid,
            _ => bail!("attempting to unlink an unlinked code pointer"),
        })
    }

    pub fn as_rel16(&self) -> Result<i16> {
        if let Self::Relative(v) = self {
            return Ok(*v);
        }
        bail!("not a rel16 code pointer");
    }

    pub fn as_rel32(&self) -> Result<i32> {
        if let Self::RelativeLong(v) = self {
            return Ok(*v);
        }
        bail!("not a rel32 code pointer");
    }

    pub fn as_off16(&self) -> Result<u16> {
        if let Self::Offset(v) = self {
            return Ok(*v);
        }
        bail!("not an off16 code pointer: {self}");
    }

    pub fn as_value(&self) -> Value {
        match self {
            Self::Uuid(uuid) => uuid,
            _ => panic!("attempted to get value of an unlinked code pointer"),
        }
        .to_string()
        .into()
    }

    pub fn from_value(v: &Value) -> Result<Self> {
        let uuid = Uuid::from_str(v.to_str()?)?;
        Ok(Self::Uuid(uuid))
    }

    pub fn display_content(&self, offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        Ok(format!(
            "{:04X}",
            offset_map
                .get(&self.as_uuid()?)
                .ok_or_else(|| anyhow!("UUID not in offset map"))?
        ))
    }
}

impl Display for CodePtr {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Uuid(uuid) => write!(f, "UUID({})", uuid),
            Self::Offset(offset) => write!(f, "Offset({})", offset),
            Self::Relative(rel) => write!(f, "Rel16({})", rel),
            Self::RelativeLong(rel) => write!(f, "Rel32({})", rel),
        }
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum InstrMagic {
    // just bare data section, no header
    Bare,
    // Single byte header with second byte used
    Byte(u8),
    // Two byte header with second byte zeroed
    Word(u8),
}

pub trait ShInstrSpec: Sized {
    const MAGIC: InstrMagic;
    const SIZE: Option<usize>;
    const NAME: &'static str;
    const SHORT_NAME: &'static str = Self::NAME;
    const COLOR: Color = Color::White;
    const STYLE: StyleFlags = StyleFlags::empty();
    const NOTES: &'static str = "";

    fn extract(idata: &[u8]) -> Result<(InstrDetail, usize)> {
        if let Some(size) = Self::SIZE {
            Ok((InstrDetail::NoDetail(idata[..size].into()), size))
        } else {
            panic!(
                "Attempted generic extract of an instruction without a fixed size for instr {}",
                Self::NAME
            )
        }
    }

    fn to_bytes(&self, _buf: &mut [u8]) -> Result<()> {
        Ok(())
    }

    fn link(
        &mut self,
        _offset: usize,
        _size: usize,
        _uuid_map: &mut HashMap<usize, (Uuid, usize)>,
    ) -> Result<()> {
        Ok(())
    }

    fn unlink(
        &mut self,
        _offset: usize,
        _size: usize,
        _offset_map: &HashMap<Uuid, usize>,
    ) -> Result<()> {
        Ok(())
    }

    fn as_value(&self, _map: &mut Dict) -> Result<()> {
        Ok(())
    }

    fn display_content(&self, _offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        Ok("Whoops: Missing display_content implementation!".into())
    }
}

// An enum that may be any kind of Instr specialization, allowing us to hold the details of any
// specific instruction inside our more generic common instruction wrapper. Any instruction that
// we have no further detail on than the size does not need a detail here or `extract` method.
#[derive(Clone, Debug)]
pub enum InstrDetail {
    NoDetail(SmallVec<[u8; 20]>),
    Unk06(i06),
    Unmask(i12_Unmask),
    UnmaskLong(i6E_UnmaskLong),
    JumpToFrame(i40_JumpToFrame),
    SourceName(i42_SourceName),
    Jump(i48_Jump),
    VertexBuffer(i82_VertexBuffer),
    JumpToDetail(iA6_JumpToDetail),
    JumpToDamage(iAC_JumpToDamage),
    XformUnmask(iC4_XformUnmask),
    XformUnmaskLong(iC6_XformUnmaskLong),
    JumpToLOD(iC8_JumpToLOD),
    TextureIndex(iE0_TextureIndex),
    TextureFile(iE2_TextureFile),
    X86Trampoline(X86Trampoline),
    X86Message(X86Message),
    X86Code(iF0_X86Code),
    PtrToObjEnd(iF2_PtrToObjEnd),
    VertexInfo(iF6_VertexInfo),
    Face(iFC_Face),
}

macro_rules! impl_for_all_instr {
    ($self:expr, $f:ident ($($args:expr),*), $default:expr) => {
        match $self {
            InstrDetail::Unmask(instr) => instr.$f($($args),*)?,
            InstrDetail::Unk06(instr) => instr.$f($($args),*)?,
            InstrDetail::UnmaskLong(instr) => instr.$f($($args),*)?,
            InstrDetail::JumpToFrame(instr) => instr.$f($($args),*)?,
            InstrDetail::SourceName(instr) => instr.$f($($args),*)?,
            InstrDetail::Jump(instr) => instr.$f($($args),*)?,
            InstrDetail::VertexBuffer(instr) => instr.$f($($args),*)?,
            InstrDetail::JumpToDetail(instr) => instr.$f($($args),*)?,
            InstrDetail::JumpToDamage(instr) => instr.$f($($args),*)?,
            InstrDetail::XformUnmask(instr) => instr.$f($($args),*)?,
            InstrDetail::XformUnmaskLong(instr) => instr.$f($($args),*)?,
            InstrDetail::JumpToLOD(instr) => instr.$f($($args),*)?,
            InstrDetail::TextureIndex(instr) => instr.$f($($args),*)?,
            InstrDetail::TextureFile(instr) => instr.$f($($args),*)?,
            InstrDetail::X86Trampoline(instr) => instr.$f($($args),*)?,
            InstrDetail::X86Message(instr) => instr.$f($($args),*)?,
            InstrDetail::X86Code(instr) => instr.$f($($args),*)?,
            InstrDetail::PtrToObjEnd(instr) => instr.$f($($args),*)?,
            InstrDetail::VertexInfo(instr) => instr.$f($($args),*)?,
            InstrDetail::Face(instr) => instr.$f($($args),*)?,
            InstrDetail::NoDetail(_) => $default
        }
    }
}

impl InstrDetail {
    pub fn from_value(name: &str, map: &Dict) -> Result<(Self, usize)> {
        Ok(match name {
            i12_Unmask::NAME => i12_Unmask::from_value(map)?,
            i6E_UnmaskLong::NAME => i6E_UnmaskLong::from_value(map)?,
            i40_JumpToFrame::NAME => i40_JumpToFrame::from_value(map)?,
            i42_SourceName::NAME => i42_SourceName::from_value(map)?,
            i48_Jump::NAME => i48_Jump::from_value(map)?,
            i82_VertexBuffer::NAME => i82_VertexBuffer::from_value(map)?,
            iA6_JumpToDetail::NAME => iA6_JumpToDetail::from_value(map)?,
            iAC_JumpToDamage::NAME => iAC_JumpToDamage::from_value(map)?,
            iC4_XformUnmask::NAME => iC4_XformUnmask::from_value(map)?,
            iC6_XformUnmaskLong::NAME => iC6_XformUnmaskLong::from_value(map)?,
            iC8_JumpToLOD::NAME => iC8_JumpToLOD::from_value(map)?,
            iE0_TextureIndex::NAME => iE0_TextureIndex::from_value(map)?,
            iE2_TextureFile::NAME => iE2_TextureFile::from_value(map)?,
            X86Trampoline::NAME => X86Trampoline::from_value(map)?,
            X86Message::NAME => X86Message::from_value(map)?,
            iF0_X86Code::NAME => iF0_X86Code::from_value(map)?,
            iF2_PtrToObjEnd::NAME => iF2_PtrToObjEnd::from_value(map)?,
            iF6_VertexInfo::NAME => iF6_VertexInfo::from_value(map)?,
            iFC_Face::NAME => iFC_Face::from_value(map)?,
            // Note: this gets decoded for real in the parent, along with trampoline.
            _ => {
                let packed_data = map.try_get("data")?.to_str()?;
                let data = s2bs(packed_data);
                let code_size = data.len();
                (InstrDetail::NoDetail(SmallVec::from(data)), code_size)
            }
        })
    }

    fn display_content(
        &self,
        magic: InstrMagic,
        offset_map: &HashMap<Uuid, usize>,
    ) -> Result<String> {
        Ok(if let Self::NoDetail(raw) = self {
            let start = match magic {
                InstrMagic::Bare => 0,
                InstrMagic::Byte(_) => 1,
                InstrMagic::Word(_) => 2,
            };
            format!(
                "{}{}{}",
                ansi().fg(Color::Red).style(StyleFlags::empty()),
                bs2s(&raw[start..]),
                ansi(),
            )
        } else {
            impl_for_all_instr!(self, display_content(offset_map), "".to_owned())
        })
    }
}

pub fn trampoline_by_name(name: &str, map: &Dict, raw: usize) -> Result<ShInstr> {
    Ok(match name {
        X86Message::NAME => ShInstr::from_value_typed::<X86Message>(map, raw)?,
        X86Unknown::NAME => ShInstr::from_value_typed::<X86Unknown>(map, raw)?,
        X86Trampoline::NAME => ShInstr::from_value_typed::<X86Trampoline>(map, raw)?,
        i00_EndObject::NAME => ShInstr::from_value_typed::<i00_EndObject>(map, raw)?,
        i01_EndShape::NAME => ShInstr::from_value_typed::<i01_EndShape>(map, raw)?,
        i06::NAME => ShInstr::from_value_typed::<i06>(map, raw)?,
        i08::NAME => ShInstr::from_value_typed::<i08>(map, raw)?,
        i0C::NAME => ShInstr::from_value_typed::<i0C>(map, raw)?,
        i0E::NAME => ShInstr::from_value_typed::<i0E>(map, raw)?,
        i10::NAME => ShInstr::from_value_typed::<i10>(map, raw)?,
        i12_Unmask::NAME => ShInstr::from_value_typed::<i12_Unmask>(map, raw)?,
        i1E_Pad::NAME => ShInstr::from_value_typed::<i1E_Pad>(map, raw)?,
        i2E::NAME => ShInstr::from_value_typed::<i2E>(map, raw)?,
        i38::NAME => ShInstr::from_value_typed::<i38>(map, raw)?,
        i3A::NAME => ShInstr::from_value_typed::<i3A>(map, raw)?,
        i40_JumpToFrame::NAME => ShInstr::from_value_typed::<i40_JumpToFrame>(map, raw)?,
        i42_SourceName::NAME => ShInstr::from_value_typed::<i42_SourceName>(map, raw)?,
        i44::NAME => ShInstr::from_value_typed::<i44>(map, raw)?,
        i46::NAME => ShInstr::from_value_typed::<i46>(map, raw)?,
        i48_Jump::NAME => ShInstr::from_value_typed::<i48_Jump>(map, raw)?,
        i4E::NAME => ShInstr::from_value_typed::<i4E>(map, raw)?,
        i50::NAME => ShInstr::from_value_typed::<i50>(map, raw)?,
        i66::NAME => ShInstr::from_value_typed::<i66>(map, raw)?,
        i68::NAME => ShInstr::from_value_typed::<i68>(map, raw)?,
        i6C::NAME => ShInstr::from_value_typed::<i6C>(map, raw)?,
        i6E_UnmaskLong::NAME => ShInstr::from_value_typed::<i6E_UnmaskLong>(map, raw)?,
        i72::NAME => ShInstr::from_value_typed::<i72>(map, raw)?,
        i76::NAME => ShInstr::from_value_typed::<i76>(map, raw)?,
        i78::NAME => ShInstr::from_value_typed::<i78>(map, raw)?,
        i7A::NAME => ShInstr::from_value_typed::<i7A>(map, raw)?,
        i82_VertexBuffer::NAME => ShInstr::from_value_typed::<i82_VertexBuffer>(map, raw)?,
        i96::NAME => ShInstr::from_value_typed::<i96>(map, raw)?,
        iA6_JumpToDetail::NAME => ShInstr::from_value_typed::<iA6_JumpToDetail>(map, raw)?,
        iAC_JumpToDamage::NAME => ShInstr::from_value_typed::<iAC_JumpToDamage>(map, raw)?,
        iB2::NAME => ShInstr::from_value_typed::<iB2>(map, raw)?,
        iB8::NAME => ShInstr::from_value_typed::<iB8>(map, raw)?,
        iBC::NAME => ShInstr::from_value_typed::<iBC>(map, raw)?,
        iC4_XformUnmask::NAME => ShInstr::from_value_typed::<iC4_XformUnmask>(map, raw)?,
        iC6_XformUnmaskLong::NAME => ShInstr::from_value_typed::<iC6_XformUnmaskLong>(map, raw)?,
        iC8_JumpToLOD::NAME => ShInstr::from_value_typed::<iC8_JumpToLOD>(map, raw)?,
        iCA::NAME => ShInstr::from_value_typed::<iCA>(map, raw)?,
        iCE::NAME => ShInstr::from_value_typed::<iCE>(map, raw)?,
        iD0::NAME => ShInstr::from_value_typed::<iD0>(map, raw)?,
        iD2::NAME => ShInstr::from_value_typed::<iD2>(map, raw)?,
        iDA::NAME => ShInstr::from_value_typed::<iDA>(map, raw)?,
        iDC::NAME => ShInstr::from_value_typed::<iDC>(map, raw)?,
        iE0_TextureIndex::NAME => ShInstr::from_value_typed::<iE0_TextureIndex>(map, raw)?,
        iE2_TextureFile::NAME => ShInstr::from_value_typed::<iE2_TextureFile>(map, raw)?,
        iE4::NAME => ShInstr::from_value_typed::<iE4>(map, raw)?,
        iE6::NAME => ShInstr::from_value_typed::<iE6>(map, raw)?,
        iE8::NAME => ShInstr::from_value_typed::<iE8>(map, raw)?,
        iEA::NAME => ShInstr::from_value_typed::<iEA>(map, raw)?,
        iEE::NAME => ShInstr::from_value_typed::<iEE>(map, raw)?,
        iF0_X86Code::NAME => ShInstr::from_value_typed::<iF0_X86Code>(map, raw)?,
        iF2_PtrToObjEnd::NAME => ShInstr::from_value_typed::<iF2_PtrToObjEnd>(map, raw)?,
        iF6_VertexInfo::NAME => ShInstr::from_value_typed::<iF6_VertexInfo>(map, raw)?,
        iFC_Face::NAME => ShInstr::from_value_typed::<iFC_Face>(map, raw)?,
        iFF_Header::NAME => ShInstr::from_value_typed::<iFF_Header>(map, raw)?,
        _ => panic!("unknown instruction name {name}"),
    })
}

// Every instruction has some common properties that we want to track in a generic way.
// There are also details for each instruction that we may want to extract out of the
// code stream for use when evaluating the code later.
#[derive(Clone, Debug)]
pub struct ShInstr {
    magic: InstrMagic,
    name: &'static str,
    short_name: &'static str,
    color: Color,
    style: StyleFlags,

    // Pointer to raw location and length at that location.
    offset: usize,
    pub(crate) size: usize,

    // generated per parse, not consistent between runs
    // Target usages is the number of code pointers that point at this instruction.
    uuid: Uuid,
    uuid_target_usages: usize,

    detail: InstrDetail,
}

impl ShInstr {
    pub fn parse<T: ShInstrSpec>(
        (code, offset, instrs, end_obj_addr): (&[u8], &mut usize, &mut Vec<ShInstr>, Option<usize>),
    ) -> Result<()> {
        if let Some(size) = T::SIZE {
            ensure!(size <= code.len() - *offset, "no room for instruction");
        }
        match T::MAGIC {
            InstrMagic::Bare => {}
            InstrMagic::Byte(b) => {
                ensure!(code[*offset] == b, "in common parse with wrong byte magic")
            }
            InstrMagic::Word(b) => {
                ensure!(code[*offset] == b, "in common parse with wrong word magic");
                ensure!(code[*offset + 1] == 0, "in common parse with non-zero post");
            }
        }
        assert_ne!(T::MAGIC, iF0_X86Code::MAGIC, "parse is not for code");
        let (mut detail, mut code_size) = T::extract(&code[*offset..])?;

        // If the end_obj lands inside the decoded instruction, we've discovered
        // one of the random dead blocks before 2EndO, specifically SOLDIER & CATGUY.
        if let Some(addr) = end_obj_addr {
            if addr > *offset && addr < *offset + code_size {
                // Grab the unknown region and try again
                instrs.push(Self::new_for::<X86Unknown>(
                    *offset,
                    addr - *offset,
                    InstrDetail::NoDetail(code[*offset..addr].into()),
                ));
                *offset = addr;
                return Ok(());
            }
        } else if T::MAGIC == InstrMagic::Byte(0x00) && code_size > 80 && code[*offset + 2] == 0x10
        {
            // BULLET and EXP don't have a 2EndO instruction. Bullet has more
            // content that is pointed to by instructions we understand. EXP
            // probably will too once we understand more. For now look for
            // the 0x10 to distinguish the cases and just hard-code behavior.
            code_size = 18;
            if let InstrDetail::NoDetail(data) = &mut detail {
                data.truncate(18);
            }
        }

        // Build the instruction
        let code_offset = *offset;
        *offset += code_size;
        let instr = Self {
            magic: T::MAGIC,
            name: T::NAME,
            short_name: T::SHORT_NAME,
            color: T::COLOR,
            style: T::STYLE,
            offset: code_offset,
            size: code_size,
            uuid: Uuid::new_v4(),
            uuid_target_usages: 0,
            detail,
        };
        instrs.push(instr);
        Ok(())
    }

    pub fn to_value(&self, code_offset: usize) -> Result<Value> {
        let mut map = Dict::default();
        map.insert("name", self.name.into());
        map.insert(
            "file_offset0",
            format!("0x{:04X}", code_offset + self.offset).into(),
        );
        map.insert("code_offset0", format!("0x{:04X}", self.offset).into());
        if self.uuid_target_usages > 0 {
            map.insert("uuid", self.uuid.to_string().into());
        }
        impl_for_all_instr!(&self.detail, as_value(&mut map), ());
        if let InstrDetail::NoDetail(data) = &self.detail {
            map.insert("data", bs2s(data).into());
        }
        Ok(Value::Dict(map))
    }

    // Create an instruction from it's Value representation.
    pub fn from_value(v: &Value, byte_offset: usize) -> Result<Self> {
        let map = v.to_dict()?;
        let name = map
            .get("name")
            .ok_or_else(|| anyhow!("expected name"))?
            .to_str()?;
        trampoline_by_name(name, map, byte_offset)
    }

    // Create an instruction of known type from Dict.
    pub(crate) fn from_value_typed<T: ShInstrSpec>(map: &Dict, byte_offset: usize) -> Result<Self> {
        let name = map
            .get("name")
            .ok_or_else(|| anyhow!("expected name"))?
            .to_str()?;
        let uuid = if let Some(uuid_str) = map.get("uuid") {
            Uuid::from_str(uuid_str.to_str()?)?
        } else {
            // Not referred to, so generate a new one.
            Uuid::new_v4()
        };
        let (detail, code_size) = InstrDetail::from_value(name, map)?;

        // Make sure the instr magic reflects the code header, if not present.
        let magic = if let InstrDetail::X86Code(x86) = &detail {
            x86.actual_magic()
        } else {
            T::MAGIC
        };

        Ok(Self {
            magic,
            name: T::NAME,
            short_name: T::SHORT_NAME,
            color: T::COLOR,
            style: T::STYLE,
            offset: byte_offset,
            size: code_size,
            uuid,
            uuid_target_usages: 0,
            detail,
        })
    }

    // Allocates a vec. Most users should use to_bytes_inline with a slice in a
    // pre-allocated byte vec of the right size for the whole file.
    pub fn to_bytes(&self) -> Result<Vec<u8>> {
        let mut out = vec![0u8; self.size];
        self.to_bytes_inline(&mut out)?;
        Ok(out)
    }

    pub fn to_bytes_inline(&self, s: &mut [u8]) -> Result<()> {
        // Note: to_bytes below gets the full raw range to fill and should ensure
        // the magic is set up correctly if not handled by the following.
        match self.magic {
            InstrMagic::Bare => {}
            InstrMagic::Byte(b) => s[0] = b,
            InstrMagic::Word(b) => {
                s[0] = b;
                s[1] = 0;
            }
        }
        if let InstrDetail::NoDetail(bytes) = &self.detail {
            s.copy_from_slice(bytes);
        } else {
            impl_for_all_instr!(&self.detail, to_bytes(s), ());
        }
        Ok(())
    }

    // FIXME: slice above this level
    // This is called on the instruction after it was created by from_value and from_value_detail.
    pub fn reconstruct_bytestream(&self, raw: &mut [u8]) -> Result<()> {
        // Note: to_bytes below gets the full raw range to fill and should ensure
        // the magic is set up correctly if not handled by the following.
        match self.magic {
            InstrMagic::Bare => {}
            InstrMagic::Byte(b) => raw[self.offset] = b,
            InstrMagic::Word(b) => {
                raw[self.offset] = b;
                raw[self.offset + 1] = 0;
            }
        }
        impl_for_all_instr!(
            &self.detail,
            to_bytes(&mut raw[self.offset..self.offset + self.size]),
            ()
        );
        Ok(())
    }

    pub fn magic(&self) -> InstrMagic {
        self.magic
    }

    // Offset in bytes from start of CODE section.
    pub fn instr_offset(&self) -> usize {
        self.offset
    }

    // Size in bytes of this instruction in the CODE block.
    pub fn instr_size(&self) -> usize {
        self.size
    }

    pub fn uuid(&self) -> &Uuid {
        &self.uuid
    }

    pub(crate) fn new_magic<T: ShInstrSpec>(
        magic: InstrMagic,
        offset: usize,
        size: usize,
        detail: InstrDetail,
    ) -> Self {
        assert!(size > 0);
        Self {
            magic,
            name: T::NAME,
            short_name: T::SHORT_NAME,
            color: T::COLOR,
            style: T::STYLE,
            offset,
            size,
            uuid: Uuid::new_v4(),
            uuid_target_usages: 0,
            detail,
        }
    }

    pub(crate) fn new_for<T: ShInstrSpec>(offset: usize, size: usize, detail: InstrDetail) -> Self {
        Self::new_magic::<T>(T::MAGIC, offset, size, detail)
    }

    pub fn detail(&self) -> &InstrDetail {
        &self.detail
    }

    pub fn detail_mut(&mut self) -> &mut InstrDetail {
        &mut self.detail
    }

    pub fn display_content(&self, offset_map: &HashMap<Uuid, usize>) -> Result<String> {
        // @{addr} SHORT: {inst header}| {raw data or fields}

        let hdr = match self.magic {
            InstrMagic::Bare => "     ".to_owned(),
            InstrMagic::Byte(b) => format!("{:02X}   ", b),
            InstrMagic::Word(b) => format!("{:02X} 00", b),
        };
        Ok(format!(
            "@{:04X} {}{}{}: {}{}{}| {}",
            self.offset,
            ansi().fg(self.color).style(self.style | StyleFlags::BOLD),
            self.short_name,
            ansi(),
            ansi().white().dimmed(),
            hdr,
            ansi(),
            self.detail.display_content(self.magic, offset_map)?
        ))
    }
}

/// Parse bytes into Shape codes and provide them in useful formats.
#[derive(Clone, Debug)]
pub struct ShCode {
    time_date_stamp: u32,
    instrs: Vec<ShInstr>,

    // map instruction uuid to the offset of the instruction in `instrs`
    instr_map: HashMap<Uuid, usize>,
    code_start_in_pe: usize,

    analysis: ShAnalysis,
    damage_analysis: Option<ShAnalysis>,
}

impl ShCode {
    pub fn from_bytes(file_contents: &[u8]) -> Result<Self> {
        let mut pe = PortableExecutable::from_bytes(file_contents)?;
        pe.relocate(SHAPE_LOAD_BASE)?;

        // File to raw code
        let mut instrs = Self::disassemble_code_section(&pe)?;

        // Link and build map
        Self::link(&mut instrs)?;
        let mut instr_map = HashMap::new();
        for (i, instr) in instrs.iter().enumerate() {
            instr_map.insert(*instr.uuid(), i);
        }

        // Run the analysis pass to build the Analysis
        let (analysis, damage_analysis) = ShAnalysis::analyze(&pe, &instr_map, &mut instrs)?;

        Ok(Self {
            time_date_stamp: pe.coff_header().time_date_stamp(),
            instrs,
            instr_map,
            code_start_in_pe: pe.code_section_header().pointer_to_raw_data() as usize,
            analysis,
            damage_analysis,
        })
    }

    pub fn code(&self) -> &[ShInstr] {
        &self.instrs
    }

    pub fn code_mut(&mut self) -> &mut [ShInstr] {
        &mut self.instrs
    }

    pub fn ptr_to_offset(&self, code_ptr: &CodePtr) -> Result<usize> {
        self.uuid_to_offset(&code_ptr.as_uuid()?)
    }

    pub fn ptr_to_instr(&self, code_ptr: &CodePtr) -> Result<&ShInstr> {
        Ok(&self.instrs[self.ptr_to_offset(code_ptr)?])
    }

    pub fn uuid_to_offset(&self, uuid: &Uuid) -> Result<usize> {
        Ok(self
            .instr_map
            .get(uuid)
            .ok_or_else(|| anyhow!("unknown uuid in to_offset {uuid:?}"))?
            .to_owned())
    }

    fn link(instrs: &mut [ShInstr]) -> Result<()> {
        // Create a map from the start of each instruction to the instr's uuid.
        let mut uuid_map = HashMap::new();
        for instr in instrs.iter() {
            uuid_map.insert(instr.offset, (instr.uuid, 0usize));
        }

        // Visit each instruction and link any code pointers we know about.
        for instr in instrs.iter_mut() {
            impl_for_all_instr!(
                &mut instr.detail,
                link(instr.offset, instr.size, &mut uuid_map),
                ()
            );
        }

        // Write back usage data to each instruction.
        for instr in instrs.iter_mut() {
            if let Some((uuid, usages)) = uuid_map.get(&instr.offset) {
                assert_eq!(*uuid, instr.uuid);
                instr.uuid_target_usages = *usages;
            }
        }

        Ok(())
    }

    // Create a map from the uuid to the start of each instruction.
    fn make_offset_map(code: &[ShInstr]) -> HashMap<Uuid, usize> {
        let mut offset_map = HashMap::new();
        for instr in code.iter() {
            offset_map.insert(instr.uuid, instr.offset);
        }
        offset_map
    }

    fn unlink(code: &mut [ShInstr]) -> Result<()> {
        let offset_map = Self::make_offset_map(code);

        // Visit each instruction, recreating offsets
        for instr in code.iter_mut() {
            impl_for_all_instr!(
                &mut instr.detail,
                unlink(instr.offset, instr.size, &offset_map),
                ()
            );
        }

        Ok(())
    }

    fn is_end_sh_marker(code: &[u8]) -> bool {
        code == [1, 2, 3, 2, 1, 2, 3, 2, 1]
    }

    // Return the allocated code and EndOfSh code.
    fn remove_end_of_sh(pe: &PortableExecutable) -> Result<(Vec<u8>, usize, ShInstr)> {
        let full_code = pe.code().to_owned();
        let code = &full_code[0..pe.trampolines_start()];

        let mut end_offset = code.len() - 9;
        while end_offset > 0 && !Self::is_end_sh_marker(&code[end_offset..end_offset + 9]) {
            end_offset -= 1;
        }
        assert_ne!(end_offset, 0);

        let mut tmp = Vec::new();
        let mut tmp_off = end_offset;
        Self::disassemble_one(code.len(), pe, (code, &mut tmp_off, &mut tmp, None))?;
        let end_of_sh = tmp.drain(..).next().unwrap();

        Ok((full_code, end_offset, end_of_sh))
    }

    fn get_end_obj_offset(instrs: &[ShInstr]) -> Option<usize> {
        if instrs.is_empty() {
            return None;
        }

        let last = instrs.last().unwrap();
        if last.magic == iF2_PtrToObjEnd::MAGIC {
            match &last.detail {
                InstrDetail::PtrToObjEnd(p2oe) => {
                    p2oe.code_ptr.compute_absolute(last.offset + last.size).ok()
                }
                _ => None,
            }
        } else {
            None
        }
    }

    fn disassemble_code_section(pe: &PortableExecutable) -> Result<Vec<ShInstr>> {
        let (full_code, end_offset, end_of_sh) = Self::remove_end_of_sh(pe)?;
        let code = &full_code[0..end_offset];
        let mut offset = 0;
        let mut obj_end_addr = None;
        let mut instrs = Vec::new();
        while offset < code.len() {
            Self::disassemble_one(
                code.len(),
                pe,
                (code, &mut offset, &mut instrs, obj_end_addr),
            )?;

            // Detect the start of the obj_end instr
            if let Some(v) = Self::get_end_obj_offset(&instrs) {
                obj_end_addr = Some(v);
            }
        }
        ensure!(offset == code.len(), "finished decoding unaligned!");
        offset += end_of_sh.size;
        instrs.push(end_of_sh);

        for trampoline in pe.trampolines() {
            let instr = ShInstr::new_for::<X86Trampoline>(
                offset,
                X86Trampoline::SIZE.unwrap(),
                InstrDetail::X86Trampoline(X86Trampoline::new(trampoline)),
            );
            instrs.push(instr);
            offset += X86Trampoline::SIZE.unwrap();
        }

        Ok(instrs)
    }

    pub(crate) fn disassemble_one(
        end_offset: usize,
        pe: &PortableExecutable,
        state: (&[u8], &mut usize, &mut Vec<ShInstr>, Option<usize>),
    ) -> Result<()> {
        match state.0[*state.1] {
            0x00 => ShInstr::parse::<i00_EndObject>(state)?,
            0x01 => ShInstr::parse::<i01_EndShape>(state)?,
            0x06 => ShInstr::parse::<i06>(state)?,
            0x08 => ShInstr::parse::<i08>(state)?,
            0x0C => ShInstr::parse::<i0C>(state)?,
            0x0E => ShInstr::parse::<i0E>(state)?,
            0x10 => ShInstr::parse::<i10>(state)?,
            0x12 => ShInstr::parse::<i12_Unmask>(state)?,
            0x1E => ShInstr::parse::<i1E_Pad>(state)?,
            0x2E => ShInstr::parse::<i2E>(state)?,
            0x38 => ShInstr::parse::<i38>(state)?,
            0x3A => ShInstr::parse::<i3A>(state)?,
            0x40 => ShInstr::parse::<i40_JumpToFrame>(state)?,
            0x42 => ShInstr::parse::<i42_SourceName>(state)?,
            0x44 => ShInstr::parse::<i44>(state)?,
            0x46 => ShInstr::parse::<i46>(state)?,
            0x48 => ShInstr::parse::<i48_Jump>(state)?,
            0x4E => ShInstr::parse::<i4E>(state)?,
            0x50 => ShInstr::parse::<i50>(state)?,
            0x66 => ShInstr::parse::<i66>(state)?,
            0x68 => ShInstr::parse::<i68>(state)?,
            0x6C => ShInstr::parse::<i6C>(state)?,
            0x6E => ShInstr::parse::<i6E_UnmaskLong>(state)?,
            0x72 => ShInstr::parse::<i72>(state)?,
            0x76 => ShInstr::parse::<i76>(state)?,
            0x78 => ShInstr::parse::<i78>(state)?,
            0x7A => ShInstr::parse::<i7A>(state)?,
            0x82 => ShInstr::parse::<i82_VertexBuffer>(state)?,
            0x96 => ShInstr::parse::<i96>(state)?,
            0xA6 => ShInstr::parse::<iA6_JumpToDetail>(state)?,
            0xAC => ShInstr::parse::<iAC_JumpToDamage>(state)?,
            0xB2 => ShInstr::parse::<iB2>(state)?,
            0xB8 => ShInstr::parse::<iB8>(state)?,
            0xBC => ShInstr::parse::<iBC>(state)?,
            0xC4 => ShInstr::parse::<iC4_XformUnmask>(state)?,
            0xC6 => ShInstr::parse::<iC6_XformUnmaskLong>(state)?,
            0xC8 => ShInstr::parse::<iC8_JumpToLOD>(state)?,
            0xCA => ShInstr::parse::<iCA>(state)?,
            0xCE => ShInstr::parse::<iCE>(state)?,
            0xD0 => ShInstr::parse::<iD0>(state)?,
            0xD2 => ShInstr::parse::<iD2>(state)?,
            0xDC => ShInstr::parse::<iDC>(state)?,
            0xDA => ShInstr::parse::<iDA>(state)?,
            0xF0 => {
                // Extracting code fragments takes special care and broader access.
                iF0_X86Code::from_bytes(end_offset, pe, state)?;
            }
            0xF2 => ShInstr::parse::<iF2_PtrToObjEnd>(state)?,
            0xE0 => ShInstr::parse::<iE0_TextureIndex>(state)?,
            0xE2 => ShInstr::parse::<iE2_TextureFile>(state)?,
            0xE4 => ShInstr::parse::<iE4>(state)?,
            0xE6 => ShInstr::parse::<iE6>(state)?,
            0xE8 => ShInstr::parse::<iE8>(state)?,
            0xEA => ShInstr::parse::<iEA>(state)?,
            0xEE => ShInstr::parse::<iEE>(state)?,
            0xF6 => ShInstr::parse::<iF6_VertexInfo>(state)?,
            0xFC => ShInstr::parse::<iFC_Face>(state)?,
            0xFF => ShInstr::parse::<iFF_Header>(state)?,
            _ => bail!(
                "Unrecognized Sh instructions: 0x{:X} @ {:04X}",
                state.0[*state.1],
                state.1
            ),
        }
        Ok(())
    }

    pub fn serialize_yaml(&self) -> Result<String> {
        let mut map = Dict::default();
        let mut instr_seq = List::default();
        for instr in &self.instrs {
            instr_seq.push(instr.to_value(self.code_start_in_pe)?);
        }
        map.insert("time_date_stamp", self.time_date_stamp.into());
        map.insert("code_start_in_pe", self.code_start_in_pe.into());
        map.insert("instructions", instr_seq.into());
        Value::Dict(map).serialize_yaml()
    }

    pub fn deserialize_yaml(yaml: &str) -> Result<Self> {
        let val = Value::deserialize_yaml(yaml)?;
        let main = val.to_dict()?;
        let time_date_stamp = main
            .get("time_date_stamp")
            .unwrap_or_else(|| &Value::Integer(0))
            .to_int()?;
        let code_start_in_pe = main
            .get("code_start_in_pe")
            .unwrap_or_else(|| &Value::Integer(0x200))
            .to_int()?;
        let instrs = main
            .get("instructions")
            .ok_or_else(|| anyhow!("expected instructions"))?
            .to_list()?;

        // Extract from sh instr into both code streams simultaneously, but with empty blocks
        // of raw bytes for detail instructions because we haven't unlinked the code ptrs.
        let mut code = Vec::new();
        let mut byte_offset = 0;
        for v in instrs.iter() {
            let instr = ShInstr::from_value(v, byte_offset)?;
            byte_offset += instr.size;
            code.push(instr);
        }

        // Build the instruction map so that we can unlink to offsets
        let mut instr_map = HashMap::new();
        for (i, instr) in code.iter().enumerate() {
            instr_map.insert(*instr.uuid(), i);
        }

        // At this point we have a (mostly) correct instruction set.
        // Since we don't have a PE, we can't fully disassemble the code sections.
        // We do have enough to re-create the PE, however, so we can dump that
        // PE, then do a normal load.
        let obj = Self {
            time_date_stamp: time_date_stamp as u32,
            instrs: code,
            instr_map,
            code_start_in_pe: code_start_in_pe as usize,
            analysis: ShAnalysis::default(),
            damage_analysis: None,
        };
        let data = obj.compile_pe()?;
        Self::from_bytes(&data)
    }

    // Collect all instruction-relative relocs into a complete code-relative list.
    pub fn collect_relocs(&self) -> Vec<usize> {
        let mut out = Vec::new();
        for instr in &self.instrs {
            match &instr.detail {
                InstrDetail::X86Code(x86_code) => {
                    out.extend_from_slice(&x86_code.relocs_relative_to_base(instr.offset));
                }
                InstrDetail::X86Trampoline(_) => {
                    out.push(instr.offset + 2);
                }
                _ => {
                    if instr.magic == iD2::MAGIC {
                        out.push(instr.offset + 4);
                    }
                }
            }
        }
        out
    }

    pub fn collect_imports(&self) -> Vec<String> {
        let mut out = Vec::new();
        for instr in &self.instrs {
            if let InstrDetail::X86Trampoline(tramp) = &instr.detail {
                out.push(tramp.name().to_owned());
            }
        }
        out
    }

    fn code_to_raw_bytes(&self) -> Result<Vec<u8>> {
        // Replace uuid internal references with offsets
        let mut instrs = self.instrs.clone();
        Self::unlink(&mut instrs)?;

        let mut raw_size = 0;
        for instr in &instrs {
            raw_size += instr.size;
        }
        let mut raw = vec![0u8; raw_size];
        for instr in &instrs {
            instr.to_bytes_inline(&mut raw[instr.offset..instr.offset + instr.size])?;
        }
        Ok(raw)
    }

    pub fn compile_pe(&self) -> Result<Vec<u8>> {
        let relocs = self.collect_relocs();
        let imports = self.collect_imports();
        let mut code = self.code_to_raw_bytes()?;

        // We store the raw code with a pre-relocation of 0xAA00_0000. We need to manually
        // un-relocate it back to the expected load address.
        for &reloc in &relocs {
            if code[reloc + 3] == 0xAA {
                code[reloc + 3] -= 0xAA;
                code[reloc + 1] += 0x10;
            }
        }

        let mut builder = PeBuilder::default().with_time_date_stamp(self.time_date_stamp);
        for &reloc in &relocs {
            builder.add_reloc(reloc.try_into()?);
        }
        for import in &imports {
            builder.add_import(import);
        }
        Ok(builder.build(code))
    }

    pub fn instructions(&self) -> impl Iterator<Item = &ShInstr> {
        self.instrs.iter()
    }

    pub fn instruction(&self, uuid: &Uuid) -> Result<&ShInstr> {
        self.instrs
            .get(self.uuid_to_offset(uuid)?)
            .ok_or_else(|| anyhow!("no such uuid '{uuid}' in this shape"))
    }

    pub fn instruction_mut(&mut self, uuid: &Uuid) -> Result<&mut ShInstr> {
        let offset = self.uuid_to_offset(uuid)?;
        self.instrs
            .get_mut(offset)
            .ok_or_else(|| anyhow!("no such uuid '{uuid}' in this shape"))
    }

    pub fn analysis(&self, selection: DrawSelection) -> Result<&ShAnalysis> {
        match selection {
            DrawSelection::NormalModel => Ok(&self.analysis),
            DrawSelection::DamageModel => self
                .damage_analysis
                .as_ref()
                .ok_or_else(|| anyhow!("no damage model on shape")),
        }
    }

    pub fn has_damage_model(&self) -> bool {
        self.analysis.has_damage_model()
    }

    pub fn iter_draw_order(&self, selection: DrawSelection) -> DrawVisitor {
        DrawVisitor::new(self.code(), &self.instr_map, selection)
    }

    pub fn display_content(&self) -> Result<String> {
        let offset_map = Self::make_offset_map(&self.instrs);
        let mut lines = Vec::new();
        for instr in &self.instrs {
            lines.push(instr.display_content(&offset_map)?);
        }
        Ok(lines.join("\n"))
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use catalog::{FileSystem, Order, Search};
    use installations::Installations;
    use nitrous::make_partial_test;

    #[test]
    fn test_can_parse_all_shapes() -> Result<()> {
        env_logger::init();

        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(Search::for_extension("SH").sort(Order::Asc).must_match())? {
            println!("At: {info}");

            let shape = ShCode::from_bytes(&info.data()?)?;

            // Assert that all instructions abut
            let mut expect_offset = 0;
            for instr in shape.code() {
                assert_eq!(instr.offset, expect_offset);
                expect_offset += instr.size;
            }
        }
        Ok(())
    }

    fn can_roundtrip_all_shapes(section: usize, total_sections: usize) -> Result<()> {
        env_logger::init();
        assert!(total_sections > 0);
        assert!(section < total_sections);

        let (libs, _) = Installations::for_testing()?;
        let all = libs.search(Search::for_extension("SH").in_collection("FA").must_match())?;
        let files = nitrous::segment_tests(section, total_sections, &all);

        for info in files {
            println!("At: {info}");
            let shape_data = info.data()?;
            let mut shape = ShCode::from_bytes(&shape_data)?;
            let pe = PortableExecutable::from_bytes(shape_data.as_ref())?;

            // Assert that all instructions abut
            let mut expect_offset = 0;
            for instr in shape.code() {
                assert_eq!(instr.offset, expect_offset);
                expect_offset += instr.size;
            }

            let yaml = shape.serialize_yaml()?;
            let mut yaml_shape = ShCode::deserialize_yaml(&yaml)?;

            ShCode::unlink(&mut shape.instrs)?;
            ShCode::unlink(&mut yaml_shape.instrs)?;

            for (a, b) in shape.instrs.iter().zip(yaml_shape.instrs.iter()) {
                // println!("A ({info}): {:#?}", a);
                // println!("B ({info}): {:#?}", b);
                assert_eq!(a.magic, b.magic);
                assert_eq!(a.size, b.size);
                assert_eq!(a.offset, b.offset);

                let code_a = a.to_bytes()?;
                let code_b = b.to_bytes()?;
                assert_eq!(code_a, code_b);
            }

            ShCode::link(&mut shape.instrs)?;
            ShCode::link(&mut yaml_shape.instrs)?;

            let relocs = pe
                .relocs()
                .iter()
                .map(|re| re.code_offset())
                .collect::<Vec<usize>>();
            let yaml_relocs = yaml_shape.collect_relocs();
            assert_eq!(relocs.len(), yaml_relocs.len());
            assert_eq!(relocs, yaml_relocs);

            let yaml_raw_data = yaml_shape.compile_pe()?;
            for (i, (a, b)) in shape_data.iter().zip(yaml_raw_data.iter()).enumerate() {
                assert_eq!(a, b, "{a:02X} != {b:02X} at offset {i:04X}");
            }
            assert_eq!(shape_data, yaml_raw_data);
        }
        Ok(())
    }
    make_partial_test!(can_roundtrip_all_shapes, 20);
}

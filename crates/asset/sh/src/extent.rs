// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    coordinate::matrix_for_xform, fa_pos_to_pt3, AfterBurnerState, AirbrakeState, BayState,
    DrawState, GearState, HookState, ShCapabilities, VertBufUsage, Xformer,
};
use absolute_unit::prelude::*;
use anyhow::Result;
use bevy_ecs::prelude::*;
use geometry::{intersect::sphere_vs_ray, Aabb3, Ray, Sphere};
use nitrous::{inject_nitrous_component, NitrousComponent};
use std::time::{Duration, Instant};

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum GearFootprintKind {
    Nose,
    Tail,
    Skid,
    Car,
    Tank,
}

pub(crate) struct ExtentBuilder {
    extent: ShExtent,
    radius: Length<Meters>,
    start: Instant,
    now: Instant,
    draw_state: DrawState,

    // Only consider points in this window when extending a bounding box.
    window: Aabb3<Meters>,
}

impl ExtentBuilder {
    pub(crate) fn new(caps: ShCapabilities) -> Self {
        let start = Instant::now();
        let mut draw_state = DrawState::empty(caps);
        draw_state.set_gear_state(GearState::Extended);
        draw_state.set_gear_position(1.);
        draw_state.set_bay_state(BayState::Open);
        draw_state.set_bay_position(1.);
        draw_state.set_afterburner(AfterBurnerState::On);
        draw_state.set_hook(HookState::Extended);
        draw_state.set_airbrake(AirbrakeState::Extended);
        Self {
            extent: ShExtent::default(),
            radius: meters!(0),
            start,
            now: start + Duration::from_secs(10),
            draw_state,
            window: Aabb3::infinite(),
        }
    }

    pub(crate) fn set_window(mut self, window: Aabb3<Meters>) -> Self {
        self.window = window;
        self
    }

    pub(crate) fn extend_with(
        &mut self,
        usage: &VertBufUsage,
        xformer: &mut Option<&mut Xformer>,
        position: &[i16; 3],
    ) -> Result<()> {
        let mut pos = fa_pos_to_pt3(position);
        if let Some(xformer) = xformer {
            let mut out = [0f32; 6];
            if xformer
                .run(&self.draw_state, &self.start, &self.now, &mut out)
                .is_ok()
            {
                let m = matrix_for_xform(out);
                let p2 = m.transform_point3(pos.dvec3().as_vec3());
                pos = Pt3::new_dvec3(p2.as_dvec3());
            }
        }

        // Ignore out-of-window points: e.g. for gear footprint detection
        if !self.window.contains(&pos) {
            return Ok(());
        }

        if pos.length() > self.radius {
            self.radius = pos.length();
        }
        self.extent.aabb_full.extend(&pos);

        // Identify sub-components that we either need separately for some reason, or
        // that should not be part fo the body because they may extend the desired
        // relatively tight fitting box.
        match usage {
            VertBufUsage::Afterburner(_) => self.extent.aabb_afterburner.extend(&pos),
            VertBufUsage::Bay(_) => self.extent.aabb_bay.extend(&pos),
            VertBufUsage::Brake(_) => self.extent.aabb_airbrake.extend(&pos),
            VertBufUsage::Hook(_) => self.extent.aabb_hook.extend(&pos),
            VertBufUsage::Gear(_) => self.extent.aabb_gear.extend(&pos),
            _ => self.extent.aabb_body.extend(&pos),
        }

        Ok(())
    }

    pub(crate) fn build(mut self) -> ShExtent {
        self.extent.sphere = Sphere::from_center_and_radius(Pt3::zero(), self.radius);
        self.extent.offset_to_ground = self.extent.aabb_full.lo().z();
        self.extent
    }
}

#[derive(NitrousComponent, Clone, Debug, Default)]
pub struct ShExtent {
    // Bounding box in meters (including all drawn stuff)
    aabb_full: Aabb3<Meters>,

    // Excluding movers that might extend outside the main box
    aabb_body: Aabb3<Meters>,

    // Sub-boxes for everything with a distinct usage.
    aabb_afterburner: Aabb3<Meters>,
    aabb_airbrake: Aabb3<Meters>,
    aabb_bay: Aabb3<Meters>,
    aabb_hook: Aabb3<Meters>,
    aabb_gear: Aabb3<Meters>,
    aabb_gear_footprint: Aabb3<Meters>,

    // Gear touchpoints and strut length inference.
    gear_max_deflection: Length<Meters>,
    gear_touchpoints: Vec<Pt3<Meters>>,

    // Pre-compute useful metrics
    sphere: Sphere<Meters>, // meters
    offset_to_ground: Length<Meters>,
}

#[inject_nitrous_component]
impl ShExtent {
    // Given our existing extents, compute a window where the gear footprint could lie.
    // Currently this looks between gear.lo and body.lo, which seems to work well enough for now.
    // TODO: see if we can improve this. there are lots of weird edge cases.
    pub fn gear_search_window(&self) -> Aabb3<Meters> {
        assert!(!self.aabb_body.is_degenerate());

        let win_lo = *self.aabb_gear().lo() - Pt3::new(meters!(0), meters!(feet!(1)), meters!(0));
        let mut win_hi = *self.aabb_gear().hi();
        win_hi.set_y(
            self.aabb_body()
                .lo()
                .y()
                .max(self.aabb_gear().lo().y() + meters!(feet!(1))),
        );
        Aabb3::from_bounds(win_lo, win_hi)
    }

    // The layout is just a best guess based on the kind of object. It may not even make sense.
    pub fn lay_out_gear(&mut self, layout: GearFootprintKind) {
        if !self.aabb_body.is_finite()
            || self.aabb_body.is_degenerate()
            || !self.aabb_gear.is_finite()
            || self.aabb_gear.is_degenerate()
        {
            return;
        }

        // Note: the shape analysis will not find fixed gear, skids, etc, so in
        // that case just guess on the body.
        let fp = if self.aabb_gear_footprint.is_finite() {
            self.aabb_gear_footprint
        } else {
            self.aabb_body
        };
        let max_deflection =
            (self.aabb_body.lo().y() - self.aabb_gear.lo().y()).max(meters!(0)) / scalar!(2);
        match layout {
            GearFootprintKind::Tank | GearFootprintKind::Skid => {
                self.gear_touchpoints.extend_from_slice(&[
                    *fp.lo(),
                    *fp.lo() + V3::new(fp.span(0), meters!(0), meters!(0)),
                    *fp.lo() + V3::new(meters!(0), meters!(0), fp.span(2)),
                    *fp.lo() + V3::new(fp.span(0), meters!(0), fp.span(2)),
                ]);
                self.gear_max_deflection = meters!(0);
            }
            GearFootprintKind::Car => {
                self.gear_touchpoints.extend_from_slice(&[
                    *fp.lo(),
                    *fp.lo() + V3::new(fp.span(0), meters!(0), meters!(0)),
                    *fp.lo() + V3::new(meters!(0), meters!(0), fp.span(2)),
                    *fp.lo() + V3::new(fp.span(0), meters!(0), fp.span(2)),
                ]);
                self.gear_max_deflection = max_deflection;
            }
            GearFootprintKind::Nose => {
                self.gear_touchpoints.extend_from_slice(&[
                    *fp.lo(),
                    *fp.lo() + V3::new(fp.span(0), meters!(0), meters!(0)),
                    *fp.lo() + V3::new(fp.span(0) / scalar!(2), meters!(0), fp.span(2)),
                ]);
                self.gear_max_deflection = max_deflection;
            }
            GearFootprintKind::Tail => {
                self.gear_touchpoints.extend_from_slice(&[
                    *fp.lo() + V3::new(fp.span(0), meters!(0), fp.span(2)),
                    *fp.lo() + V3::new(fp.span(0), meters!(0), meters!(0)),
                    *fp.lo() + V3::new(fp.span(0) / scalar!(2), meters!(0), meters!(0)),
                ]);
                self.gear_max_deflection = max_deflection;
            }
        }
    }

    pub fn aabb_full(&self) -> &Aabb3<Meters> {
        &self.aabb_full
    }

    pub fn aabb_body(&self) -> &Aabb3<Meters> {
        &self.aabb_body
    }

    pub fn aabb_afterburner(&self) -> &Aabb3<Meters> {
        &self.aabb_afterburner
    }

    pub fn aabb_bay(&self) -> &Aabb3<Meters> {
        &self.aabb_bay
    }

    pub fn aabb_airbrake(&self) -> &Aabb3<Meters> {
        &self.aabb_airbrake
    }

    pub fn aabb_hook(&self) -> &Aabb3<Meters> {
        &self.aabb_hook
    }

    pub fn aabb_gear(&self) -> &Aabb3<Meters> {
        &self.aabb_gear
    }

    pub fn aabb_gear_footprint(&self) -> &Aabb3<Meters> {
        &self.aabb_gear_footprint
    }

    pub fn set_aabb_gear_footprint(&mut self, footprint: Aabb3<Meters>) {
        self.aabb_gear_footprint = footprint;
    }

    pub fn sphere(&self) -> &Sphere<Meters> {
        &self.sphere
    }

    pub fn gear_touchpoints(&self) -> impl Iterator<Item = &Pt3<Meters>> {
        self.gear_touchpoints.iter()
    }

    pub fn gear_max_deflection(&self) -> &Length<Meters> {
        &self.gear_max_deflection
    }

    pub fn offset_to_ground(&self) -> Length<Meters> {
        self.offset_to_ground
    }

    pub fn intersect_ray(
        &self,
        position: &Pt3<Meters>,
        scale: f64,
        ray: &Ray<Meters>,
    ) -> Option<Pt3<Meters>> {
        // if let Some(intersect) = aabb_vs_ray(&self.aabb_full, ray) {}

        let hit_sphere = Sphere::from_center_and_radius(
            *position + *self.sphere.center(),
            self.sphere.radius() * scalar!(scale),
        );

        if let Some(intersect) = sphere_vs_ray(&hit_sphere, ray) {
            return Some(intersect);
        }
        None
    }
}

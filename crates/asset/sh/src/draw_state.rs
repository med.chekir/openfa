// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::coordinate::fa_angle_to_rot16;
use crate::{
    AfterBurnerState, AileronPosition, AirbrakeState, BayState, EjectState, FlapState, GearState,
    HookState, PlayerState, RudderPosition, SamCount, ShCapabilities, SlatsState,
};
use absolute_unit::{radians, Angle, AngleUnit, Radians};
use bevy_ecs::prelude::*;
use bitflags::bitflags;
use nitrous::{inject_nitrous_component, NitrousComponent};
use std::time::Instant;

const BAY_ANIMATION_EXTENT: f32 = -8192f32;
const GEAR_ANIMATION_EXTENT: f32 = -8192f32;

bitflags! {
    #[derive(Clone, Copy, Debug, Eq, PartialEq)]
    pub struct DrawStateFlags: u16 {
        const PLAYER_DEAD         = 0x0040;
        const RUDDER_LEFT         = 0x0080;
        const RUDDER_RIGHT        = 0x0100;
        const LEFT_AILERON_DOWN   = 0x0200;
        const LEFT_AILERON_UP     = 0x0400;
        const RIGHT_AILERON_DOWN  = 0x0800;
        const RIGHT_AILERON_UP    = 0x1000;
    }
}

// Per instance state of all flag and xform inputs.
#[derive(NitrousComponent, Clone, Copy, Debug, PartialEq)]
pub struct DrawState {
    base_time: Instant,

    afterburner_state: AfterBurnerState,
    airbrake_state: AirbrakeState,
    bay_position: f32,
    bay_state: BayState,
    flaps_state: FlapState,
    gear_position: f32,
    gear_state: GearState,
    hook_state: HookState,
    slats_state: SlatsState,

    thrust_vector_pitch: Angle<Radians>,
    vtol_angle: Angle<Radians>,
    swing_wing_angle: Angle<Radians>,

    rudder_position: RudderPosition,
    aileron_position: AileronPosition,

    player_state: PlayerState,
    eject_state: EjectState,

    sam_count: SamCount,

    // wing_sweep_pos: i16,
    // wing_sweep_delta: i16,
    // sam_count: i8,
    // eject_state: u8,
    // flags: DrawStateFlags,
    caps: ShCapabilities,
}

#[inject_nitrous_component]
impl DrawState {
    pub fn empty(caps: ShCapabilities) -> Self {
        Self {
            base_time: Instant::now(),

            afterburner_state: AfterBurnerState::default(),
            airbrake_state: AirbrakeState::default(),
            bay_position: 0.,
            bay_state: BayState::Closed,
            flaps_state: FlapState::default(),
            gear_position: 0.,
            gear_state: GearState::Retracted,
            hook_state: HookState::default(),
            slats_state: SlatsState::default(),

            thrust_vector_pitch: radians!(0),
            vtol_angle: radians!(0),
            swing_wing_angle: radians!(0),

            rudder_position: RudderPosition::default(),
            aileron_position: AileronPosition::default(),

            player_state: PlayerState::default(),
            eject_state: EjectState::default(),

            sam_count: SamCount::default(),

            caps,
            //
            // wing_sweep_pos: 0,
            // wing_sweep_delta: 0,
            // sam_count: 3,
            // eject_state: 0,
            // flags: DrawStateFlags::empty(),
        }
    }

    pub fn time_origin(&self) -> &Instant {
        &self.base_time
    }

    // //// AFTERBURNER

    pub fn afterburner_state(&self) -> AfterBurnerState {
        self.afterburner_state
    }

    pub fn set_afterburner(&mut self, state: AfterBurnerState) {
        self.afterburner_state = state;
    }

    pub fn x86_afterburner_state(&self) -> u32 {
        self.afterburner_state as i8 as u8 as u32
    }

    // //// AIRBRAKE

    pub fn airbrake_state(&self) -> AirbrakeState {
        self.airbrake_state
    }

    pub fn set_airbrake(&mut self, position: AirbrakeState) {
        self.airbrake_state = position;
    }

    // //// BAY

    pub fn bay_state(&self) -> BayState {
        self.bay_state
    }

    pub fn set_bay_state(&mut self, state: BayState) {
        self.bay_state = state;
    }

    // Map [0,1] to [-8192,0]
    pub fn set_bay_position(&mut self, f: f32) {
        self.bay_position = (1. - f) * BAY_ANIMATION_EXTENT;
    }

    pub fn x86_bay_open(&self) -> u32 {
        self.bay_state as i8 as i32 as u32
    }

    pub fn x86_bay_position(&self) -> u32 {
        self.bay_position as i32 as u32
    }

    // //// FLAPS

    pub fn flaps_state(&self) -> FlapState {
        self.flaps_state
    }

    pub fn set_flaps(&mut self, position: FlapState) {
        self.flaps_state = position;
    }

    // //// GEAR

    pub fn gear_state(&self) -> GearState {
        self.gear_state
    }

    pub fn set_gear_state(&mut self, state: GearState) {
        self.gear_state = state;
    }

    // Map [0,1] to [-8192,0]
    pub fn set_gear_position(&mut self, f: f32) {
        self.gear_position = (1. - f) * GEAR_ANIMATION_EXTENT;
    }

    pub fn x86_gear_down(&self) -> u32 {
        self.gear_state as i8 as i32 as u32
    }

    pub fn x86_gear_position(&self) -> u32 {
        self.gear_position as i32 as u32
    }

    // //// HOOK

    pub fn hook_state(&self) -> HookState {
        self.hook_state
    }

    pub fn set_hook(&mut self, state: HookState) {
        self.hook_state = state;
    }

    // //// SLATS

    pub fn slats_state(&self) -> SlatsState {
        self.slats_state
    }

    pub fn set_slats(&mut self, state: SlatsState) {
        self.slats_state = state;
    }

    pub fn x86_slats_down(&self) -> u32 {
        self.slats_state as i8 as i32 as u32
    }

    // //// CANARD

    pub fn set_thrust_vector_pitch<U: AngleUnit>(&mut self, pitch: Angle<U>) {
        self.thrust_vector_pitch = radians!(pitch);
    }

    pub fn x86_canard_position(&self) -> u32 {
        fa_angle_to_rot16(self.thrust_vector_pitch) as u32
    }

    // //// VTOL

    pub fn set_vtol_angle<U: AngleUnit>(&mut self, angle: Angle<U>) {
        self.vtol_angle = radians!(angle);
    }

    pub fn x86_vtol_angle(&self) -> u32 {
        fa_angle_to_rot16(self.vtol_angle) as u32
    }

    pub fn x86_vtol_on(&self) -> u32 {
        match self.vtol_angle != radians!(0) {
            false => 0u32,
            true => 1u32,
        }
    }

    // //// SWING WING

    pub fn set_swing_wing_angle<U: AngleUnit>(&mut self, angle: Angle<U>) {
        self.swing_wing_angle = radians!(angle);
    }

    pub fn x86_swing_wing_angle(&self) -> u32 {
        fa_angle_to_rot16(self.swing_wing_angle) as u32
    }

    // pub fn sam_count(&self) -> i8 {
    //     self.sam_count
    // }
    //
    // pub fn eject_state(&self) -> u8 {
    //     self.eject_state
    // }
    //
    // pub fn wing_sweep_angle(&self) -> i16 {
    //     self.wing_sweep_pos
    // }
    //
    // pub fn x86_swing_wing(&self) -> u32 {
    //     i32::from(self.wing_sweep_angle()) as u32
    // }
    //
    // #[method]
    // pub fn player_dead(&self) -> bool {
    //     self.flags.contains(DrawStateFlags::PLAYER_DEAD)
    // }
    //
    // #[method]
    // pub fn rudder_left(&self) -> bool {
    //     self.flags.contains(DrawStateFlags::RUDDER_LEFT)
    // }
    //
    // #[method]
    // pub fn rudder_right(&self) -> bool {
    //     self.flags.contains(DrawStateFlags::RUDDER_RIGHT)
    // }
    //
    // #[method]
    // pub fn left_aileron_down(&self) -> bool {
    //     self.flags.contains(DrawStateFlags::LEFT_AILERON_DOWN)
    // }
    //
    // #[method]
    // pub fn left_aileron_up(&self) -> bool {
    //     self.flags.contains(DrawStateFlags::LEFT_AILERON_UP)
    // }
    //
    // #[method]
    // pub fn right_aileron_down(&self) -> bool {
    //     self.flags.contains(DrawStateFlags::RIGHT_AILERON_DOWN)
    // }
    //
    // #[method]
    // pub fn right_aileron_up(&self) -> bool {
    //     self.flags.contains(DrawStateFlags::RIGHT_AILERON_UP)
    // }
    //
    // #[method]
    // pub fn toggle_player_dead(&mut self) {
    //     self.flags.toggle(DrawStateFlags::PLAYER_DEAD);
    // }
    //
    // #[method]
    // pub fn bump_eject_state(&mut self) {
    //     self.eject_state += 1;
    //     self.eject_state %= 5;
    // }
    //
    // pub fn move_right_aileron_up(&mut self) {
    //     self.flags.remove(DrawStateFlags::RIGHT_AILERON_DOWN);
    //     self.flags.insert(DrawStateFlags::RIGHT_AILERON_UP);
    // }
    //
    // pub fn move_right_aileron_down(&mut self) {
    //     self.flags.remove(DrawStateFlags::RIGHT_AILERON_UP);
    //     self.flags.insert(DrawStateFlags::RIGHT_AILERON_DOWN);
    // }
    //
    // pub fn move_right_aileron_center(&mut self) {
    //     self.flags.remove(DrawStateFlags::RIGHT_AILERON_UP);
    //     self.flags.remove(DrawStateFlags::RIGHT_AILERON_DOWN);
    // }
    //
    // pub fn move_left_aileron_up(&mut self) {
    //     self.flags.remove(DrawStateFlags::LEFT_AILERON_DOWN);
    //     self.flags.insert(DrawStateFlags::LEFT_AILERON_UP);
    // }
    //
    // pub fn move_left_aileron_down(&mut self) {
    //     self.flags.remove(DrawStateFlags::LEFT_AILERON_UP);
    //     self.flags.insert(DrawStateFlags::LEFT_AILERON_DOWN);
    // }
    //
    // pub fn move_left_aileron_center(&mut self) {
    //     self.flags.remove(DrawStateFlags::LEFT_AILERON_UP);
    //     self.flags.remove(DrawStateFlags::LEFT_AILERON_DOWN);
    // }
    //
    // pub fn move_rudder_center(&mut self) {
    //     self.flags.remove(DrawStateFlags::RUDDER_LEFT);
    //     self.flags.remove(DrawStateFlags::RUDDER_RIGHT);
    // }
    //
    // pub fn move_rudder_left(&mut self) {
    //     self.flags.insert(DrawStateFlags::RUDDER_LEFT);
    //     self.flags.remove(DrawStateFlags::RUDDER_RIGHT);
    // }
    //
    // pub fn move_rudder_right(&mut self) {
    //     self.flags.remove(DrawStateFlags::RUDDER_LEFT);
    //     self.flags.insert(DrawStateFlags::RUDDER_RIGHT);
    // }
    //
    // pub fn move_elevator_up(&mut self) {}
    //
    // pub fn move_elevator_down(&mut self) {}
    //
    // pub fn move_elevator_center(&mut self) {}
    //
    // pub fn increase_wing_sweep(&mut self) {
    //     self.wing_sweep_delta = 50;
    // }
    //
    // pub fn decrease_wing_sweep(&mut self) {
    //     self.wing_sweep_delta = -50;
    // }
    //
    // pub fn stop_wing_sweep(&mut self) {
    //     self.wing_sweep_delta = 0;
    // }
    //
    // #[method]
    // pub fn consume_sam(&mut self) {
    //     self.sam_count -= 1;
    //     if self.sam_count < 0 {
    //         self.sam_count = 3;
    //     }
    // }
}

// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::{radians, Angle, AngleUnit, Feet, Meters, Pt3, Radians};
use glam::{Mat4, Vec3, Vec4};
use std::f64::consts::PI;

// FA coordinates for position appear to be:
//   side: x
//   forward: y
//   up: z
// e.g. right handed with z-up
// With counter-clockwise winding of polygons after reversing up and forward to get left-handed
// coordinates for use with nitrogen.

// Unlike the other primitives, the Z and Y coords appear to be flipped in the VxBuf
// compared to the VxNorm and Xform coords. :shrug:
pub fn fa_pos_to_pt3(position: &[i16; 3]) -> Pt3<Meters> {
    Pt3::<Meters>::from(&Pt3::<Feet>::new_unit(
        position[0] as f64,
        position[2] as f64,
        position[1] as f64,
    ))
}

// FA coordinates for normals and xform positions appear to be:
//   side: x
//   up: y
//   forward: z
// e.g. left handed, z-forward, matching nitrogen
// With counter-clockwise winding of polygons

// For reasons I don't understand, the coordinate ordering for Xform (C400) blocks is
// not the same as the VxBuf instruction?
pub fn fa_xform_to_pt3(position: &[i16]) -> Pt3<Meters> {
    Pt3::<Meters>::from(&Pt3::<Feet>::new_unit(
        position[0] as f64,
        position[1] as f64,
        position[2] as f64,
    ))
}

// Ditto for VxNorm blocks: they are in the same space as the xform, not the vxbuf.
pub fn fa_norm_to_vec(norm: &[i8; 3]) -> Vec3 {
    Vec3::new(norm[0] as f32, norm[1] as f32, norm[2] as f32).normalize()
}

// Translate from FA rotation in i16 to radians.
// It appears that i16 rotations are encoded as 8192 steps in the half circle
// so that a full 360 can be encoded in either direction.
pub fn fa_rot16_to_angle(d: i16) -> Angle<Radians> {
    radians!(-f64::from(d) * PI / 8192_f64)
}

pub fn fa_angle_to_rot16<T: AngleUnit>(d: Angle<T>) -> i16 {
    let d = radians!(d).f64();
    (-d * 8192_f64 / PI) as i16
}

// Copies of what we're doing with the xform buffer on the gpu.
pub(crate) fn from_euler_angles(roll: f32, pitch: f32, yaw: f32) -> Mat4 {
    let sr = (-roll).sin();
    let cr = (-roll).cos();
    let sp = (pitch).sin();
    let cp = (pitch).cos();
    let sy = (yaw).sin();
    let cy = (yaw).cos();

    Mat4::from_cols(
        Vec4::new(cy * cp, sy * cp, -sp, 0.),
        Vec4::new(cy * sp * sr - sy * cr, sy * sp * sr + cy * cr, cp * sr, 0.),
        Vec4::new(cy * sp * cr + sy * sr, sy * sp * cr - cy * sr, cp * cr, 0.),
        Vec4::new(0., 0., 0., 1.),
    )
}

// Given an xform 6-tuple, produce the equivalent transformation matrix.
pub fn matrix_for_xform(xform: [f32; 6]) -> Mat4 {
    let t0 = xform[0];
    let t1 = xform[1];
    let t2 = xform[2];
    let r0 = xform[3];
    let r1 = xform[4];
    let r2 = xform[5];
    // let s = xform[7];
    let s = 1.;
    let trans = Mat4::from_cols(
        Vec4::new(s, 0.0, 0.0, 0.0),
        Vec4::new(0.0, s, 0.0, 0.0),
        Vec4::new(0.0, 0.0, s, 0.0),
        Vec4::new(t0, t1, t2, 1.0),
    );
    let rot = from_euler_angles(r0, r1, r2);
    trans * rot
}

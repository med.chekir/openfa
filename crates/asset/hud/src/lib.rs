// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::{bail, ensure, Result};
use enum_iterator::Sequence;
use memoffset::offset_of;
use packed_struct::packed_struct;
use peff::{PeBuilder, PortableExecutable};
use std::fmt::{Display, Formatter};
use std::{
    mem,
    str::{self, FromStr},
};
use zerocopy::AsBytes;

fn name_str<const N: usize>(raw: &[u8; N]) -> Result<String> {
    let name_raw = match raw.iter().position(|&n| n == 0u8) {
        Some(last) => &raw[..last],
        None => &raw[0..N],
    };
    Ok(str::from_utf8(name_raw)?.to_owned())
}

fn pic_name_to_bytes(pic: &str) -> Result<[u8; 13]> {
    ensure!(pic.ends_with(".PIC"));
    ensure!(pic.len() - 4 < 13);
    let mut data = [0u8; 13];
    data[0..pic.len() - 4].copy_from_slice(pic[0..pic.len() - 4].to_lowercase().as_bytes());
    Ok(data)
}

pub const HUD_LOAD_BASE: u32 = 0xAA00_0000;
pub const HUD_BYTES: [u8; 25] = [
    b'h', b'u', b'd', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
];
pub const HUDSYM_BYTES: [u8; 17] = [
    b'h', b'u', b'd', b's', b'y', b'm', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
];

// Used with FA and ATFGOLD
#[packed_struct]
struct HudDataV5 {
    // Pic references
    pad0: u8,
    pic_standard: [u8; 13],
    pic_high: [u8; 13],
    pic_super: [u8; 13],

    // coords of some sort; looks like generally vga resolutions?
    coords: [u16; 6],
    pad4: u16,
    pad5: u16,

    // hud and hudsym block; no idea but it's always the same
    hud: [u8; 25],
    hudsym: [u8; 17],

    // Some relative coordinates. Maybe where to draw things?
    rel_coords: [i16; 8],
    unk0: u16,

    pad6: [u8; 188],

    unk1: u32,
    unk2: [u8; 0x105],

    bomb_sight_kind: u16,

    unk3: [u8; 0x80],
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Sequence)]
#[repr(u16)]
pub enum BombSightKind {
    None = 0,
    Ccip = 1,
    Camera = 256,
    CurvedCcip = 257,
}

impl TryFrom<u16> for BombSightKind {
    type Error = anyhow::Error;
    fn try_from(value: u16) -> Result<Self> {
        Ok(match value {
            0 => Self::None,
            1 => Self::Ccip,
            256 => Self::Camera,
            257 => Self::CurvedCcip,
            _ => bail!("Unknown bomb sight kind: {value}"),
        })
    }
}

impl FromStr for BombSightKind {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> Result<Self> {
        Ok(match s {
            "None" => Self::None,
            "CCIP" => Self::Ccip,
            "Camera" => Self::Camera,
            "CCIP-Curved" => Self::CurvedCcip,
            _ => bail!("not a valid kind of bomb sight: {s}"),
        })
    }
}

impl BombSightKind {
    pub fn label(&self) -> &'static str {
        match self {
            Self::None => "None",
            Self::Ccip => "CCIP",
            Self::Camera => "Camera",
            Self::CurvedCcip => "CCIP-Curved",
        }
    }
}

impl Display for BombSightKind {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.label())
    }
}

#[allow(unused)]
#[derive(Clone, Debug)]
pub struct Hud {
    time_date_stamp: u32,
    pic_standard: String,
    pic_high: String,
    pic_super: String,
    coords: [u16; 6],
    rel_coords: [i16; 8],
    unk0: u16,
    unk1: u32,
    unk2: [u8; 0x105],
    bomb_sight_kind: BombSightKind,
    unk3: [u8; 0x80],
}

impl Hud {
    pub fn from_bytes(pe_data: &[u8], _name: &str) -> Result<Self> {
        let mut pe = PortableExecutable::from_bytes(pe_data)?;
        pe.relocate(HUD_LOAD_BASE)?;
        assert_eq!(pe.code().len(), 699, "only support v5 (FA) HUDs for now");
        assert_eq!(pe.code().len(), mem::size_of::<HudDataV5>());
        assert_eq!(pe.relocs().len(), 0);

        let data = HudDataV5::overlay(pe.code())?;
        assert_eq!(data.pad0(), 0);
        assert_eq!(data.pad4(), 0);
        assert_eq!(data.pad5(), 0);
        assert_eq!(data.pad6(), [0u8; 188]);
        assert_eq!(data.hud(), HUD_BYTES);
        assert_eq!(data.hudsym(), HUDSYM_BYTES);
        assert_eq!(offset_of!(HudDataV5, bomb_sight_kind), 0x239);
        let hud = Hud {
            time_date_stamp: pe.coff_header().time_date_stamp(),
            pic_standard: name_str(&data.pic_standard())?.to_uppercase() + ".PIC",
            pic_high: name_str(&data.pic_high())?.to_uppercase() + ".PIC",
            pic_super: name_str(&data.pic_super())?.to_uppercase() + ".PIC",
            coords: data.coords(),
            rel_coords: data.rel_coords(),
            unk0: data.unk0(),
            unk1: data.unk1(),
            unk2: data.unk2(),
            bomb_sight_kind: data.bomb_sight_kind().try_into()?,
            unk3: data.unk3(),
        };

        Ok(hud)
    }

    pub fn to_fa_hud_pe_bytes(&self) -> Result<Vec<u8>> {
        let data = HudDataV5 {
            pad0: 0,
            pic_standard: pic_name_to_bytes(&self.pic_standard)?,
            pic_high: pic_name_to_bytes(&self.pic_high)?,
            pic_super: pic_name_to_bytes(&self.pic_super)?,
            coords: self.coords,
            pad4: 0,
            pad5: 0,
            hud: HUD_BYTES,
            hudsym: HUDSYM_BYTES,
            rel_coords: self.rel_coords,
            unk0: self.unk0,
            pad6: [0u8; 188],
            unk1: self.unk1,
            unk2: self.unk2,
            bomb_sight_kind: self.bomb_sight_kind as u16,
            unk3: self.unk3,
        };
        let mut builder = PeBuilder::default().with_time_date_stamp(self.time_date_stamp);
        let pe_data = builder.build(data.as_bytes().to_owned());
        Ok(pe_data)
    }

    pub fn pic_standard_mut(&mut self) -> &mut String {
        &mut self.pic_standard
    }

    pub fn pic_high_mut(&mut self) -> &mut String {
        &mut self.pic_high
    }

    pub fn pic_super_mut(&mut self) -> &mut String {
        &mut self.pic_super
    }

    pub fn abs_coords_mut(&mut self) -> &mut [u16; 6] {
        &mut self.coords
    }

    pub fn rel_coords_mut(&mut self) -> &mut [i16; 8] {
        &mut self.rel_coords
    }

    pub fn unk0_mut(&mut self) -> &mut u16 {
        &mut self.unk0
    }

    pub fn unk1_mut(&mut self) -> &mut u32 {
        &mut self.unk1
    }

    pub fn bomb_sight_kind(&self) -> BombSightKind {
        self.bomb_sight_kind
    }

    pub fn set_bomb_sight_kind(&mut self, bomb_sight_kind: BombSightKind) {
        self.bomb_sight_kind = bomb_sight_kind;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use catalog::{FileSystem, Search};
    use installations::Installations;

    #[test]
    fn it_works() -> Result<()> {
        env_logger::init();
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(
            Search::for_extension("HUD")
                .in_collections(["FA", "ATFGOLD"])
                .must_match(),
        )? {
            println!("At: {info}");

            let data = info.data()?;
            let _hud = Hud::from_bytes(data.as_ref(), &format!("{info}"))?;
        }
        Ok(())
    }

    #[test]
    fn it_can_round_trip() -> Result<()> {
        env_logger::init();
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(
            Search::for_extension("HUD")
                .in_collections(["FA", "ATFGOLD"])
                .must_match(),
        )? {
            println!("At: {info}");

            let original_data = info.data()?;
            let hud = Hud::from_bytes(original_data.as_ref(), &format!("{info}"))?;
            let trip_data = hud.to_fa_hud_pe_bytes()?;
            assert_eq!(original_data, trip_data);
        }
        Ok(())
    }
}

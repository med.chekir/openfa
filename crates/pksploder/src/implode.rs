// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.

// An implementation of PKWare compression. This is a direct apples-to-apples
// port (using c2rust and heavy editing of the results) of Ladislav Zezula's
// implementation included in stormlib. As such, the original license (MIT)
// also still holds for this code.
use anyhow::Result;
use std::io::{Read, Write};

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
enum Phase {
    #[default]
    Phase0,
    Phase1,
    Phase2,
}

impl Phase {
    fn next(&mut self) {
        *self = match self {
            Self::Phase0 => Self::Phase1,
            Self::Phase1 => Self::Phase2,
            Self::Phase2 => panic!("ran out of phases!"),
        };
    }
}

pub static DIST_BITS: [u8; 64] = [
    0x2, 0x4, 0x4, 0x5, 0x5, 0x5, 0x5, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6,
    0x6, 0x6, 0x6, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7,
    0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x8, 0x8, 0x8, 0x8, 0x8, 0x8, 0x8, 0x8, 0x8,
    0x8, 0x8, 0x8, 0x8, 0x8, 0x8, 0x8,
];
pub static DIST_CODES: [u8; 64] = [
    0x03, 0x0d, 0x05, 0x19, 0x09, 0x11, 0x01, 0x3e, 0x1e, 0x2e, 0x0e, 0x36, 0x16, 0x26, 0x06, 0x3a,
    0x1a, 0x2a, 0x0a, 0x32, 0x12, 0x22, 0x42, 0x02, 0x7c, 0x3c, 0x5c, 0x1c, 0x6c, 0x2c, 0x4c, 0x0c,
    0x74, 0x34, 0x54, 0x14, 0x64, 0x24, 0x44, 0x04, 0x78, 0x38, 0x58, 0x18, 0x68, 0x28, 0x48, 0x08,
    0xf0, 0x70, 0xb0, 0x30, 0xd0, 0x50, 0x90, 0x10, 0xe0, 0x60, 0xa0, 0x20, 0xc0, 0x40, 0x80, 0x00,
];
pub static EX_LEN_BITS: [u8; 16] = [
    0, 0, 0, 0, 0, 0, 0, 0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8,
];
pub static LEN_BITS: [u8; 16] = [
    0x3, 0x2, 0x3, 0x3, 0x4, 0x4, 0x4, 0x5, 0x5, 0x5, 0x5, 0x6, 0x6, 0x6, 0x7, 0x7,
];
pub static LEN_CODES: [u8; 16] = [
    0x5, 0x3, 0x1, 0x6, 0xa, 0x2, 0xc, 0x14, 0x4, 0x18, 0x8, 0x30, 0x10, 0x20, 0x40, 0x0,
];
pub static HUFF_BITS_ASC: [u8; 256] = [
    0xb, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0x8, 0x7, 0xc, 0xc, 0x7, 0xc, 0xc, 0xc, 0xc, 0xc,
    0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xd, 0xc, 0xc, 0xc, 0xc, 0xc, 0x4, 0xa, 0x8, 0xc, 0xa, 0xc,
    0xa, 0x8, 0x7, 0x7, 0x8, 0x9, 0x7, 0x6, 0x7, 0x8, 0x7, 0x6, 0x7, 0x7, 0x7, 0x7, 0x8, 0x7, 0x7,
    0x8, 0x8, 0xc, 0xb, 0x7, 0x9, 0xb, 0xc, 0x6, 0x7, 0x6, 0x6, 0x5, 0x7, 0x8, 0x8, 0x6, 0xb, 0x9,
    0x6, 0x7, 0x6, 0x6, 0x7, 0xb, 0x6, 0x6, 0x6, 0x7, 0x9, 0x8, 0x9, 0x9, 0xb, 0x8, 0xb, 0x9, 0xc,
    0x8, 0xc, 0x5, 0x6, 0x6, 0x6, 0x5, 0x6, 0x6, 0x6, 0x5, 0xb, 0x7, 0x5, 0x6, 0x5, 0x5, 0x6, 0xa,
    0x5, 0x5, 0x5, 0x5, 0x8, 0x7, 0x8, 0x8, 0xa, 0xb, 0xb, 0xc, 0xc, 0xc, 0xd, 0xd, 0xd, 0xd, 0xd,
    0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd,
    0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd,
    0xd, 0xd, 0xd, 0xd, 0xd, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc,
    0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc,
    0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xc, 0xd, 0xc, 0xd, 0xd,
    0xd, 0xc, 0xd, 0xd, 0xd, 0xc, 0xd, 0xd, 0xd, 0xd, 0xc, 0xd, 0xd, 0xd, 0xc, 0xc, 0xc, 0xd, 0xd,
    0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd,
];
pub static HUFF_CODES_ASC: [u16; 256] = [
    0x490, 0xfe0, 0x7e0, 0xbe0, 0x3e0, 0xde0, 0x5e0, 0x9e0, 0x1e0, 0xb8, 0x62, 0xee0, 0x6e0, 0x22,
    0xae0, 0x2e0, 0xce0, 0x4e0, 0x8e0, 0xe0, 0xf60, 0x760, 0xb60, 0x360, 0xd60, 0x560, 0x1240,
    0x960, 0x160, 0xe60, 0x660, 0xa60, 0xf, 0x250, 0x38, 0x260, 0x50, 0xc60, 0x390, 0xd8, 0x42,
    0x2, 0x58, 0x1b0, 0x7c, 0x29, 0x3c, 0x98, 0x5c, 0x9, 0x1c, 0x6c, 0x2c, 0x4c, 0x18, 0xc, 0x74,
    0xe8, 0x68, 0x460, 0x90, 0x34, 0xb0, 0x710, 0x860, 0x31, 0x54, 0x11, 0x21, 0x17, 0x14, 0xa8,
    0x28, 0x1, 0x310, 0x130, 0x3e, 0x64, 0x1e, 0x2e, 0x24, 0x510, 0xe, 0x36, 0x16, 0x44, 0x30,
    0xc8, 0x1d0, 0xd0, 0x110, 0x48, 0x610, 0x150, 0x60, 0x88, 0xfa0, 0x7, 0x26, 0x6, 0x3a, 0x1b,
    0x1a, 0x2a, 0xa, 0xb, 0x210, 0x4, 0x13, 0x32, 0x3, 0x1d, 0x12, 0x190, 0xd, 0x15, 0x5, 0x19,
    0x8, 0x78, 0xf0, 0x70, 0x290, 0x410, 0x10, 0x7a0, 0xba0, 0x3a0, 0x240, 0x1c40, 0xc40, 0x1440,
    0x440, 0x1840, 0x840, 0x1040, 0x40, 0x1f80, 0xf80, 0x1780, 0x780, 0x1b80, 0xb80, 0x1380, 0x380,
    0x1d80, 0xd80, 0x1580, 0x580, 0x1980, 0x980, 0x1180, 0x180, 0x1e80, 0xe80, 0x1680, 0x680,
    0x1a80, 0xa80, 0x1280, 0x280, 0x1c80, 0xc80, 0x1480, 0x480, 0x1880, 0x880, 0x1080, 0x80,
    0x1f00, 0xf00, 0x1700, 0x700, 0x1b00, 0xb00, 0x1300, 0xda0, 0x5a0, 0x9a0, 0x1a0, 0xea0, 0x6a0,
    0xaa0, 0x2a0, 0xca0, 0x4a0, 0x8a0, 0xa0, 0xf20, 0x720, 0xb20, 0x320, 0xd20, 0x520, 0x920,
    0x120, 0xe20, 0x620, 0xa20, 0x220, 0xc20, 0x420, 0x820, 0x20, 0xfc0, 0x7c0, 0xbc0, 0x3c0,
    0xdc0, 0x5c0, 0x9c0, 0x1c0, 0xec0, 0x6c0, 0xac0, 0x2c0, 0xcc0, 0x4c0, 0x8c0, 0xc0, 0xf40,
    0x740, 0xb40, 0x340, 0x300, 0xd40, 0x1d00, 0xd00, 0x1500, 0x540, 0x500, 0x1900, 0x900, 0x940,
    0x1100, 0x100, 0x1e00, 0xe00, 0x140, 0x1600, 0x600, 0x1a00, 0xe40, 0x640, 0xa40, 0xa00, 0x1200,
    0x200, 0x1c00, 0xc00, 0x1400, 0x400, 0x1800, 0x800, 0x1000, 0,
];

#[allow(unused)]
#[derive(Copy, Clone, Default)]
#[repr(u8)]
pub enum CompressionType {
    #[default]
    Binary,
    Ascii,
}

#[allow(unused)]
#[derive(Copy, Clone, Default)]
pub enum DictSize {
    #[default]
    Size3 = 4096,
    Size2 = 2048,
    Size1 = 1024,
}

#[derive(Copy, Clone)]
pub struct Compressor {
    // Configuration
    ctype: CompressionType,
    dsize_bytes: u32,
    dist_bits: [u8; 64],
    dist_codes: [u8; 64],
    n_ch_bits: [u8; 774],
    n_ch_codes: [u16; 774],

    // Work
    distance: u32,
    out_bytes: usize,
    out_bits: usize,
    dsize_bits: u32,
    dsize_mask: u32,
    moffsets: [u16; 516],
    phash_to_index: [u16; 2304],
    out_buff: [u8; 2050],
    work_buff: [u8; 8708],
    phash_offs: [u16; 8708],
}

impl Compressor {
    pub fn new(ctype: CompressionType, dsize: DictSize) -> Result<Self> {
        let (dsize_bits, dsize_mask) = match dsize {
            DictSize::Size3 => (6, 0x3f),
            DictSize::Size2 => (5, 0x1f),
            DictSize::Size1 => (4, 0x0f),
        };

        let mut n_ch_bits = [0u8; 774];
        let mut n_ch_codes = [0u16; 774];
        match ctype {
            CompressionType::Binary => {
                for i in 0..0x100 {
                    n_ch_bits[i] = 9;
                    n_ch_codes[i] = (i * 2) as u16;
                }
            }
            CompressionType::Ascii => {
                for i in 0..0x100 {
                    n_ch_bits[i] = HUFF_BITS_ASC[i] + 1;
                    n_ch_codes[i] = HUFF_CODES_ASC[i] * 2;
                }
            }
        }

        let mut count = 0x100usize;
        for i in 0..0x10usize {
            let mut inner = 0i32;
            while inner < 1 << EX_LEN_BITS[i] as i32 {
                n_ch_bits[count] = (EX_LEN_BITS[i] as u32 + LEN_BITS[i] as u32 + 1u32) as u8;
                n_ch_codes[count] = ((inner << (LEN_BITS[i] as i32 + 1)) as u32
                    | (LEN_CODES[i] as u32 & 0xffff00ff).wrapping_mul(2)
                    | 1u32) as u16;
                count += 1;
                inner += 1;
            }
        }

        let mut compressor = Self {
            distance: 0,
            out_bytes: 0,
            out_bits: 0,
            dsize_bits,
            dsize_mask,
            ctype,
            dsize_bytes: dsize as u32,
            dist_bits: [0; 64],
            dist_codes: [0; 64],
            n_ch_bits,
            n_ch_codes,
            moffsets: [0; 516],
            phash_to_index: [0; 2304],
            out_buff: [0; 2050],
            work_buff: [0; 8708],
            phash_offs: [0; 8708],
        };
        compressor.dist_codes.copy_from_slice(DIST_CODES.as_slice());
        compressor.dist_bits.copy_from_slice(DIST_BITS.as_slice());
        Ok(compressor)
    }

    fn write_huffman_code<W>(&mut self, offset: usize, writer: &mut W) -> Result<()>
    where
        W: Write,
    {
        self.write_bits(
            self.n_ch_bits[offset] as usize,
            self.n_ch_codes[offset] as u64,
            writer,
        )
    }

    fn write_distance_pair<W>(&mut self, shift: u32, mask: u32, writer: &mut W) -> Result<()>
    where
        W: Write,
    {
        self.write_bits(
            self.dist_bits[(self.distance >> shift) as usize] as usize,
            self.dist_codes[(self.distance >> shift) as usize] as u64,
            writer,
        )?;
        self.write_bits(shift as usize, (self.distance & mask) as u64, writer)?;
        Ok(())
    }

    fn write_bits<W>(&mut self, mut nbits: usize, mut bit_buff: u64, writer: &mut W) -> Result<()>
    where
        W: Write,
    {
        // Recurse until we have inputs in the byte range.
        if nbits > 8 {
            self.write_bits(8, bit_buff, writer)?;
            bit_buff >>= 8;
            nbits -= 8;
        }

        // Now we update the workbuf output byte state to contain the in-range inputs
        let out_bits = self.out_bits;
        self.out_buff[self.out_bytes] |= (bit_buff << out_bits) as u8;
        self.out_bits += nbits;
        if self.out_bits > 8 {
            self.out_bytes += 1;
            bit_buff >>= 8 - out_bits;
            self.out_buff[self.out_bytes] = bit_buff as u8;
            self.out_bits &= 7;
        } else {
            self.out_bits &= 7;
            if self.out_bits == 0 {
                self.out_bytes += 1;
            }
        }

        // If our output byte state is large enough, write a block out.
        // Note, this acts as a buffer, so there is less need for buffering the output stream.
        if self.out_bytes >= 0x800 {
            self.flush_buf(writer)?;
        }

        Ok(())
    }

    fn flush_buf<W>(&mut self, writer: &mut W) -> Result<()>
    where
        W: Write,
    {
        assert!(self.out_bytes >= 0x800);
        writer.write_all(&self.out_buff[0..0x800])?;

        // Clear the buffer, preserving any in-progress bits at the end of the buffer
        let save_ch1 = self.out_buff[0x800];
        let save_ch2 = self.out_buff[self.out_bytes];
        self.out_bytes -= 0x800;
        self.out_buff.copy_from_slice(&[0; 0x802]);
        if self.out_bytes != 0 {
            self.out_buff[0] = save_ch1;
        }
        if self.out_bits != 0 {
            self.out_buff[self.out_bytes] = save_ch2;
        }
        Ok(())
    }

    // Macro for calculating hash of the current byte pair.
    // Note that most exact byte pair hash would be buffer[0] + buffer[1] << 0x08,
    // but even this way gives nice indication of equal byte pairs, with significantly
    // smaller size of the array that holds numbers of those hashes
    fn pair_hash(work_buff: &[u8], offset: usize) -> usize {
        let a = *work_buff.get(offset).unwrap_or(&0) as i32;
        let b = *work_buff.get(offset + 1).unwrap_or(&0) as i32;
        (a * 4 + b * 5) as usize
    }

    // Builds the "hash_to_index" table and "pair_hash_offsets" table.
    // Every element of "hash_to_index" will contain lowest index to the
    // "pair_hash_offsets" table, effectively giving offset of the first
    // occurence of the given PAIR_HASH in the input data.
    fn sort_buffer(&mut self, start_offset: usize, end_offset: usize) {
        // Zero the entire "phash_to_index" table
        self.phash_to_index.copy_from_slice(&[0; 2304]);

        // Step 1: Count amount of each PAIR_HASH in the input buffer
        // The table will look like this:
        //  offs 0x000: Number of occurences of PAIR_HASH 0
        //  offs 0x001: Number of occurences of PAIR_HASH 1
        //  ...
        //  offs 0x8F7: Number of occurences of PAIR_HASH 0x8F7 (the highest hash value)
        for offset in start_offset..end_offset {
            self.phash_to_index[Self::pair_hash(&self.work_buff, offset)] += 1;
        }

        // Step 2: Convert the table to the array of PAIR_HASH amounts.
        // Each element contains count of PAIR_HASHes that is less or equal
        // to element index
        // The table will look like this:
        //  offs 0x000: Number of occurences of PAIR_HASH 0 or lower
        //  offs 0x001: Number of occurences of PAIR_HASH 1 or lower
        //  ...
        //  offs 0x8F7: Number of occurences of PAIR_HASH 0x8F7 or lower
        let mut total_sum = 0u16;
        for phash in &mut self.phash_to_index {
            total_sum = (total_sum as i32 + *phash as i32) as u16;
            *phash = total_sum;
        }

        // Step 3: Convert the table to the array of indexes.
        // Now, each element contains index to the first occurence of given PAIR_HASH
        for bpoffset in (start_offset..end_offset).rev() {
            let bphash = Self::pair_hash(&self.work_buff, bpoffset);

            self.phash_to_index[bphash] -= 1;
            self.phash_offs[self.phash_to_index[bphash] as usize] = bpoffset as u16;
        }
    }

    // This function searches for a repetition
    // (a previous occurence of the current byte sequence)
    // Returns length of the repetition, and stores the backward distance
    // to pWork structure.
    fn find_rep(&mut self, start_offset: usize, input_data: *const u8) -> u32 {
        // Calculate the previous position of the PAIR_HASH
        let phash = Self::pair_hash(&self.work_buff, start_offset);
        let min_phash_offs = (start_offset - self.dsize_bytes as usize + 1) as u16;
        let mut phash_offs_index = self.phash_to_index[phash];

        unsafe {
            // If the PAIR_HASH offset is below the limit, find a next one
            let phash_to_index = self.phash_to_index.as_mut_ptr().add(phash);
            let mut phash_offs = self.phash_offs.as_ptr().offset(phash_offs_index as isize);
            if *phash_offs < min_phash_offs {
                while *phash_offs < min_phash_offs {
                    phash_offs_index = phash_offs_index.wrapping_add(1);
                    phash_offs = phash_offs.offset(1);
                }
                *phash_to_index = phash_offs_index;
            }

            // Get the first location of the PAIR_HASH,
            // and thus the first eventual location of byte repetition
            phash_offs = self.phash_offs.as_ptr().offset(phash_offs_index as isize);
            let mut prev_repetition =
                (self.work_buff).as_mut_ptr().offset(*phash_offs as isize) as *const u8;
            let repetition_limit = input_data.offset(-1);

            // If the current PAIR_HASH was not encountered before,
            // we haven't found a repetition.
            if prev_repetition >= repetition_limit {
                return 0;
            }

            // We have found a match of a PAIR_HASH. Now we have to make sure
            // that it is also a byte match, because PAIR_HASH is not unique.
            // We compare the bytes and count the length of the repetition
            let mut rep_length = 1u32;
            let mut equal_byte_count: u32;
            let mut input_data_ptr = input_data;
            loop {
                // If the first byte of the repetition and the so-far-last byte
                // of the repetition are equal, we will compare the blocks.
                if *input_data_ptr == *prev_repetition
                    && *input_data_ptr.offset(rep_length.wrapping_sub(1) as isize)
                        == *prev_repetition.offset(rep_length.wrapping_sub(1) as isize)
                {
                    // Skip the current byte
                    prev_repetition = prev_repetition.offset(1);
                    input_data_ptr = input_data_ptr.offset(1);
                    equal_byte_count = 2;

                    // Now count how many more bytes are equal
                    while equal_byte_count < 0x204 {
                        prev_repetition = prev_repetition.offset(1);
                        input_data_ptr = input_data_ptr.offset(1);
                        if *prev_repetition != *input_data_ptr {
                            break;
                        }
                        equal_byte_count += 1;
                    }

                    // If we found a repetition of at least the same length, take it.
                    // If there are multiple repetitions in the input buffer, this will
                    // make sure that we find the most recent one, which in turn allows
                    // us to store backward length in less amount of bits
                    input_data_ptr = input_data;
                    if equal_byte_count >= rep_length {
                        // Calculate the backward distance of the repetition.
                        // Note that the distance is stored as decremented by 1
                        self.distance = (input_data.offset_from(prev_repetition) as i64
                            + equal_byte_count as i64
                            - 1i64) as u32;

                        // Repetitions longer than 10 bytes will be stored in more bits,
                        // so they need a bit different handling
                        rep_length = equal_byte_count;
                        if rep_length > 10 {
                            break;
                        }
                    }
                }

                // Move forward in the table of PAIR_HASH repetitions.
                // There might be a more recent occurence of the same repetition.
                phash_offs_index = phash_offs_index.wrapping_add(1);
                phash_offs = phash_offs.offset(1);
                prev_repetition = (self.work_buff).as_mut_ptr().offset(*phash_offs as isize);

                // If the next repetition is beyond the minimum allowed repetition, we are done.
                if prev_repetition >= repetition_limit {
                    // A repetition must have at least 2 bytes, otherwise it's not worth it
                    return if rep_length >= 2 { rep_length } else { 0 };
                }
            }

            // If the repetition has max length of 0x204 bytes, we can't go any further
            if equal_byte_count == 0x204 {
                self.distance -= 1;
                return equal_byte_count;
            }

            // Check for possibility of a repetition that occurs at more recent position
            phash_offs = self.phash_offs.as_ptr().offset(phash_offs_index as isize);
            if (self.work_buff)
                .as_ptr()
                .offset(*phash_offs.offset(1) as isize)
                >= repetition_limit
            {
                return rep_length;
            }

            //
            // The following part checks if there isn't a longer repetition at
            // a latter offset, that would lead to better compression.
            //
            // Example of data that can trigger this optimization:
            //
            //   "EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEQQQQQQQQQQQQ"
            //   "XYZ"
            //   "EEEEEEEEEEEEEEEEQQQQQQQQQQQQ";
            //
            // Description of data in this buffer
            //   [0x00] Single byte "E"
            //   [0x01] Single byte "E"
            //   [0x02] Repeat 0x1E bytes from [0x00]
            //   [0x20] Single byte "X"
            //   [0x21] Single byte "Y"
            //   [0x22] Single byte "Z"
            //   [0x23] 17 possible previous repetitions of length at least 0x10 bytes:
            //          - Repetition of 0x10 bytes from [0x00] "EEEEEEEEEEEEEEEE"
            //          - Repetition of 0x10 bytes from [0x01] "EEEEEEEEEEEEEEEE"
            //          - Repetition of 0x10 bytes from [0x02] "EEEEEEEEEEEEEEEE"
            //          ...
            //          - Repetition of 0x10 bytes from [0x0F] "EEEEEEEEEEEEEEEE"
            //          - Repetition of 0x1C bytes from [0x10] "EEEEEEEEEEEEEEEEQQQQQQQQQQQQ"
            //          The last repetition is the best one.
            //

            self.moffsets[0] = 0xFFFF;
            self.moffsets[1] = 0;
            let mut di_val = 0u16;

            // Note: I failed to figure out what does the table "offs09BC" mean.
            // If anyone has an idea, let me know to zezula_at_volny_dot_cz
            let mut offs_in_rep = 1u16;
            while (offs_in_rep as u32) < rep_length {
                if offs_in_rep != di_val {
                    di_val = self.moffsets[di_val as usize];
                    if di_val != 0xFFFF {
                        continue;
                    }
                }
                di_val = di_val.wrapping_add(1);
                offs_in_rep = offs_in_rep.wrapping_add(1);
                self.moffsets[offs_in_rep as usize] = di_val;
            }

            //
            // Now go through all the repetitions from the first found one
            // to the current input data, and check if any of them might be
            // a start of a greater sequence match.
            //
            prev_repetition = (self.work_buff)
                .as_mut_ptr()
                .offset(*phash_offs.offset(0) as isize);
            let mut prev_rep_end = prev_repetition.offset(rep_length as isize);
            let mut rep_length2 = rep_length;

            loop {
                rep_length2 = self.moffsets[rep_length2 as usize] as u32;
                if rep_length2 == 0xFFFF {
                    rep_length2 = 0;
                }

                // Get the pointer to the previous repetition
                phash_offs = self.phash_offs.as_ptr().offset(phash_offs_index as isize);

                // Skip those repetitions that don't reach the end
                // of the first found repetition
                loop {
                    phash_offs = phash_offs.offset(1);
                    phash_offs_index = phash_offs_index.wrapping_add(1);
                    prev_repetition = (self.work_buff).as_mut_ptr().offset(*phash_offs as isize);
                    if prev_repetition >= repetition_limit {
                        return rep_length;
                    }
                    if prev_repetition.offset(rep_length2 as isize) >= prev_rep_end {
                        break;
                    }
                }

                // Verify if the last but one byte from the repetition matches
                // the last but one byte from the input data.
                // If not, find the next repetition
                let pre_last_byte = *input_data.offset(rep_length.wrapping_sub(2) as isize);
                // let pre_last_byte = *self.work_buff.get(start_offset + (rep_length as usize - 2)).unwrap_or(&0);
                if pre_last_byte == *prev_repetition.offset(rep_length.wrapping_sub(2) as isize) {
                    // If the new repetition reaches beyond the end
                    // of previously found repetition, reset the repetition length to zero.
                    if prev_repetition.offset(rep_length2 as isize) != prev_rep_end {
                        prev_rep_end = prev_repetition;
                        rep_length2 = 0;
                    }
                } else {
                    phash_offs = self.phash_offs.as_ptr().offset(phash_offs_index as isize);
                    loop {
                        phash_offs = phash_offs.offset(1);
                        phash_offs_index = phash_offs_index.wrapping_add(1);
                        prev_repetition =
                            (self.work_buff).as_mut_ptr().offset(*phash_offs as isize);
                        if prev_repetition >= repetition_limit {
                            return rep_length;
                        }
                        if !(*prev_repetition.offset(rep_length.wrapping_sub(2) as isize)
                            != pre_last_byte
                            || *prev_repetition != *input_data)
                        {
                            break;
                        }
                    }

                    // Reset the length of the repetition to 2 bytes only
                    prev_rep_end = prev_repetition.offset(2);
                    rep_length2 = 2;
                }

                // Find out how many more characters are equal to the first repetition.
                while *prev_rep_end == *input_data.offset(rep_length2 as isize) {
                    rep_length2 += 1;
                    if rep_length2 >= 0x204 {
                        break;
                    }
                    prev_rep_end = prev_rep_end.offset(1);
                }

                // Is the newly found repetion at least as long as the previous one?
                if rep_length2 >= rep_length {
                    // Calculate the distance of the new repetition
                    self.distance = (input_data.offset_from(prev_repetition) - 1) as u32;
                    rep_length = rep_length2;
                    if rep_length == 0x204 {
                        return rep_length;
                    }

                    // Update the additional elements in the "offs09BC" table
                    // to reflect new rep length
                    while (offs_in_rep as u32) < rep_length2 {
                        if offs_in_rep != di_val {
                            di_val = self.moffsets[di_val as usize];
                            if di_val != 0xFFFF {
                                continue;
                            }
                        }
                        di_val = di_val.wrapping_add(1);
                        offs_in_rep = offs_in_rep.wrapping_add(1);
                        self.moffsets[offs_in_rep as usize] = di_val;
                    }
                }
            }
        }
    }

    fn compress_stream<R, W>(&mut self, reader: &mut R, writer: &mut W) -> Result<()>
    where
        R: Read + ?Sized,
        W: Write,
    {
        // Write header and clear buffer
        self.out_buff[0] = self.ctype as u8;
        self.out_buff[1] = self.dsize_bits as u8;
        self.out_buff[2..].copy_from_slice(&[0; 0x800]);
        self.out_bytes = 2;
        self.out_bits = 0;

        unsafe {
            let mut start_offset = self.dsize_bytes as usize + 0x204;
            let mut input_data: *mut u8 = (self.work_buff)
                .as_mut_ptr()
                .offset(self.dsize_bytes as isize)
                .offset(0x204);
            let mut phase = Phase::default();

            let mut input_data_ended = false;
            'main_loop: while !input_data_ended {
                let mut bytes_to_load = 0x1000;
                let mut total_loaded = 0u32;

                // Load the bytes from the input stream, up to 0x1000 bytes
                while bytes_to_load != 0 {
                    let off = (self.dsize_bytes + 0x204 + total_loaded) as usize;
                    let tgt = &mut self.work_buff[off..off + bytes_to_load as usize];
                    let bytes_loaded = reader.read(tgt)? as u32;
                    if bytes_loaded == 0 {
                        if total_loaded == 0 && phase == Phase::Phase0 {
                            break 'main_loop;
                        }
                        input_data_ended = true;
                        break;
                    } else {
                        bytes_to_load -= bytes_loaded;
                        total_loaded += bytes_loaded;
                    }
                }

                let mut end_offset = (self.dsize_bytes + total_loaded) as usize;
                if input_data_ended {
                    end_offset += 0x204;
                }

                // Search the PAIR_HASHes of the loaded blocks. Also, include
                // previously compressed data, if we have any in the output buffer.
                match phase {
                    Phase::Phase0 => {
                        self.sort_buffer(start_offset, end_offset + 1);
                        phase.next();
                        if self.dsize_bytes != 0x1000 {
                            phase.next();
                        }
                    }
                    Phase::Phase1 => {
                        self.sort_buffer(
                            start_offset - self.dsize_bytes as usize + 0x204,
                            end_offset + 1,
                        );
                        phase.next();
                    }
                    Phase::Phase2 => {
                        self.sort_buffer(start_offset - self.dsize_bytes as usize, end_offset + 1);
                    }
                }

                // Perform the compression of the current block
                'compress_block_loop: while start_offset < end_offset {
                    // Find if the current byte sequence wasn't there before.
                    let mut rep_length = self.find_rep(start_offset, input_data);
                    while rep_length != 0 {
                        // If we found repetition of 2 bytes, that is 0x100 or further back,
                        // don't bother. Storing the distance of 0x100 bytes would actually
                        // take more space than storing the 2 bytes as-is.
                        if rep_length == 2 && self.distance >= 0x100 {
                            break;
                        }

                        // When we are at the end of the input data, we cannot allow
                        // the repetition to go past the end of the input data.
                        if input_data_ended && start_offset + rep_length as usize > end_offset {
                            // Shorten the repetition length so that it only covers valid data
                            rep_length = (end_offset - start_offset).try_into()?;
                            if rep_length < 2 {
                                break;
                            }

                            // If we got repetition of 2 bytes, that is 0x100 or more backward, don't bother
                            if rep_length == 2 && self.distance >= 0x100 {
                                break;
                            }
                        } else if !(rep_length >= 8 || start_offset + 1 >= end_offset) {
                            // Try to find better repetition 1 byte later.
                            // Example: "ARROCKFORT" "AROCKFORT"
                            // When "input_data" points to the second string, FindRep
                            // returns the occurence of "AR". But there is longer repetition "ROCKFORT",
                            // beginning 1 byte after.
                            let save_rep_length = rep_length;
                            let save_distance = self.distance;
                            rep_length = self.find_rep(start_offset + 1, input_data.offset(1));

                            // Only use the new repetition if it's length is greater than the previous one
                            // Note: if only 1 better and previous distance is less than 0x80, it's not worth it
                            if rep_length > save_rep_length && rep_length > save_rep_length + 1
                                || save_distance > 0x80
                            {
                                // Flush one byte, so that input_data will point to the secondary repetition
                                self.write_huffman_code(
                                    self.work_buff[start_offset] as usize,
                                    writer,
                                )?;
                                input_data = input_data.offset(1);
                                start_offset += 1;
                                continue;
                            }

                            // Revert to the previous repetition
                            rep_length = save_rep_length;
                            self.distance = save_distance;
                        }

                        // Write out the discovered repetition
                        self.write_huffman_code(rep_length as usize + 0xfe, writer)?;
                        if rep_length == 2 {
                            self.write_distance_pair(2, 0x3, writer)?;
                        } else {
                            self.write_distance_pair(self.dsize_bits, self.dsize_mask, writer)?;
                        }
                        input_data = input_data.offset(rep_length as isize);
                        start_offset += rep_length as usize;
                        continue 'compress_block_loop;
                    }

                    // If there was no previous repetition for the current position in the input data,
                    // just output the 9-bit literal for the one character
                    self.write_huffman_code(self.work_buff[start_offset] as usize, writer)?;
                    input_data = input_data.offset(1);
                    start_offset += 1;
                }

                if !input_data_ended {
                    input_data = input_data.offset(-0x1000);
                    start_offset -= 0x1000;
                    self.work_buff
                        .copy_within(0x1000..0x1000 + self.dsize_bytes as usize + 0x204, 0);
                }
            }

            // Write the termination literal
            self.write_huffman_code(0x305, writer)?;
            if self.out_bits != 0 {
                self.out_bytes += 1;
            }

            // Note: flush_buf only flushes full buffers; this is the only unbuffered
            // write and does not need to maintain state, so we just make the write directly.
            writer.write_all(&self.out_buff[0..self.out_bytes])?;
        }
        Ok(())
    }
}

pub fn implode<R, W>(
    reader: &mut R,
    writer: &mut W,
    ctype: CompressionType,
    dsize: DictSize,
) -> Result<()>
where
    R: Read + ?Sized,
    W: Write,
{
    let mut compressor = Compressor::new(ctype, dsize)?;
    compressor.compress_stream(reader, writer)?;
    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;
    use anyhow::ensure;
    use std::{
        io::{Read, Write},
        process::Command,
    };
    use tempfile::Builder;
    use walkdir::WalkDir;

    #[allow(unused)]
    fn roundtrip_proc(in_buf: &[u8]) -> Result<()> {
        let in_file = Builder::new().tempfile()?;
        let in_path = in_file.path().canonicalize()?;
        let in_name = in_path.to_str().unwrap();
        let out_file = Builder::new().tempfile()?;
        let out_path = out_file.path().canonicalize()?;
        let out_name = out_path.to_str().unwrap();

        in_file.as_file().write_all(in_buf)?;

        let out = Command::new("/path/to/implode/implode")
            .args(["--", in_name, out_name])
            .output()?;
        if !out.status.success() {
            println!("status: {}", out.status);
            println!("stdout: {}", String::from_utf8(out.stdout)?);
            println!("stderr: {}", String::from_utf8(out.stderr)?);
        }
        ensure!(out.status.success());

        let mut out_buf = Vec::new();
        out_file.as_file().read_to_end(&mut out_buf)?;
        assert_ne!(in_buf, out_buf);

        let rt_buf = crate::explode::explode(&out_buf, Some(in_buf.len()))?;
        assert_eq!(in_buf.len(), rt_buf.len());
        for (i, inc) in in_buf.iter().enumerate() {
            assert_eq!(*inc, rt_buf[i]);
        }
        assert_eq!(in_buf, rt_buf);

        Ok(())
    }

    fn roundtrip(in_buf: &[u8]) -> Result<()> {
        let mut out_buf = Vec::new();
        implode(
            &mut std::io::BufReader::new(in_buf),
            &mut out_buf,
            CompressionType::Binary,
            DictSize::Size3,
        )?;

        let rt_buf = crate::explode::explode(&out_buf, Some(in_buf.len()))?;
        assert_eq!(in_buf.len(), rt_buf.len());
        for (i, inc) in in_buf.iter().enumerate() {
            assert_eq!(*inc, rt_buf[i]);
        }
        assert_eq!(in_buf, rt_buf);

        Ok(())
    }

    #[test]
    fn test_smoke() -> Result<()> {
        roundtrip("Hello, World!".as_bytes())?;
        let mut buf = vec![0u8; 1024 * 1024];
        for i in 0u32..1024 * 1024 {
            buf[i as usize] = ((i >> 24) ^ (i >> 16) ^ (i >> 8) ^ i) as u8;
        }
        roundtrip(&buf)?;
        Ok(())
    }

    #[ignore]
    #[test]
    fn test_roundtrip_all() -> Result<()> {
        for entry in WalkDir::new("../../test_data/unpacked/")
            .into_iter()
            .filter_map(|e| e.ok())
        {
            if entry.file_type().is_file() {
                println!("At: {}", entry.path().display());
                let buf = std::fs::read(entry.path())?;
                roundtrip(&buf)?;
            }
        }
        Ok(())
    }
}

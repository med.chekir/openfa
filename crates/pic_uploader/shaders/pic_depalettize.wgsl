// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.

fn unpack_block(v: u32) -> vec4<u32> {
    return vec4<u32>(
        (v >> 0u) & 255u,
        (v >> 8u) & 255u,
        (v >> 16u) & 255u,
        (v >> 24u) & 255u
    );
}

var<private> WORKGROUP_WIDTH: u32 = 65536u;

@group(0) @binding(0) var<storage> palette: array<u32,256u>;
@group(0) @binding(1) var<storage, read> raw_img: array<u32>;
@group(0) @binding(2) var<storage, read_write> tgt_img: array<u32>;

@compute @workgroup_size(64, 2, 1)
fn main(@builtin(global_invocation_id) global_idx: vec3<u32>) {
    // Unpack 4 packed, 1 byte pixels
    let block_offset = global_idx.x + global_idx.y * WORKGROUP_WIDTH;
    let p = unpack_block(raw_img[block_offset]);

    // look up each pixel in the palette, then write back to the target.
    tgt_img[4u * block_offset + 0u] = palette[p[0]];
    tgt_img[4u * block_offset + 1u] = palette[p[1]];
    tgt_img[4u * block_offset + 2u] = palette[p[2]];
    tgt_img[4u * block_offset + 3u] = palette[p[3]];
}

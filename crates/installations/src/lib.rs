// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.

mod game_info;
mod installations;

pub use crate::{
    game_info::{GameInfo, GAME_INFO},
    installations::{GameInstallation, Installations, LibsArgs, LibsOpts},
};

use anyhow::Result;
use codepage_437::{BorrowFromCp437, FromCp437, CP437_CONTROL};
use glob::{glob_with, MatchOptions};
use std::{
    borrow::Cow,
    path::{Path, PathBuf},
};

pub fn from_dos_string(input: Cow<[u8]>) -> Cow<str> {
    match input {
        Cow::Borrowed(r) => Cow::borrow_from_cp437(r, &CP437_CONTROL),
        Cow::Owned(o) => Cow::from(String::from_cp437(o, &CP437_CONTROL)),
    }
}

/// For some tools, the input list may be a empty (in which case glob), or a set of files,
/// in which case return the set of paths, or a list of names of lib entries.
/// If no files match, this returns None, otherwise Some and the set of matching files.
pub fn input_files(inputs: &[String], glob_pattern: &str) -> Result<Vec<PathBuf>> {
    Ok(if inputs.is_empty() {
        glob_with(
            glob_pattern,
            MatchOptions {
                case_sensitive: false,
                require_literal_separator: false,
                require_literal_leading_dot: false,
            },
        )?
        .filter_map(|v| v.ok())
        .collect()
    } else {
        inputs
            .iter()
            .map(|v| Path::new(v).to_owned())
            .filter(|v| v.exists())
            .collect()
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use anyhow::Result;

    #[test]
    fn test_catalog_builder() -> Result<()> {
        let (_catalog, _installs) = Installations::for_testing()?;
        Ok(())
    }
}

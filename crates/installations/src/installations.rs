// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::game_info::{GameInfo, Search, GAME_INFO};
use anyhow::{anyhow, Result};
use catalog::{AssetCatalog, AssetCollection, DirectoryProvider, FileSystem};
use clap::Args;
use lib::{LibProvider, Priority};
use log::{info, trace, warn};
use nitrous::{inject_nitrous_resource, method, HeapMut, NitrousResource};
use pal::Palette;
use runtime::{Extension, Runtime};
use std::env::current_dir;
use std::{
    collections::HashSet,
    env,
    fmt::Write,
    fs,
    path::{Path, PathBuf},
};
use structopt::StructOpt;

#[derive(Clone, Debug, Default, Args)]
pub struct LibsArgs {
    /// The path to look in for game files (default: pwd)
    #[arg(short, long)]
    game_path: Option<PathBuf>,

    /// If not all required libs are found in the game path, look here. If the
    /// CD's LIB files have been copied into the game directory, this is unused.
    #[arg(short, long)]
    cd_path: Option<PathBuf>,

    /// For Fighter's Anthology, if the second disk's LIB files have not been
    /// copied into the game directory, and you want to use the reference
    /// materials, also provide this path. There is no ability to switch the
    /// disk, currently. (Note: reference still WIP, so not much point yet.)
    #[arg(long)]
    cd2_path: Option<PathBuf>,

    /// Extra directories to treat as libraries
    #[arg(short = 'l', long)]
    lib_paths: Vec<PathBuf>,

    /// Select the game, if there is more than one available
    #[arg(short = 'S', long)]
    select_game: Option<String>,

    /// Use special JANES dir lookup logic for test harness
    #[arg(long)]
    for_testing: bool,
}

#[derive(Clone, Debug, Default, StructOpt)]
pub struct LibsOpts {
    /// The path to look in for game files (default: pwd)
    #[structopt(short, long)]
    game_path: Option<PathBuf>,

    /// If not all required libs are found in the game path, look here. If the
    /// CD's LIB files have been copied into the game directory, this is unused.
    #[structopt(short, long)]
    cd_path: Option<PathBuf>,

    /// For Fighter's Anthology, if the second disk's LIB files have not been
    /// copied into the game directory, and you want to use the reference
    /// materials, also provide this path. There is no ability to switch the
    /// disk, currently. (Note: reference still WIP, so not much point yet.)
    #[structopt(long)]
    cd2_path: Option<PathBuf>,

    /// Select the game, if there is more than one available
    #[structopt(short = "S", long)]
    select_game: Option<String>,

    /// Use special JANES dir lookup logic for test harness
    #[structopt(long)]
    for_testing: bool,
}

impl LibsOpts {
    pub fn for_testing() -> Self {
        Self {
            for_testing: true,
            ..Default::default()
        }
    }
}

#[derive(Debug)]
pub struct GameInstallation {
    name: String,
    game: &'static GameInfo,
    game_path: PathBuf,
    cd1_path: Option<PathBuf>,
    cd2_path: Option<PathBuf>,
    palette: Palette,
    // catalog: Catalog,
}

impl GameInstallation {
    fn from_path(
        name: &str,
        opts: &LibsOpts,
        game: &'static GameInfo,
        game_path: &Path,
        collection: &mut AssetCollection,
    ) -> Result<GameInstallation> {
        // Load libs from the installdir
        Self::populate_collection(name, game_path, 0, collection)?;

        let cd1_path = Self::find_and_populate_cdrom(
            &format!("{name}-cd1"),
            game.cd_libs,
            &opts.cd_path,
            game_path,
            -10,
            collection,
        )?;
        let cd2_path = if !game.optional_cd_libs.is_empty() {
            Self::find_and_populate_cdrom(
                &format!("{name}-cd2"),
                game.optional_cd_libs,
                &opts.cd2_path,
                game_path,
                -20,
                collection,
            )?
        } else {
            None
        };

        // Always load the palette; as a test and to provide it to _many_ places later.
        let palette = Palette::from_bytes(&collection.lookup("PALETTE.PAL")?.data()?)?;

        info!("Successfully found {} at {:?}...", game.name, game_path);
        Ok(GameInstallation {
            name: name.to_owned(),
            game,
            game_path: game_path.to_owned(),
            cd1_path,
            cd2_path,
            palette,
            // catalog,
        })
    }

    fn find_and_populate_cdrom(
        name: &str,
        sentinels: &'static [&'static str],
        opt_cd_path: &Option<PathBuf>,
        game_path: &Path,
        priority_adjust: i64,
        collection: &mut AssetCollection,
    ) -> Result<Option<PathBuf>> {
        if let Some(cd_path) = Self::find_cdrom(sentinels, opt_cd_path, game_path)? {
            info!("Using CD path: {:?}", cd_path);
            Self::populate_collection(name, &cd_path, priority_adjust, collection)?;
            Ok(Some(cd_path))
        } else {
            Ok(None)
        }
    }

    fn find_cdrom(
        sentinels: &'static [&'static str],
        opt_cd_path: &Option<PathBuf>,
        game_path: &Path,
    ) -> Result<Option<PathBuf>> {
        assert!(!sentinels.is_empty());

        // If the cd libs are copied into the game directory, no need to do more work.
        if Self::path_contains_libs(sentinels, game_path)? {
            return Ok(None);
        }

        // If the user has specified a cd path, check it before scanning.
        if let Some(cd_path) = opt_cd_path {
            if Self::path_contains_libs(sentinels, cd_path)? {
                return Ok(Some(cd_path.to_owned()));
            }
        }

        // Otherwise, scan for a cd path
        if let Some(cd_path) = Self::detect_cd_path(sentinels)? {
            return Ok(Some(cd_path.to_owned()));
        }

        // We'll continue if we can't find a cdrom, but may fail later, or disable functionality.
        warn!("failed to find CD-ROM; we will continue but may fail later or disable features");
        Ok(None)
    }

    fn path_contains_libs(sentinels: &'static [&'static str], path: &Path) -> Result<bool> {
        Ok(Self::listing_contains_libs(
            sentinels,
            &Installations::list_directory_canonical(path)?,
        ))
    }

    fn listing_contains_libs(sentinels: &'static [&'static str], files: &HashSet<String>) -> bool {
        sentinels.iter().all(|&name| files.contains(name))
    }

    fn populate_collection(
        name: &str,
        path: &Path,
        priority_adjust: i64,
        collection: &mut AssetCollection,
    ) -> Result<()> {
        // Always add all files in the root, as FA allows (and older games require for T2 access).
        collection.add_provider(
            name,
            Priority::from_path(path, priority_adjust)?.as_provider_priority(),
            DirectoryProvider::new(path)?,
        )?;
        // Add all libs and subdirs
        for entry in (fs::read_dir(path)?).flatten() {
            let lib_name = entry
                .path()
                .file_name()
                .ok_or_else(|| anyhow!("no file_name in {path:?}"))?
                .to_string_lossy()
                .to_string();
            let ext = entry
                .path()
                .extension()
                .map(|s| s.to_string_lossy().to_ascii_lowercase());
            if ext.as_deref() == Some("lib") {
                collection.add_provider(
                    &lib_name,
                    Priority::from_path(&entry.path(), priority_adjust)?.as_provider_priority(),
                    LibProvider::new(&entry.path())?,
                )?;
            } else if entry.path().is_dir() {
                // Unpacked libs don't have an extension, I don't think compatibility problems
                // are likely, because DirectoryProvider does not recurse.
                collection.add_provider(
                    &lib_name,
                    Priority::from_path(&entry.path(), priority_adjust + 1)?.as_provider_priority(),
                    DirectoryProvider::new(&entry.path())?,
                )?;
            }
        }
        Ok(())
    }

    #[cfg(target_os = "windows")]
    fn detect_cd_path(sentinels: &'static [&'static str]) -> Result<Option<PathBuf>> {
        Ok(Self::detect_cd_path_windows(sentinels))
    }
    #[allow(unused)]
    fn detect_cd_path_windows(sentinels: &'static [&'static str]) -> Option<PathBuf> {
        // Believe it or not, this is the normal way to discover a CD drive. :shrug:
        for letter in "ABCDEFGHIJKLMNOPQRSTUVWXYZ".chars() {
            let is_cd_path = sentinels.iter().all(|sentinel| {
                let mut buf = PathBuf::new();
                buf.push(format!("{letter}:\\{sentinel}"));
                trace!("Checking for CD with {:?}", buf);
                buf.exists()
            });

            if is_cd_path {
                let mut buf = PathBuf::new();
                buf.push(format!("{letter}:\\"));
                return Some(buf);
            }
        }
        None
    }

    #[cfg(not(target_os = "windows"))]
    fn detect_cd_path(sentinels: &'static [&'static str]) -> Result<Option<PathBuf>> {
        Self::detect_cd_path_unix(sentinels)
    }
    #[allow(unused)]
    fn detect_cd_path_unix(sentinels: &'static [&'static str]) -> Result<Option<PathBuf>> {
        for entry in (fs::read_dir("/mnt")?).flatten() {
            if !entry.path().is_dir() {
                continue;
            }
            let is_cd_path = sentinels.iter().all(|sentinel| {
                let mut buf = entry.path().to_owned();
                buf.push(sentinel);
                trace!("Checking for CD with {:?}", buf);
                buf.exists()
            });
            if is_cd_path {
                return Ok(Some(entry.path().to_owned()));
            }
        }
        Ok(None)
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn game(&self) -> &GameInfo {
        self.game
    }

    pub fn game_path(&self) -> &Path {
        &self.game_path
    }

    pub fn cd1_path(&self) -> Option<&Path> {
        self.cd1_path.as_deref()
    }

    pub fn cd2_path(&self) -> Option<&Path> {
        self.cd2_path.as_deref()
    }

    pub fn palette(&self) -> &Palette {
        &self.palette
    }
}

/// Search for artifacts and create catalogs for any and every game we
/// can get our hands on.
#[derive(NitrousResource)]
pub struct Installations {
    installations: Vec<GameInstallation>,
    selected_installation: Option<String>,
}

impl Extension for Installations {
    type Opts = LibsOpts;

    fn init(runtime: &mut Runtime, opts: LibsOpts) -> Result<()> {
        let installs =
            Installations::bootstrap(&mut runtime.resource_mut::<AssetCatalog>(), &opts)?;
        runtime.inject_resource(installs)?;
        Ok(())
    }
}

/// All detected game installations that are available to read assets from.
/// Catalog collections will be order to match the installations sort order.
/// Installation names match the catalog collection names, so info collection
/// can be used to map to the installation to, for example, get the palette.
/// Installations make the distinction between `primary` (shared with catalog)
/// and `selected`, for a game that has been opted into, either through
/// command line or executable directory.
#[inject_nitrous_resource]
impl Installations {
    pub fn for_testing() -> Result<(AssetCatalog, Self)> {
        let mut catalog = AssetCatalog::default();
        let libs = Self::bootstrap(&mut catalog, &LibsOpts::for_testing())?;
        Ok((catalog, libs))
    }

    pub fn bootstrap_args(catalog: &mut AssetCatalog, args: &LibsArgs) -> Result<Self> {
        let opts = LibsOpts {
            game_path: args.game_path.clone(),
            cd_path: args.cd_path.clone(),
            cd2_path: args.cd2_path.clone(),
            select_game: args.select_game.clone(),
            for_testing: args.for_testing,
        };
        Self::bootstrap(catalog, &opts)
    }

    pub fn bootstrap(catalog: &mut AssetCatalog, opts: &LibsOpts) -> Result<Self> {
        // If we specified a path manually, start there, else fall back to the current directory.
        let mut initial_path = opts.game_path.clone().unwrap_or_else(|| {
            env::current_dir()
                .expect("Cannot find the current directory, much less an FA series game to load; make sure you are running OpenFA on a computer and not a potato.")
        });

        if opts.for_testing {
            // Run up the tree until we find the JANES directory and pin start there.
            let mut path = current_dir()?;
            loop {
                let mut check = path.clone();
                check.push("JANES");
                if check.exists() {
                    initial_path = check;
                    break;
                }
                if let Some(parent) = path.parent() {
                    path = parent.to_owned();
                } else {
                    break;
                }
            }
        }

        // If we find a game in the base path instead of the Janes directory, try the parent.
        let base_path = if Self::detect_game_in_path(&initial_path)?.is_some() {
            initial_path
                .parent()
                .ok_or_else(|| {
                    anyhow!("Is your game installation in the drive root? Don't do that, it's a bad idea.")
                })?
                .to_owned()
        } else {
            // Otherwise, maybe we booted at root, or in a test env.
            // Look for a JANES dir under the current dir to pull from.
            let mut buf = PathBuf::from(&initial_path);
            buf.push("JANES");
            if buf.exists() {
                buf
            } else {
                initial_path.clone()
            }
        };

        let mut selected_installation: Option<String> = None;

        // Look for all game installations in subdirs of the base path, add each as a catalog.
        let mut installations = Vec::new();
        for entry in (fs::read_dir(base_path)?).flatten() {
            let path = entry.path();
            if path.is_dir() {
                if let Some(game) = Self::detect_game_in_path(&path)? {
                    let name = path
                        .file_name()
                        .ok_or_else(|| anyhow!("no file name"))?
                        .to_string_lossy()
                        .to_string();
                    // FIXME: priority?
                    catalog.add_collection(&name, 0)?;
                    let game_catalog = GameInstallation::from_path(
                        &name,
                        opts,
                        game,
                        &path,
                        catalog.collection_mut(&name)?,
                    )?;
                    installations.push(game_catalog);

                    // If the initial path (e.g. where we started the binary) is a game directory (rather than
                    // the JANES directory), set the initial game to the initial path.
                    if initial_path == path {
                        selected_installation = Some(name.clone());
                    }

                    // If we specified a specific name on the command, prefer that
                    if let Some(select_name) = &opts.select_game {
                        if select_name == &name {
                            selected_installation = Some(name);
                        }
                    }
                }
            }
        }

        // Construct the Libs
        let mut installs = Self {
            installations,
            selected_installation,
        };
        installs.select_installation(
            "not actually an installation, we just need to sort",
            catalog,
        )?;
        let selected = installs.selected_installation.clone();
        if let Some(name) = selected.as_deref() {
            installs.select_installation(name, catalog)?;
        }
        Ok(installs)
    }

    pub fn selected_installation(&self) -> Option<&str> {
        self.selected_installation.as_deref()
    }

    #[method]
    fn list_installations(&self) -> Result<String> {
        let mut s = String::new();
        for gcat in &self.installations {
            writeln!(
                s,
                "{}: {:>7} {} cd:{}",
                gcat.name,
                gcat.game.test_dir,
                gcat.game_path.display(),
                gcat.cd1_path.is_some()
            )?;
        }
        Ok(s)
    }

    #[method]
    pub fn select_install(&mut self, name: &str, mut heap: HeapMut) -> Result<()> {
        self.select_installation(name, &mut heap.resource_mut::<AssetCatalog>())
    }

    // #[method]
    // fn search(&self, glob: &str) -> Result<String> {
    //     let mut s = String::new();
    //     for name in self.catalog().find(FileSearch::for_glob(glob))? {
    //         let stat = self.catalog().stat_name(name)?;
    //         writeln!(
    //             s,
    //             "{:<11} {:>4} {:>6}",
    //             stat.name(),
    //             stat.compression().unwrap_or("none"),
    //             stat.unpacked_size()
    //         )
    //         .ok();
    //     }
    //     Ok(s)
    // }
    //
    // /// FIXME: return the gcat, but maybe do this after we move our tools
    // pub fn all(&self) -> impl Iterator<Item = (&'static GameInfo, &Palette, &Catalog)> + '_ {
    //     self.catalogs
    //         .iter()
    //         .map(|gcat| (gcat.game, &gcat.palette, &gcat.catalog))
    // }

    // /// FIXME: return the gcat, but maybe do this after we move our tools
    // /// Return one game if we selected a game, otherwise iterate all games
    // pub fn selected(
    //     &self,
    // ) -> Box<dyn Iterator<Item = (&'static GameInfo, &Palette, &Catalog)> + '_> {
    //     if let Some(selected) = self.selected_game {
    //         Box::new(
    //             self.catalogs
    //                 .iter()
    //                 .skip(selected)
    //                 .take(1)
    //                 .map(|gcat| (gcat.game, &gcat.palette, &gcat.catalog)),
    //         )
    //     } else {
    //         Box::new(self.all())
    //     }
    // }
    //
    // pub fn selected_game(&self) -> Option<&GameInstallation> {
    //     if let Some(offset) = self.selected_game {
    //         Some(&self.catalogs[offset])
    //     } else {
    //         None
    //     }
    // }

    // pub fn all_games(&self) -> &[GameCatalog] {
    //     &self.catalogs
    // }

    pub fn select_installation(&mut self, name: &str, catalog: &mut AssetCatalog) -> Result<()> {
        // Sort the installs by release date descending so that if we don't
        // select a default, we'll at least have a reasonable default catalog.
        self.installations.sort_by_key(|gi| {
            const TOP: i64 = 100_000;
            if gi.name == name {
                0
            } else {
                let game = gi.game();
                let julian_months = game.release_year as i64 * 12 + game.release_month as i64;
                TOP - julian_months
            }
        });

        // Propagate the installation ordering into the collection.
        for (i, installation) in self.installations.iter().enumerate() {
            catalog.update_collection_priority(installation.name(), i as i64)?;
        }

        Ok(())
    }

    pub fn installation_names(&self) -> impl Iterator<Item = &str> {
        self.installations
            .iter()
            .map(|installs| installs.name.as_ref())
    }

    /// Return the selected (or most recent, alphanumerically first) game installation.
    pub fn primary(&self) -> &GameInstallation {
        &self.installations[0]
    }

    /// Go from a collection name (e.g. in a file listing) and get the game's installation.
    pub fn installation(&self, name: &str) -> Result<&GameInstallation> {
        self.installations
            .iter()
            .find(|gi| gi.name == name)
            .ok_or_else(|| anyhow!("no such installation: {name}"))
    }

    /// Get the installation's base palette, given a collection name.
    /// Panics if installation does not exist.
    pub fn palette(&self, name: &str) -> Result<&Palette> {
        Ok(self.installation(name)?.palette())
    }

    /// Gets the primary installation's palette
    pub fn primary_palette(&self) -> &Palette {
        self.installations[0].palette()
    }

    /// Get the installation's GameInfo, given a collection name.
    pub fn game_info(&self, name: &str) -> Result<&GameInfo> {
        Ok(self.installation(name)?.game)
    }

    fn detect_game_in_path(path: &Path) -> Result<Option<&'static GameInfo>> {
        Ok(Self::detect_game_from_files(
            &Self::list_directory_canonical(path)?,
        ))
    }

    fn detect_game_from_files(game_files: &HashSet<String>) -> Option<&'static GameInfo> {
        GAME_INFO.into_iter().rev().find(|&game| {
            game.unique_files.iter().all(|&search| match search {
                Search::Present(name) => game_files.contains(name),
                Search::Absent(name) => !game_files.contains(name),
            })
        })
    }

    fn list_directory_canonical(path: &Path) -> Result<HashSet<String>> {
        // Filename capitalization is :shrug: so list and canonicalize everything all instead of
        // trying to rely on sane or predicable behavior from the filesystem or OS layers.
        let mut game_files = HashSet::new();
        for p in (fs::read_dir(path)?).flatten() {
            game_files.insert(
                p.file_name()
                    .to_ascii_uppercase()
                    .to_string_lossy()
                    .into_owned(),
            );
        }
        Ok(game_files)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use catalog::Search;

    #[test]
    fn test_lib_test_ordering() -> Result<()> {
        let (libs, installs) = Installations::for_testing()?;
        let all = libs.search(Search::for_extension("SH").must_match())?;
        assert_eq!(
            installs
                .game_info(all.first().unwrap().collection_name())?
                .test_dir,
            "FA"
        );
        assert_eq!(
            installs
                .game_info(all.last().unwrap().collection_name())?
                .test_dir,
            "USNF"
        );
        Ok(())
    }

    #[test]
    fn test_test_select_game_ordering() -> Result<()> {
        let mut opts = LibsOpts::for_testing();
        opts.select_game = Some("ATF".into());
        let mut catalog = AssetCatalog::default();
        let installs = Installations::bootstrap(&mut catalog, &opts)?;
        let all = catalog.search(Search::for_extension("SH").must_match())?;
        assert_eq!(all.first().unwrap().collection_name(), "ATF");
        assert_eq!(
            installs
                .game_info(all.last().unwrap().collection_name())?
                .test_dir,
            "USNF"
        );
        Ok(())
    }
}

// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
mod ux;

use crate::ux::{
    choose_game::ChooseGameUx, credits::CreditsUx, edit_libs::EditLibsUx, mission::MissionUx,
    reference::ReferenceUx, toplevel::ToplevelUx, view_world::ViewWorldUx, Ux,
};
use animate::Timeline;
use anyhow::Result;
use arcball::ArcBallSystem;
use asset_loader::AssetLoader;
use atmosphere::Atmosphere;
use camera::ScreenCamera;
use catalog::{AssetCatalog, CatalogOpts};
use composite::Composite;
use event_mapper::EventMapper;
use flight_dynamics::ClassicFlightModel;
use game_editor::EditLibUx;
use geodb::GeoDb;
use gui::Gui;
use installations::{Installations, LibsOpts};
use mantle::{Core, DetailLevelOpts, DisplayOpts};
use marker::Markers;
use orrery::Orrery;
use player::PlayerCameraController;
use runtime::{Runtime, ScriptHerder, StartupOpts, StdPaths, StdPathsOpts};
use shape::Shape;
use spog::{Dashboard, Terminal};
use stars::Stars;
use structopt::StructOpt;
use t2_terrain::T2TerrainBuffer;
use terminal_size::{terminal_size, Width};
use terrain::{Terrain, TerrainOpts};
use tracelog::{TraceLog, TraceLogOpts};
use vehicle::{
    AirbrakeEffector, BayEffector, FlapsEffector, GearEffector, HookEffector, PitchInceptor,
    PowerSystem, RollInceptor, ThrustVectorPitchControl, ThrustVectorPitchEffector,
    ThrustVectorYawControl, ThrustVectorYawEffector, VtolAngleControl, VtolAngleEffector,
    YawInceptor,
};
use world::World;
use xt::TypeManager;

const PRELUDE: &str = r#"
// Game Controls
    // System controls
    bindings.bind_in("ux:mission", "Control+q", "ux.return_to_top()");
    bindings.bind_in("ux:mission", "Escape", "@ux.mission_ux.toggle_show_menu()");
    bindings.bind_in("ux:mission", "c", "time.next_time_compression()");
    bindings.bind_in("ux:mission", "Control+F3", "dashboard.toggle()");

    // Orrery
    bindings.bind_in("ux:mission", "+mouse2", "orrery.move_sun($pressed)");
    bindings.bind_in("ux:mission", "mouseMotion", "orrery.handle_mousemove($dx)");

    // Camera Controls
    bindings.bind_in("ux:mission", "F1", "@camera.controller.set_mode('Forward')");
    bindings.bind_in("ux:mission", "F2", "@camera.controller.set_mode('Backward')");
    bindings.bind_in("ux:mission", "F3", "@camera.controller.set_mode('LookUp')");
    bindings.bind_in("ux:mission", "F4", "@camera.controller.set_mode('Target')");
    bindings.bind_in("ux:mission", "F5", "@camera.controller.set_mode('Incoming')");
    bindings.bind_in("ux:mission", "F6", "@camera.controller.set_mode('Wingman')");
    bindings.bind_in("ux:mission", "F7", "@camera.controller.set_mode('PlayerToTarget')");
    bindings.bind_in("ux:mission", "F8", "@camera.controller.set_mode('TargetToPlayer')");
    bindings.bind_in("ux:mission", "F9", "@camera.controller.set_mode('FlyBy')");
    bindings.bind_in("ux:mission", "F10", "@camera.controller.set_mode('External')");
    bindings.bind_in("ux:mission", "F12", "@camera.controller.set_mode('Missle')");
    bindings.bind_in("ux:mission", "+mouse1", "@camera.controller.set_pan_view($pressed)");
    bindings.bind_in("ux:mission", "mouseMotion", "@camera.controller.handle_mousemotion($dx, $dy)");
    bindings.bind_in("ux:mission", "mouseWheel", "@camera.controller.handle_mousewheel($vertical_delta)");

    // Flight controls
    bindings.bind_in("ux:mission", "key1", "@Player.throttle.set_military(0.)");
    bindings.bind_in("ux:mission", "key2", "@Player.throttle.set_military(25.)");
    bindings.bind_in("ux:mission", "key3", "@Player.throttle.set_military(50.)");
    bindings.bind_in("ux:mission", "key4", "@Player.throttle.set_military(75.)");
    bindings.bind_in("ux:mission", "key5", "@Player.throttle.set_military(100.)");
    bindings.bind_in("ux:mission", "key6", "@Player.throttle.set_afterburner(0)");
    bindings.bind_in("ux:mission", "b", "@Player.airbrake.toggle()");
    bindings.bind_in("ux:mission", "f", "@Player.flaps.toggle()");
    bindings.bind_in("ux:mission", "h", "@Player.hook.toggle()");
    bindings.bind_in("ux:mission", "o", "@Player.bay.toggle()");
    bindings.bind_in("ux:mission", "g", "@Player.gear.toggle()");
    bindings.bind_in("ux:mission", "+Up", "@Player.stick_pitch.key_move_front($pressed)");
    bindings.bind_in("ux:mission", "+Down", "@Player.stick_pitch.key_move_back($pressed)");
    bindings.bind_in("ux:mission", "+Left", "@Player.stick_roll.key_move_left($pressed)");
    bindings.bind_in("ux:mission", "+Right", "@Player.stick_roll.key_move_right($pressed)");
    bindings.bind_in("ux:mission", "+Comma", "@Player.pedals_yaw.key_move_left($pressed)");
    bindings.bind_in("ux:mission", "+Period", "@Player.pedals_yaw.key_move_right($pressed)");
    bindings.bind_in("ux:mission", "Control+Up", "@Player.thrust_vector_pitch.key_move_front($pressed)");
    bindings.bind_in("ux:mission", "Control+Down", "@Player.thrust_vector_pitch.key_move_back($pressed)");
    bindings.bind_in("ux:mission", "Control+Left", "@Player.thrust_vector_yaw.key_move_left($pressed)");
    bindings.bind_in("ux:mission", "Control+Right", "@Player.thrust_vector_yaw.key_move_right($pressed)");
    bindings.bind_in("ux:mission", "key0", "@Player.thrust_vector_pitch.recenter(); @Player.thrust_vector_yaw.recenter()");
    //bindings.bind_in("ux:mission", "joyX", "@Player.elevator.set_position($axis)");
    bindings.bind_in("ux:mission", "n", "system.toggle_show_normals()");
    bindings.bind_in("ux:mission", "Shift+Slash", "system.toggle_show_help()");

// Credits Controls
    bindings.bind_in("ux:credits", "Escape", "ux.return_to_top()");

// Game Chooser Ux Controls
    bindings.bind_in("ux:choose_game", "Escape", "window.request_quit()");

// Toplevel Ux Controls
    bindings.bind_in("ux:toplevel", "Escape", "window.request_quit()");

// Edit Libs Controls
    bindings.bind_in("ux:edit_libs", "Escape", "ux.return_to_top()");

// Edit Lib Controls
    bindings.bind_in("ux:edit_lib", "Escape", "ux.escape_pressed()");
    bindings.bind_in("ux:edit_lib", "Control+z", "@ux.edit_lib_ux.undo_pressed()");
    bindings.bind_in("ux:edit_lib", "Control+Shift+z", "@ux.edit_lib_ux.redo_pressed()");

// Edit Shape controls
    bindings.bind_in("ux:edit_lib:shape", "Escape", "ux.escape_pressed()");
    bindings.bind_in("ux:edit_lib:shape", "Control+z", "@ux.edit_lib_ux.undo_pressed()");
    bindings.bind_in("ux:edit_lib:shape", "Control+Shift+z", "@ux.edit_lib_ux.redo_pressed()");
    bindings.bind_in("ux:edit_lib:shape", "key1", "@Player.throttle.set_military(0.)");
    bindings.bind_in("ux:edit_lib:shape", "key2", "@Player.throttle.set_military(25.)");
    bindings.bind_in("ux:edit_lib:shape", "key3", "@Player.throttle.set_military(50.)");
    bindings.bind_in("ux:edit_lib:shape", "key4", "@Player.throttle.set_military(75.)");
    bindings.bind_in("ux:edit_lib:shape", "key5", "@Player.throttle.set_military(100.)");
    bindings.bind_in("ux:edit_lib:shape", "key6", "@Player.throttle.set_afterburner(0)");
    bindings.bind_in("ux:edit_lib:shape", "b", "@Player.airbrake.toggle()");
    bindings.bind_in("ux:edit_lib:shape", "f", "@Player.flaps.toggle()");
    bindings.bind_in("ux:edit_lib:shape", "h", "@Player.hook.toggle()");
    bindings.bind_in("ux:edit_lib:shape", "o", "@Player.bay.toggle()");
    bindings.bind_in("ux:edit_lib:shape", "g", "@Player.gear.toggle()");
    bindings.bind_in("ux:edit_lib:shape", "+Up", "@Player.stick_pitch.key_move_front($pressed)");
    bindings.bind_in("ux:edit_lib:shape", "+Down", "@Player.stick_pitch.key_move_back($pressed)");
    bindings.bind_in("ux:edit_lib:shape", "+Left", "@Player.stick_roll.key_move_left($pressed)");
    bindings.bind_in("ux:edit_lib:shape", "+Right", "@Player.stick_roll.key_move_right($pressed)");
    bindings.bind_in("ux:edit_lib:shape", "+Comma", "@Player.pedals_yaw.key_move_left($pressed)");
    bindings.bind_in("ux:edit_lib:shape", "+Period", "@Player.pedals_yaw.key_move_right($pressed)");
    bindings.bind_in("ux:edit_lib:shape", "+Control+Up", "@Player.thrust_vector_pitch.key_move_front($pressed)");
    bindings.bind_in("ux:edit_lib:shape", "+Control+Down", "@Player.thrust_vector_pitch.key_move_back($pressed)");
    bindings.bind_in("ux:edit_lib:shape", "+Control+Left", "@Player.thrust_vector_yaw.key_move_left($pressed)");
    bindings.bind_in("ux:edit_lib:shape", "+Control+Right", "@Player.thrust_vector_yaw.key_move_right($pressed)");
    bindings.bind_in("ux:edit_lib:shape", "z", "@Player.vtol_angle.next_detent()");
    bindings.bind_in("ux:edit_lib:shape", "x", "@Player.vtol_angle.prev_detent()");
    bindings.bind_in("ux:edit_lib:shape", "key0", "@Player.thrust_vector_pitch.recenter(); @Player.thrust_vector_yaw.recenter(); @Player.vtol_angle.recenter();");
    //bindings.bind_in("ux:edit_lib:shape", "Shift+Slash", "@ux.edit_lib_ux.toggle_show_help()");
    //bindings.bind_in("ux:edit_lib:shape", "F1", "@ux.edit_lib_ux.toggle_show_help()");
    bindings.bind_in("ux:edit_lib:shape", "+mouse1", "@ux.edit_lib_ux.mouse_press($pressed, False)");
    bindings.bind_in("ux:edit_lib:shape", "+Shift+mouse1", "@ux.edit_lib_ux.mouse_press($pressed, True)");
    bindings.bind_in("ux:edit_lib:shape", "mouseCursor", "@ux.edit_lib_ux.mouse_motion($cursor_x, $cursor_y, False)");
    bindings.bind_in("ux:edit_lib:shape", "Shift+mouseCursor", "@ux.edit_lib_ux.mouse_motion($cursor_x, $cursor_y, True)");
    // Orrery
    bindings.bind_in("ux:edit_lib:shape", "+Control+mouse2", "orrery.move_sun($pressed)");
    bindings.bind_in("ux:edit_lib:shape", "Control+mouseMotion", "orrery.handle_mousemove($dx)");
    // View
    bindings.bind_in("ux:edit_lib:shape", "+mouse2", "@camera.arcball.pan_view($pressed)");
    bindings.bind_in("ux:edit_lib:shape", "+mouse3", "@camera.arcball.move_view($pressed)");
    bindings.bind_in("ux:edit_lib:shape", "mouseMotion", "@camera.arcball.handle_mousemotion($dx, $dy)");
    bindings.bind_in("ux:edit_lib:shape", "mouseWheel", "@camera.arcball.handle_mousewheel($vertical_delta)");
    bindings.bind_in("ux:edit_lib:shape", "+Shift+Up", "@camera.arcball.target_up_fast($pressed)");
    bindings.bind_in("ux:edit_lib:shape", "+Shift+Down", "@camera.arcball.target_down_fast($pressed)");
    bindings.bind_in("ux:edit_lib:shape", "+Shift+Control+Up", "@camera.arcball.target_up($pressed)");
    bindings.bind_in("ux:edit_lib:shape", "+Shift+Control+Down", "@camera.arcball.target_down($pressed)");

// Reference controls
    bindings.bind_in("ux:reference_ux", "key1", "@Player.throttle.set_military(0.)");
    bindings.bind_in("ux:reference_ux", "key2", "@Player.throttle.set_military(25.)");
    bindings.bind_in("ux:reference_ux", "key3", "@Player.throttle.set_military(50.)");
    bindings.bind_in("ux:reference_ux", "key4", "@Player.throttle.set_military(75.)");
    bindings.bind_in("ux:reference_ux", "key5", "@Player.throttle.set_military(100.)");
    bindings.bind_in("ux:reference_ux", "key6", "@Player.throttle.set_afterburner(0)");
    bindings.bind_in("ux:reference_ux", "b", "@Player.airbrake.toggle()");
    bindings.bind_in("ux:reference_ux", "f", "@Player.flaps.toggle()");
    bindings.bind_in("ux:reference_ux", "h", "@Player.hook.toggle()");
    bindings.bind_in("ux:reference_ux", "o", "@Player.bay.toggle()");
    bindings.bind_in("ux:reference_ux", "g", "@Player.gear.toggle()");
    bindings.bind_in("ux:reference_ux", "+Up", "@Player.stick_pitch.key_move_front($pressed)");
    bindings.bind_in("ux:reference_ux", "+Down", "@Player.stick_pitch.key_move_back($pressed)");
    bindings.bind_in("ux:reference_ux", "+Left", "@Player.stick_roll.key_move_left($pressed)");
    bindings.bind_in("ux:reference_ux", "+Right", "@Player.stick_roll.key_move_right($pressed)");
    bindings.bind_in("ux:reference_ux", "+Comma", "@Player.pedals_yaw.key_move_left($pressed)");
    bindings.bind_in("ux:reference_ux", "+Period", "@Player.pedals_yaw.key_move_right($pressed)");
    bindings.bind_in("ux:reference_ux", "+Control+Up", "@Player.thrust_vector_pitch.key_move_front($pressed)");
    bindings.bind_in("ux:reference_ux", "+Control+Down", "@Player.thrust_vector_pitch.key_move_back($pressed)");
    bindings.bind_in("ux:reference_ux", "+Control+Left", "@Player.thrust_vector_yaw.key_move_left($pressed)");
    bindings.bind_in("ux:reference_ux", "+Control+Right", "@Player.thrust_vector_yaw.key_move_right($pressed)");
    bindings.bind_in("ux:reference_ux", "z", "@Player.vtol_angle.next_detent()");
    bindings.bind_in("ux:reference_ux", "x", "@Player.vtol_angle.prev_detent()");
    bindings.bind_in("ux:reference_ux", "key0", "@Player.thrust_vector_pitch.recenter(); @Player.thrust_vector_yaw.recenter(); @Player.vtol_angle.recenter();");
    bindings.bind_in("ux:reference_ux", "Shift+Slash", "@ux.reference_ux.toggle_show_help()");
    bindings.bind_in("ux:reference_ux", "F1", "@ux.reference_ux.toggle_show_help()");
    // Orrery
    bindings.bind_in("ux:reference_ux", "+Control+mouse2", "orrery.move_sun($pressed)");
    bindings.bind_in("ux:reference_ux", "Control+mouseMotion", "orrery.handle_mousemove($dx)");
    // View
    bindings.bind_in("ux:reference_ux", "+mouse2", "@camera.arcball.pan_view($pressed)");
    bindings.bind_in("ux:reference_ux", "+mouse3", "@camera.arcball.move_view($pressed)");
    bindings.bind_in("ux:reference_ux", "mouseMotion", "@camera.arcball.handle_mousemotion($dx, $dy)");
    bindings.bind_in("ux:reference_ux", "mouseWheel", "@camera.arcball.handle_mousewheel($vertical_delta)");
    bindings.bind_in("ux:reference_ux", "+Shift+Up", "@camera.arcball.target_up_fast($pressed)");
    bindings.bind_in("ux:reference_ux", "+Shift+Down", "@camera.arcball.target_down_fast($pressed)");
    bindings.bind_in("ux:reference_ux", "+Shift+Control+Up", "@camera.arcball.target_up($pressed)");
    bindings.bind_in("ux:reference_ux", "+Shift+Control+Down", "@camera.arcball.target_down($pressed)");
    // Exit
    bindings.bind_in("ux:reference_ux", "Escape", "ux.return_to_top()");

// View World
    bindings.bind_in("ux:view_world", "+mouse2", "orrery.move_sun($pressed)");
    bindings.bind_in("ux:view_world", "mouseMotion", "orrery.handle_mousemove($dx)");
    bindings.bind_in("ux:view_world", "+mouse1", "@camera.arcball.pan_view($pressed)");
    bindings.bind_in("ux:view_world", "+mouse3", "@camera.arcball.move_view($pressed)");
    bindings.bind_in("ux:view_world", "mouseMotion", "@camera.arcball.handle_mousemotion($dx, $dy)");
    bindings.bind_in("ux:view_world", "mouseWheel", "@camera.arcball.handle_mousewheel($vertical_delta)");
    bindings.bind_in("ux:view_world", "+Shift+Up", "@camera.arcball.target_up_fast($pressed)");
    bindings.bind_in("ux:view_world", "+Shift+Down", "@camera.arcball.target_down_fast($pressed)");
    bindings.bind_in("ux:view_world", "+Shift+Control+Up", "@camera.arcball.target_up($pressed)");
    bindings.bind_in("ux:view_world", "+Shift+Control+Down", "@camera.arcball.target_down($pressed)");
    bindings.bind_in("ux:view_world", "Escape", "ux.return_to_top()");
"#;

/// Show resources from Jane's Fighters Anthology engine LIB files.
#[derive(Clone, Debug, StructOpt)]
#[structopt(set_term_width = if let Some((Width(w), _)) = terminal_size() { w as usize } else { 80 })]
pub struct OfaOpt {
    #[structopt(flatten)]
    installation_opts: LibsOpts,

    #[structopt(flatten)]
    catalog_opts: CatalogOpts,

    #[structopt(flatten)]
    detail_opts: DetailLevelOpts,

    #[structopt(flatten)]
    display_opts: DisplayOpts,

    #[structopt(flatten)]
    tracelog_opts: TraceLogOpts,

    #[structopt(flatten)]
    startup_opts: StartupOpts,

    /// Optional: One or more files to view, depending on type.
    inputs: Vec<String>,
}

pub async fn shared_main(opts: OfaOpt, window_title: &str) -> Result<()> {
    Core::boot(opts.display_opts.clone(), opts, window_title, setup_runtime).await?;
    Ok(())
}

fn setup_runtime(runtime: &mut Runtime, opt: OfaOpt) -> Result<()> {
    runtime
        .load_extension_with::<StdPaths>(StdPathsOpts::new("openfa"))?
        .load_extension_with::<TraceLog>(opt.tracelog_opts)?
        .load_extension_with::<AssetCatalog>(opt.catalog_opts)?
        .load_extension_with::<Installations>(opt.installation_opts)?
        .load_extension::<GeoDb>()?
        .load_extension::<EventMapper>()?
        .load_extension::<Gui>()?
        .load_extension::<Dashboard>()?
        .load_extension::<Terminal>()?
        .load_extension::<Atmosphere>()?
        .load_extension::<Orrery>()?
        .load_extension::<Stars>()?
        .load_extension_with::<Terrain>(TerrainOpts::from_detail(
            opt.detail_opts.cpu_detail(),
            opt.detail_opts.gpu_detail(),
        ))?
        .load_extension::<T2TerrainBuffer>()?
        .load_extension::<World>()?
        .load_extension::<Markers>()?
        .load_extension::<Composite>()?
        .load_extension::<Timeline>()?
        .load_extension::<ScreenCamera>()?
        .load_extension::<PlayerCameraController>()?
        .load_extension::<ArcBallSystem>()?
        .load_extension::<TypeManager>()?
        .load_extension::<Shape>()?
        .load_extension::<AssetLoader>()?
        .load_extension::<ClassicFlightModel>()?
        .load_extension::<PowerSystem>()?
        .load_extension::<PitchInceptor>()?
        .load_extension::<RollInceptor>()?
        .load_extension::<YawInceptor>()?
        .load_extension::<ThrustVectorPitchControl>()?
        .load_extension::<ThrustVectorPitchEffector>()?
        .load_extension::<ThrustVectorYawControl>()?
        .load_extension::<ThrustVectorYawEffector>()?
        .load_extension::<VtolAngleControl>()?
        .load_extension::<VtolAngleEffector>()?
        .load_extension::<AirbrakeEffector>()?
        .load_extension::<BayEffector>()?
        .load_extension::<FlapsEffector>()?
        .load_extension::<GearEffector>()?
        .load_extension::<HookEffector>()?
        .load_extension::<ChooseGameUx>()?
        .load_extension::<ToplevelUx>()?
        .load_extension::<EditLibsUx>()?
        .load_extension::<EditLibUx>()?
        .load_extension::<ReferenceUx>()?
        .load_extension::<MissionUx>()?
        .load_extension::<ViewWorldUx>()?
        .load_extension::<CreditsUx>()?
        .load_extension::<Ux>()?
        .load_extension_with::<StartupOpts>(opt.startup_opts.with_prelude(PRELUDE))?;

    if opt.inputs.is_empty() {
        return Ok(());
    }

    if !opt.inputs.is_empty() {
        let mut herder = runtime.resource_mut::<ScriptHerder>();
        let initial = &opt.inputs[0];
        if initial.ends_with(".LIB") {
            herder.run_string(&format!("ux.edit_lib({:?})", initial))?;
        } else if initial.ends_with(".M") {
            herder.run_string(&format!("ux.load_mission({:?})", initial))?;
        } else if initial.ends_with(".SH") {
            herder.run_string("ux.edit_lib(\"FA\")")?;
            herder.run_string(&format!("@ux.edit_lib_ux.edit_sh({:?})", initial))?;
        }
    }

    Ok(())
}

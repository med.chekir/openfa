// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::ux::{
    choose_game::ChooseGameUxStep, edit_libs::EditLibsUxStep, mission::MissionUxStep,
    shared::sunrise::Sunrise, toplevel::ToplevelUxStep, view_world::ViewWorldUxStep, UxState,
    UxTag,
};
use anyhow::Result;
use arcball::ArcBallController;
use bevy_ecs::prelude::*;
use camera::{CameraStep, ScreenCameraController};
use event_mapper::EventMapper;
use game_editor::EditLibUxStep;
use game_ux::menubar::show_basic_menubar;
use gui::{Gui, GuiStep};
use installations::Installations;
use mantle::Window;
use nitrous::{inject_nitrous_component, HeapMut, NitrousComponent};
use runtime::{report_errors, Extension, Runtime, ScriptHerder};
use spog::TerminalStep;
use terrain::TerrainStep;

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum CreditsUxStep {
    RotateCamera,
    DrawUx,
}

#[derive(Debug, NitrousComponent)]
pub(crate) struct CreditsUx {
    sunrise: Sunrise,
}

impl Extension for CreditsUx {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        runtime.add_frame_system(
            Self::sys_draw_ux
                .pipe(report_errors)
                .in_set(CreditsUxStep::DrawUx)
                .after(GuiStep::StartFrame)
                .before(TerminalStep::Show)
                .before(GuiStep::EndFrame)
                // Routines that may call quit on the window don't conflict with users of the size
                .ambiguous_with(CameraStep::HandleDisplayChange)
                .ambiguous_with(TerrainStep::HandleDisplayChange)
                // It also does not run concurrent with other UX panes
                .ambiguous_with(ChooseGameUxStep::DrawUx)
                .ambiguous_with(EditLibUxStep::DrawUx)
                .ambiguous_with(EditLibsUxStep::DrawUx)
                .ambiguous_with(MissionUxStep::DrawUx)
                .ambiguous_with(ToplevelUxStep::DrawUx)
                .ambiguous_with(ViewWorldUxStep::DrawUx),
        );
        runtime.add_frame_system(
            Self::sys_rotate_camera
                .in_set(CreditsUxStep::RotateCamera)
                .before(CameraStep::MoveCameraToFrame)
                // Most UX modify the camera, but don't receive actions concurrently
                .ambiguous_with(ChooseGameUxStep::RotateCamera)
                .ambiguous_with(ToplevelUxStep::RotateCamera)
                .ambiguous_with(ViewWorldUxStep::DrawUx),
        );
        Ok(())
    }
}

#[inject_nitrous_component]
impl CreditsUx {
    pub(crate) fn new(heap: HeapMut) -> Result<Self> {
        Ok(Self {
            sunrise: Sunrise::new(heap)?,
        })
    }

    pub fn unload(self, heap: HeapMut) -> Result<()> {
        self.sunrise.unload(heap)
    }

    fn sys_rotate_camera(
        mapper: Res<EventMapper>,
        mut query: Query<&mut ArcBallController, With<ScreenCameraController>>,
    ) {
        if mapper.input_focus() == &UxState::Credits.focus() {
            for mut camera in query.iter_mut() {
                camera.pan_view(true);
                camera.handle_mousemotion(-0.05f64, 0f64);
            }
        }
    }

    fn sys_draw_ux(
        gui: Res<Gui>,
        mut herder: ResMut<ScriptHerder>,
        mut window: ResMut<Window>,
        mut installs: ResMut<Installations>,
        mut query: Query<&mut CreditsUx, With<UxTag>>,
    ) -> Result<()> {
        for mut credits in query.iter_mut() {
            credits.draw_credits(&gui, &mut installs, &mut window, &mut herder)?;
        }
        Ok(())
    }

    fn draw_credits(
        &mut self,
        gui: &Gui,
        _installs: &mut Installations,
        window: &mut Window,
        herder: &mut ScriptHerder,
    ) -> Result<()> {
        show_basic_menubar(gui.screen().ctx(), "menubar_credits", window, herder)?;

        // egui::Window::new("Choose Game")
        //     .frame(
        //         egui::Frame::window(&gui.screen().ctx().style())
        //             .shadow(egui::epaint::Shadow::NONE)
        //             .fill(hex_color!("202020E0")),
        //     )
        //     .resizable(true)
        //     .collapsible(false)
        //     .anchor(egui::Align2::CENTER_CENTER, egui::Vec2::default())
        //     .show(gui.screen().ctx(), |ui| {
        //         ui.push_id("game_cards", |ui| {
        //             self.draw_game_chooser(installs, ui, herder);
        //         });
        //     });

        Ok(())
    }
}

// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::ux::{
    choose_game::ChooseGameUxStep, edit_libs::EditLibsUxStep, view_world::ViewWorldUxStep, UxTag,
};
use anyhow::Result;
use asset_loader::AssetLoader;
use bevy_ecs::prelude::*;
use camera::{CameraStep, ScreenCameraController};
use game_editor::EditLibUxStep;
use gui::{Gui, GuiStep};
use mantle::{GpuStep, Window};
use nitrous::{inject_nitrous_component, method, HeapMut, NitrousComponent};
use phase::Frame;
use player::PlayerCameraController;
use runtime::{Extension, Runtime, ScriptHerder};
use spog::TerminalStep;
use terrain::TerrainStep;

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum MissionUxStep {
    DrawUx,
}

#[derive(NitrousComponent, Debug)]
pub(crate) struct MissionUx {
    camera_ent: Entity,
    showing_menu: bool,
}

impl Extension for MissionUx {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        runtime.add_frame_system(
            Self::sys_draw_ux
                .in_set(MissionUxStep::DrawUx)
                .after(GuiStep::StartFrame)
                .before(TerminalStep::Show)
                .before(GuiStep::EndFrame)
                // Routines that may call quit on the window don't conflict with users of the size
                .ambiguous_with(CameraStep::HandleDisplayChange)
                .ambiguous_with(GpuStep::CreateTargetSurface)
                .ambiguous_with(TerrainStep::HandleDisplayChange)
                // It also does not run concurrent with other UX panes
                .ambiguous_with(ChooseGameUxStep::DrawUx)
                .ambiguous_with(EditLibUxStep::DrawUx)
                .ambiguous_with(EditLibsUxStep::DrawUx)
                .ambiguous_with(ViewWorldUxStep::DrawUx),
        );
        Ok(())
    }
}

#[inject_nitrous_component]
impl MissionUx {
    pub fn new(name: &str, mut heap: HeapMut) -> Result<Self> {
        let camera_ent = heap
            .spawn("camera")?
            .inject(Frame::default())?
            .inject(PlayerCameraController::new("external")?)?
            .inject(ScreenCameraController)?
            .id();

        heap.resource_scope(|heap, loader: Mut<AssetLoader>| loader.load_mission(name, heap))?;

        Ok(Self {
            camera_ent,
            showing_menu: false,
        })
    }

    pub fn unload(&self, mut heap: HeapMut) -> Result<()> {
        heap.resource_scope(|heap, loader: Mut<AssetLoader>| loader.unload_mission(heap))?;
        heap.despawn_by_id(self.camera_ent);
        Ok(())
    }

    #[method]
    pub fn toggle_show_menu(&mut self) {
        self.showing_menu = !self.showing_menu;
    }

    fn sys_draw_ux(
        gui: Res<Gui>,
        mut window: ResMut<Window>,
        mut herder: ResMut<ScriptHerder>,
        query: Query<&MissionUx, With<UxTag>>,
    ) {
        for mission_ux in query.iter() {
            if mission_ux.showing_menu {
                egui::TopBottomPanel::top("top_panel_mission").show(gui.screen().ctx(), |ui| {
                    egui::menu::bar(ui, |ui| {
                        ui.menu_button("File", |ui| {
                            if ui.button("End Mission").clicked() {
                                herder.run_string("ux.return_to_top()").unwrap();
                            }
                            if ui.button("Quit to Desktop").clicked() {
                                window.request_quit();
                            }
                        });
                    });
                });
            }
        }
    }
}

// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::{anyhow, Result};
use arcball::ArcBallController;
use bevy_ecs::prelude::*;
use camera::{CameraStep, ScreenCameraController};
use catalog::{AssetCatalog, AssetCollection};
use egui::hex_color;
use game_editor::EditLibUxStep;
use game_ux::{menubar::show_basic_menubar, UxTag};
use gui::{Gui, GuiStep};
use installations::{GameInstallation, Installations};
use itertools::Itertools;
use lib::{LibProvider, LibWriter, Priority};
use mantle::{GpuStep, Window};
use nitrous::{inject_nitrous_component, HeapMut, NitrousComponent};
use orrery::Orrery;
use phase::Frame;
use runtime::{report_errors, Extension, Runtime, ScriptHerder};
use spog::TerminalStep;
use terrain::TerrainStep;

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum EditLibsUxStep {
    DrawUx,
}

#[derive(Debug, NitrousComponent)]
pub(crate) struct EditLibsUx {
    camera_ent: Entity,
    new_lib_name: String,
}

impl Extension for EditLibsUx {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        runtime.add_frame_system(
            Self::sys_draw_ux
                .pipe(report_errors)
                .in_set(EditLibsUxStep::DrawUx)
                .after(GuiStep::StartFrame)
                .before(TerminalStep::Show)
                .before(GuiStep::EndFrame)
                // Routines that may call quit on the window don't conflict with users of the size
                .ambiguous_with(CameraStep::HandleDisplayChange)
                .ambiguous_with(GpuStep::CreateTargetSurface)
                .ambiguous_with(TerrainStep::HandleDisplayChange)
                // We also don't conflict with views that do not run concurrently
                .ambiguous_with(EditLibUxStep::DrawUx),
        );
        Ok(())
    }
}

#[inject_nitrous_component]
impl EditLibsUx {
    pub(crate) fn new(mut heap: HeapMut) -> Result<Self> {
        let mut arcball = ArcBallController::default();
        let name = "ISS";
        let target = arcball.notable_location(name)?;
        let bearing = arcball.bearing_for_notable_location(name);
        let pitch = arcball.pitch_for_notable_location(name);
        let distance = arcball.distance_for_notable_location(name);
        arcball.set_target(target);
        arcball.set_bearing(bearing);
        arcball.set_pitch(pitch);
        arcball.set_distance(distance)?;

        heap.resource_mut::<Orrery>()
            .set_date_time(1964, 2, 24, 2, 0, 0);

        let camera_ent = heap
            .spawn("camera")?
            .inject(Frame::default())?
            .inject(arcball)?
            .inject(ScreenCameraController)?
            .id();

        Ok(Self {
            camera_ent,
            new_lib_name: String::new(),
        })
    }

    pub fn unload(&self, mut heap: HeapMut) -> Result<()> {
        heap.despawn_by_id(self.camera_ent);
        Ok(())
    }

    fn sys_draw_ux(
        gui: Res<Gui>,
        installs: Res<Installations>,
        mut catalog: ResMut<AssetCatalog>,
        mut herder: ResMut<ScriptHerder>,
        mut window: ResMut<Window>,
        mut query: Query<&mut EditLibsUx, With<UxTag>>,
    ) -> Result<()> {
        for mut edit_libs in query.iter_mut() {
            edit_libs.draw_window(
                installs.primary(),
                catalog.primary_mut(),
                &gui,
                &mut window,
                &mut herder,
            )?;
        }
        Ok(())
    }

    fn draw_window(
        &mut self,
        install: &GameInstallation,
        assets: &mut AssetCollection,
        gui: &Gui,
        window: &mut Window,
        herder: &mut ScriptHerder,
    ) -> Result<()> {
        show_basic_menubar(gui.screen().ctx(), "top_panel_edit_libs", window, herder)?;

        egui::Window::new(&format!("Edit Libs in {}", install.name()))
            .frame(
                egui::Frame::window(&gui.screen().ctx().style())
                    .shadow(egui::epaint::Shadow::NONE)
                    .fill(hex_color!("202020E0")),
            )
            .resizable(true)
            .collapsible(false)
            .anchor(egui::Align2::CENTER_CENTER, egui::Vec2::default())
            .show(gui.screen().ctx(), |ui| -> Result<()> {
                ui.push_id("libs_list", |ui| -> Result<()> {
                    self.draw_libs_list(install, assets, ui, herder)
                })
                .inner?;
                Ok(())
            })
            .ok_or_else(|| anyhow!("failed to show window"))?
            .inner
            .ok_or_else(|| anyhow!("expected response"))?
    }

    fn draw_libs_list(
        &mut self,
        install: &GameInstallation,
        assets: &mut AssetCollection,
        ui: &mut egui::Ui,
        herder: &mut ScriptHerder,
    ) -> Result<()> {
        // Libs list
        ui.label("Loaded Libs:");
        egui::ScrollArea::vertical()
            .auto_shrink([false; 2])
            .max_height(400.)
            .show(ui, |ui| -> Result<()> {
                for (name, _priority, _provider) in assets
                    .all_providers()
                    .sorted_by_key(|(_name, priority, _provider)| -priority)
                    .filter(|(name, _priority, _provider)| name.ends_with(".LIB"))
                {
                    if install.game().all_libs.contains(&name) {
                        ui.label(name);
                    } else if ui.button(name).clicked() {
                        herder.run_string(&format!("ux.edit_lib(\"{}\")", name))?;
                    }
                }
                Ok(())
            })
            .inner?;

        // Create a lib
        ui.label("Create new lib:");
        ui.horizontal(|ui| -> Result<()> {
            ui.label(install.game().lib_prefix);
            let resp = ui.text_edit_singleline(&mut self.new_lib_name);
            ui.label(".LIB");
            if ui.button("Create").clicked() || resp.lost_focus() {
                let mut path = install.game_path().to_owned();
                let libname = install.game().lib_prefix.to_owned() + &self.new_lib_name + ".LIB";
                path.push(&libname);
                LibWriter::new(&path, 0)?.finish()?;
                assets.add_provider(
                    &path.file_name().unwrap().to_string_lossy(),
                    Priority::from_path(&path, 0)?.as_provider_priority(),
                    LibProvider::new(&path)?,
                )?;
                self.new_lib_name = String::new();
                herder.run_string(&format!("ux.edit_lib(\"{}\")", libname))?;
            }
            Ok(())
        })
        .inner
    }
}

// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::ux::{
    edit_libs::EditLibsUxStep, toplevel::ToplevelUxStep, view_world::ViewWorldUxStep, UxState,
    UxTag,
};
use animate::Timeline;
use anyhow::Result;
use arcball::ArcBallController;
use bevy_ecs::prelude::*;
use camera::{CameraStep, ScreenCameraController};
use catalog::AssetCatalog;
use egui::{
    epaint::{
        text::{LayoutJob, TextFormat},
        Color32, FontFamily, FontId,
    },
    hex_color,
};
use event_mapper::EventMapper;
use game_editor::EditLibUxStep;
use game_ux::menubar::show_basic_menubar;
use gui::{Gui, GuiStep};
use installations::Installations;
use mantle::Window;
use nitrous::{inject_nitrous_component, HeapMut, NitrousComponent, Value};
use orrery::Orrery;
use phase::Frame;
use runtime::{report_errors, Extension, Runtime, ScriptHerder};
use spog::TerminalStep;
use std::{borrow::Cow, sync::Arc};
use terrain::TerrainStep;

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum ChooseGameUxStep {
    RotateCamera,
    DrawUx,
}

#[derive(Debug, NitrousComponent)]
pub(crate) struct ChooseGameUx {
    camera_ent: Entity,
    sun_anim_handle: Value,
}

impl Extension for ChooseGameUx {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        runtime.add_frame_system(
            Self::sys_draw_ux
                .pipe(report_errors)
                .in_set(ChooseGameUxStep::DrawUx)
                .after(GuiStep::StartFrame)
                .before(TerminalStep::Show)
                .before(GuiStep::EndFrame)
                // Routines that may call quit on the window don't conflict with users of the size
                .ambiguous_with(CameraStep::HandleDisplayChange)
                .ambiguous_with(TerrainStep::HandleDisplayChange)
                // It also does not run concurrent with other UX panes
                .ambiguous_with(EditLibUxStep::DrawUx)
                .ambiguous_with(EditLibsUxStep::DrawUx),
        );
        runtime.add_frame_system(
            Self::sys_rotate_camera
                .in_set(ChooseGameUxStep::RotateCamera)
                .before(CameraStep::MoveCameraToFrame)
                // Most UX modify the camera, but don't receive actions concurrently
                .ambiguous_with(ToplevelUxStep::RotateCamera)
                .ambiguous_with(ViewWorldUxStep::DrawUx),
        );
        Ok(())
    }
}

#[inject_nitrous_component]
impl ChooseGameUx {
    pub(crate) fn new(mut heap: HeapMut) -> Result<Self> {
        let mut arcball = ArcBallController::default();
        let name = "Everest";
        let target = arcball.notable_location(name)?;
        let bearing = arcball.bearing_for_notable_location(name);
        let pitch = arcball.pitch_for_notable_location(name);
        let distance = arcball.distance_for_notable_location(name);
        arcball.set_target(target);
        arcball.set_bearing(bearing);
        arcball.set_pitch(pitch);
        arcball.set_distance(distance)?;

        heap.resource_mut::<Orrery>()
            .set_date_time(1964, 2, 24, 22, 0, 0);

        let start_time = heap.resource::<Orrery>().get_unix_ms();
        let sun_anim_handle = heap.resource_mut::<Timeline>().ease_in(
            Value::RustMethod(Arc::new(|args, mut heap| {
                let val = args[0].to_numeric()?;
                heap.resource_mut::<Orrery>().set_unix_ms(val);
                Ok(Value::Boolean(false))
            })),
            start_time,
            10000000.,
            15.0,
        )?;

        let camera_ent = heap
            .spawn("camera")?
            .inject(Frame::default())?
            .inject(arcball)?
            .inject(ScreenCameraController)?
            .id();

        Ok(Self {
            camera_ent,
            sun_anim_handle,
        })
    }

    pub fn unload(&self, mut heap: HeapMut) -> Result<()> {
        heap.resource_mut::<Timeline>().stop(
            self.sun_anim_handle
                .to_dict()?
                .get("handle")
                .unwrap()
                .clone(),
        )?;
        heap.despawn_by_id(self.camera_ent);
        Ok(())
    }

    fn sys_rotate_camera(
        mapper: Res<EventMapper>,
        mut query: Query<&mut ArcBallController, With<ScreenCameraController>>,
    ) {
        if mapper.input_focus() == &UxState::ChooseGame.focus() {
            for mut camera in query.iter_mut() {
                camera.pan_view(true);
                camera.handle_mousemotion(-0.05f64, 0f64);
            }
        }
    }

    fn sys_draw_ux(
        gui: Res<Gui>,
        mut herder: ResMut<ScriptHerder>,
        mut window: ResMut<Window>,
        mut installs: ResMut<Installations>,
        mut catalog: ResMut<AssetCatalog>,
        mut query: Query<&mut ChooseGameUx, With<UxTag>>,
    ) -> Result<()> {
        for mut chooser in query.iter_mut() {
            chooser.draw_chooser(&gui, &mut installs, &mut catalog, &mut window, &mut herder)?;
        }
        Ok(())
    }

    fn draw_chooser(
        &mut self,
        gui: &Gui,
        installs: &mut Installations,
        catalog: &mut AssetCatalog,
        window: &mut Window,
        herder: &mut ScriptHerder,
    ) -> Result<()> {
        show_basic_menubar(gui.screen().ctx(), "menubar_chooser", window, herder)?;

        egui::Window::new("Choose Game")
            .frame(
                egui::Frame::window(&gui.screen().ctx().style())
                    .shadow(egui::epaint::Shadow::NONE)
                    .fill(hex_color!("202020E0")),
            )
            .resizable(true)
            .collapsible(false)
            .anchor(egui::Align2::CENTER_CENTER, egui::Vec2::default())
            .show(gui.screen().ctx(), |ui| {
                ui.push_id("game_cards", |ui| {
                    self.draw_game_chooser(installs, catalog, ui, herder);
                });
            });

        Ok(())
    }

    fn draw_game_chooser(
        &mut self,
        installs: &mut Installations,
        catalog: &mut AssetCatalog,
        ui: &mut egui::Ui,
        herder: &mut ScriptHerder,
    ) {
        ui.label("Discovered Directories:");
        egui::ScrollArea::vertical()
            .auto_shrink([false; 2])
            .max_height(480.)
            .show(ui, |ui| {
                let mut select_game = None;
                for name in installs.installation_names() {
                    let install = installs.installation(name).unwrap();
                    let mut job = LayoutJob::default();
                    job.append(
                        name,
                        0.0,
                        TextFormat {
                            font_id: FontId::new(16.0, FontFamily::Proportional),
                            color: Color32::WHITE,
                            ..Default::default()
                        },
                    );
                    job.append("\n", 0.0, TextFormat::default());
                    job.append(
                        "Game: ",
                        10.0,
                        TextFormat {
                            font_id: FontId::new(10.0, FontFamily::Proportional),
                            color: Color32::GRAY,
                            italics: true,
                            ..Default::default()
                        },
                    );
                    job.append(
                        &install.game_path().to_string_lossy(),
                        0.0,
                        TextFormat {
                            font_id: FontId::new(10.0, FontFamily::Proportional),
                            color: Color32::GRAY,
                            italics: true,
                            ..Default::default()
                        },
                    );
                    job.append("\n", 0.0, TextFormat::default());
                    job.append(
                        "CD-ROM: ",
                        10.0,
                        TextFormat {
                            font_id: FontId::new(10.0, FontFamily::Proportional),
                            color: Color32::GRAY,
                            italics: true,
                            ..Default::default()
                        },
                    );
                    let cd_path = &install
                        .cd1_path()
                        .map(|v| v.to_string_lossy())
                        .unwrap_or_else(|| Cow::Borrowed("No CD Found"));
                    job.append(
                        cd_path,
                        0.0,
                        TextFormat {
                            font_id: FontId::new(10.0, FontFamily::Proportional),
                            color: Color32::GRAY,
                            italics: true,
                            ..Default::default()
                        },
                    );
                    if ui.button(job).clicked() {
                        select_game = Some(name.to_owned());
                    }
                }
                if let Some(game_name) = select_game {
                    installs.select_installation(&game_name, catalog).unwrap();
                    herder.run_string("ux.return_to_top()").unwrap();
                }
            });
    }
}

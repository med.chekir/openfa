// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use animate::Timeline;
use anyhow::Result;
use arcball::ArcBallController;
use bevy_ecs::prelude::*;
use camera::ScreenCameraController;
use nitrous::{HeapMut, Value};
use orrery::Orrery;
use phase::Frame;
use std::sync::Arc;

#[derive(Debug)]
pub(crate) struct Sunrise {
    camera_ent: Entity,
    sun_anim_handle: Value,
}

impl Sunrise {
    pub fn new(mut heap: HeapMut) -> Result<Sunrise> {
        let mut arcball = ArcBallController::default();
        let name = "Everest";
        let target = arcball.notable_location(name)?;
        let bearing = arcball.bearing_for_notable_location(name);
        let pitch = arcball.pitch_for_notable_location(name);
        let distance = arcball.distance_for_notable_location(name);
        arcball.set_target(target);
        arcball.set_bearing(bearing);
        arcball.set_pitch(pitch);
        arcball.set_distance(distance)?;

        heap.resource_mut::<Orrery>()
            .set_date_time(1964, 2, 24, 22, 0, 0);

        let start_time = heap.resource::<Orrery>().get_unix_ms();
        let sun_anim_handle = heap.resource_mut::<Timeline>().ease_in(
            Value::RustMethod(Arc::new(|args, mut heap| {
                let val = args[0].to_numeric()?;
                heap.resource_mut::<Orrery>().set_unix_ms(val);
                Ok(Value::Boolean(false))
            })),
            start_time,
            10000000.,
            15.0,
        )?;

        let camera_ent = heap
            .spawn("camera")?
            .inject(Frame::default())?
            .inject(arcball)?
            .inject(ScreenCameraController)?
            .id();

        Ok(Sunrise {
            camera_ent,
            sun_anim_handle,
        })
    }

    pub fn unload(self, mut heap: HeapMut) -> Result<()> {
        heap.resource_mut::<Timeline>().stop(
            self.sun_anim_handle
                .to_dict()?
                .get("handle")
                .unwrap()
                .clone(),
        )?;
        heap.despawn_by_id(self.camera_ent);
        Ok(())
    }
}

// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
pub(crate) mod choose_game;
pub(crate) mod credits;
pub(crate) mod edit_libs;
pub(crate) mod mission;
pub(crate) mod reference;
mod shared;
pub(crate) mod toplevel;
pub(crate) mod view_world;

use crate::ux::{
    choose_game::ChooseGameUx, credits::CreditsUx, edit_libs::EditLibsUx, mission::MissionUx,
    reference::ReferenceUx, toplevel::ToplevelUx, view_world::ViewWorldUx,
};
use anyhow::Result;
use bevy_ecs::prelude::*;
use event_mapper::EventMapper;
use game_editor::EditLibUx;
use game_ux::{UxState, UxTag};
use installations::Installations;
use nitrous::{inject_nitrous_resource, method, HeapMut, NitrousResource};
use runtime::{Extension, Runtime};
use std::str::FromStr;

/// Top-level controls for our ux; does not contain state itself.
#[derive(NitrousResource, Clone, Debug)]
pub struct Ux {
    ux_entity: Entity,
}

#[inject_nitrous_resource]
impl Ux {
    pub fn unload_ux(&self, mut heap: HeapMut) -> Result<()> {
        match UxState::from_str(heap.resource::<EventMapper>().input_focus().name())? {
            UxState::ChooseGame => {
                let prior_ux = heap
                    .entity_mut("ux")?
                    .take::<ChooseGameUx>()
                    .expect("mismatched UxState!");
                prior_ux.unload(heap.as_mut())?;
            }
            UxState::Toplevel => {
                let prior_ux = heap
                    .entity_mut("ux")?
                    .take::<ToplevelUx>()
                    .expect("mismatched UxState!");
                prior_ux.unload(heap.as_mut())?;
            }
            UxState::EditLibs => {
                let prior_ux = heap
                    .entity_mut("ux")?
                    .take::<EditLibsUx>()
                    .expect("mismatched UxState!");
                prior_ux.unload(heap.as_mut())?;
            }
            UxState::EditLib => {
                let prior_ux = heap
                    .entity_mut("ux")?
                    .take::<EditLibUx>()
                    .expect("mismatched UxState!");
                prior_ux.unload(heap.as_mut())?;
            }
            UxState::Reference => {
                let prior_ux = heap
                    .entity_mut("ux")?
                    .take::<ReferenceUx>()
                    .expect("mismatched UxState!");
                prior_ux.unload(heap.as_mut())?;
            }
            UxState::Mission => {
                let prior_ux = heap
                    .entity_mut("ux")?
                    .take::<MissionUx>()
                    .expect("mismatched UxState!");
                prior_ux.unload(heap.as_mut())?;
            }
            UxState::ViewWorld => {
                let prior_ux = heap
                    .entity_mut("ux")?
                    .take::<ViewWorldUx>()
                    .expect("mismatched UxState!");
                prior_ux.unload(heap.as_mut())?;
            }
            UxState::Credits => {
                let prior_ux = heap
                    .entity_mut("ux")?
                    .take::<CreditsUx>()
                    .expect("mismatched UxState!");
                prior_ux.unload(heap.as_mut())?;
            }
        }
        Ok(())
    }

    // Note: this is awkward. Since escape may kill the component, we can't run it as
    // a component level call or the release event will error. Instead we pass through
    // the resource and manually dispatch, if it makes sense.
    #[method]
    pub fn escape_pressed(&self, mut heap: HeapMut) -> Result<()> {
        if UxState::from_str(heap.resource::<EventMapper>().input_focus().name())?
            != UxState::EditLib
        {
            return Ok(());
        }
        let Ok(mut ent) = heap.entity_mut("ux") else {
            return Ok(());
        };
        let Some(mut edit_lib_ux) = ent.take::<EditLibUx>() else {
            return Ok(());
        };
        edit_lib_ux.escape_pressed(heap.as_mut())?;
        heap.entity_mut("ux")?.inject(edit_lib_ux)?;
        Ok(())
    }

    #[method]
    pub fn choose_game(&self, mut heap: HeapMut) -> Result<()> {
        self.unload_ux(heap.as_mut())?;
        let next_ux = ChooseGameUx::new(heap.as_mut())?;
        heap.entity_by_id_mut(self.ux_entity)?.inject(next_ux)?;
        heap.resource_mut::<EventMapper>()
            .set_input_focus(UxState::ChooseGame.focus());
        Ok(())
    }

    #[method]
    pub fn return_to_top(&self, mut heap: HeapMut) -> Result<()> {
        self.unload_ux(heap.as_mut())?;
        let next_ux = ToplevelUx::new(heap.as_mut())?;
        heap.entity_by_id_mut(self.ux_entity)?.inject(next_ux)?;
        heap.resource_mut::<EventMapper>()
            .set_input_focus(UxState::Toplevel.focus());
        Ok(())
    }

    #[method]
    pub fn edit_libs(&self, mut heap: HeapMut) -> Result<()> {
        self.unload_ux(heap.as_mut())?;
        let next_ux = EditLibsUx::new(heap.as_mut())?;
        heap.entity_by_id_mut(self.ux_entity)?.inject(next_ux)?;
        heap.resource_mut::<EventMapper>()
            .set_input_focus(UxState::EditLibs.focus());
        Ok(())
    }

    #[method]
    pub fn edit_lib(&self, name: &str, mut heap: HeapMut) -> Result<()> {
        self.unload_ux(heap.as_mut())?;
        let next_ux = EditLibUx::new(name, heap.as_mut())?;
        heap.entity_by_id_mut(self.ux_entity)?.inject(next_ux)?;
        heap.resource_mut::<EventMapper>()
            .set_input_focus(UxState::EditLib.focus());
        Ok(())
    }

    #[method]
    pub fn reference(&self, mut heap: HeapMut) -> Result<()> {
        self.unload_ux(heap.as_mut())?;
        let next_ux = ReferenceUx::new("A10.SH", heap.as_mut())?;
        heap.entity_by_id_mut(self.ux_entity)?.inject(next_ux)?;
        heap.resource_mut::<EventMapper>()
            .set_input_focus(UxState::Reference.focus());
        Ok(())
    }

    #[method]
    pub fn load_mission(&self, name: &str, mut heap: HeapMut) -> Result<()> {
        self.unload_ux(heap.as_mut())?;
        let next_ux = MissionUx::new(name, heap.as_mut())?;
        heap.entity_by_id_mut(self.ux_entity)?.inject(next_ux)?;
        heap.resource_mut::<EventMapper>()
            .set_input_focus(UxState::Mission.focus());
        Ok(())
    }

    #[method]
    pub fn view_world(&self, mut heap: HeapMut) -> Result<()> {
        self.unload_ux(heap.as_mut())?;
        let next_ux = ViewWorldUx::new(heap.as_mut())?;
        heap.entity_by_id_mut(self.ux_entity)?.inject(next_ux)?;
        heap.resource_mut::<EventMapper>()
            .set_input_focus(UxState::ViewWorld.focus());
        Ok(())
    }

    #[method]
    pub fn credits(&self, mut heap: HeapMut) -> Result<()> {
        self.unload_ux(heap.as_mut())?;
        let next_ux = CreditsUx::new(heap.as_mut())?;
        heap.entity_by_id_mut(self.ux_entity)?.inject(next_ux)?;
        heap.resource_mut::<EventMapper>()
            .set_input_focus(UxState::Credits.focus());
        Ok(())
    }
}

impl Extension for Ux {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        let ux_entity = runtime.spawn("ux")?.inject(UxTag)?.id();

        let ux_state = if runtime
            .resource::<Installations>()
            .selected_installation()
            .is_some()
        {
            // Create the singular ux entity, with a toplevel ux attached to view our selected game.
            let toplevel_ux = ToplevelUx::new(runtime.heap_mut())?;
            runtime.heap_mut().entity_mut("ux")?.inject(toplevel_ux)?;
            UxState::Toplevel
        } else {
            // Create the game chooser, to pick a game to view.
            let chooser_ux = ChooseGameUx::new(runtime.heap_mut())?;
            runtime.heap_mut().entity_mut("ux")?.inject(chooser_ux)?;
            UxState::ChooseGame
        };

        // Mark the top-level ux as active in the input system.
        runtime
            .resource_mut::<EventMapper>()
            .set_input_focus(ux_state.focus());

        // Create the ux resource for convenient control from scripts.
        runtime.inject_resource(Self { ux_entity })?;

        Ok(())
    }
}

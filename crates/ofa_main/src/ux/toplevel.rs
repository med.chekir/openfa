// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::ux::{
    choose_game::ChooseGameUxStep, edit_libs::EditLibsUxStep, mission::MissionUxStep,
    view_world::ViewWorldUxStep, UxState, UxTag,
};
use animate::Timeline;
use anyhow::Result;
use arcball::ArcBallController;
use bevy_ecs::prelude::*;
use camera::{CameraStep, ScreenCameraController};
use catalog::{AssetCatalog, AssetCollection, FileSystem, Order, Search};
use egui::hex_color;
use egui_extras::{Column, TableBuilder};
use event_mapper::EventMapper;
use game_editor::EditLibUxStep;
use game_ux::menubar::add_help_menu_items;
use gui::{Gui, GuiStep};
use installations::{from_dos_string, Installations};
use mantle::{GpuStep, Window};
use mmm::{is_mission_template, Mission};
use nitrous::{inject_nitrous_component, HeapMut, NitrousComponent, Value};
use orrery::Orrery;
use phase::Frame;
use runtime::{report_errors, Extension, Runtime, ScriptHerder};
use spog::TerminalStep;
use std::sync::Arc;
use terrain::TerrainStep;
use xt::TypeManager;

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum ToplevelUxStep {
    DrawUx,
    RotateCamera,
}

#[derive(Debug, Default)]
pub enum ToplevelMenu {
    #[default]
    Main,
    SelectMission(Vec<(String, Mission)>),
}

#[derive(Debug, NitrousComponent)]
pub(crate) struct ToplevelUx {
    camera_ent: Entity,
    sun_anim_handle: Value,
    state: ToplevelMenu,
}

impl Extension for ToplevelUx {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        runtime.add_frame_system(
            Self::sys_draw_ux
                .pipe(report_errors)
                .in_set(ToplevelUxStep::DrawUx)
                .after(GuiStep::StartFrame)
                .before(TerminalStep::Show)
                .before(GuiStep::EndFrame)
                // Routines that may call quit on the window don't conflict with users of the size
                .ambiguous_with(CameraStep::HandleDisplayChange)
                .ambiguous_with(GpuStep::CreateTargetSurface)
                .ambiguous_with(TerrainStep::HandleDisplayChange)
                // It also does not run concurrent with other UX panes
                .ambiguous_with(ChooseGameUxStep::DrawUx)
                .ambiguous_with(EditLibUxStep::DrawUx)
                .ambiguous_with(EditLibsUxStep::DrawUx)
                .ambiguous_with(MissionUxStep::DrawUx),
        );
        runtime.add_frame_system(
            Self::sys_rotate_camera
                .in_set(ToplevelUxStep::RotateCamera)
                .before(CameraStep::MoveCameraToFrame)
                // It does not run concurrent with other UX panes
                .ambiguous_with(ViewWorldUxStep::DrawUx),
        );
        Ok(())
    }
}

#[inject_nitrous_component]
impl ToplevelUx {
    pub(crate) fn new(mut heap: HeapMut) -> Result<Self> {
        let mut arcball = ArcBallController::default();
        let name = "Everest";
        let target = arcball.notable_location(name)?;
        let bearing = arcball.bearing_for_notable_location(name);
        let pitch = arcball.pitch_for_notable_location(name);
        let distance = arcball.distance_for_notable_location(name);
        arcball.set_target(target);
        arcball.set_bearing(bearing);
        arcball.set_pitch(pitch);
        arcball.set_distance(distance)?;

        heap.resource_mut::<Orrery>()
            .set_date_time(1964, 2, 24, 10, 0, 0);

        let start_time = heap.resource::<Orrery>().get_unix_ms();
        let sun_anim_handle = heap.resource_mut::<Timeline>().ease_in(
            Value::RustMethod(Arc::new(|args, mut heap| {
                let val = args[0].to_numeric()?;
                heap.resource_mut::<Orrery>().set_unix_ms(val);
                Ok(Value::Boolean(false))
            })),
            start_time,
            10000000.,
            15.0,
        )?;

        let camera_ent = heap
            .spawn("camera")?
            .inject(Frame::default())?
            .inject(arcball)?
            .inject(ScreenCameraController)?
            .id();

        Ok(Self {
            camera_ent,
            sun_anim_handle,
            state: ToplevelMenu::default(),
        })
    }

    pub fn unload(&self, mut heap: HeapMut) -> Result<()> {
        heap.resource_mut::<Timeline>().stop(
            self.sun_anim_handle
                .to_dict()?
                .get("handle")
                .unwrap()
                .clone(),
        )?;
        heap.despawn_by_id(self.camera_ent);
        Ok(())
    }

    fn sys_rotate_camera(
        mapper: Res<EventMapper>,
        mut query: Query<&mut ArcBallController, With<ScreenCameraController>>,
    ) {
        if mapper.input_focus() == &UxState::Toplevel.focus() {
            for mut camera in query.iter_mut() {
                camera.pan_view(true);
                camera.handle_mousemotion(-0.05f64, 0f64);
            }
        }
    }

    fn sys_draw_ux(
        gui: Res<Gui>,
        (installs, catalog, typeman): (Res<Installations>, Res<AssetCatalog>, Res<TypeManager>),
        mut herder: ResMut<ScriptHerder>,
        mut window: ResMut<Window>,
        mut query: Query<&mut ToplevelUx, With<UxTag>>,
    ) -> Result<()> {
        if let Ok(mut toplevel) = query.get_single_mut() {
            toplevel.draw_toplevel(
                (&installs, catalog.primary(), &typeman),
                &gui,
                &mut window,
                &mut herder,
            )?;
        }
        Ok(())
    }

    fn draw_toplevel(
        &mut self,
        (installs, collection, typeman): (&Installations, &AssetCollection, &TypeManager),
        gui: &Gui,
        window: &mut Window,
        herder: &mut ScriptHerder,
    ) -> Result<()> {
        egui::TopBottomPanel::top("top_panel_main").show(gui.screen().ctx(), |ui| {
            egui::menu::bar(ui, |ui| {
                ui.menu_button("File", |ui| {
                    if !installs.installation_names().count() > 1
                        && ui.button("Select Game").clicked()
                    {
                        herder.run_string("ux.choose_game()").unwrap();
                    }
                    if ui.button("Quit to Desktop").clicked() {
                        window.request_quit();
                    }
                });
                ui.menu_button("Edit", |ui| {
                    if ui.button("LIBs...").clicked() {
                        herder.run_string("ux.edit_libs()").unwrap();
                    }
                });
                ui.menu_button("View", |ui| {
                    if ui.button("World...").clicked() {
                        herder.run_string("ux.view_world()").unwrap();
                    }
                });
                ui.menu_button("Help", |ui| -> Result<()> {
                    add_help_menu_items(ui, herder)
                });
            });
        });

        let mut next_state = None;
        match &self.state {
            ToplevelMenu::Main => {
                egui::SidePanel::right("toplevel_main_menu_side_panel")
                    .default_width(250.)
                    .frame(
                        egui::Frame::window(&gui.screen().ctx().style())
                            .shadow(egui::epaint::Shadow::NONE)
                            .fill(hex_color!("202020E0")),
                    )
                    .resizable(false)
                    .show(gui.screen().ctx(), |ui| -> Result<()> {
                        if ui.button("Load Mission").clicked() {
                            next_state =
                                Some(self.enter_select_mission_screen(collection, typeman)?);
                        }
                        ui.separator();
                        if ui.button("Toolkit").clicked() {
                            herder.run_string("ux.edit_libs()")?;
                        }
                        if ui.button("Explore World").clicked() {
                            herder.run_string("ux.view_world()()")?;
                        }
                        ui.separator();
                        if ui.button("Exit to Desktop").clicked() {
                            window.request_quit();
                        }
                        Ok(())
                    })
                    .inner?;
            }
            ToplevelMenu::SelectMission(missions) => {
                if self.draw_mission_select(missions, gui, herder)? {
                    next_state = Some(ToplevelMenu::Main);
                }
            }
        };

        if let Some(state) = next_state {
            self.state = state;
        }

        Ok(())
    }

    fn enter_select_mission_screen(
        &mut self,
        collection: &AssetCollection,
        typeman: &TypeManager,
    ) -> Result<ToplevelMenu> {
        // Collect all missions that we could load.
        let mut missions = Vec::new();
        for info in collection.search(Search::for_extension("M").sort(Order::Asc))? {
            if is_mission_template(info.name()) {
                continue;
            }
            let contents = from_dos_string(info.data()?);
            let mission = Mission::from_str(contents.as_ref(), typeman, collection)?;
            missions.push((info.name().to_owned(), mission));
        }
        Ok(ToplevelMenu::SelectMission(missions))
    }

    fn draw_mission_select(
        &self,
        missions: &[(String, Mission)],
        gui: &Gui,
        herder: &mut ScriptHerder,
    ) -> Result<bool> {
        egui::Window::new("Select Mission")
            .frame(
                egui::Frame::window(&gui.screen().ctx().style())
                    .shadow(egui::epaint::Shadow::NONE)
                    .fill(hex_color!("202020E0")),
            )
            .resizable(false)
            .collapsible(false)
            .anchor(egui::Align2::CENTER_CENTER, egui::Vec2::default())
            .show(gui.screen().ctx(), |ui| -> Result<bool> {
                let available_height = ui.available_height();
                egui::ScrollArea::vertical()
                    .min_scrolled_height(available_height)
                    .show(ui, |ui| -> Result<bool> {
                        let row_height_sans_spacing =
                            ui.text_style_height(&egui::TextStyle::Body) + 2.;
                        let table = TableBuilder::new(ui)
                            .striped(true)
                            .resizable(true)
                            .min_scrolled_height(available_height)
                            .max_scroll_height(available_height)
                            .cell_layout(egui::Layout::left_to_right(egui::Align::Center))
                            .column(Column::auto_with_initial_suggestion(40.0).range(40.0..=300.0))
                            .columns(
                                Column::auto_with_initial_suggestion(40.0).range(40.0..=300.0),
                                7,
                            )
                            .column(Column::remainder());
                        table
                            .header(20.0, |mut header| {
                                for column in [
                                    "",
                                    "File",
                                    "Map",
                                    "Time",
                                    "On Ground",
                                    "Waypoints",
                                    "Object#",
                                    "Layer",
                                    "Code",
                                ] {
                                    header.col(|ui| {
                                        ui.strong(column);
                                    });
                                }
                            })
                            .body(|body| {
                                body.rows(row_height_sans_spacing, missions.len(), |mut row| {
                                    let (name, mission) = &missions[row.index()];
                                    let player = mission.mission_objects().iter().find(|obj| {
                                        obj.name().as_ref().map(|v| v.as_ref()) == Some("Player")
                                    });
                                    row.col(|ui| {
                                        if ui.button("Play").clicked() {
                                            herder
                                                .run_string(&format!("ux.load_mission({:?})", name))
                                                .ok();
                                        }
                                    });
                                    row.col(|ui| {
                                        ui.label(name);
                                    });
                                    row.col(|ui| {
                                        ui.label(mission.map_name().meta_name());
                                    });
                                    row.col(|ui| {
                                        ui.label(&format!(
                                            "{:02}:{:02}",
                                            mission.time().0,
                                            mission.time().1
                                        ));
                                    });
                                    row.col(|ui| {
                                        if let Some(player) = player {
                                            if player.position().y().f64() == 0. {
                                                ui.label("✔");
                                            }
                                        }
                                    });
                                    row.col(|ui| {
                                        if let Some(player) = player {
                                            if let Some(wp) = player.waypoints() {
                                                ui.label(wp.waypoints().len().to_string());
                                            }
                                        }
                                    });
                                    row.col(|ui| {
                                        ui.label(mission.mission_objects().len().to_string());
                                    });
                                    row.col(|ui| {
                                        ui.label(mission.layer_name());
                                    });
                                    row.col(|ui| {
                                        if let Some(name) = mission.mission_code_file() {
                                            ui.label(name);
                                        }
                                    });
                                });
                            });
                        ui.separator();
                        if ui.button("Close").clicked() {
                            Ok(true)
                        } else {
                            Ok(false)
                        }
                    })
                    .inner
            })
            .unwrap()
            .inner
            .unwrap()
    }
}

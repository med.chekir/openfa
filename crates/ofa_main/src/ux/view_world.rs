// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.

use crate::ux::{
    choose_game::ChooseGameUxStep, edit_libs::EditLibsUxStep, mission::MissionUxStep,
    toplevel::ToplevelUxStep, UxTag,
};
use absolute_unit::prelude::*;
use anyhow::Result;
use arcball::ArcBallController;
use asset_loader::{AssetLoader, MissionMarker};
use bevy_ecs::prelude::*;
use camera::{CameraStep, ScreenCamera, ScreenCameraController};
use catalog::{AssetCatalog, FileSystem, Search};
use game_editor::EditLibUxStep;
use game_ux::menubar::show_basic_menubar;
use geodesy::Geodetic;
use gui::{Gui, GuiStep};
use hifitime::TimeScale;
use mantle::{GpuStep, Window};
use marker::MarkersStep;
use mmm::canonicalize;
use nitrous::{inject_nitrous_component, method, HeapMut, NitrousComponent};
use orrery::{Orrery, OrreryStep};
use phase::Frame;
use runtime::{report_errors, Extension, Runtime, ScriptHerder};
use shape::ShapeStep;
use spog::TerminalStep;
use terrain::TerrainStep;

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum ViewWorldUxStep {
    DrawUx,
}

#[derive(Debug, NitrousComponent)]
pub(crate) struct ViewWorldUx {
    camera_ent: Entity,
    showing_help: bool,
}

impl Extension for ViewWorldUx {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        runtime.add_frame_system(
            Self::sys_draw_ux
                .pipe(report_errors)
                .in_set(ViewWorldUxStep::DrawUx)
                .after(GuiStep::StartFrame)
                .before(TerminalStep::Show)
                .before(GuiStep::EndFrame)
                // Routines that may call quit on the window don't conflict with users of the size
                .ambiguous_with(CameraStep::HandleDisplayChange)
                .ambiguous_with(GpuStep::CreateTargetSurface)
                .ambiguous_with(TerrainStep::HandleDisplayChange)
                // It also does not run concurrent with other UX panes
                .ambiguous_with(ChooseGameUxStep::DrawUx)
                .ambiguous_with(EditLibUxStep::DrawUx)
                .ambiguous_with(EditLibsUxStep::DrawUx)
                .ambiguous_with(MissionUxStep::DrawUx)
                .ambiguous_with(ToplevelUxStep::DrawUx)
                // Mutating the sun position means we need to sync with other users
                .ambiguous_with(MarkersStep::Render)
                .after(OrreryStep::UploadToGpu)
                // And we chose a type-in camera position
                .before(MarkersStep::UploadGeometry)
                .before(CameraStep::MoveCameraToFrame)
                .before(ShapeStep::ApplyTransforms)
                .before(TerrainStep::OptimizePatches),
        );
        Ok(())
    }
}

#[inject_nitrous_component]
impl ViewWorldUx {
    pub(crate) fn new(mut heap: HeapMut) -> Result<Self> {
        // Set location
        let position = Geodetic::new(degrees!(0.), degrees!(0.), feet!(80_000));

        let mut arcball = ArcBallController::default();
        arcball.set_target(position.clone());
        arcball.set_bearing_degrees(0.);
        arcball.set_pitch_degrees(28.);
        arcball.set_eye_distance_meters(100.);
        let camera_ent = heap
            .spawn("camera")?
            .inject(Frame::default())?
            .inject(arcball)?
            .inject(ScreenCameraController)?
            .id();

        heap.resource_mut::<Orrery>()
            .set_unix_ms(-184_601_753_067.224);

        let collection_name = heap.resource::<AssetCatalog>().primary_collection_name();
        let mm_names = heap
            .resource::<AssetCatalog>()
            .search(Search::for_extension("MM").in_collection(collection_name))?
            .into_iter()
            .filter(|info| !canonicalize(info.name()).starts_with('~'))
            .map(|info| info.name().to_owned())
            .collect::<Vec<_>>();
        for name in &mm_names {
            println!("Loading {name}");
            heap.resource_scope(|heap, game: Mut<AssetLoader>| game.load_map(name, heap))?;
        }

        Ok(Self {
            camera_ent,
            showing_help: false,
        })
    }

    pub fn unload(&self, mut heap: HeapMut) -> Result<()> {
        heap.despawn_by_id(self.camera_ent);

        let mut set = Vec::new();
        let mut q = heap.query_filtered::<Entity, With<MissionMarker>>();
        for id in q.iter(heap.world()) {
            set.push(id);
        }
        for &id in &set {
            heap.despawn_by_id(id);
        }

        Ok(())
    }

    #[method]
    fn toggle_show_help(&mut self) {
        self.showing_help = !self.showing_help;
    }

    fn sys_draw_ux(
        gui: Res<Gui>,
        mut herder: ResMut<ScriptHerder>,
        mut window: ResMut<Window>,
        mut query: Query<&mut ViewWorldUx, With<UxTag>>,
        mut camera_query: Query<(&mut ArcBallController, &ScreenCameraController)>,
        mut camera: ResMut<ScreenCamera>,
        mut orrery: ResMut<Orrery>,
    ) -> Result<()> {
        for mut view_world in query.iter_mut() {
            view_world.draw_window(&gui, &mut window, &mut herder)?;
            if let Ok((mut arcball, _)) = camera_query.get_single_mut() {
                Self::draw_info_area(&gui, &mut orrery, &mut arcball, &mut camera);
            }
        }
        Ok(())
    }

    fn draw_window(
        &mut self,
        gui: &Gui,
        window: &mut Window,
        herder: &mut ScriptHerder,
    ) -> Result<()> {
        show_basic_menubar(gui.screen().ctx(), "menubar_view_world", window, herder)
    }

    fn draw_info_area(
        gui: &Gui,
        orrery: &mut Orrery,
        arcball: &mut ArcBallController,
        camera: &mut ScreenCamera,
    ) {
        egui::Area::new("info")
            .pivot(egui::Align2::RIGHT_TOP)
            .fixed_pos([gui.screen().width(), 0.])
            .show(gui.screen().ctx(), |ui| {
                egui::Frame::none()
                    .fill(egui::hex_color!("#202020a0"))
                    .stroke(egui::Stroke::new(2., egui::Color32::BLACK))
                    .inner_margin(egui::style::Margin {
                        left: 10.,
                        bottom: 6.,
                        top: 4.,
                        right: 4.,
                    })
                    .rounding(egui::Rounding {
                        sw: 10.,
                        ..Default::default()
                    })
                    .show(ui, |ui| {
                        egui::CollapsingHeader::new("OpenFA")
                            .show_background(false)
                            .default_open(false)
                            .show(ui, |ui| {
                                egui::Grid::new("controls_grid")
                                    .num_columns(2)
                                    .spacing([4.0, 2.0])
                                    .striped(false)
                                    .show(ui, |ui| {
                                        Self::fill_info_grid(ui, orrery, arcball, camera).ok();
                                    });
                            });
                    });
            });
    }

    fn fill_info_grid(
        ui: &mut egui::Ui,
        orrery: &mut Orrery,
        arcball: &mut ArcBallController,
        camera: &mut ScreenCamera,
    ) -> Result<()> {
        ui.label("Date");
        let date0 = orrery.get_naive_date()?;
        let mut date = date0;
        ui.add(egui_extras::DatePickerButton::new(&mut date));
        if date != date0 {
            orrery.set_naive_date(date)?;
        }
        ui.end_row();

        ui.label("Time");
        ui.label(orrery.get_time().to_gregorian_str(TimeScale::UTC))
            .on_hover_text("Middle mouse + drag to change time");
        ui.end_row();

        ui.label("Latitude");
        let mut lat = arcball.target().lat::<Degrees>().f32();
        ui.add(egui::Slider::new(&mut lat, -90.0..=90.0).suffix("°"))
            .on_hover_text("Right click + drag for fine control");
        arcball.target_mut().set_lat(degrees!(lat));
        ui.end_row();

        ui.label("Longitude");
        let mut lon = arcball.target().lon::<Degrees>().f32();
        ui.add(egui::Slider::new(&mut lon, -180.0..=180.0).suffix("°"))
            .on_hover_text("Right click + drag for fine control");
        arcball.target_mut().set_lon(degrees!(lon));
        ui.end_row();

        ui.label("Altitude");
        let mut altitude = arcball.target().asl::<Feet>().f32();
        ui.add(egui::Slider::new(&mut altitude, 0.0..=40_000.0).suffix("'"))
            .on_hover_text("Up + Down arrows for fine control");
        arcball.target_mut().set_asl(feet!(altitude));
        ui.end_row();

        ui.label("FoV").on_hover_text("Field of View");
        let mut fov = camera.fov_y::<Degrees>().f32();
        ui.add(egui::Slider::new(&mut fov, 5.0..=120.0).suffix("°"))
            .on_hover_text("Change the field of view");
        camera.set_fov_y(radians!(degrees!(fov)));
        ui.end_row();

        Ok(())
    }
}

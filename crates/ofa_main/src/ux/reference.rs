// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::ux::{choose_game::ChooseGameUxStep, toplevel::ToplevelUxStep, UxTag};
use absolute_unit::prelude::*;
use anyhow::{ensure, Result};
use arcball::ArcBallController;
use asset_loader::{AssetLoader, MissionMarker};
use bevy_ecs::prelude::*;
use camera::{CameraStep, ScreenCameraController};
use catalog::{AssetCatalog, FileSystem};
use egui::{hex_color, SliderOrientation};
use game_editor::EditLibUxStep;
use geodesy::{Bearing, Geodetic, PitchCline};
use gui::{Gui, GuiStep};
use installations::Installations;
use mantle::{Gpu, GpuStep};
use nitrous::{inject_nitrous_component, method, HeapMut, NitrousComponent};
use orrery::Orrery;
use phase::Frame;
use runtime::{Extension, PlayerMarker, Runtime};
use sh::{GearFootprintKind, ShCapabilities};
use shape::Shape;
use spog::TerminalStep;
use std::time::Duration;
use terrain::TerrainStep;
use vehicle::{
    AirbrakeControl, AirbrakeEffector, Airframe, BayControl, BayEffector, FlapsControl,
    FlapsEffector, FuelSystem, FuelTank, FuelTankKind, GearControl, GearEffector, GliderEngine,
    HookControl, HookEffector, PitchInceptor, PowerSystem, RollInceptor, ThrottleInceptor,
    ThrustVectorPitchControl, ThrustVectorPitchEffector, ThrustVectorYawControl,
    ThrustVectorYawEffector, VtolAngleControl, VtolAngleEffector, YawInceptor,
};

// Tag items to despawn when we unload.
#[derive(Debug, NitrousComponent)]
pub(crate) struct ShowShTag;

#[inject_nitrous_component]
impl ShowShTag {}

const LATITUDE: f64 = 58.287_f64;
const LONGITUDE: f64 = 25.641_f64;
const ALTITUDE: f64 = 20_000.0_f64;

type PlaneQuery<'a> = (
    &'a ShCapabilities,
    (
        &'a mut ThrottleInceptor,
        &'a mut ThrustVectorPitchControl,
        &'a mut ThrustVectorYawControl,
        &'a mut VtolAngleControl,
        &'a PowerSystem,
    ),
    &'a mut GearControl,
    &'a GearEffector,
    &'a mut BayControl,
    &'a BayEffector,
    &'a mut AirbrakeControl,
    &'a AirbrakeEffector,
    &'a mut FlapsControl,
    &'a FlapsEffector,
    &'a mut HookControl,
    &'a HookEffector,
);

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum ReferenceUxStep {
    Ux,
}

#[derive(Debug, NitrousComponent)]
pub(crate) struct ReferenceUx {
    shape_ent: Entity,
    camera_ent: Entity,
    showing_help: bool,
}

impl Extension for ReferenceUx {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        runtime.add_frame_systems((Self::sys_draw_ux
            .in_set(ReferenceUxStep::Ux)
            .after(GuiStep::StartFrame)
            .before(TerminalStep::Show)
            .before(GuiStep::EndFrame)
            // Quit and getting size don't race
            .ambiguous_with(CameraStep::HandleDisplayChange)
            .ambiguous_with(GpuStep::CreateTargetSurface)
            .ambiguous_with(TerrainStep::HandleDisplayChange)
            // Don't run at same time
            .ambiguous_with(ChooseGameUxStep::RotateCamera)
            .ambiguous_with(EditLibUxStep::DrawUx)
            .ambiguous_with(ToplevelUxStep::RotateCamera),));
        Ok(())
    }
}

#[inject_nitrous_component]
impl ReferenceUx {
    pub(crate) fn new(shape_name: &str, mut heap: HeapMut) -> Result<Self> {
        ensure!(shape_name.ends_with(".SH"));

        // Set location
        let position = Geodetic::new(degrees!(LATITUDE), degrees!(LONGITUDE), feet!(ALTITUDE));

        let mut arcball = ArcBallController::default();
        arcball.set_target(position.clone());
        arcball.set_bearing_degrees(-113.);
        arcball.set_pitch_degrees(28.);
        arcball.set_eye_distance_meters(108.);
        let camera_ent = heap
            .spawn("camera")?
            .inject(Frame::default())?
            .inject(arcball)?
            .inject(ScreenCameraController)?
            .id();

        heap.resource_scope(|heap, game: Mut<AssetLoader>| game.load_map("BAL.MM", heap))?;

        let shape_ids = heap.resource_scope(|heap, mut shapes: Mut<Shape>| {
            shapes.upload_shape(
                heap.resource::<Installations>().primary_palette(),
                heap.resource::<AssetCatalog>().lookup(shape_name)?,
                heap.resource::<AssetCatalog>(),
                heap.resource::<Gpu>(),
            )
        })?;

        let (shape_bundle, flag_bundle, xform_bundle) = heap.resource::<Shape>().shape_bundle(
            shape_name,
            shape_ids,
            Some(GearFootprintKind::Nose),
        )?;

        heap.resource_mut::<Orrery>().set_unix_ms(13459754321.0);
        let frame = Frame::from_geodetic_bearing_and_pitch(
            position,
            Bearing::new(degrees!(0_f64)),
            PitchCline::new(degrees!(0_f64)),
        );
        let fuel = FuelSystem::default()
            .with_internal_tank(FuelTank::new(FuelTankKind::Center, kilograms!(0_f64)))?;
        let power = PowerSystem::default().with_engine(GliderEngine::default());
        let shape_ent = {
            let mut ent = heap.spawn("Player")?;
            ent.inject((
                PlayerMarker,
                frame.clone(),
                Airframe::new(kilograms!(10_f64)),
                fuel,
                power,
                (
                    PitchInceptor::default(),
                    RollInceptor::default(),
                    YawInceptor::default(),
                    ThrottleInceptor::new_min_power(),
                    ThrustVectorPitchControl::new(degrees!(-30), degrees!(30), degrees!(1)),
                    ThrustVectorYawControl::new(degrees!(-30), degrees!(30), degrees!(1)),
                    VtolAngleControl::new(degrees!(-100), degrees!(10), degrees!(10)),
                    AirbrakeControl::default(),
                    BayControl::default(),
                    GearControl::default(),
                    FlapsControl::default(),
                    HookControl::default(),
                ),
                (
                    ThrustVectorPitchEffector::new(0., Duration::from_millis(1)),
                    ThrustVectorYawEffector::new(0., Duration::from_millis(1)),
                    VtolAngleEffector::new(0., Duration::from_secs(1)),
                    AirbrakeEffector::new(0., Duration::from_millis(1)),
                    BayEffector::new(0., Duration::from_secs(2)),
                    FlapsEffector::new(0., Duration::from_millis(1)),
                    GearEffector::new(0., Duration::from_secs(4)),
                    HookEffector::new(0., Duration::from_millis(1)),
                ),
                shape_bundle,
            ))?;
            if let Some(flag_bundle) = flag_bundle {
                ent.inject(flag_bundle)?;
            }
            if let Some(xform_bundle) = xform_bundle {
                ent.inject(xform_bundle)?;
            }
            ent.id()
        };

        // Get the frames from the shape we're displaying so we can know where to push updates
        // let id = heap.entity_by_name("Player");
        // let shape_id = *heap.get_by_id::<ShapeId>(id);
        // let frames = heap
        //     .resource::<Shape>()
        //     .get_texture_frames(shape_id)
        //     .to_owned();

        Ok(Self {
            shape_ent,
            camera_ent,
            showing_help: false,
        })
    }

    pub fn unload(&self, mut heap: HeapMut) -> Result<()> {
        heap.despawn_by_id(self.shape_ent);
        heap.despawn_by_id(self.camera_ent);

        let mut set = Vec::new();
        let mut q = heap.query_filtered::<Entity, With<MissionMarker>>();
        for id in q.iter(heap.world()) {
            set.push(id);
        }
        for &id in &set {
            heap.despawn_by_id(id);
        }

        Ok(())
    }

    #[method]
    fn toggle_show_help(&mut self) {
        self.showing_help = !self.showing_help;
    }

    fn sys_draw_ux(
        gui: Res<Gui>,
        query: Query<&mut ReferenceUx, With<UxTag>>,
        plane_query: Query<PlaneQuery, With<PlayerMarker>>,
    ) {
        if let Ok(reference) = query.get_single() {
            Self::draw_plane_display_status(&gui, plane_query);
            if reference.showing_help {
                Self::draw_help(&gui);
            }
        }
    }

    fn draw_plane_display_status(gui: &Gui, plane_query: Query<PlaneQuery, With<PlayerMarker>>) {
        egui::Area::new("plane_info")
            .pivot(egui::Align2::LEFT_TOP)
            .fixed_pos([0., 0.])
            .show(gui.screen().ctx(), |ui| {
                egui::Frame::none()
                    .fill(egui::hex_color!("#202020E0"))
                    .stroke(egui::Stroke::new(2., egui::Color32::BLACK))
                    .inner_margin(egui::style::Margin {
                        left: 4.,
                        bottom: 6.,
                        top: 4.,
                        right: 10.,
                    })
                    .rounding(egui::Rounding {
                        se: 10.,
                        ..Default::default()
                    })
                    .show(ui, |ui| {
                        egui::Grid::new("plane_grid")
                            .num_columns(3)
                            .spacing([4.0, 2.0])
                            .striped(false)
                            .show(ui, |ui| {
                                ui.label("Shape Render State:");
                                ui.end_row();
                                Self::fill_plane_grid(ui, plane_query);
                            });
                    });
            });
    }

    fn fill_plane_grid(ui: &mut egui::Ui, mut plane_query: Query<PlaneQuery, With<PlayerMarker>>) {
        if plane_query.is_empty() {
            return;
        }
        let (
            caps,
            (mut throttle, mut tv_pitch_control, mut tv_yaw_control, mut vtol_angle_control, power),
            mut gear_control,
            gear_effector,
            mut bay_control,
            bay_effector,
            mut airbrake_control,
            airbrake_effector,
            mut flaps_control,
            flaps_effector,
            mut hook_control,
            hook_effector,
        ) = plane_query.single_mut();

        fn t(s: &str) -> egui::RichText {
            egui::RichText::new(s).size(16.)
        }

        if caps.contains(ShCapabilities::HAS_GEAR) {
            ui.label(t("Gear:"));
            let mut control = gear_control.is_enabled();
            ui.checkbox(&mut control, "");
            ui.label(t(&format!("{:0.0}%", gear_effector.position() * 100.)));
            gear_control.set_enabled(control);
            ui.end_row();
        }

        if caps.contains(ShCapabilities::HAS_BAY) {
            ui.label(t("Bay:"));
            let mut control = bay_control.is_enabled();
            ui.checkbox(&mut control, "");
            ui.label(t(&format!("{:0.0}%", bay_effector.position() * 100.)));
            bay_control.set_enabled(control);
            ui.end_row();
        }

        if caps.contains(ShCapabilities::HAS_AIRBRAKE) {
            ui.label(t("Airbrake:"));
            let mut control = airbrake_control.is_enabled();
            ui.checkbox(&mut control, "");
            ui.label(t(&format!("{:0.0}%", airbrake_effector.position() * 100.)));
            airbrake_control.set_enabled(control);
            ui.end_row();
        }

        if caps.contains(ShCapabilities::HAS_FLAPS) {
            ui.label(t("Flaps:"));
            let mut control = flaps_control.is_enabled();
            ui.checkbox(&mut control, "");
            ui.label(t(&format!("{:0.0}%", flaps_effector.position() * 100.)));
            flaps_control.set_enabled(control);
            ui.end_row();
        }

        if caps.contains(ShCapabilities::HAS_HOOK) {
            ui.label(t("Hook:"));
            let mut control = hook_control.is_enabled();
            ui.checkbox(&mut control, "");
            ui.label(t(&format!("{:0.0}%", hook_effector.position() * 100.)));
            hook_control.set_enabled(control);
            ui.end_row();
        }

        if caps.contains(ShCapabilities::HAS_AFTERBURNER) {
            ui.label(t("Engine:"));
            let mut military = throttle.position().military();
            let mut afterburner = throttle.position().is_afterburner();
            ui.style_mut().spacing.slider_width = 30.;
            ui.vertical(|ui| {
                ui.checkbox(&mut afterburner, "AFT");
                ui.add(
                    egui::Slider::new(&mut military, 0.0..=100.0)
                        .orientation(egui::SliderOrientation::Vertical)
                        .show_value(false),
                );
            });
            ui.label(t(&format!("{}", power.engine(0).current_power())));
            if afterburner {
                throttle.set_afterburner();
            } else {
                throttle.set_military(military);
            }
            ui.end_row();

            if caps.contains(ShCapabilities::XFORM_TV_CANARD) {
                ui.label("Thrust Vectoring:");
                let mut tv_pitch = tv_pitch_control.angle::<Degrees>().f64();
                let mut tv_yaw = tv_yaw_control.angle::<Degrees>().f64();
                ui.add(
                    egui::Slider::new(&mut tv_pitch, -30.0..=30.0)
                        .orientation(SliderOrientation::Vertical)
                        .show_value(false),
                );
                ui.add(egui::Slider::new(&mut tv_yaw, -30.0..=30.0).show_value(false));
                tv_pitch_control.set_angle(degrees!(tv_pitch));
                tv_yaw_control.set_angle(degrees!(tv_yaw));
                ui.end_row();
            }
        }

        if caps.intersects(ShCapabilities::XFORM_HAS_VT | ShCapabilities::XFORM_VT_ANGLE) {
            ui.label("VTOL Angle:");
            let mut vtol_angle = vtol_angle_control.angle::<Degrees>().f64();
            ui.add(
                egui::Slider::new(&mut vtol_angle, vtol_angle_control.range::<Degrees>())
                    .show_value(true),
            );
            vtol_angle_control.set_angle(degrees!(vtol_angle));
            ui.end_row();
        }
    }

    fn draw_help(gui: &Gui) {
        const HELP: &str = r#"
    F1, ?        - show or hide this help text
    left mouse   - change view angle
    middle mouse - change time of day
    right mouse  - move view position
    f            - toggle flaps
    g            - toggle gear
    b            - toggle airbrake
    o            - toggle bay
    h            - toggle hook
    . & ,        - rudder left and right
    1-5          - turn off afterburner
    6            - turn on afterburner
    Ctrl+Arrow   - thrust vectoring
    x & z        - vtol vectoring
    Shift+b      - toggle bounding boxes
    Shift+n      - toggle normals display
    Shift+f      - toggle faces display
    "#;
        egui::Window::new("How to use this program:")
            .frame(
                egui::Frame::window(&gui.screen().ctx().style())
                    .shadow(egui::epaint::Shadow::NONE)
                    .fill(hex_color!("202020A0")),
            )
            .resizable(false)
            .collapsible(false)
            .anchor(egui::Align2::CENTER_CENTER, egui::Vec2::default())
            .show(gui.screen().ctx(), |ui| {
                ui.add(
                    egui::Label::new(
                        egui::RichText::new(HELP)
                            .text_style(egui::TextStyle::Monospace)
                            .color(egui::Color32::GREEN)
                            .size(18.),
                    )
                    .wrap(false),
                );
            });
    }
}

// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
pub mod menubar;

use anyhow::{bail, Result};
use bevy_ecs::prelude::*;
use event_mapper::InputFocus;
use nitrous::{inject_nitrous_component, NitrousComponent};
use std::str::FromStr;

/// An enum OpenFA uses to constrain the values in InputFocus, prevent typos, etc.
#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
pub enum UxState {
    #[default]
    ChooseGame,
    Toplevel,
    EditLibs,
    EditLib,
    Reference,
    Mission,
    ViewWorld,
    Credits,
}

impl UxState {
    pub fn as_str(&self) -> &'static str {
        match self {
            UxState::ChooseGame => "ux:choose_game",
            UxState::Toplevel => "ux:toplevel",
            UxState::EditLibs => "ux:edit_libs",
            UxState::EditLib => "ux:edit_lib",
            UxState::Reference => "ux:reference",
            UxState::Mission => "ux:mission",
            UxState::ViewWorld => "ux:view_world",
            UxState::Credits => "ux:credits",
        }
    }

    pub fn focus(&self) -> InputFocus {
        InputFocus::new(self.as_str())
    }
}

impl FromStr for UxState {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> Result<Self> {
        Ok(match s {
            "ux:choose_game" => UxState::ChooseGame,
            "ux:toplevel" => UxState::Toplevel,
            "ux:edit_libs" => UxState::EditLibs,
            "ux:edit_lib" => UxState::EditLib,
            "ux:reference" => UxState::Reference,
            "ux:mission" => UxState::Mission,
            "ux:view_world" => UxState::ViewWorld,
            "ux:credits" => UxState::Credits,
            s => {
                if let Some(prefix) = s.rsplitn(2, ':').last() {
                    return UxState::from_str(prefix);
                }
                bail!("no such ux state {s}")
            }
        })
    }
}

/// Tag to identify the top-level ux component.
#[derive(Debug, NitrousComponent)]
pub struct UxTag;

#[inject_nitrous_component]
impl UxTag {}

FROM ubuntu:22.04
ENV VERSION=0.2.1

# Update and install build deps
RUN set -eux; \
    apt update; \
     apt install -y bzip2 curl gcc binutils-mingw-w64 mingw-w64 zip;

# Install rustup
RUN set -eux; \
    curl --location --fail \
        "https://static.rust-lang.org/rustup/dist/x86_64-unknown-linux-gnu/rustup-init" \
        --output rustup-init; \
    chmod +x rustup-init; \
    ./rustup-init -y --no-modify-path --default-toolchain stable; \
    rm rustup-init;

# Add rustup to path, check that it works
ENV PATH=${PATH}:/root/.cargo/bin
RUN set -eux; \
    rustup --version;

# Build the project
WORKDIR /app
COPY . .

# Linux Build
RUN set -eux; \
    cargo build --release --target x86_64-unknown-linux-gnu --workspace --all-targets;

# Windows Build
RUN set -eux; \
    cargo build --release --target x86_64-pc-windows-gnu --workspace --all-targets;

# Package windows OpenFA
RUN set -eux; \
    export TMP="ofa-${VERSION}"; \
    mkdir ${TMP}; \
    cp ./target/x86_64-pc-windows-gnu/release/openfa.exe ${TMP}; \
    strip ${TMP}/*; \
    cd ${TMP}; \
    zip ../openfa-win-x86_64.zip ./*; \
    cd ..; \
    rm -rf ${TMP};

# Package windows OFA-Tools
RUN set -eux; \
    export TMP="ofa-${VERSION}"; \
    mkdir ${TMP}; \
    cp ./target/x86_64-pc-windows-gnu/release/ofa-tools.exe ${TMP}; \
    strip ${TMP}/*; \
    cd ${TMP}; \
    zip ../ofa-tools-win-x86_64.zip ./*; \
    cd ..; \
    rm -rf ${TMP};

# Package linux OpenFA
RUN set -eux; \
    export TMP="ofa-${VERSION}"; \
    mkdir ${TMP}; \
    cp ./target/x86_64-unknown-linux-gnu/release/openfa ${TMP}; \
    strip ${TMP}/*; \
    tar -C ${TMP} -cjvf openfa-linux-x86_64.tar.bz2 .; \
    rm -rf ${TMP};

# Package linux OFA-Tools
RUN set -eux; \
    export TMP="ofa-${VERSION}"; \
    mkdir ${TMP}; \
    cp ./target/x86_64-unknown-linux-gnu/release/ofa-tools ${TMP}; \
    strip ${TMP}/*; \
    tar -C ${TMP} -cjvf ofa-tools-linux-x86_64.tar.bz2 .; \
    rm -rf ${TMP};

CMD ["/bin/bash"]
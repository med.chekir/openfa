[workspace.package]
description = "An open source, black box re-implementation of the Jane's Fighters Anthology engine."
version = "0.2.5"
authors = ["Terrence Cole <terrence.d.cole@gmail.com>"]
edition = "2021"
rust-version = "1.76"
readme = "https://gitlab.com/openfa/openfa/-/blob/main/README.md"
homepage = "https://gitlab.com/openfa/openfa"
documentation = "https://gitlab.com/openfa/openfa"
repository = "https://gitlab.com/openfa/openfa"
license = "GPL-3.0-only"

[workspace]
resolver = "2"
default-members = ["apps/openfa"]
members = [
    "apps/openfa",
    "apps/ofa-tools",
    # "crates/asset/dlg",
    "crates/asset/fnt",
    "crates/asset/hud",
    "crates/asset/jt",
    "crates/asset/lay",
    "crates/asset/lib",
    "crates/asset/mc",
    "crates/asset/mmm",
    # "crates/asset/mnu",
    "crates/asset/nt",
    "crates/asset/ot",
    "crates/asset/pal",
    "crates/asset/pic",
    "crates/asset/pt",
    "crates/asset/sh",
    "crates/asset/t2",
    "crates/asset/xt",
    "crates/asset/xt_parse",
    "crates/asset_loader",
    "crates/fa_vehicle",
    "crates/flight_dynamics",
    "crates/font_fnt",
    "crates/game_editor",
    "crates/game_ux",
    "crates/i386",
    "crates/installations",
    "crates/instrument_envelope",
    "crates/lzss",
    "crates/ofa_main",
    "crates/peff",
    "crates/pic_uploader",
    "crates/pksploder",
    "crates/player",
    "crates/reverse",
    "crates/shape",
    "crates/t2_terrain",
    # List nitrogen deps here so that editors will index them as part of the workspace
    "crates/nitrogen/absolute_unit",
    "crates/nitrogen/animate",
    "crates/nitrogen/ansi",
    "crates/nitrogen/arcball",
    "crates/nitrogen/atlas",
    "crates/nitrogen/atmosphere",
    "crates/nitrogen/build_shaders",
    "crates/nitrogen/camera",
    "crates/nitrogen/catalog",
    "crates/nitrogen/composite",
    "crates/nitrogen/event_mapper",
    "crates/nitrogen/font_common",
    "crates/nitrogen/font_ttf",
    "crates/nitrogen/geodb",
    "crates/nitrogen/geodesy",
    "crates/nitrogen/geometry",
    "crates/nitrogen/gui",
    "crates/nitrogen/nitrogen_main",
    "crates/nitrogen/mantle",
    "crates/nitrogen/marker",
    "crates/nitrogen/nitrous",
    "crates/nitrogen/nitrous_injector",
    "crates/nitrogen/orrery",
    "crates/nitrogen/packed_injector",
    "crates/nitrogen/packed_struct",
    "crates/nitrogen/phase",
    "crates/nitrogen/planck",
    "crates/nitrogen/runtime",
    "crates/nitrogen/shader_shared",
    "crates/nitrogen/spog",
    "crates/nitrogen/star_catalog",
    "crates/nitrogen/stars",
    "crates/nitrogen/terrain",
    "crates/nitrogen/tracelog",
    "crates/nitrogen/vehicle",
    "crates/nitrogen/world", "crates/game_editor", "crates/game_ux",
]

[workspace.dependencies]
# nitrogen transitive deps
anyhow = { version = "^ 1", features = ["backtrace"] }
approx = "^ 0.5"
arboard = "^ 3.2"
bevy_ecs = { version = "^ 0.13", features = ["trace"] }
bevy_utils = "^ 0.13"
bitflags = "^ 2"
bv = "^ 0.11"
bzip2 = "^ 0.4"
chrono = "^ 0.4"
color-hex = "^ 0.2"
convert_case = "^ 0.6"
crossbeam = "^ 0.8"
csscolorparser = "^ 0.6"
egui = { version = "^ 0.26", features = ["color-hex"] }
egui_extras = { version = "^ 0.26", features = ["datepicker"] }
egui_plot = "^ 0.26"
egui-wgpu = "^ 0.26"
ellipse = "^ 0.2"
env_logger = "^ 0.11"
fastrand = "^ 2.0"
futures = { version = "^ 0.3", default-features = false, features = ["executor"] }
fxhash = "^ 0.2"
gilrs = "^ 0.10"
glam = { version = "^ 0.25", features = ["approx"] }
glob = "^ 0.3"
hashbag = "^ 0.1"
hifitime = "^ 3.9"
image = { version = "^ 0.25", default-features = false, features = ["rayon", "bmp", "gif", "ico", "jpeg", "png", "tiff"] }
indexmap = "^ 2.0"
itertools = "^ 0.12"
json = "^ 0.12"
libc = "0.2"
libyaml = "^ 0.2"
log = "^ 0.4"
lyon_geom = "^ 1.0"
md5 = "^ 0.7"
memmap2 = "^ 0.9"
memoffset = "^ 0.9"
naga = { version = "^ 0.19", features = ["spv-out", "dot-out", "wgsl-in"] }
num-traits = "^ 0.2"
once_cell = "^ 1"
ordered-float = "^ 4"
parking_lot = "^ 0.12"
paste = "^ 1"
platform-dirs = "^ 0.3"
pollster = "^ 0.3"
pretty-type-name = "1.0"
proc-macro2 = "^ 1"
progress-streams = "^ 1"
quote = "^ 1"
rayon = "^ 1.6"
regex = "^ 1"
ringbuffer = "^ 0.15"
rusttype = "^ 0.9"
serde = { version = "^ 1", features = ["derive"] }
serde_yaml = "^ 0.9"
simba = { version = "^ 0.8", default-features = false }
smallvec = "^ 1"
static_assertions = "^ 1"
strsim = "^ 0.11"
structopt = "^ 0.3"
syn = { version = "= 1.0.92", features = ["default", "extra-traits", "full", "visit"] }
tempfile = "^ 3"
terminal_size = "^ 0.3"
tracing = { version = "0.1", features = ["release_max_level_info"] }
tracing-subscriber = { version = "0.3.1", features = ["registry", "env-filter"] }
tracing-chrome = "0.7.0"
tracing-log = "0.2"
tracing-error = "0.2.0"
triggered = "^ 0.1"
unicase = "^ 2.6"
ureq = "^ 2.6"
wgpu = "^ 0.19"
winit = "= 0.29.14" # matching wgpu's import
zerocopy = { version = "^ 0.7", features = ["derive"] }

# OpenXR support
ash = "0.37.3"
openxr = { version = "0.18", features = ["loaded"] }
wgpu-hal = "0.19"
wgpu-types = "0.19"

# Wasm Specific
console_error_panic_hook = "^ 0.1"
console_log = "^ 1"
instant = { version = "0.1", features = ["wasm-bindgen"] }
wasm-bindgen = "^ 0.2.87"
wasm-bindgen-futures = "^ 0.4.37"
web-sys = { version = "^ 0.3.60", features = [
    "console",
    "Window",
    "Document",
    "Element",
] }
js-sys = "^ 0.3.60"

# nitrogen libs
absolute_unit = { path = "crates/nitrogen/absolute_unit" }
animate = { path = "crates/nitrogen/animate" }
ansi = { path = "crates/nitrogen/ansi" }
arcball = { path = "crates/nitrogen/arcball" }
atlas = { path = "crates/nitrogen/atlas" }
atmosphere = { path = "crates/nitrogen/atmosphere" }
build_shaders = { path = "crates/nitrogen/build_shaders" }
camera = { path = "crates/nitrogen/camera" }
catalog = { path = "crates/nitrogen/catalog" }
composite = { path = "crates/nitrogen/composite" }
event_mapper = { path = "crates/nitrogen/event_mapper" }
font_common = { path = "crates/nitrogen/font_common" }
font_ttf = { path = "crates/nitrogen/font_ttf" }
geodb = { path = "crates/nitrogen/geodb" }
geodesy = { path = "crates/nitrogen/geodesy" }
geometry = { path = "crates/nitrogen/geometry" }
gui = { path = "crates/nitrogen/gui" }
main = { path = "crates/nitrogen/main" }
mantle = { path = "crates/nitrogen/mantle" }
marker = { path = "crates/nitrogen/marker" }
nitrous = { path = "crates/nitrogen/nitrous" }
nitrous_injector = { path = "crates/nitrogen/nitrous_injector" }
orrery = { path = "crates/nitrogen/orrery" }
packed_injector = { path = "crates/nitrogen/packed_injector" }
packed_struct = { path = "crates/nitrogen/packed_struct" }
phase = { path = "crates/nitrogen/phase" }
planck = { path = "crates/nitrogen/planck" }
runtime = { path = "crates/nitrogen/runtime" }
shader_shared = { path = "crates/nitrogen/shader_shared" }
spog = { path = "crates/nitrogen/spog" }
star_catalog = { path = "crates/nitrogen/star_catalog" }
stars = { path = "crates/nitrogen/stars" }
terrain = { path = "crates/nitrogen/terrain" }
tracelog = { path = "crates/nitrogen/tracelog" }
vehicle = { path = "crates/nitrogen/vehicle" }
world = { path = "crates/nitrogen/world" }

# openfa deps
byteorder = "^ 1"
clap = { version = "^ 4.4", features = ["derive"] }
codepage-437 = "^ 0.1"
csv = "^ 1.1"
earcutr = "^ 0.4"
enum-iterator = "^ 1.4"
humansize = "^ 2"
thiserror = "^ 1"
triangulate = "^ 0.1"
uuid = { version = "^ 1.4", features = ["v4", "fast-rng"] }
walkdir = "^ 2.3"

# openfa libs
fnt = { path = "crates/asset/fnt" }
hud = { path = "crates/asset/hud" }
jt = { path = "crates/asset/jt" }
lay = { path = "crates/asset/lay" }
lib = { path = "crates/asset/lib" }
mc = { path = "crates/asset/mc" }
mmm = { path = "crates/asset/mmm" }
mnu = { path = "crates/asset/mnu" }
nt = { path = "crates/asset/nt" }
ot = { path = "crates/asset/ot" }
pal = { path = "crates/asset/pal" }
pic = { path = "crates/asset/pic" }
pt = { path = "crates/asset/pt" }
sh = { path = "crates/asset/sh" }
t2 = { path = "crates/asset/t2" }
xt = { path = "crates/asset/xt" }
xt_parse = { path = "crates/asset/xt_parse" }
asset_loader = { path = "crates/asset_loader" }
fa_vehicle = { path = "crates/fa_vehicle" }
flight_dynamics = { path = "crates/flight_dynamics" }
font_fnt = { path = "crates/font_fnt" }
game_editor = { path = "crates/game_editor" }
game_ux = { path = "crates/game_ux" }
i386 = { path = "crates/i386" }
installations = { path = "crates/installations" }
instrument_envelope = { path = "crates/instrument_envelope" }
lzss = { path = "crates/lzss" }
ofa_main = { path = "crates/ofa_main" }
peff = { path = "crates/peff" }
pic_uploader = { path = "crates/pic_uploader" }
pksploder = { path = "crates/pksploder" }
player = { path = "crates/player" }
reverse = { path = "crates/reverse" }
shape = { path = "crates/shape" }
t2_terrain = { path = "crates/t2_terrain" }

# Change to emit debugging symbols when tracing is required
[profile.release]
#debug = true
debug = false

# Turn on a small amount of optimisation while developing, mostly targetted
# at triangulate, image, and other packages that are artificially
# slow in debug builds. Note that we have to turn it on from _all_, not just
# dependencies because of inlining.
[profile.dev]
debug = true
debug-assertions = true
opt-level = 1

[profile.dev.package.pksploder]
debug = false
debug-assertions = false
opt-level = 3

[profile.dev.package.lib]
debug = false
debug-assertions = false
opt-level = 3
